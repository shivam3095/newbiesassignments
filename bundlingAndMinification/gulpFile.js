var gulp = require('gulp');
let browserify = require("browserify");
let source = require("vinyl-source-stream");
let fs = require("fs");
const Terser = require("terser");
var src = require("./testFolder/vendor.min");



gulp.task('copy', function(){
  return gulp.src('sample.txt')
      .pipe(gulp.dest('testFolder'))
});


gulp.task('bundle', function () {
  let dir = "testFolder";
  return browserify({
    basedir: '.',
    entries: ['vendor.ts'],
  })
      .plugin("tsify")
      .bundle()
      .pipe(source("vendor.min.js"))
      .pipe(gulp.dest(dir));
});

gulp.task('minify', function () {
  var options = {
    keep_classnames: true,
    keep_fnames: true,
    mangle: false
  }
  return new Promise((resolve) => {
    let code = fs.readFileSync("./testFolder/vendor.min.js");
    let minifiedFile = Terser.minify(code.toString(), options);
    fs.writeFileSync(src, minifiedFile.code);
    minifiedFile.map && fs.writeFileSync(src + '.map', minifiedFile.map);
    resolve();
  })

});