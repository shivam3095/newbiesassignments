/* global angular */
(function () {
    'use strict';
    const app = angular.module("DDS.UI", []);
}());
/* global angular */
(function () {
    'use strict';

   const DDS_APP = angular.module('DDS.UI');
    DDS_APP.provider('DDS.UI.ApiService', function () {
       let self = this;
       
       self.config = function (config) {
           angular.extend(self, config);
       };

       function Api($http) {

           let ApiVersionUrl = self.baseUrl + self.apiVersions,
               deviceDataSetApi = '/devicedataset',
               tcoDeviceApi = '/tco';

           //Check baseUrl initialized or not.If not then throw exception.
           if (!self.baseUrl) {
               throw 'Base url is not configured properly';
           }

           /**
            * @desc get dds asset info
            * @param id
            * @param tenant
            * @returns {*}
            */
           function getDeviceDataSetById(id, tenantName) {
               let url = `${ApiVersionUrl}${deviceDataSetApi}/asset/${id}?tenantName=${tenantName}`;
               return $http.get(url);
           }

           /**
            * @desc
            * @returns {*}
            */
           function getDeviceDataSet() {
                return $http.get(`${ApiVersionUrl}${deviceDataSetApi}/asset`);
           }

           /**
            * @desc submit email request and save user request in db
            * @param emailRequest
            * @returns {*}
            */
           function dataToEmailService(emailRequest) {
               let url = `${ApiVersionUrl}${deviceDataSetApi}/notification`
               return $http.post(url, emailRequest);
           }

           /**
            * @desc save asset dds info
            * @param requestData
            * @param tenants
            * @returns {*}
            */
           function saveDeviceDataSet(requestData, tenants) {
               let url = `${ApiVersionUrl}${deviceDataSetApi}/asset?`;
               let params = {};
               if (tenants.cartos) {
                   url += 'cartos=true&';
                   params.cartos = true;
               }
               if (tenants.pharos) {
                   url += 'pharos=true&';
                   params.pharos = true;
               }
               if (tenants.portico) {
                   url += 'portico=true&';
                   params.portico = true;
               }
               return $http.post(url, requestData);
           }

           /**
            * @desc update dds asset info
            * @param id
            * @param req
            * @param tenant
            * @returns {*}
            */
           function updateDeviceDataSet(id, req, tenant) {
               let url = `${ApiVersionUrl}${deviceDataSetApi}/asset/${id}?`;
               switch(tenant) {
                   case "cartos":
                       url += 'cartos=true';
                       break;
                   case "portico":
                       url += 'portico=true';
                       break;
                   case "pharos":
                       url += 'pharos=true';
                       break;
               }
               return $http.put(url, req);
           }

           /**
            * @desc get dds tco info
            * @param id
            * @param tenants
            * @returns {*}
            */
           function getTcoDeviceById(id, tenants) {
               let url = `${ApiVersionUrl}${deviceDataSetApi}/tcodevice/${id}?`;
               if (tenants.cartos) {
                   url += 'tenantName=cartos&';
               }
               if (tenants.pharos) {
                   url += 'tenantName=pharos&';
                   params.pharos = true;
               }
               if (tenants.portico) {
                   url += 'tenantName=portico&';
                   params.portico = true;
               }
               return $http.get(url);
           }

           /**
            * desc saves tco device data
            * @param data
            * @param tenants
            * @returns {*}
            */
           function saveTcoDevice(data, tenants) {
               let url = `${ApiVersionUrl}${deviceDataSetApi}/tcodevice?`;
               let params = {};
               if (tenants.cartos) {
                   url += 'cartos=true&';
               }
               if (tenants.pharos) {
                   url += 'pharos=true&';
                   params.pharos = true;
               }
               if (tenants.portico) {
                   url += 'portico=true&';
                   params.portico = true;
               }
               return $http.post(url, data);
           }

           /**
            * @desc I dont know what it does
            */
           function updateTcoDevice() {
                //todo
           }

           /**
            * @desc get user submission by id
            * @param id
            * @returns {*}
            */
           function getChangeRequestById(id) {
               let url = `${ApiVersionUrl}${deviceDataSetApi}/notification/${id}`;
               return $http.get(url);
           }

           return {
               getDeviceDataSet: getDeviceDataSet,
               getDeviceDataSetById: getDeviceDataSetById,
               saveDeviceDataSet: saveDeviceDataSet,
               updateDeviceDataSet: updateDeviceDataSet,
               getTcoDevice: getTcoDeviceById,
               getTcoDeviceById: getTcoDeviceById,
               saveTcoDevice: saveTcoDevice,
               updateTcoDevice: updateTcoDevice,
               dataToEmailService: dataToEmailService,
               getChangeRequestById: getChangeRequestById
           };
       }

       self.$get = [
           '$http',
           Api
       ];
   });
}());
/*global angular*/
(function () {
    'use strict';
    function DDSUIService($log, $window, $location, $rootScope, $routeParams, DDSConfig) {

        let self = this;
        let commonModel;
        let tenantSpecificModel;

        function prepareDDSQueryParams() {
        }

        function getActiveTenant() {
            if ($rootScope.activeTenant) {
                return $rootScope.activeTenant;
            } else {
                return DDSConfig.tenants.ANY;
            }
        }

        function init() {
            let tenant = getActiveTenant();
        }

        init();

        return {
            getActiveTenant: getActiveTenant,
            prepareDDSQueryParams: prepareDDSQueryParams
        };
    };

    var app = angular.module('DDS.UI'),
        requires = [
            '$log',
            '$window',
            '$location',
            '$rootScope',
            '$routeParams',
            'DDS.UI.Config',
            DDSUIService
        ];
    app.service('DDS.UI.Service', requires);
}());
/* global angular */

(function () {
    'use strict';

    var app = angular.module('DDS.UI');
    app.constant('DDS.UI.Config', {
        tenants: {
            ANY: 'any',
            CARTOS: 'cartos',
            PORTICO: 'portico',
            PHAROS: 'pharos'
        },
        specificTenants: ['cartos', 'portico', 'pharos'],
        defyValues: ["", "undefined", "[]", "{}"]
    });
}());
/* global angular */
(function () {
    'use strict';

    var config = {
        deviceDataSet: {
            dataTypes: {
                make: "string",
                model: "string",
                printTechnology: "selector",
                print: "boolean",
                hasColor: "boolean",
                copy: "boolean",
                fax: "boolean",
                hasA3: "boolean",
                productNumber: "string",
                deviceType: "selector",
                printerType: "string",
                productStatus: "string",
                billingModelGroup: "string",
                countryList: "array",
                productStatusList: "array",
                odgList: "array",
                rmpvList: "array",
                bestColorPercentList: "array",
                colorPercentList: "array",
                engineLife: "string",
                hasHolePunch: "boolean",
                bookletMaker: "boolean",
                hasDuplex: "boolean",
                placement: "selector",
                hasHdd: "boolean",
                mailBin: "boolean",
                hasStapler: "boolean",
                hasColorProf: "boolean",
                isInk: "boolean",
                isMfp: "boolean",
                isOdg: "boolean",
                maxTrays: "number",
                stdTrays: "number",
                printerFamily: "string",
                printerSegment: "string",
                updateDate: "date",
                productId: "string",
                isMpsSupported: "boolean",
                confidence: "string",
                hasStacker: "boolean",
                hiCapInput: "boolean",
                lifeTotalMonthly: "number",
                monoTotalMonthly: "number",
                colorTotalMonthly: "number",
                colorProMonthlyVolume: "number",
                lifeTotalLastReading: "number",
                monoTotalLastReading: "number",
                colorTotalLastReading: "number",
                monoA3LastReading: "number",
                monoA4LastReading: "number",
                monoA3Monthly: "number",
                monoA4Monthly: "number",
                colorA3LastReading: "number",
                colorA4LastReading: "number",
                colorA3Monthly: "number",
                colorA4Monthly: "number",
                colorSplitVolume: "number"
            },
            division: {
                numberFields: [
                    "maxTrays",
                    "stdTrays",
                    "lifeTotalMonthly",
                    "monoTotalMonthly",
                    "colorTotalMonthly",
                    "colorProMonthlyVolume",
                    "lifeTotalLastReading",
                    "monoTotalLastReading",
                    "colorTotalLastReading",
                    "monoA3LastReading",
                    "monoA4LastReading",
                    "monoA3Monthly",
                    "monoA4Monthly",
                    "colorA3LastReading",
                    "colorA4LastReading",
                    "colorA3Monthly",
                    "colorA4Monthly",
                    "colorSplitVolume"
                ],
                binaryFields: [
                    "print",
                    "hasColor",
                    "copy",
                    "fax",
                    "hasA3",
                    "hasHolePunch",
                    "bookletMaker",
                    "hasDuplex",
                    "hasHdd",
                    "mailBin",
                    "hasStapler",
                    "hasColorProf",
                    "isInk",
                    "isMfp",
                    "isOdg",
                    "isMpsSupported",
                    "hasStacker",
                    "hiCapInput"
                ],
                textFields: [
                    "make",
                    "model",
                    "productNumber",
                    "printerType",
                    "productStatus",
                    "billingModelGroup",
                    "engineLife",
                    "printerFamily",
                    "printerSegment",
                    "productId",
                    "confidence"
                ],
                selectionFields: [
                    {
                        name: "printTechnology",
                        options: ["laser", "ink", "solidInk", "led", "dyeSub", "thermal", "ribbon", "dotMatrix"]
                    },
                    {
                        name: "deviceType",
                        options: ["printMono", "printColor", "multifunctionMono", "multifunctionColor", "fax", "scanner",
                                "plotter", "unknown"]
                    },
                    {
                        name: "placement",
                        options: ['floorStanding', "desktop"]
                    }
                ],
                multiValFields: [
                    "countryList",
                    "productStatusList",
                    "odgList",
                    "rmpvList",
                    "bestColorPercentList",
                    "colorPercentList"
                ]
            },
            allFields: ["productNumber", "model", "make", "productStatus", "billingModelGroup", "countryList", "productStatusList", "rmpvList", "colorPercentList", "bestColorPercentList", "engineLife", "print", "hasHolePunch", "bookletMaker", "hasDuplex", "fax", "mailBin", "hasStapler", "hasA3", "hasColor", "hasColorProf", "isInk", "isMfp", "isOdg", "maxTrays", "stdTrays", "printerFamily", "printerSegment", "isMpsSupported", "deviceType", "rmpv", "printTechnology"]
        },
        tcoDataSet: {
            emptyModel: {
                longLifeConsumables: [],
                servicePacks: [],
                price: null,
                partNumber: null,
                description: "",
                type: "",
                category: '',
                guid: '',
                model: '',
                make: ''
            },
            dataTypes: {
                longLifeConsumables: 'array',
                servicePacks: 'array',
                price: 'number',
                partNumber: 'number',
                blackLifePages: 'number',
                colorLifePages: 'number',
                color: 'string',
                description: 'string',
                type: 'string',
                category: 'string',
                guid: 'string',
                model: 'string',
                make: 'string'
            },
            allFields: ["price", "partNumber", "description", "type",
                "category", "guid", "model", "make", "longLifeConsumables", "servicePacks"],
            division: {
                numberFields: ["price", "partNumber"],
                binaryFields: [],
                textFields: ["description", "type", "category", "guid", "model", "make"],
                selectionFields: [],
                multiValFields: ["longLifeConsumables", "servicePacks"]
            },
            nestedProperties: {
                longLifeConsumables: {
                    "partNumber": '',
                    "color": '',
                    "description": '',
                    "type": '',
                    "category": '',
                    "colorLifePages": null,
                    "blackLifePages": null,
                    "price": null
                },
                servicePacks: {
                    "months": "",
                    "partNumber": "",
                    "description": "",
                    "category": "",
                    "colorLifePages": null,
                    "blackLifePages": null,
                    "price": null
                }
            }
        }
    };

    var ddsApp = angular.module('DDS.UI');
    ddsApp.constant('DDS.UI.CartosConfig', config);
}());
/* global angular */
(function () {
    'use strict';

    var config = {
        deviceDataSet: {
            dataTypes: {
                modelID: 'number',
                model: 'string',
                modelShortName: 'string',
                manufacturerID: 'number',
                segment: 'string',
                originalSegment: 'string',
                importSourceSegment: 'string',
                deviceType: 'selector',
                printTechnology: 'selector',
                hasDuplex: 'boolean',
                hasColor: 'boolean',
                hasA3: 'boolean',
                dateIntroduced: 'date',
                speedMono: 'number',
                speedColor: 'number',
                dutyCycle: 'number',
                totalCurrentMonthlyVolume: 'number',
                externalKey: 'string',
                hashTag: 'string',
                administrativeNotes: 'string',
                tec: 'number',
                usbVendorId: 'string',
                usbProductId: 'string',
                excludeFromDesign: 'boolean',
                isMpsSupported: 'boolean',
                supportDependency: 'string',
                costingConstraint: 'string',
                defaultMonoCpp: 'number',
                defaultColorCpp: 'number',
                assignedMonoCpp: 'number',
                assignedColorCpp: 'number',
                dateCppUpdated: 'date',
                dateCreated: 'date',
                updatedDate: 'date'
            },
            division: {
                "numberFields": [
                    "modelID",
                    "manufacturerID",
                    "speedMono",
                    "speedColor",
                    "dutyCycle",
                    "totalCurrentMonthlyVolume",
                    "tec",
                    "defaultMonoCpp",
                    "defaultColorCpp",
                    "assignedMonoCpp",
                    "assignedColorCpp"
                ],
                "binaryFields": [
                    "hasDuplex",
                    "hasColor",
                    "hasA3",
                    "excludeFromDesign",
                    "isMpsSupported"
                ],
                "textFields": [
                    "model",
                    "modelShortName",
                    "segment",
                    "originalSegment",
                    "importSourceSegment",
                    "externalKey",
                    "hashTag",
                    "administrativeNotes",
                    "usbVendorId",
                    "usbProductId",
                    "supportDependency",
                    "costingConstraint"
                ],
                selectionFields: [
                    {
                        name: "printTechnology",
                        options: ["laser", "ink", "solidInk", "led", "dyeSub", "thermal", "ribbon", "dotMatrix"]
                    },
                    {
                        name: "deviceType",
                        options: ["printMono", "printColor", "multifunctionMono", "multifunctionColor", "fax", "scanner",
                            "plotter", "unknown"]
                    }
                ],
                "multiValFields": []
            },
            allFields: ["modelID", "model", "modelShortName", "manufacturerID", "segment", "originalSegment", "importSourceSegment", "deviceType", "printTechnology", "hasDuplex", "hasColor", "hasA3", "dateIntroduced", "speedMono", "speedColor", "dutyCycle", "totalCurrentMonthlyVolume", "externalKey", "hashTag", "administrativeNotes", "tec", "usbVendorId", "usbProductId", "excludeFromDesign", "isMpsSupported", "supportDependency", "costingConstraint", "defaultMonoCpp", "defaultColorCpp", "assignedMonoCpp", "assignedColorCpp", "dateCppUpdated", "dateCreated", "updatedDate"]
        },
        tcoDataSet: {
            emptyModel: {
                longLifeConsumables: [],
                partNumber: null,
                description: '',
                type: '',
                category: '',
                price: null,
                servicePacks: [],
                guid: '',
                make: '',
                model: ''
            },
            dataTypes: {
                longLifeConsumables: 'array',
                partNumber: 'number',
                color: 'string',
                description: 'string',
                type: 'string',
                category: 'string',
                colorLifePages: 'number',
                blackLifePages: 'number',
                price: 'number',
                servicePacks: 'array',
                guid: 'string',
                make: 'string',
                model: 'string'
            },
            allFields: ["longLifeConsumables", "partNumber", "description", "type", "category", "price", "servicePacks", "guid", "make", "model"],
            division: {
                "numberFields": [
                    "partNumber",
                    "price"
                ],
                "binaryFields": [],
                "textFields": [
                    "description",
                    "type",
                    "category",
                    "guid",
                    "make",
                    "model"
                ],
                "selectionFields": [],
                "multiValFields": [
                    "longLifeConsumables",
                    "servicePacks"
                ]
            },
            nestedProperties: {
                longLifeConsumables: {
                    "partNumber": '',
                    "color": '',
                    "description": '',
                    "type": '',
                    "category": '',
                    "colorLifePages": null,
                    "blackLifePages": null,
                    "price": null
                },
                servicePacks: {
                    "months": "",
                    "partNumber": "",
                    "description": "",
                    "category": "",
                    "colorLifePages": null,
                    "blackLifePages": null,
                    "price": null
                }
            }
        }
    };

    var ddsApp = angular.module('DDS.UI');
    ddsApp.constant('DDS.UI.PharosConfig', config);
}());
/* global angular */
(function () {
    'use strict';

    var config = {
        deviceDataSet: {
            dataTypes: {
                productNumber: 'string',
                model: 'string',
                make: 'string',
                productStatus: 'string',
                billingModelGroup: 'string',
                countryList: 'string',
                productStatusList: 'string',
                rmpvList: 'string',
                colorPercentList: 'string',
                bestColorPercentList: 'string',
                engineLife: 'number',
                print: 'boolean',
                hasHolePunch: 'boolean',
                bookletMaker: 'boolean',
                hasDuplex: 'boolean',
                fax: 'boolean',
                mailBin: 'boolean',
                hasStapler: 'boolean',
                hasA3: 'boolean',
                hasColor: 'boolean',
                hasColorProf: 'boolean',
                isInk: 'boolean',
                isMfp: 'boolean',
                isOdg: 'boolean',
                maxTrays: 'number',
                stdTrays: 'number',
                printerFamily: 'string',
                printerSegment: 'number',
                isMpsSupported: 'boolean',
                deviceType: 'selector',
                rmpv: 'number',
                printTechnology: 'selector'
            },
            division: {
                "numberFields": [
                    "engineLife",
                    "maxTrays",
                    "stdTrays",
                    "printerSegment",
                    "rmpv"
                ],
                "binaryFields": [
                    "print",
                    "hasHolePunch",
                    "bookletMaker",
                    "hasDuplex",
                    "fax",
                    "mailBin",
                    "hasStapler",
                    "hasA3",
                    "hasColor",
                    "hasColorProf",
                    "isInk",
                    "isMfp",
                    "isOdg",
                    "isMpsSupported"
                ],
                "textFields": [
                    "productNumber",
                    "model",
                    "make",
                    "productStatus",
                    "billingModelGroup",
                    "countryList",
                    "productStatusList",
                    "rmpvList",
                    "colorPercentList",
                    "bestColorPercentList",
                    "printerFamily"
                ],
                selectionFields: [
                    {
                        name: "printTechnology",
                        options: ["laser", "ink", "solidInk", "led", "dyeSub", "thermal", "ribbon", "dotMatrix"]
                    },
                    {
                        name: "deviceType",
                        options: ["printMono", "printColor", "multifunctionMono", "multifunctionColor", "fax", "scanner",
                            "plotter", "unknown"]
                    }
                ],
                "multiValFields": []
            },
            allFields: ["productNumber", "model", "make", "productStatus", "billingModelGroup", "countryList", "productStatusList", "rmpvList", "colorPercentList", "bestColorPercentList", "engineLife", "print", "hasHolePunch", "bookletMaker", "hasDuplex", "fax", "mailBin", "hasStapler", "hasA3", "hasColor", "hasColorProf", "isInk", "isMfp", "isOdg", "maxTrays", "stdTrays", "printerFamily", "printerSegment", "isMpsSupported", "deviceType", "rmpv", "printTechnology"]
        },
        tcoDataSet: {
            emptyModel: {
                longLifeConsumables: [],
                servicePacks: [],
                price: [],
                partNumber: null,
                description: '',
                type: '',
                category: '',
                guid: '',
                model: '',
                make: ''
            },
            dataTypes: {
                longLifeConsumables: 'array',
                servicePacks: 'array',
                price: 'number',
                partNumber: 'number',
                blackLifePages: 'number',
                colorLifePages: 'number',
                color: 'string',
                description: 'string',
                type: 'string',
                category: 'string',
                guid: 'string',
                model: 'string',
                make: 'string'
            },
            allFields: ["price", "partNumber", "description", "type",
                "category", "guid", "model", "make", "longLifeConsumables", "servicePacks"],
            division: {
                "numberFields": [
                    "price",
                    "partNumber",
                    "blackLifePages",
                    "colorLifePages"
                ],
                "binaryFields": [],
                "textFields": [
                    "description",
                    "type",
                    "category",
                    "guid",
                    "model",
                    "make"
                ],
                "selectionFields": [],
                "multiValFields": [
                    "longLifeConsumables",
                    "servicePacks"
                ]
            },
            nestedProperties: {
                longLifeConsumables: {
                    "partNumber": '',
                    "color": '',
                    "description": '',
                    "type": '',
                    "category": '',
                    "colorLifePages": null,
                    "blackLifePages": null,
                    "price": null
                },
                servicePacks: {
                    "months": "",
                    "partNumber": "",
                    "description": "",
                    "category": "",
                    "colorLifePages": null,
                    "blackLifePages": null,
                    "price": null
                }
            }
        }
    };

    var ddsApp = angular.module('DDS.UI');
    ddsApp.constant('DDS.UI.PorticoConfig', config);
}());
/*global angular*/
(function () {

    function DDSUiController($q, $log, $scope, $injector, $rootScope, $routeParams, CartosConfig, PorticoConfig, PharosConfig,
                               DDSConfig, DDSUIService, ApiService) {

        const $routeChangeSuccess = '$routeChangeSuccess';
        const EDIT_DDS = 'editdds';
        const DDS_UI = 'ddsui';

        /**
         * @desc function to find only required fields
         * @param keys
         * @param obj
         * @returns {*}
         */
        const destructurer = (keys, obj) => keys.reduce((a, c) => ({ ...a, [c]: obj[c] }), {});

        /**
         * @desc in dds ui route config
         * @param assetInfo
         * @param data
         */
        function onDDSUI(assetInfo, data) {
            try {
                let allFields = $scope[$scope.tenantConfig[$rootScope.activeTenant]].deviceDataSet.allFields;
                $scope.DDSChangeRequest = JSON.parse(data);
                $scope.deviceDataSet = destructurer(allFields, $scope.DDSChangeRequest.deviceDataSet);
                if ($scope.DDSChangeRequest.tcoDevice) {
                    $scope[$scope.tenantTcoModel[$rootScope.activeTenant]] = $scope.DDSChangeRequest.tcoDevice;
                }
                $scope.originalDDSValue = angular.copy($scope.deviceDataSet);
            } catch (e) {
                alert(e);
                $log.error(e);
            }
        }

        /**
         * @desc get asset dds info
         * @param id
         * @param tenant
         */
        function getOriginalDDS(id, tenant) {
            ApiService.getDeviceDataSetById(id, tenant).then((response) => {
                $scope.originalDDSValue = response.data.data;
            }, (error) => {
                $log.error(error);
            });
        }

        /**
         * @desc get user submit req by id
         */
        function getChangeRequestById() {
            ApiService.getChangeRequestById($routeParams.id)
                .then((response) => {
                    if (response && response.data && response.data.data && response.data.data.length) {
                        $scope.DDSUIrequest.userData = response.data.data[0].userData;
                        $scope.DDSUIrequest.tenant = response.data.data[0].tenant;
                        let ids = Object.keys(response.data.data[0].appeal);
                        $scope.DDSUIrequest.deviceDataSet = response.data.data[0].appeal[ids[0]];
                        $scope.deviceDataSet = $scope.DDSUIrequest.deviceDataSet.asset;
                        $scope[$scope.tenantTcoModel[$scope.DDSUIrequest.tenant]] = $scope.DDSUIrequest.deviceDataSet.tcoDevice;
                        getOriginalDDS($scope.DDSUIrequest.deviceDataSet.id, $scope.DDSUIrequest.tenant);
                    }
                }, (error) => {
                    $log.error(error);
                })
        }

        function getAllowedTenants() {
            return {
                cartos: $scope.DDSUI.FLAG.isEnabledCartos,
                pharos: $scope.DDSUI.FLAG.isEnabledPharos,
                portico: $scope.DDSUI.FLAG.isEnabledPortico
            };
        }

        function saveTcoInfo() {
            let tcoRequest = {};

            if ($scope.DDSUI.FLAG.isEnabledCartos) {
                tcoRequest.cartosTco = $scope.cartosExtended;
            }
            if ($scope.DDSUI.FLAG.isEnabledPharos) {
                tcoRequest.pharosTco = $scope.pharosExtended;
            }
            if ($scope.DDSUI.FLAG.isEnabledPortico) {
                tcoRequest.porticoTco = $scope.porticoExtended;
            }

            const forTenant = getAllowedTenants();
            
            return ApiService.saveTcoDevice(angular.toJson(tcoRequest), forTenant);

        }

        function saveAssetInfo() {
            let request = $scope.deviceDataSet;

            if ($scope.DDSUIrequest.tenant) {
                let requiredFields = $scope[$scope.tenantConfig[$scope.DDSUIrequest.tenant]].deviceDataSet.allFields;
                $scope.deviceDataSet = destructurer(requiredFields, $scope.deviceDataSet);
            }

            let basicRequest = filterAssetRequest();
            const forTenant = getAllowedTenants();

            if ($scope.DDSUIrequest.deviceDataSet && $scope.DDSUIrequest.deviceDataSet.id) {
                //update
              return  ApiService.updateDeviceDataSet($scope.DDSUIrequest.deviceDataSet.id, basicRequest, $scope.DDSUIrequest.tenant);
            } else {
              //create
              return ApiService.saveDeviceDataSet(request, forTenant);
            }
        }

        function sendAck() {
            let ack = {
                userData: $scope.DDSUIrequest.userData,
                message: $scope.DDSUI.message,
                sendAck: true,
                id: $routeParams.id,
                processed: {
                    id: $scope.DDSUIrequest.deviceDataSet.id
                }
            };
            return ApiService.dataToEmailService(JSON.stringify(ack));
        }


        $scope.onSave = function () {
          let saveRequest = [];
          saveRequest.saveAssetInfo = saveAssetInfo();
          saveRequest.saveTcoInfo = saveTcoInfo();
          if ($scope.DDSUIrequest.userData || $scope.userRequested) {
            saveRequest.sendAck = sendAck();
          }
          $q.all(saveRequest)
              .then((response) => {
                alert("Successfully saved the data in db. \n" +
                    "Will be available to use in 24 Hrs");
              }, (error) => {
                $log.error(error);
                alert("Unsuccessful, Please try again.");
              });
        };


      /**
       * @desc find tenant
       * @returns {boolean}
       */
      function isSingleTenant() {
        let enabledTenantCount = 0;
        if ($scope.DDSUI.FLAG.isEnabledCartos) {
          ++enabledTenantCount;
        }
        if ($scope.DDSUI.FLAG.isEnabledPharos) {
          ++enabledTenantCount;
        }
        if ($scope.DDSUI.FLAG.isEnabledPortico) {
          ++enabledTenantCount;
        }
        return enabledTenantCount <= 1;
      }

        /**
         * @desc remove null, undefined values in devicedataset asset info
         */
        function filterAssetRequest(dds) {
            let tempDDSReq = angular.copy($scope.deviceDataSet);
            for (let key in tempDDSReq) {
                if (DDSConfig.defyValues.includes(JSON.stringify(tempDDSReq[key]))) {
                    delete tempDDSReq[key];
                }
            }
            return tempDDSReq;
        }

        /**
         * @desc remove null, undefined values in tco request
         */
        function filterTcoRequest() {
            let tcoRequest = {};
            if ($scope.DDSUI.FLAG.isEnabledCartos) {
                let tempCartosTco = angular.copy($scope.cartosExtended);
                for (let key in tempCartosTco) {
                    if (DDSConfig.defyValues.includes(JSON.stringify(tempCartosTco[key]))) {
                        delete tempCartosTco[key];
                    }
                }
                tcoRequest.cartosTco = angular.copy(tempCartosTco);
            }
            if ($scope.DDSUI.FLAG.isEnabledPortico) {
                let tempPorticoTco = angular.copy($scope.porticoExtended);
                for (let key in tempPorticoTco) {
                    if (DDSConfig.defyValues.includes(JSON.stringify(tempPorticoTco[key]))) {
                        delete tempPorticoTco[key];
                    }
                }
                tcoRequest.porticoTco = angular.copy(tempPorticoTco);
            }
            if ($scope.DDSUI.FLAG.isEnabledPharos) {
                let tempPharosTco = angular.copy($scope.pharosExtended);
                for (let key in tempPharosTco) {
                    if (DDSConfig.defyValues.includes(JSON.stringify(tempPharosTco[key]))) {
                        delete tempPharosTco[key];
                    }
                }
                tcoRequest.pharosTco = angular.copy(tempPharosTco);
            }
            return tcoRequest;
        }

        /**
         * @desc send email notification with the request
         */
        function emailServiceData() {

            // filter tenant specific request params
            if ($rootScope.activeTenant !== DDSConfig.tenants.ANY) {
                let requiredFields = $scope[$scope.tenantConfig[$rootScope.tenant]].deviceDataSet.allFields;
                $scope.deviceDataSet = destructurer(requiredFields, $scope.deviceDataSet);
            }

            let tcoRequest = filterTcoRequest();
            let basicRequest = filterAssetRequest();

            let emailRequest = {
                userData: {
                    userId: $rootScope.token.sysId,
                    email: $rootScope.token.userId
                },
                tenant: $rootScope.tenant,
                deviceDataSet: {
                    id: $scope.DDSChangeRequest.id,
                    asset:  basicRequest,
                    tcoDevice: tcoRequest
                }
            };

            function onSubmitSuccess(result) {
                alert("Your request has been submitted successfully.\n" +
                    " Our team is notified to take care of the this request.");
                $scope.navigateBack();
            }

            ApiService.dataToEmailService(JSON.stringify(emailRequest)).then(onSubmitSuccess, (error) => {
                alert("Unsuccessful, Please try again or click cancel to go back.");
            });
        }


        /**
         * @desc submit the request by sending email
         */
        $scope.onSubmit = function () {
            emailServiceData();
        };

      /**
       * @desc onRouteChangeEvent
       */
      function onRouteChangeEvent () {
          if ($routeParams.id) {
            getChangeRequestById();
          }
        }

      /**
       * @desc populates form with selected dds info.
       * @param $event
       * @param data
       */
        function onEditClick($event, data) {
          if (data && data.tenant && data.deviceDataSet) {
            $scope.deviceDataSet = JSON.parse(data.deviceDataSet);
            $scope.originalDDSValue = JSON.parse(data.deviceDataSet);
            enableTenantSection(data.tenant);
            enableDDSTab();
          }
        }

        /**
         * @desc warning highlight for all editable fields
         * @param itVal
         * @param type
         * @returns {boolean}
         */
        $scope.hasWarning = function (itVal, type) {
            switch (type) {
                case "boolean":
                    let comparator = $scope.originalDDSValue[itVal];
                    if (typeof $scope.originalDDSValue[itVal] === "boolean") {
                        switch ($scope.deviceDataSet[itVal]) {
                            case false:
                                comparator = $scope.originalDDSValue[itVal].toString();
                                break;
                            case true:
                                comparator = $scope.originalDDSValue[itVal].toString();
                                break;
                            default:
                                comparator = false;
                        }

                    } else {
                        switch (typeof $scope.originalDDSValue[itVal]) {
                            case "string":
                                comparator = false;
                                break;
                            case "undefined":
                                if ($scope.deviceDataSet[itVal]) {
                                    comparator = "false";
                                } else {
                                    comparator = false;
                                }
                                break;
                            case "boolean":
                                comparator = "false";
                                break;
                            default:
                                comparator = false;
                        }
                    }
                    return !!(comparator && $scope.deviceDataSet[itVal] !== $scope.originalDDSValue[itVal]);

                default:
                    return !!($scope.originalDDSValue[itVal] && $scope.deviceDataSet[itVal] !== $scope.originalDDSValue[itVal]);
            }
        };

        /**
         * @desc fetches tco device by id
         */
        $scope.fetchTcoDevice = function () {
            let forTenant = {
                [$scope.DDSUI.FLAG.activeTenant]: true
            };
            ApiService.getTcoDevice($scope.DDSUI.tcoDeviceId, forTenant)
                .then((data) => {},
                    (err) => {
                    $log.error(err);
                });
        };

        /**
         * @desc deletes last llc
         */
        $scope.deleteLastLLC = function createNewCartosLLC() {
            $scope.cartosExtended.longLifeConsumables.pop();
        };


        /**
         * @desc to add new sp item
         * @param tenant
         * @param model
         */
        $scope.addNewSP = function (tenant, model) {
            $scope[model].servicePacks.push(angular.copy($scope[$scope.tenantConfig[tenant]].tcoDataSet.nestedProperties.servicePacks));
        };

        /**
         * @desc to add new llc item
         * @param tenant
         * @param model
         */
        $scope.addNewLLC = function (tenant, model) {
            let newLLC = angular.copy($scope[$scope.tenantConfig[tenant]].tcoDataSet.nestedProperties.longLifeConsumables);
            $scope[model].longLifeConsumables.push(newLLC);
        };

        /**
         * @desc get original dds
         * @param id
         * @param tenant
         */
        function fetchCurrentDDS(id, tenant) {
            ApiService.getDeviceDataSetById(id, tenant)
                .then((data) => {
                    $scope.originalDDSValue = data.data.data;
                });
        }

      /**
       * @desc auto toggle tenant specific section
       * @param tenant
       */
        function enableTenantSection(tenant) {
          switch (tenant) {
            case DDSConfig.tenants.CARTOS:
              $scope.DDSUI.FLAG.isEnabledCartos = true;
              break;
            case DDSConfig.tenants.PHAROS:
              $scope.DDSUI.FLAG.isEnabledPharos = true;
              break;
            case DDSConfig.tenants.PORTICO:
              $scope.DDSUI.FLAG.isEnabledPortico = true;
              break;
          }
        }

        /**
         * @desc assign the data to models
         */
        function processFileData() {
            if ($scope.DDSUIrequest.deviceDataSet.id && $scope.DDSUIrequest.deviceDataSet.asset) {
                $scope.deviceDataSet = $scope.DDSUIrequest.deviceDataSet.asset;
                if ($scope.DDSUIrequest.deviceDataSet.id) {
                    fetchCurrentDDS($scope.DDSUIrequest.deviceDataSet.id, $scope.DDSUIrequest.tenant);
                }
                if ($scope.DDSUIrequest.deviceDataSet.tcoDevice && $scope.DDSUIrequest.deviceDataSet.tcoDevice.cartosTco) {
                  $scope[$scope.tenantTcoModel[$scope.DDSUIrequest.tenant]] =
                      $scope.DDSUIrequest.deviceDataSet.tcoDevice.cartosTco;
                }
                $scope.DDSUI.FLAG.fileUploadDone = true;
            }
            enableTenantSection($scope.DDSUIrequest.tenant);
            enableDDSTab();
        }

        /**
         * @desc reads the file and process data
         */
        $scope.readTheFile = function () {
            let fileContent = '';
            let fileSize = 0;
            let fileName = '';
            var file = document.getElementById("DDSFileInput").files[0];
            if (file) {
                let aReader = new FileReader();
                aReader.readAsText(file, "UTF-8");
                aReader.onload = function (evt) {
                    $scope.fileContent = aReader.result;
                    $scope.fileName = document.getElementById("DDSFileInput").files[0].name;
                    $scope.fileSize = document.getElementById("DDSFileInput").files[0].size;
                    $scope.parseddeviceDataSet = JSON.parse($scope.fileContent);
                    $scope.DDSUIrequest = $scope.parseddeviceDataSet;
                    processFileData();
                };
                aReader.onerror = function (evt) {
                    $scope.fileContent = "error";
                }
            }
        };

      /**
       * @desc enable / show hide tabs
       */
      function enableDDSTab() {
        $scope.DDSUI.FLAG.showTab = $scope.DDSUI.FLAG.isEnabledCartos ||
            $scope.DDSUI.FLAG.isEnabledPharos || $scope.DDSUI.FLAG.isEnabledPortico;
      }

        /**
         * @desc Show / hide UI for tenant section
         */
        $scope.toggleUiSection = function () {
          if (!$scope.DDSUI.FLAG.isEnabledCartos) {
            $scope.DDSUI.FLAG.enableCartosTCO = false;
            $scope.DDSUI.FLAG.enableCartosDDS = false;
          }
          if (!$scope.DDSUI.FLAG.isEnabledPortico) {
            $scope.DDSUI.FLAG.enablePorticoTCO = false;
            $scope.DDSUI.FLAG.enablePorticoDDS = false;
          }
          if (!$scope.DDSUI.FLAG.isEnabledPharos) {
            $scope.DDSUI.FLAG.enablePharosDDS = false;
            $scope.DDSUI.FLAG.enablePharosTCO = false;
          }
          enableDDSTab();
        };

        /**
         * show hide tco section to enter data for selected tenant
         * @param tenantTco
         */
        $scope.toggleTenantTabTco = function (tenantTco) {
            switch (tenantTco) {
                case 'enableCartosTCO':
                    $scope.DDSUI.FLAG.enablePorticoTCO = false;
                    $scope.DDSUI.FLAG.enablePharosTCO = false;
                    break;
                case 'enablePharosTCO':
                    $scope.DDSUI.FLAG.enablePorticoTCO = false;
                    $scope.DDSUI.FLAG.enableCartosTCO = false;
                    break;
                case 'enablePorticoTCO':
                    $scope.DDSUI.FLAG.enableCartosTCO = false;
                    $scope.DDSUI.FLAG.enablePharosTCO = false;
                    break;
            }
        };

        /**
         * @desc show hide section to enter data for selected tenant
         * @param tenantDDS
         */
        $scope.toggleTenantTab = function (tenantDDS) {
            switch (tenantDDS) {
                case 'enableCartosDDS':
                    $scope.DDSUI.FLAG.enablePorticoDDS = false;
                    $scope.DDSUI.FLAG.enablePharosDDS = false;
                    break;
                case 'enablePharosDDS':
                    $scope.DDSUI.FLAG.enablePorticoDDS = false;
                    $scope.DDSUI.FLAG.enableCartosDDS = false;
                    break;
                case 'enablePorticoDDS':
                    $scope.DDSUI.FLAG.enableCartosDDS = false;
                    $scope.DDSUI.FLAG.enablePharosDDS = false;
                    break;
            }
        };

        /**
         * @desc onclick new form
         */
        $scope.clearAllModels = function () {
            onLoad();
        };


        /**
         * navigate back to previous url
         */
        $scope.navigateBack = function () {
            window.history.back();
        };

        /**
         * @desc on button cancel click
         */
        $scope.onCancel = function () {
            if ($scope.DDSUI.FLAG.activeTenant === DDSConfig.tenants.ANY) {
                onLoad()
            } else {
                $scope.navigateBack();
            }
        };

        /**
         * adding dependencies
         */
        function initDependencies() {
            //todo
          enableDDSTab();
        }

        /**
         * @desc initialize only tenant specific models
         * @param tenant
         */
        function initModelFor(tenant) {
            try {
                //initialize [tenant].Extended model for tco data
                $scope[$scope.tenantTcoModel[tenant]] =
                    angular.copy($scope[$scope.tenantConfig[tenant]].tcoDataSet.emptyModel);

                $scope.DDSUI.FLAG.isEnabledCartos = $scope.DDSUI.FLAG.activeTenant === DDSConfig.tenants.CARTOS;
                $scope.DDSUI.FLAG.isEnabledPharos = $scope.DDSUI.FLAG.activeTenant === DDSConfig.tenants.PHAROS;
                $scope.DDSUI.FLAG.isEnabledPortico = $scope.DDSUI.FLAG.activeTenant === DDSConfig.tenants.PORTICO;
            } catch (e) {
                $log.error(e);
            }
        }

        /**
         * @desc initialize models for extended dds properties
         * e.g., TCO
         */
        function initAllModels() {
            $scope.cartosExtended = {
                longLifeConsumables: [angular.copy($scope.CartosConfig.tcoDataSet.nestedProperties.longLifeConsumables)],
                servicePacks: [angular.copy($scope.CartosConfig.tcoDataSet.nestedProperties.servicePacks)],
                price: null,
                partNumber: null,
                blackLifePages: null,
                colorLifePages: null,
                color: "",
                description: "",
                type: "",
                category: '',
                guid: '',
                model: '',
                make: ''
            };
            $scope.pharosExtended = {
                longLifeConsumables: [angular.copy($scope.PharosConfig.tcoDataSet.nestedProperties.longLifeConsumables)],
                partNumber: null,
                color: '',
                description: '',
                type: '',
                category: '',
                colorLifePages: null,
                blackLifePages: null,
                price: null,
                servicePacks: [angular.copy($scope.PharosConfig.tcoDataSet.nestedProperties.servicePacks)],
                guid: '',
                make: '',
                model: ''
            };
            $scope.porticoExtended = {
                longLifeConsumables: [angular.copy($scope.PorticoConfig.tcoDataSet.nestedProperties.longLifeConsumables)],
                servicePacks: [angular.copy($scope.PorticoConfig.tcoDataSet.nestedProperties.servicePacks)],
                price: [],
                partNumber: null,
                blackLifePages: null,
                colorLifePages: null,
                color: '',
                description: '',
                type: '',
                category: '',
                guid: '',
                model: '',
                make: ''
            };
        }

        /**
         * @desc on load of controller do this
         */
        function onLoad() {
            $scope.userRequested = "";
            $scope.DDSChangeRequest = {};
            $scope.tenantConfig = {
                cartos: 'CartosConfig',
                pharos: 'PorticoConfig',
                portico: 'PharosConfig'
            };
            // Only TCO models are tenant specific.
            $scope.tenantTcoModel = {
                cartos: 'cartosExtended',
                pharos: 'pharosExtended',
                portico: 'porticoExtended'
            };

            $scope.DDSUIrequest = {};
            $scope.DDSConfig = DDSConfig;   //to access in html
            $scope.deviceDataSet = {};      //dds model is common for all tenants.
            $scope.originalDDSValue = {};   //original dds value

            //Assigning constants configuration to local models
            $scope.CartosConfig = CartosConfig;       //for accessing in html
            $scope.PorticoConfig = PorticoConfig;     //for accessing in html
            $scope.PharosConfig = PharosConfig;       //for accessing in html

            $scope.DDSUI ={
                FLAG: {
                    tenant: DDSUIService.getActiveTenant(),
                    activeTenant: DDSUIService.getActiveTenant(),
                    isEnabledCartos: false,
                    isEnabledPharos: false,
                    isEnabledPortico: false,
                    enableCartosDDS: false,
                    enablePorticoDDS: false,
                    enablePharosDDS: false,
                    enableCartosTCO: false,
                    enablePorticoTCO: false,
                    enablePharosTCO: false,
                    showTab: false,
                    newDeviceDataSetText: "",
                    newTcoDeviceDataText: "",
                    selectedTenant: 'cartos',
                    tenants: DDSConfig.specificTenants,
                    tcoDeviceId: '',
                    fileUploadDone: false  //flag file upload
                },
                message: ""
            };

            if ($scope.DDSUI.FLAG.activeTenant === DDSConfig.tenants.ANY) {
                initAllModels();
            } else {
                initModelFor($scope.DDSUI.FLAG.activeTenant);
            }
            initDependencies();
        }

        onLoad();

        /**
         * @desc ddsUI event
         */
        $scope.$on(DDS_UI, onDDSUI);
        /**
         * @desc edit dds selected
         */
        $scope.$on(EDIT_DDS, onEditClick);
        /**
         * @desc routechange event
         */
        $scope.$on($routeChangeSuccess, onRouteChangeEvent);
    }

    var app = angular.module('DDS.UI'),
        requires = [
            '$q',
            '$log',
            '$scope',
            '$injector',
            '$rootScope',
            '$routeParams',
            'DDS.UI.CartosConfig',
            'DDS.UI.PharosConfig',
            'DDS.UI.PorticoConfig',
            'DDS.UI.Config',
            'DDS.UI.Service',
            'DDS.UI.ApiService',
            DDSUiController
        ];
    app.controller('DDS.UI.Controller', requires);
}());
//# sourceMappingURL=../client/DDS.UI.js.map
