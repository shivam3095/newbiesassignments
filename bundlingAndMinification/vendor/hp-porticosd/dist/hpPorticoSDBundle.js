/*jslint bitwise: true*/
(function () {
  'use strict';

  /**
   * This script is responsible to polyfill. We will mainly polyfill those methods
   * which are added to the ECMAScript 6 specification and may not be available in
   * all JavaScript implementations yet. We will also add methods which are specially
   * required in Cartos. Those methods will be shimmable. They will behave same accross
   * all browsers.
   */

  //Javascript types.
  const NUMBER = 'number',
      STRING = 'string';

  if (!String.prototype.includes) {
    /**
     * Determines whether one string may be found within another string, returning true or
     * false as appropriate. This makes a case sensitive check.
     * @param   {string} search A string to be searched for within this string.
     * @param   {number} start  Optional. The position in this string at which
     *                          to begin searching for searchString; defaults to 0.
     * @returns {boolean}  true or false
     */
    String.prototype.includes = function (search, start) {

      if (typeof start !== NUMBER) {
        start = 0;
      }

      if (start + search.length > this.length) {
        return false;
      }
      return this.indexOf(search, start) !== -1;
    };
  }

  if (!String.prototype.isEmpty) {
    /**
     * Checks if a string is blank or contains only white-space.
     * @returns {boolean} true if string is blank or has only white-space.
     */
    String.prototype.isEmpty = function () {
      return (this.length === 0 || !this.trim());
    };
  }

  if (!String.prototype.toPascalCase) {
    /**
     * Convert the number to Pascal Case:
     * { 
     * BUILDING : Building
     * double-barrel = Double-Barrel
     * DOUBLE-BARREL = Double-Barrel
     * DoUbLE-BaRRel = Double-Barrel
     * double barrel = Double Barrel
     * }
     * @returns {string} converted string
     */
    String.prototype.toPascalCase = function () {
      return this.replace(/(\w)(\w*)/g,
          function (g0, g1, g2) {
            return g1.toUpperCase() + g2.toLowerCase();
          });
    };
  }

  if (!Date.prototype.isValid) {
    /**
     * Determines whether the date is valid or not, returning true or false accordingly.
     * @returns {boolean} true if date is valid.
     */
    Date.prototype.isValid = function () {
      // An invalid date object returns NaN for getTime() and NaN is the only
      // object not strictly equal to itself.
      return !isNaN(this.getTime());
    };
  }

  if (!Date.prototype.isSame) {
    /**
     * Determines whether two date objects represent same date or not.
     * @param date instance of date to be checked.
     * @returns {boolean} flag to indicate dates are same or not.
     */
    Date.prototype.isSame = function (date) {
      if (!(date instanceof Date)) {
        return false;
      }
      return this.getTime() === date.getTime();
    };
  }

  if(!Array.prototype.first){
    /**
     * Returns the item at first position in the array.
     * var lastItem = [3,2,1,5].first();
     * The value of lastItem is 3.
     * @returns {object} item stored at first index.
     */
    Array.prototype.first = function () {
      return this[0];
    };
  }

  if (!Array.prototype.last) {
    /**
     * Returns the item at last position in the array.
     * var lastItem = [3,2,1,5].last();
     * The value of lastItem is 5.
     * @returns {object} item stored ar last index.
     */
    Array.prototype.last = function () {
      return this[this.length - 1];
    };
  }

  if (!Array.prototype.insert) {
    /**
     * Insert an item to an array at specific index. It uses splice for insertion. If the index is
     * greater than length of array then item will be inserted at the end of the array. If index
     * is negative then is will insert item from right. For invalid values of index is simply unshift
     * the item in the array that is insert at 0. eg.
     * var x=[2,3,1,4,5,6];
     * x.insert(10,22); //index greater then length
     * console.log(x);//[2, 3, 1, 4, 5, 6, 22]
     * x.insert(-2,25);//index is negative
     * console.log(x);//[2, 3, 1, 4, 5, 25, 6, 22]
     * @param index position at which item will be added.
     * @param item item to be added.
     */
    Array.prototype.insert = function (index, item) {
      this.splice(index, 0, item);
    };
  }

  if (!Math.toRadians) {
    /**
     * Converts from degrees to radians.
     * @param degrees
     * @returns {number}
     */
    Math.toRadians = function (degrees) {
      return degrees * this.PI / 180;
    };
  }

  if (!Math.toDegrees) {
    /**
     * Converts from radians to degrees.
     * @param radians
     * @returns {number}
     */
    Math.toDegrees = function (radians) {
      return radians * 180 / this.PI;
    };
  }

  if (!String.prototype.toPascalCase) {
    String.prototype.toPascalCase = function () {
      return this.replace(/(\w)(\w*)/g,
          function (g0, g1, g2) {
            return g1.toUpperCase() + g2.toLowerCase();
          });
    };
  }


  if (!Object.values) {
    /**
     * The Object.values() method returns an array of a given object's own enumerable property values,
     * in the same order as that provided by a for...in loop (the difference being that a for-in loop
     * enumerates properties in the prototype chain as well).
     * @param O The object whose enumerable own property values are to be returned.
     * @returns {*} An array containing the given object's own enumerable property values.
     */
    Object.values = function (O) {
      var reduce = Function.bind.call(Function.call, Array.prototype.reduce),
          isEnumerable = Function.bind.call(Function.call, Object.prototype.propertyIsEnumerable),
          concat = Function.bind.call(Function.call, Array.prototype.concat),
          keys = Reflect.ownKeys;
      return reduce(keys(O), function (v, k) {
        return concat(v, typeof k === STRING && isEnumerable(O, k) ? [O[k]] : []);
      }, []);
    };
  }
  
}());

/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';
  let app = angular.module('app');
  app.constant('ngPorticoSD.constants.MultiVendorConfiguration', {

    TEMPLATE_CURRENCY_MAPPING: {
      "APCD - Australia": "AUD",
      "Ativa - Brazil": "BRL",
      "BLM - United States": "USD",
      "BLM Puerto Rico - Puerto Rico": "USD",
      "Barrister - Puerto Rico": "USD",
      "Barrister - United States": "USD",
      "Barrister Canada - Canada": "CAD",
      "CMGS - Australia": "AUD",
      "CMGS NZ - New Zealand": "NZD",
      "Custom Canada Service - Canada": "CAD",
      "Custom Canada Service 2 - Canada": "CAD",
      "Custom Puerto Rico b-f - Puerto Rico": "USD",
      "Custom Test - United States": "USD",
      "Custom US - United States": "USD",
      "Custom US BreakFix 2 - United States": "USD",
      "D1 - United States": "USD",
      "Field Solutions - United States": "USD",
      "Hemmersbach - Euro Countries": "EUR",
      "Hemmersbach - France": "EUR",
      "Hemmersbach - Germany": "EUR",
      "Hemmersbach - United Kingdom": "GBP",
      "Hemmersbach - pMPS Catalog - Austria": "EUR",
      "Hemmersbach - pMPS Catalog - Belgium": "EUR",
      "Hemmersbach - pMPS Catalog - France": "EUR",
      "Hemmersbach - pMPS Catalog - Germany": "EUR",
      "Hemmersbach - pMPS Catalog - Italy": "EUR",
      "Hemmersbach - pMPS Catalog - Luxembourg": "EUR",
      "Hemmersbach - pMPS Catalog - Netherlands": "EUR",
      "Hemmersbach - pMPS Catalog - Portugal": "EUR",
      "Hemmersbach - pMPS Catalog - Spain": "EUR",
      "Hemmersbach - pMPS Catalog - Switzerland": "CHF",
      "Hemmersbach - pMPS Catalog - United Kingdom": "GBP",
      "Hemmersbach France - Euro Countries": "EUR",
      "Hemmersbach Germany - Euro Countries": "EUR",
      "Hemmersbach Guernsey - Euro Countries": "EUR",
      "Hemmersbach Ireland - Euro Countries": "EUR",
      "Hemmersbach Poland - Euro Countries": "EUR",
      "Hemmersbach South Africa - Euro Countries": "EUR",
      "Hemmersbach UK - Euro Countries": "GBP",
      "Italy Thermal Service - Euro Countries": "EUR",
      "Konica Minolta EMEA Service - Euro Countries": "EUR",
      "Konica Minolta US Service - United States": "USD",
      "LMS - United States": "USD",
      "Neisa - Euro Countries": "EUR",
      "New CA Partner - Canada": "CAD",
      "New MX Partner - Mexico": "MXN",
      "PCI- Service - China": "CNY",
      "PMPS Service Delivery - United States": "USD",
      "Raise- Service - China": "CNY",
      "Rexion - Euro Countries": "EUR",
      "Seiton MX - Mexico": "MXN",
      "Solutions 30 - Euro Countries": "EUR",
      "Zebra - *": "USD",
      "iQor - India": "INR",
      "Alpha - India": "INR",
      "Custom Canada Supplies - Canada": "CAD",
      "Custom PR Supplies Partner - Puerto Rico": "USD",
      "Custom Test Supplies - United States": "USD",
      "Custom US Supplies - United States": "USD",
      "HP Inc Supplies Only - *": "USD",
      "HP Inc Supplies and MKRS - *": "USD",
      "Hermida - Euro Countries": "EUR",
      "Hermida France - Euro Countries": "EUR",
      "Hermida Germany - Euro Countries": "EUR",
      "Hermida Guernsey - Euro Countries": "EUR",
      "Hermida Ireland - Euro Countries": "EUR",
      "Hermida Italy - Euro Countries": "EUR",
      "Hermida Poland - Euro Countries": "EUR",
      "Hermida South Africa - Euro Countries": "ZAR",
      "Hermida UK - Euro Countries": "GBP",
      "IT Xcel - United States": "USD",
      "IT Xcel Australia - Australia": "AUD",
      "IT Xcel Brazil - Brazil": "BRL",
      "IT Xcel Canada - Canada": "CAD",
      "IT Xcel Colombia - Colombia": "COP",
      "IT Xcel Mexico - Mexico": "MXN",
      "IT Xcel NZ - New Zealand": "NZD",
      "Italy Thermal - Euro Countries": "EUR",
      "New Partner - Argentina": "ARS",
      "New Partner - Australia": "AUD",
      "New Partner - Brazil": "BRL",
      "New Partner - Chile": "CLP",
      "New Partner - China": "CNY",
      "New Partner - Colombia": "COP",
      "New Partner - Costa Rica": "CRC",
      "New Partner - Euro Countries": "EUR",
      "New Partner - Hong Kong": "HKD",
      "New Partner - India": "INR",
      "New Partner - Indonesia": "IDR",
      "New Partner - Japan": "JPY",
      "New Partner - Malaysia": "MYR",
      "New Partner - New Zealand": "NZD",
      "New Partner - Peru": "PEN",
      "New Partner - Philippines": "PHP",
      "New Partner - Puerto Rico": "USD",
      "New Partner - Singapore": "SGD",
      "New Partner - South Korea": "KRW",
      "New Partner - Taiwan": "TWD",
      "New Partner - Thailand": "THB",
      "New Partner - United States": "USD",
      "OfficeMax - Australia": "AUD",
      "PCI- Supplies - China": "CNY",
      "PMPS Supplies Delivery - United States": "USD",
      "Raise- Supplies - China": "CNY",
      "US pMPS Direct HP Toner - United States": "USD"
    }
  });
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';
  var app = angular.module('app');
  app.constant('ngPorticoSD.constants.PorticoApiConfig', {
    serverPort: {
      pricing: '9010'
    },
    DEVICE: 'device'
  });
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';
  var app = angular.module('app');
  app.constant('ngPorticoSD.constants.PorticoSDConfiguration', {
    LEVEL_PROJECT: 'PROJECT',
    LEVEL_COUNTRY: 'COUNTRY',
    LEVEL_SITE: 'SITE',
    LEVEL_BUILDING: 'BUILDING',
    LEVEL_FLOOR: 'FLOOR',

    TREE_LEVEL_PROJECT: 0,
    TREE_LEVEL_COUNTRY: 1,
    TREE_LEVEL_SITE: 2,
    TREE_LEVEL_BUILDING: 3,
    TREE_LEVEL_FLOOR: 4,
    TREE_LEVEL_ASSET: 5,
    TREE_LEVEL_ACCESSORY: 6,

    LEVEL: 'level',
    ASSET: 'asset',
    PROJECT: 'project',
    ACCESSORY: 'accessory',
    VIRTUAL_STATE: 'virtual',
    REAL_STATE: 'real',
    COUNTRY_NAME: 'CountryName',

    ZERO_PERCENT: '0%',
    EXCEL_CONTENT_TYPE: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    OUTCOME_CANNOT_APPLY_MESSAGE: 'OUTCOME_CANNOT_BE_ASSIGNED_TO_EMPTY_ASSET',

    PAGE_SIZE_A3: 'A3',
    PAGE_SIZE_A4: 'A4',

    COLUMN_WIDTH_120: 120,
    COLUMN_WIDTH_235: 235,
    COLUMN_WIDTH_210: 210,

    NOT_APPLICABLE: 'N/A',
    EMPTY: 'empty',
    ZERO: '0',
    NUMBER_TWO: 2,
    NUMBER_FIVE: 5,
    HP: "hp",

    LEVEL_ID: "levelId",

    ADVANCE_PLACEMENT: 'advancePlacement',
    RETAIN_ADVANCE_PLACEMENT: 'retainAdvancePlacement',

    STRING_PROJECT_ID: 'projectId',
    STRING_SOURCE: 'source',
    STRING_TCO: 'tco',

    DEFAULT_VIEW: 'defaultView',
    UPDATE_VIEW: 'updateView',
    DETAIL_TONER: 'detailsToner',
    DETAIL_RP: 'detailsRP',

    TRUE: "true",
    OK: 'Ok',
    _EMPTY: 'Empty',
    PLUS: '+',

    STATES: {
      CURRENT: 'current',
      TRANSITION: 'transition',
      FUTURE: 'future'
    },

    ADJUST_PRICING: {
      ADD: 'add',
      SUBTRACT: 'subtract'
    },

    PRICING_SOURCE: {
      MANUAL: 'Manual',
      DART: 'DART',
      TCO: 'TCO',
      OTHER: 'other',
      MULTI_VENDOR: 'MultiVendor'
    },

    PORTICOSD_GRID_COLUMNS: {
      level: 'level',
      subEnvID: 'subEnvID',
      selection: 'selection',
      mapId: 'mapId',
      deviceModel: 'deviceModel',
      pageSize: 'pageSize',
      deviceType: 'deviceType',
      deviceQty: 'deviceQty',
      zone: 'zone',
      hwDisposition: 'hwDisposition',
      sku: 'sku',
      pvtInA3: 'pvtInA3',
      pvtIn: 'pvtIn',
      pvtOutA3: 'pvtOutA3',
      pvtOut: 'pvtOut',
      ampv: 'ampv',
      monoPages: 'monoPages',
      colorPages: 'colorPages',
      colorPro: 'colorPro',
      monthlyMonoPages: 'monthlyMonoPages',
      monthlyColorPages: 'monthlyColorPages',
      monthlyColorProfPages: 'monthlyColorProfPages',
      monthlyPages: 'monthlyPages',
      floor: 'floor',
      colorPercentage: 'colorPercentage',
      colorTotalPagesPercentage: 'colorTotalPagesPercentage',
      a3Percentage: 'a3Percentage',
      colorA3Percentage: 'colorA3Percentage',
      monoA3Percentage: 'monoA3Percentage',
      age: 'age',
      lifeTimeEngineLife: 'lifeTimeEngineLife',
      eoslPast: 'eoslPast',
      comments: 'comments',
      netPriceUsd: 'netPriceUsd',
      upfrontChargeUsd: 'upfrontChargeUsd',
      totalRevenueUsd: 'totalRevenueUsd',
      totalCPPUsd: 'totalCPPUsd',
      monoCPPUsd: 'monoCPPUsd',
      colorCPPUsd: 'colorCPPUsd',
      baseFeeUsd: 'baseFeeUsd',
      monoClickUsd: 'monoClickUsd',
      colorClickUsd: 'colorClickUsd',
      bestColorClickUsd: 'bestColorClickUsd',
      netPriceLc: 'netPriceLc',
      upfrontChargeLc: 'upfrontChargeLc',
      totalRevenueLc: 'totalRevenueLc',
      totalCPPLc: 'totalCPPLc',
      monoCPPLc: 'monoCPPLc',
      colorCPPLc: 'colorCPPLc',
      baseFeeLc: 'baseFeeLc',
      monoClickLc: 'monoClickLc',
      colorClickLc: 'colorClickLc',
      bestColorClickLc: 'bestColorClickLc',
      dartUploadStrategy: 'dartUploadStrategy'
    },

    COLUMNS_TOTAL: ["baseFeeUsd",
      "baseFeeLc",
      "netPriceLc",
      "netPriceUsd",
      "upfrontChargeUsd",
      "totalRevenueUsd",
      "totalRevenueLc",
      "upfrontChargeLc"],

    COLUMNS_AVG: ["totalCPPUsd",
      "totalCPPLc",
      "monoCPPUsd",
      "monoCPPLc",
      "colorCPPUsd",
      "colorCPPLc",
      "monoClickUsd",
      "monoClickLc",
      "colorClickUsd",
      "colorClickLc",
      "bestColorClickUsd",
      "bestColorClickLc"],

    DART_PRICING_COLUMNS: [
      'netPriceUsd',
      'upfrontChargeUsd',
      'totalRevenueUsd',
      'totalCPPUsd',
      'monoCPPUsd',
      'colorCPPUsd',
      'baseFeeUsd',
      'monoClickUsd',
      'colorClickUsd',
      'bestColorClickUsd',
      'bestColorClickLc',
      'colorClickLc',
      'monoClickLc',
      'baseFeeLc',
      'colorCPPLc',
      'monoCPPLc',
      'totalCPPLc',
      'totalRevenueLc',
      'upfrontChargeLc',
      'netPriceLc'
    ],
    TASK_STATUS_OPERATION_ID: {
      EXPORT_CDF_REPORT: "Export_Cdf_Report",
      EXPORT_RFQ_REPORT: "Export_Rfq_Report",
      EXPORT_TCO_REPORT: "Export_Tco_Report",
      BOG_IMPORT: "Bog_Import"
    },
    TASK_STATUS: {
      PROCESSING: "processing",
      ERROR: "error",
      SUCCESS: "success"
    },
    ASSESSMENT_VIEW: [
      'level',
      'subEnvID',
      'selection',
      'mapId',
      'deviceModel',
      'pageSize',
      'deviceType',
      'deviceQty',
      'hwDisposition',
      'zone',
      'sku',
      'pvtInA3',
      'pvtIn',
      'pvtOutA3',
      'pvtOut',
      'ampv',
      'monoPages',
      'colorPages',
      'monthlyMonoPages',
      'monthlyColorPages',
      'monthlyColorProfPages',
      'monthlyPages',
      'department',
      'floor',
      'colorPercentage',
      'colorTotalPagesPercentage',
      'colorPro',
      'colorProTotalPagesPercentage',
      'a3Percentage',
      'colorA3Percentage',
      'monoA3Percentage',
      'age',
      'lifeTimeEngineLife',
      'eoslPast',
      'comments'
    ],

    MULTI_VENDOR_VIEW: [
      'mvSupply',
      'mvService',
      'mvSupplyCostStatus',
      'mvServiceCostStatus',
      'mvDartZone'
    ],

    TCO_VIEW: [
      'detailsToner',
      'totalCppToner',
      'monoCppToner',
      'colorCppToner',
      'totalTcoToner',
      'monoTcoToner',
      'colorTcoToner',
      'detailsRP',
      'laborRateRP',
      'laborTravelHoursRP',
      'laborRepairHoursRP',
      'totalCppRP',
      'monoCppRP',
      'colorCppRP',
      'totalTcoRP',
      'monoTcoRP',
      'colorTcoRP',
      'avgPwcService',
      'totalCppService',
      'monoCppService',
      'colorCppService',
      'totalTcoService',
      'monoTcoService',
      'colorTcoService',
      'avgPowPricePower',
      'workHoursPerDayPower',
      'workDaysPerWeekPower',
      'weeksPerYearPower',
      'isOnNightAndWeekends',
      'powerRateActive',
      'powerRateIdle',
      'powerRateSave',
      'totalCppPower',
      'monoCppPower',
      'colorCppPower',
      'totalTcoPower',
      'monoTcoPower',
      'colorTcoPower',
      'paperCostPerSheetPaper',
      'duplexPercentagePaper',
      'isDuplexCapablePaper',
      'totalCppPaper',
      'monoCppPaper',
      'colorCppPaper',
      'totalTcoPaper',
      'monoTcoPaper',
      'colorTcoPaper',
      'deviceCostHardware',
      'totalCppHardware',
      'monoCppHardware',
      'colorCppHardware',
      'totalTcoHardware',
      'monoTcoHardware',
      'colorTcoHardware',
      'totalEnergyInKWH',
      'co2PaperKG',
      'energyCO2KG',
      'energyCO2KGbyGeography',
      'noOfTreesCutDown',
      'literOfOilRequired',
      'totalAssetManagementCost',
      'supplyProcessTotalManHrsCostsPerYr',
      'totalDeviceSpaceCostperYr',
      'helpDeskCostperPrinterBasedonNumberofPrinters',
      'helpDeskCostperPrinterBasedonNumberofUsers',
      'totalPurchasingCostofDeviceperYr',
      'totalITMgmtCostofDevicePerYr',
      'totalMonoCPP',
      'totalColorCPP',
      'totalCPP'
    ],

    TCO_LEVEL_PRICING: [
      'totalCppToner',
      'monoCppToner',
      'colorCppToner',
      'totalTcoToner',
      'monoTcoToner',
      'colorTcoToner',
      'laborRateRP',
      'laborTravelHoursRP',
      'laborRepairHoursRP',
      'totalCppRP',
      'monoCppRP',
      'colorCppRP',
      'totalTcoRP',
      'monoTcoRP',
      'colorTcoRP',
      'avgPwcService',
      'totalCppService',
      'monoCppService',
      'colorCppService',
      'totalTcoService',
      'monoTcoService',
      'colorTcoService',
      'avgPowPricePower',
      'workHoursPerDayPower',
      'workDaysPerWeekPower',
      'weeksPerYearPower',
      'powerRateActive',
      'powerRateIdle',
      'powerRateSave',
      'totalCppPower',
      'monoCppPower',
      'colorCppPower',
      'totalTcoPower',
      'monoTcoPower',
      'colorTcoPower',
      'paperCostPerSheetPaper',
      'totalCppPaper',
      'monoCppPaper',
      'colorCppPaper',
      'totalTcoPaper',
      'monoTcoPaper',
      'colorTcoPaper',
      'deviceCostHardware',
      'totalCppHardware',
      'monoCppHardware',
      'colorCppHardware',
      'totalTcoHardware',
      'monoTcoHardware',
      'colorTcoHardware',
      'totalEnergyInKWH',
      'co2PaperKG',
      'energyCO2KG',
      'energyCO2KGbyGeography',
      'noOfTreesCutDown',
      'literOfOilRequired',
      'totalAssetManagementCost',
      'supplyProcessTotalManHrsCostsPerYr',
      'totalDeviceSpaceCostperYr',
      'helpDeskCostperPrinterBasedonNumberofPrinters',
      'helpDeskCostperPrinterBasedonNumberofUsers',
      'totalPurchasingCostofDeviceperYr',
      'totalITMgmtCostofDevicePerYr',
      'totalMonoCPP',
      'totalColorCPP',
      'totalCPP'
    ],

    TABULAR_PROPERTIES: {
      level: "",
      selection: false,
      subEnvID: "",
      mapId: "",
      deviceModel: "",
      pageSize: "",
      deviceType: "",
      deviceQty: "",
      hwDisposition: "",
      mvService: "",
      mvServiceCostStatus: "",
      mvSupply: "",
      mvSupplyCostStatus: "",
      mvDartZone: "",
      sku: "",
      productId: "",
      pvtInA3: "",
      pvtIn: "",
      pvtOutA3: "",
      pvtOut: "",
      ampv: "",
      monoPages: "",
      colorPages: "",
      monthlyMonoPages: "",
      monthlyColorPages: "",
      monthlyColorProfPages: "",
      monthlyPages: "",
      colorTotalPagesPercentage: "",
      a3Percentage: "",
      colorA3Percentage: "",
      monoA3Percentage: "",
      age: "",
      lifeTimeEngineLife: "",
      currentLifeTimeEngineLife: "",
      proposedLifeTimeEngineLife: "",
      eoslPast: "",
      currentEoslPast: "",
      proposedEoslPast: "",
      comments: "",
      assetId: "",
      accessoryId: "",
      $$treeLevel: "",
      levelId: "",
      isRealAsset: "",
      type: "",
      outcome: "",
      monoCPPUsd: "",
      totalRevenueUsd: "",
      totalCPPUsd: "",
      colorCPPUsd: "",
      baseFeeUsd: "",
      monoClickUsd: "",
      colorClickUsd: "",
      bestColorClickUsd: "",
      netPriceUsd: "",
      upfrontChargeUsd: "",
      monoCPPLc: "",
      totalRevenueLc: "",
      totalCPPLc: "",
      colorCPPLc: "",
      baseFeeLc: "",
      monoClickLc: "",
      colorClickLc: "",
      bestColorClickLc: "",
      netPriceLc: "",
      upfrontChargeLc: "",
      proposedMonthlyPageVolumeMono: "",
      proposedMonthlyPageVolumeColor: "",
      proposedAmpv: "",
      proposedColorTotalPagesPercentage: "",
      initHwDispositions: {},
      initService: [],
      initSupply: [],
      initMvDartZone: [],
      countryId: "",
      siteId: "",
      buildingId: "",
      floorId: "",
      department: "",
      floor: "",
      colorPro: "",
      colorProTotalPagesPercentage: "",
      proposedColorProMonthlyVolume: "",
      proposedColorProTotalPagesPercentage: "",
      totalCppToner: "",
      monoCppToner: "",
      colorCppToner: "",
      totalTcoToner: "",
      monoTcoToner: "",
      colorTcoToner: "",
      laborRateRP: "",
      laborTravelHoursRP: "",
      laborRepairHoursRP: "",
      totalCppRP: "",
      monoCppRP: "",
      colorCppRP: "",
      totalTcoRP: "",
      monoTcoRP: "",
      colorTcoRP: "",
      avgPwcService: "",
      totalCppService: "",
      monoCppService: "",
      colorCppService: "",
      totalTcoService: "",
      monoTcoService: "",
      colorTcoService: "",
      avgPowPricePower: "",
      workHoursPerDayPower: "",
      workDaysPerWeekPower: "",
      weeksPerYearPower: "",
      isOnNightAndWeekends: "",
      powerRateActive: "",
      powerRateIdle: "",
      powerRateSave: "",
      totalCppPower: "",
      monoCppPower: "",
      colorCppPower: "",
      totalTcoPower: "",
      monoTcoPower: "",
      colorTcoPower: "",
      paperCostPerSheetPaper: "",
      duplexPercentagePaper: "",
      isDuplexCapablePaper: "",
      totalCppPaper: "",
      monoCppPaper: "",
      colorCppPaper: "",
      totalTcoPaper: "",
      monoTcoPaper: "",
      colorTcoPaper: "",
      deviceCostHardware: "",
      totalCppHardware: "",
      monoCppHardware: "",
      colorCppHardware: "",
      totalTcoHardware: "",
      monoTcoHardware: "",
      colorTcoHardware: "",
      totalEnergyInKWH: "",
      co2PaperKG: "",
      energyCO2KG: "",
      energyCO2KGbyGeography: "",
      noOfTreesCutDown: "",
      literOfOilRequired: "",
      totalAssetManagementCost: "",
      supplyProcessTotalManHrsCostsPerYr: "",
      totalDeviceSpaceCostperYr: "",
      helpDeskCostperPrinterBasedonNumberofPrinters: "",
      helpDeskCostperPrinterBasedonNumberofUsers: "",
      totalPurchasingCostofDeviceperYr: "",
      totalITMgmtCostofDevicePerYr: "",
      totalMonoCPP: "",
      totalColorCPP: "",
      totalCPP: "",
      macAddress: "",
      hostName: "",
      subnetMask: "",
      gateway: "",
      dateIntroduced: "",
      dateManufactured: "",
      dateInstalled: "",
      serviceID: "",
      location: "",
      placement: "",
      connectionType: "",
      printQueue: "",
      faxNumber: "",
      iPAddress: "",
      serialNumber: "",
      dartUploadStrategy: "",
      isEligibleForTcoRemap: false,
      isAssetProxy: false,
      isMultiVendorAsset: false,
      assetAbbreviation: ""
    },
    CUSTOM_FILTERS: {
      EQUAL: 'Equal',
      LESS_THAN: 'Less than',
      GREATER_THAN: 'Greater than'
    },
    COLUMN_FILTERS: {
      a3Percentage: [
        'Equal',
        'Less than',
        'Greater than'
      ],
      colorA3Percentage: [
        'Equal',
        'Less than',
        'Greater than'
      ],
      monoA3Percentage: [
        'Equal',
        'Less than',
        'Greater than'
      ],
      age: [
        'Equal',
        'Less than',
        'Greater than'
      ],
      lifeTimeEngineLife: [
        'Equal',
        'Less than',
        'Greater than'
      ],
      eoslPast: [
        'Equal',
        'Less than',
        'Greater than'
      ]
    },
    TCO_CATEGORIES: [{
      id: "tonerColumns",
      label: "Toner",
      active: false
    }, {
      id: "rpColumns",
      label: "Replaceable Parts",
      active: false
    }, {
      id: "serviceColumns",
      label: "Service",
      active: false
    }, {
      id: "powerColumns",
      label: "Power",
      active: false
    }, {
      id: "paperColumns",
      label: "Paper",
      active: false
    }, {
      id: "hardwareColumns",
      label: "Hardware",
      active: false
    }, {
      id: "carbonColumns",
      label: "Carbon Foot Print",
      active: false
    }, {
      id: "indirectCosts",
      label: "Indirect Costs",
      active: false
    }
    ],

    TCO_REPORT: {
      STATE_PRICE_FIELDS: {
        CURRENT_STATE_PRICE: [
          'TCO'
        ],
        FUTURE_STATE_PRICE: [
          'DART',
          'TCO'
        ]
      }
    },
    defaultPricingSettings: {
      pricingSource: 'Manual',
      currency: 'USD'
    },

    //Basket constants
    HpMpsSalable: {
      name: "hpMpsSalable",
      stateColumns: {
        category: 'CATEGORY',
        model: 'model',
        sku: 'productNumber',
        baseFee: 'MONTHLY_BASE',
        monoCPP: 'MonoCPP',
        colorCPP: 'ColorCPP',
        colorProCPP: 'ColorProCPP',
        totalPages: 'TotalPages',
        monthlyCost: 'TotalMonthly'
      },
      columns: ['make', 'model', 'productNumber', 'deviceType', 'engineLife'],
      primaryKey: 'productNumber'
    },
    BasketOfGoods: {
      name: "basketOfGoods",
      columns: [
        "Device_x0020_Solution_x0020_ID",
        "Description",
        "Accessory_x0020_List",
        "HW_x0020_Purchase",
        "Break-Fix_x0020_SLA",
        "Maint_x0020_Kit_x0020_Option",
        "Printer_x0020_Type",
        "Base_x0020_Fee",
        "Mono_x0020_Click",
        "Color_x0020_Click",
        "Best_x0020_Color_x0020_Click"],
      stateColumns: {
        category: 'CATEGORY',
        model: 'model',
        sku: 'productNumber',
        baseFee: 'MONTHLY_BASE',
        monoCPP: 'MonoCPP',
        colorCPP: 'ColorCPP',
        colorProCPP: 'ColorProCPP',
        totalPages: 'TotalPages',
        monthlyCost: 'TotalMonthly'
      },
      primaryKey: 'Device_x0020_Solution_x0020_ID',
      productNumber: 'Product_x0020_Nbr',
      printerType: 'Printer_x0020_Type',
      description: 'Description',
      baseFee: 'Base_x0020_Fee',
      monoClick: 'Mono_x0020_Click',
      colorClick: 'Color_x0020_Click',
      bestColor: 'Best_x0020_Color_x0020_Click',
      totalCPP: 'totalCpp',
      monoPages: 'Mono_x0020_Pages',
      colorPages: 'Color_x0020_Pages',
      monthlyCost: 'monthlyCost',
      accessoryList: 'Accessory_x0020_List'
    },
    OutcomeSubTypes: {
      ManyToOne: "ManyToOne",
      OneForOne: "OneForOne"
    },
    DefaultTcoAdjustment: {
      tonerRate: 0,
      hardwareRate: 0,
      replaceablePartsRate: 0,
      serviceRate: 0
    },

    TCO_PROXY_GRID_COLUMNS: [
      {
        name: 'ProductManufacturer',
        width: "25%",
        displayName: 'Make',
        enableHiding: false,
        cellTooltip: true
      },
      {name: 'ProductNumber', width: "20%", displayName: 'SKU', enableHiding: false, cellTooltip: true},
      {name: 'ProductDescription', width: "55%", displayName: 'Model', enableHiding: false, cellTooltip: true}
    ],
    CONFIDENCE: {
      HIGH: 'Confidence 1-High',
      MEDIUM: 'Confidence 2-Medium',
      LOW: 'Confidence 3-Low'
    },
    MPS_SUPPORTED: {
      YES: 'yes',
      NO: 'no',
      UNKNOWN: 'unknown'
    },
    MV_TEMPLATE_TYPE: {
      SUPPLIES: "SUPPLIES",
      SERVICE: "SERVICE"
    },
    DART_ZONES: [
      'urbanMetroCampus',
      'suburbanDistCampus',
      'remoteDistributed'
    ],
    FEATURE: {
      MULTI_VENDOR: "multiVendor",
      DART_PRICING: "dartPricing"
    },
    DART_DEAL_UPLOAD_OPTIONS: {
      newDeal: 'newDeal',
      updateOverwrite: 'updateOverwrite',
      updateSaveAsNew: 'updateSaveAsNew',
      noUpload: 'noUpload'
    },
    NON_PRESCRIPTIVE_COLUMN: [
      'pvtInA3',
      'pvtOutA3',
      'pvtIn',
      'pvtOut',
      'zone'
    ],
    TABULAR_HIDDEN_COLUMN: [
      'pvtInA3',
      'pvtOutA3',
      'monoPages',
      'colorPages',
      'colorPro',
      'ampv'
    ],
    TABULAR_VIEW_TEMPLATE_SETTINGS: {
      "ALL_COLUMNS_TEMPLATE_NAME": "ALL",
      "DEFAULT_TEMPLATES": {
        "Manual": true,
        "DART": true,
        "TCO": true,
        "MultiVendor": true,
        "Prescriptive": true
      },
      "DEFAULT_TEMPLATE_VIEW": "assessment",
      "NON_DRAGGABLE_COLUMNS": {
        "level": true,
        "selection": true
      },
      "CUSTOM": "custom",
      "MAX_CUSTOM_TEMPLATES": 5
    },
    TABULAR_DEFAULT_VIEW: "assessment",
    //multiple drag and drop
    MULTI_DND: {
      IMAGE_PATH: "../../../../img/icons/draganddrop.png"
    },
    TABULAR_VIEWS: {
      MANUAL: "Manual",
      DART: "DART",
      TCO: "TCO",
      MULTIVENDOR: "MultiVendor",
      ALL: "ALL",
      PRESCRIPTIVE: "Prescriptive"
    }
  });
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';
  var app = angular.module('app');
  app.constant('ngPorticoSD.constants.PorticoSDEvent', {
    PRESCRIPTIVE_COLUMN_MAPPING: 'Prescriptive: Column Mapping',
    TABULAR_VIEW_LEVEL_SELECTED: 'Tabular View Level: selected',
    SHOW_REPOSITION_POPUP: 'Reposition Popup: show',
    REFRESH_TABULAR_VIEW_POOL: 'Tabular View Pool: refresh',
    REPOSITION_APPLIED: 'Reposition: applied',
    UPDATE_TABULAR_VIEW_HEIGHT: 'Tabular View height: update',
    TCO_EXPORT: 'TCO: Export',
    TABULAR_VIEW_DATA_LOAD_SUCCESS: 'Tabular View Data Load : Success',
    SHOW_TCO_ADJUSTMENT_POPUP: 'Tco Adjustment Popup: show',
    SHOW_TCO_PROXY_DEVICE_POPUP: 'Tco Proxy Device Popup: show',
    SHOW_TABULAR_TEMPLATE_VIEW_SETTINGS_POPUP: 'Tabular View Template Setting: show',
    UPDATE_TABULAR_VIEW: 'Update Tabular View From User Settings: show'
  });
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';
  var app = angular.module('app');
  app.constant('ngPorticoSD.constants.PorticoSDPopupName', {

    APPLY_REPOSITION_DISPOSITION: 'applyRepositionDisposition',
    CUSTOM_FILTERS: 'customFilters',
    IMPORT_BASKET_OF_GOOD: 'importBasketOfGood',
    MV_TEMPLATE_COST: 'mvTemplateCost',
    MV_COST: 'mvCost',
    PRESCRIPTIVE_IMPORT: 'prescriptiveImport',
    TCO_PRICING: 'tcoPricing',
    TONER_DETAILS: 'tonerDetails',
    REPLACEABLE_PARTS_DETAILS: 'replaceablePartsDetails',
    TCO_REPORT: 'tcoReport',
    TCO_ADJUSTMENT: 'tcoAdjustment',
    TCO_PROXY_DEVICE: 'tcoProxyDevice',
    PRESCRIPTIVE_COLUMN_MAPPING: 'prescriptiveColumnMapping',
    TABULAR_TEMPLATE_VIEW_SETTING_POPUP: 'tabularTemplateViewSettings',
    BYOBOG:'byobog',
    FAV_BOG_POPUP: "Favourite BOG Popup"
  });
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /**
   * ngPorticoSDTemplatePath. It has locations of the views(html files) which are used
   * in PorticoSD. These path will mainly be used by configuration, directives
   * and views. ngPorticoSDTemplatePath should not be polluted or misunderstood with NavigationPath.
   * NavigationPath is mainly required for navigation. They don't care about the location
   * of view.
   */
  var app = angular.module('app');
  app.constant('PorticoSDTemplatePaths', {

    APPLY_REPOSITION_DISPOSITION_POPUP: 'vendor/hp-porticosd/ngPorticoSD/views/popups/apply-reposition-disposition.html',
    IMPORT_BASKET_OF_GOOD: 'vendor/hp-porticosd/ngPorticoSD/views/popups/import-basket-of-goods.html',
    CUSTOMIZED_BOG_POPUP: 'vendor/hp-porticosd/ngPorticoSD/views/popups/customizedBOG.html',
    CUSTOM_FILTERS: 'vendor/hp-porticosd/ngPorticoSD/views/popups/customFilters.html',
    TABULAR_VIEW: 'vendor/hp-porticosd/ngPorticoSD/views/PorticoSDView.html',
    SWITCH_BUTTON: 'vendor/hp-porticosd/ngPorticoSD/views/templates/switchView.html',
    EXPORT_CDF_REPORT: 'vendor/hp-porticosd/ngPorticoSD/views/templates/exportCdfReport.html',
    EXPORT_RFQ_REPORT: 'vendor/hp-porticosd/ngPorticoSD/views/templates/exportRfqReport.html',
    EXPORT_TCO_REPORT: 'vendor/hp-porticosd/ngPorticoSD/views/templates/exportTcoReport.html',
    MV_COST_POPUP: 'vendor/hp-porticosd/ngPorticoSD/views/popups/mvCost.html',
    MV_TEMPLATE_COST_POPUP: 'vendor/hp-porticosd/ngPorticoSD/views/popups/mvTemplateCost.html',
    PRESCRIPTIVE_IMPORT_POPUP: 'vendor/hp-porticosd/ngPorticoSD/views/popups/importPrescriptive.html',
    PROXY_DEVICE_BUTTON: 'vendor/hp-porticosd/ngPorticoSD/views/templates/proxyDeviceButton.html',
    TCO_REPORT: 'vendor/hp-porticosd/ngPorticoSD/views/popups/tcoReport.html',
    TABULAR_CELL_PVT_IN: 'vendor/hp-porticosd/ngPorticoSD/views/templates/pvtInCell.html',
    TABULAR_CELL_PVT_OUT: 'vendor/hp-porticosd/ngPorticoSD/views/templates/pvtOutCell.html',
    TABULAR_CELL_DISPOSITION: 'vendor/hp-porticosd/ngPorticoSD/views/templates/hwDispositionCell.html',
    TABULAR_CELL_TEMPLATE: 'vendor/hp-porticosd/ngPorticoSD/views/templates/cellTemplate.html',
    TABULAR_HEADER_CELL_TEMPLATE: 'vendor/hp-porticosd/ngPorticoSD/views/templates/headerTemplate.html',
    TABULAR_ROW_TEMPLATE: '/vendor/hp-porticosd/ngPorticoSD/views/templates/rowTemplate.html',
    TABULAR_CELL_MONO_PAGE_COUNT: '/vendor/hp-porticosd/ngPorticoSD/views/templates/monoPageCountCell.html',
    TABULAR_CELL_COLOR_PAGE_COUNT: '/vendor/hp-porticosd/ngPorticoSD/views/templates/colorPageCountCell.html',
    TABULAR_CELL_COLOR_TOTAL_PAGES_PERCENTAGE_COUNT: '/vendor/hp-porticosd/ngPorticoSD/views/templates/colorTotalPagesPercentageCountCell.html',
    TABULAR_CELL_TOTAL_PAGES_COUNT: '/vendor/hp-porticosd/ngPorticoSD/views/templates/totalPagesCountCell.html',
    TABULAR_CELL_COLOR_PRO_PAGE_COUNT: '/vendor/hp-porticosd/ngPorticoSD/views/templates/colorProPageCountCell.html',
    TABULAR_CELL_COLOR_PRO_TOTAL_PAGES_PERCENTAGE_COUNT: '/vendor/hp-porticosd/ngPorticoSD/views/templates/colorProTotalPagesPercentageCountCell.html',
    TCO_PRICING_POPUP: 'vendor/hp-porticosd/ngPorticoSD/views/popups/tcoPricing.html',
    TONER_DETAIL_TEMPLATE: 'vendor/hp-porticosd/ngPorticoSD/views/templates/tonerDetailsTemplate.html',
    TONER_DETAIL_POPUP: 'vendor/hp-porticosd/ngPorticoSD/views/popups/tonerDetails.html',
    REPLACEABLE_PARTS_DETAILS_TEMPLATE: 'vendor/hp-porticosd/ngPorticoSD/views/templates/replaceablePartsDetailsTemplate.html',
    REPLACEABLE_PARTS_DETAILS_POPUP: 'vendor/hp-porticosd/ngPorticoSD/views/popups/replaceablePartsDetails.html',
    TCO_ADJUSTMENT_POPUP: 'vendor/hp-porticosd/ngPorticoSD/views/popups/tcoAdjustment.html',
    TCO_PROXY_DEVICE_POPUP: 'vendor/hp-porticosd/ngPorticoSD/views/popups/tcoProxyDevice.html',
    PRESCRIPTIVE_COLUMN_MAPPING_POPUP: 'vendor/hp-porticosd/ngPorticoSD/views/popups/prescriptiveColMapping.html',
    TABULAR_TEMPLATE_VIEW_SETTING_POPUP: 'vendor/hp-porticosd/ngPorticoSD/views/popups/tabularTemplateSettings.html'
  });
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';


  /**
   * Controller to handle BOG functionality
   * @param $scope
   * @param $rootScope
   * @param PorticoSDConfiguration
   * @param $routeParams
   * @param $log
   * @param BasketOfGoodsService
   * @param NotifierService
   * @param PorticoSDPopupName
   * @param Utils
   * @param TaskStatusService
   * @constructor
   */
  function BogImportController($scope, $rootScope, $routeParams, $log, BasketOfGoodsService, NotifierService,
                               PorticoSDPopupName, Utils, PorticoSDConfiguration, TaskStatusService, Configuration) {

    /**
     * import basket of goods action.
     */
    $scope.importBasketOfGood = function () {
      if (!Utils.isValid($scope.transitionToolProjectId)) {
        NotifierService.error('ERROR_IMPORTING_BASKET_OF_GOOD');
        return;
      }

      function onImportSuccess(response) {
        if (response.status === Configuration.HTTP_STATUS_NO_CONTENT) {
          $log.info('Assets are not available in this TT project ID.');
          NotifierService.error('NO_ASSETS_AVAILABLE_TO_IMPORT_FOR_THIS_TRANSITION_TOOL_PROJECT_ID');
          TaskStatusService.updateStatus(PorticoSDConfiguration.TASK_STATUS_OPERATION_ID.BOG_IMPORT,
            'NO_ASSETS_AVAILABLE_TO_IMPORT_FOR_THIS_TRANSITION_TOOL_PROJECT_ID',
            PorticoSDConfiguration.TASK_STATUS.ERROR, 'NO_ASSETS_AVAILABLE_TO_IMPORT_FOR_THIS_TRANSITION_TOOL_PROJECT_ID');
        } else {
          NotifierService.success('BASKET_OF_GOOD_IMPORTED_SUCCESSFULY');
          TaskStatusService.updateStatus(PorticoSDConfiguration.TASK_STATUS_OPERATION_ID.BOG_IMPORT,
            'BASKET_OF_GOOD_IMPORTED_SUCCESSFULY', PorticoSDConfiguration.TASK_STATUS.SUCCESS,
            'BASKET_OF_GOOD_IMPORTED_SUCCESSFULY');
        }
        $rootScope.loader.loading = false;
        $scope.togglePopupFlag(PorticoSDPopupName.IMPORT_BASKET_OF_GOOD);
      }

      function onImportError(error) {
        $rootScope.loader.loading = false;
        $log.error('error importing bog', error);
        NotifierService.error('ERROR_IMPORTING_BASKET_OF_GOOD');
        TaskStatusService.updateStatus(PorticoSDConfiguration.TASK_STATUS_OPERATION_ID.BOG_IMPORT,
          'ERROR_IMPORTING_BASKET_OF_GOOD', PorticoSDConfiguration.TASK_STATUS.ERROR, 'ERROR_IMPORTING_BASKET_OF_GOOD');
        $scope.togglePopupFlag(PorticoSDPopupName.IMPORT_BASKET_OF_GOOD);
      }
      $rootScope.loader.loading = true;
      TaskStatusService.setStatus(PorticoSDConfiguration.TASK_STATUS_OPERATION_ID.BOG_IMPORT, 'BASKET_OF_GOODS_IMPORTING',
        PorticoSDConfiguration.TASK_STATUS.PROCESSING, 'BASKET_OF_GOODS_IMPORTING');
      BasketOfGoodsService.importBasketOfGoods($routeParams.id, $scope.transitionToolProjectId).then(onImportSuccess, onImportError)
    };

    $scope.transitionToolProjectId = '';
  }

  var app = angular.module('app'),
    requires = [
      '$scope',
      '$rootScope',
      '$routeParams',
      '$log',
      'BasketOfGoodsService',
      'ngCartosCore.services.NotifierService',
      'ngPorticoSD.constants.PorticoSDPopupName',
      'ngCartosUtils.services.UtilityService',
      'ngPorticoSD.constants.PorticoSDConfiguration',
      'ngCartosCore.services.TaskStatusService',
      'Configuration',
      BogImportController
    ];
  app.controller('BogImportController', requires);
}());
/*global angular*/
/*jslint nomen:true*/
(function () {
  'use strict';

  function CustomizedBasketController($scope, $rootScope, $log, PorticoSDPopupName, PorticoSDEvent, ViewEvent,
                                      NotifierService, AccessoriesDataMapService, CustomizedBasketService) {

    /**
     * Event will be triggered when favourite button is clicked, it will toggle a popup if any accessory available for the printer
     * @param $event
     * @param row
     * @param accessoriesFromBog
     */
    function initFavBogPopup($event, row, accessoriesFromBog) {
      if (accessoriesFromBog) {
        let asset = {"productNumber": row.sku},
          assetAccessories = AccessoriesDataMapService.getAccessoriesByAsset(asset),
          commonAccessories = CustomizedBasketService.getCommonAccessories(assetAccessories, accessoriesFromBog, row.accessoryList);

        if (row.customAccessories && row.customAccessories.length) {
          CustomizedBasketService.getLastCheckedAccessories(row.customAccessories, commonAccessories);
        }
        openFavBogPopup(commonAccessories, row);
      } else {
        deleteFavouriteBogAsset(row._id);
      }
    }

    function openFavBogPopup(commonAccessories, row) {
      if (commonAccessories && commonAccessories.length) {
        $scope.userCommonAccessories = commonAccessories;
        $scope.model = row.description;
        $scope.rowSelected = row;
        $scope.togglePopupFlag(PorticoSDPopupName.BYOBOG);
      } else {
        NotifierService.warning('NO_ACCESSORIES_AVAILABLE');
      }
    }

    function deleteFavouriteBogAsset(favouriteBogAssetId) {
      function onError() {
        NotifierService.error('ERROR_DELETING_FAVOURITE_ASSET');
      }

      CustomizedBasketService.deleteFavouriteBogAsset(favouriteBogAssetId).then(getFavBogList, onError);
    }

    $scope.addAssetInFavouriteList = function () {
      $rootScope.loader.loading = true;
      $scope.selectedAccessories = $scope.userCommonAccessories.filter((row) => {
        return row.checked;
      });
      if ($scope.selectedAccessories && $scope.selectedAccessories.length) {
        $scope.togglePopupFlag(PorticoSDPopupName.BYOBOG);
        if ($scope.rowSelected && $scope.rowSelected.customAccessories) {
          updateFavouriteBogAsset();
        } else {
          addAssetInFavouriteBogList();
        }
      } else {
        $rootScope.loader.loading = false;
        NotifierService.warning('NO_ACCESSORIES_SELECTED');
      }
    };

    function updateFavouriteBogAsset() {
      let request = {
        "accessoriesSelected": $scope.selectedAccessories,
        "favModelName": $scope.model,
        "row": $scope.rowSelected
      };

      function onError() {
        NotifierService.error('ERROR_UPDATING_ASSET_FAVORITE_BASKET');
        $rootScope.loader.loading = false;
        $scope.resetCustomizeAccessories();
      }

      CustomizedBasketService.updateFavouriteBogAsset(request).then(getFavBogList, onError);
    }

    function addAssetInFavouriteBogList() {
      let request = {
        "accessoriesSelected": $scope.selectedAccessories,
        "favModelName": $scope.model,
        "row": $scope.rowSelected
      };

      function onError() {
        NotifierService.error('ERROR_ADDING_ASSET_FAVORITE_BASKET');
        $rootScope.loader.loading = false;
        $scope.resetCustomizeAccessories();
      }

      CustomizedBasketService.addAssetInFavouriteList(request).then(getFavBogList, onError);
    }

    function getFavBogList(response) {
      let apiCallMethod = response.config.method;

      function onSuccess(response) {
        if (response && response.data && response.data.data) {
          $rootScope.$broadcast(ViewEvent.USER_FAVOURITE_BOG, response.data.data);
          $rootScope.loader.loading = false;
          CustomizedBasketService.notifySuccess(apiCallMethod);
          $scope.resetCustomizeAccessories();
        } else {
          onError();
        }
      }

      function onError(error) {
        if (error && error.data && error.data.error) {
          $rootScope.$broadcast(ViewEvent.USER_FAVOURITE_BOG, error.data.error);
          CustomizedBasketService.notifySuccess(apiCallMethod);
        } else {
          NotifierService.error('ERROR_GETTING_MY_FAVORITE_BOG');
        }
        $rootScope.loader.loading = false;
        $scope.resetCustomizeAccessories();
      }

      CustomizedBasketService.getFavouriteAssets().then(onSuccess, onError);
    }

    $scope.resetCustomizeAccessories = function () {
      if ($scope.selectedAccessories) {
        $scope.selectedAccessories = $scope.userCommonAccessories.filter((row) => {
          row.checked = false;
        });
      }
    };

    $scope.$on(ViewEvent.FAV_BOG_POPUP, initFavBogPopup);

    /**
     * Event will be triggered when popup is cancelled or clicked outside the popup
     * @param $event
     * @param title of the popup
     */
    $scope.$on(ViewEvent.POPUP_HIDE_MODAL, function ($event, title) {
      if (title === PorticoSDPopupName.BYOBOG) {
        $scope.resetCustomizeAccessories();
      }
    });

    $scope.selectedAccessories = {};
  }

  var app = angular.module('app'),
      requires = [
        '$scope',
        '$rootScope',
        '$log',
        'ngPorticoSD.constants.PorticoSDPopupName',
        'ngPorticoSD.constants.PorticoSDEvent',
        'ViewEvent',
        'ngCartosCore.services.NotifierService',
        'ngCartosCore.services.AccessoriesDataMapService',
        'ngPorticoSD.services.CustomizedBasketService',
        CustomizedBasketController
      ];
  app.controller('CustomizedBasketController', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /**
   * Controller to handle Cdf Report Export functionality
   * @param $scope
   * @param $rootScope
   * @param $routeParams
   * @param $log
   * @param PorticoSDTemplatePaths
   * @param LevelService
   * @param PorticoSDExportHelperService
   * @param Notify
   * @param FileExportService
   * @param PorticoSDConfiguration
   * @param TaskStatusService
   * @constructor
   */
  function ExportCdfReportController($scope, $rootScope,  $routeParams, $log, PorticoSDTemplatePaths, LevelService,
                                     PorticoSDExportHelperService, Notify, FileExportService,PorticoSDConfiguration,
                                     TaskStatusService) {

    $scope.TemplatePath = PorticoSDTemplatePaths;

    /**
     * To Export CDF Report for the Project
     * @param fileName
     * @param type
     */
    $scope.exportCDFReport = function (fileName, type) {
      $rootScope.loader.loading = true;
      var project = LevelService.getLevel($routeParams.id).name;

      // On sucessfully receiving the data
      function onSucess(data) {
        try {
          FileExportService.downloadXlsxFile(data, PorticoSDExportHelperService.getExportFileNameWithTimestamp(project, fileName, type));
          $rootScope.loader.loading = false;
          TaskStatusService.updateStatus(PorticoSDConfiguration.TASK_STATUS_OPERATION_ID.EXPORT_CDF_REPORT,
              'CDF_REPORT_EXPORTED_SUCCESSFULLY', PorticoSDConfiguration.TASK_STATUS.SUCCESS, 'CDF_REPORT_EXPORTED_SUCCESSFULLY');
          Notify.success('CDF_REPORT_EXPORTED_SUCCESSFULLY');
        }
        catch (ex) {
          onError(ex);
        }
      }

      //On Error while processing the request
      function onError(error) {
        $rootScope.loader.loading = false;
        $log.error('Error while exporting CDF Report - ', error);
        TaskStatusService.updateStatus(PorticoSDConfiguration.TASK_STATUS_OPERATION_ID.EXPORT_CDF_REPORT,
            'ERROR_EXPORTING_CDF_REPORT', PorticoSDConfiguration.TASK_STATUS.ERROR, 'ERROR_EXPORTING_CDF_REPORT');
        Notify.error('ERROR_EXPORTING_CDF_REPORT');
      }
      TaskStatusService.setStatus(PorticoSDConfiguration.TASK_STATUS_OPERATION_ID.EXPORT_CDF_REPORT,
          'EXPORT_CDF_REPORT', PorticoSDConfiguration.TASK_STATUS.PROCESSING, 'EXPORT_CDF_REPORT');
      PorticoSDExportHelperService.exportCdfReport().then(onSucess, onError);
    };
  }

  var app = angular.module('app'),
      requires = [
        '$scope',
        '$rootScope',
        '$routeParams',
        '$log',
        'PorticoSDTemplatePaths',
        'ngCartosCore.services.LevelService',
        'ngPorticoSD.services.PorticoSDExportHelperService',
        'ngCartosCore.services.NotifierService',
        'ngCartosExport.services.FileExportService',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'ngCartosCore.services.TaskStatusService',
        ExportCdfReportController
      ];
  app.controller('ExportCdfReportController', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /**
   * Controller to handle RFQ Report Export functionality
   * @param $scope
   * @param $rootScope
   * @param $routeParams
   * @param $log
   * @param LevelService
   * @param PorticoSDExportHelperService
   * @param Notify
   * @param FileExportService
   * @param PorticoSDConfiguration
   * @param TaskStatusService
   * @constructor
   */
  function ExportRfqReportController($scope, $rootScope,  $routeParams, $log, LevelService, PorticoSDExportHelperService,
                                     Notify, FileExportService, PorticoSDConfiguration, TaskStatusService) {

    /**
     * To Export RFQ Report for the Project
     * @param fileName
     * @param type
     */
    $scope.exportRFQReport = function (fileName, type) {
      var levelProject = LevelService.getLevel($routeParams.id),
          project = levelProject ? levelProject.name : PorticoSDConfiguration.PROJECT;
      $rootScope.loader.loading = true;

      // On successfully receiving the data
      function onSuccess(data) {
        try {
          if (!angular.isString(data)) {
            if('error_msg' in data) {
              $rootScope.loader.loading = false;
              TaskStatusService.updateStatus(PorticoSDConfiguration.TASK_STATUS_OPERATION_ID.EXPORT_RFQ_REPORT,
                data.error_msg, PorticoSDConfiguration.TASK_STATUS.ERROR, data.error_msg);
              Notify.error(data.error_msg);
            }
            else {
              $rootScope.loader.loading = false;
              $log.error('Error while exporting RFQ Report - ', error);
              TaskStatusService.updateStatus(PorticoSDConfiguration.TASK_STATUS_OPERATION_ID.EXPORT_RFQ_REPORT,
                'ERROR_EXPORTING_RFQ_REPORT', PorticoSDConfiguration.TASK_STATUS.ERROR, 'ERROR_EXPORTING_RFQ_REPORT');
              Notify.error('ERROR_EXPORTING_RFQ_REPORT');
            }
          }
          else {
            FileExportService.downloadXlsxFile(data, PorticoSDExportHelperService.getExportFileNameWithTimestamp(project, fileName, type));
            $rootScope.loader.loading = false;
            TaskStatusService.updateStatus(PorticoSDConfiguration.TASK_STATUS_OPERATION_ID.EXPORT_RFQ_REPORT,
              'RFQ_REPORT_EXPORTED_SUCCESSFULLY', PorticoSDConfiguration.TASK_STATUS.SUCCESS, 'RFQ_REPORT_EXPORTED_SUCCESSFULLY');
            Notify.success('RFQ_REPORT_EXPORTED_SUCCESSFULLY');
          }
        }
        catch (ex) {
          onError(ex);
        }
      }

      //On Error while processing the request
      function onError(error) {
        $rootScope.loader.loading = false;
        $log.error('Error while exporting RFQ Report - ', error);
        TaskStatusService.updateStatus(PorticoSDConfiguration.TASK_STATUS_OPERATION_ID.EXPORT_RFQ_REPORT,
            'ERROR_EXPORTING_RFQ_REPORT', PorticoSDConfiguration.TASK_STATUS.ERROR, 'ERROR_EXPORTING_RFQ_REPORT');
        Notify.error('ERROR_EXPORTING_RFQ_REPORT');
      }

      TaskStatusService.setStatus(PorticoSDConfiguration.TASK_STATUS_OPERATION_ID.EXPORT_RFQ_REPORT,
          'EXPORT_RFQ_REPORT', PorticoSDConfiguration.TASK_STATUS.PROCESSING, 'EXPORT_RFQ_REPORT');
      PorticoSDExportHelperService.exportRfqReport().then(onSuccess, onError);
    };
  }

  var app = angular.module('app'),
      requires = [
        '$scope',
        '$rootScope',
        '$routeParams',
        '$log',
        'ngCartosCore.services.LevelService',
        'ngPorticoSD.services.PorticoSDExportHelperService',
        'ngCartosCore.services.NotifierService',
        'ngCartosExport.services.FileExportService',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'ngCartosCore.services.TaskStatusService',
        ExportRfqReportController
      ];
  app.controller('ExportRfqReportController', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /**
   * Controller to handle TCO Report Export functionality
   * @param $scope
   * @param $rootScope
   * @param $routeParams
   * @param $log
   * @param ViewEvent
   * @param PorticoSDEvent
   * @param LevelService
   * @param PorticoSDExportHelperService
   * @param Notify
   * @param FileExportService
   * @param PorticoSDPopupName
   * @param PorticoSDConfiguration
   * @param ProjectSettingService
   * @param Utils
   * @param PorticoSDSharedService
   * @param TaskStatusService
   * @constructor
   */
  function ExportTcoReportController($scope, $rootScope, $routeParams, $log, ViewEvent, PorticoSDEvent, LevelService,
                                     PorticoSDExportHelperService, Notify, FileExportService, PorticoSDPopupName, PorticoSDConfiguration,
                                     ProjectSettingService, Utils, PorticoSDSharedService, TaskStatusService) {

    /**
     * Function to count total number of sites present in country that user chose in TCO popup
     * @return {number}
     */
    function countTotalNumberOfSites() {
      var sitesCount = 0, countryId,
          subLevels = LevelService.getSubLevels($routeParams.id);
      try {
        if (Utils.isNull(subLevels) || Utils.isEmpty(subLevels)) {
          return sitesCount;
        }
        for (var index = 0; index < subLevels.length; index++) {
          if (subLevels[index].name === $scope.tcoReportingFields.country) {
            countryId = subLevels[index]._id;
            break;
          }
        }
        for (index = 0; index < subLevels.length; index++) {
          if (countryId && subLevels[index].type === PorticoSDConfiguration.LEVEL_SITE && subLevels[index].parentId === countryId) {
            sitesCount++;
          }
        }
        return sitesCount;
      }
      catch (ex) {
        $log.info('Error while counting the total no. of sites in a country: ', ex);
      }
    }

    /**
     * Function to Export TCO Report
     * @param fileName
     * @param type
     */
    function exportTcoReporting(fileName, type) {
      var levelProject = LevelService.getLevel($routeParams.id),
          projectName = levelProject ? levelProject.name : PorticoSDConfiguration.PROJECT;

      // On successfully receiving the data
      function onSuccess(data) {
        try {
          if (projectName) {
            FileExportService.downloadXlsxFile(data, PorticoSDExportHelperService.getExportFileNameWithTimestamp(projectName, fileName, type));
            Notify.success('TCO_REPORT_EXPORTED_SUCCESSFULLY');
            TaskStatusService.updateStatus(PorticoSDConfiguration.TASK_STATUS_OPERATION_ID.EXPORT_TCO_REPORT,
                'TCO_REPORT_EXPORTED_SUCCESSFULLY', PorticoSDConfiguration.TASK_STATUS.SUCCESS, 'TCO_REPORT_EXPORTED_SUCCESSFULLY');
          }
          $rootScope.loader.loading = false;
        }
        catch (ex) {
          onError(ex);
        }
      }

      //On Error while processing the request
      function onError(error) {
        $rootScope.loader.loading = false;
        $log.error('Error while exporting TCO Report - ', error);
        TaskStatusService.updateStatus(PorticoSDConfiguration.TASK_STATUS_OPERATION_ID.EXPORT_TCO_REPORT,
            'ERROR_EXPORTING_TCO_REPORT', PorticoSDConfiguration.TASK_STATUS.ERROR, 'ERROR_EXPORTING_TCO_REPORT');
        Notify.error('ERROR_EXPORTING_TCO_REPORT');
      }

      TaskStatusService.setStatus(PorticoSDConfiguration.TASK_STATUS_OPERATION_ID.EXPORT_TCO_REPORT,
          'EXPORT_TCO_REPORT', PorticoSDConfiguration.TASK_STATUS.PROCESSING, 'EXPORT_TCO_REPORT');

      // Export TCO Report
      PorticoSDExportHelperService.exportTcoReport().then(onSuccess, onError);
    }

    /**
     * Function to Validate TCO fields
     */
    function tcoFieldsValidation() {
      var error = false;
      if (!$scope.tcoReportingFields.currentStatePrice) {
        Notify.error('PLEASE_SELECT_CURRENT_STATE_PRICE');
        error = true;
      }
      else if (!$scope.tcoReportingFields.futureStatePrice) {
        Notify.error('PLEASE_SELECT_FUTURE_STATE_PRICE');
        error = true;
      }
      else if (!$scope.tcoReportingFields.country) {
        Notify.error('PLEASE_SELECT_COUNTRY');
        error = true;
      }
      return error;
    }

    /**
     * Function to Export TCO Report for the Project
     * First it stores the data into database and then it export the TCO file
     * @param fileName
     * @param type
     */
    $scope.exportTCOFile = function (fileName, type) {
      if (tcoFieldsValidation()) {
        return;
      }
      $scope.tcoReportingFields.totalNumberOfSites = countTotalNumberOfSites();
      var request = {
        projectId: $routeParams.id,
        tcoReportSettings: $scope.tcoReportingFields
      };

      //success callback on updating project settings
      function onSuccess() {
        exportTcoReporting(fileName, type);
      }

      //error callback on updating project settings
      function onError(error) {
        $rootScope.loader.loading = false;
        $log.error('Error while storing the TCO fields in database - ', error);
        Notify.error('ERROR_STORING_TCO_FIELDS_IN_DB');
      }

      $scope.togglePopupFlag(PorticoSDPopupName.TCO_REPORT);
      $rootScope.loader.loading = true;
      //update project setting
      ProjectSettingService.updateProjectSetting(request).then(onSuccess, onError);
    };

    /**
     * Function to notify user about the TCO pricing is fetched or not based on the state
     */
    function notifyTcoPricingStatus() {
      var isTcoPricingFetched = PorticoSDSharedService.getTcoPricingFetched();
      if (isTcoPricingFetched && !isTcoPricingFetched.current && !isTcoPricingFetched.future) {
        Notify.warning('TCO_PRICE_NOT_FETCHED_FOR_CURRENT_FUTURE_STATE');
      }
      else if (isTcoPricingFetched && !isTcoPricingFetched.current && isTcoPricingFetched.future) {
        Notify.warning('TCO_PRICE_NOT_FETCHED_FOR_CURRENT_STATE');
      }
      else if (isTcoPricingFetched && isTcoPricingFetched.current && !isTcoPricingFetched.future) {
        Notify.warning('TCO_PRICE_NOT_FETCHED_FOR_FUTURE_STATE');
      }
    }

    /**
     * Function to initialize all scope variables
     * @return {null}
     */
    function initializeTcoReportFields() {
      $scope.currentStatePriceFields = PorticoSDConfiguration.TCO_REPORT.STATE_PRICE_FIELDS.CURRENT_STATE_PRICE;
      $scope.futureStatePriceFields = PorticoSDConfiguration.TCO_REPORT.STATE_PRICE_FIELDS.FUTURE_STATE_PRICE;
    }

    /**
     * Function to get all country information of project
     * @return {*}
     */
    function getAllCountryInfo() {
      var countries = [],
          subLevels = LevelService.getSubLevels($routeParams.id);
      try {
        if (Utils.isNull(subLevels) || Utils.isEmpty(subLevels)) {
          return null;
        }
        for (var index = 0; index < subLevels.length; index++) {
          if (subLevels[index].type === PorticoSDConfiguration.LEVEL_COUNTRY) {
            countries.push(subLevels[index].name);
          }
        }
        return countries;
      }
      catch (ex) {
        $log.info('Error while getting the country details: ', ex);
      }
    }

    /**
     * Function to get all field values of TCO report from DB in TCO pop-up
     */
    function initializeTcoReportSettings() {
      var projectSetting = ProjectSettingService.getProjectSettings($routeParams.id) || {};
      try {
        notifyTcoPricingStatus();
        if (projectSetting.tcoReportSettings) {
          $scope.tcoReportingFields = angular.copy(projectSetting.tcoReportSettings);
        }
        else {
          $scope.tcoReportingFields = {};
        }
        initializeTcoReportFields();
        $scope.countries = getAllCountryInfo();
        $scope.togglePopupFlag(PorticoSDPopupName.TCO_REPORT);
      }
      catch (ex) {
        $log.info('Error while initializing TCO Report settings: ', ex);
      }
    }

    /**
     * Function to reset all Tco Adjustment fields to it's default value.
     */
    $scope.resetFields = function () {
      $scope.tcoReportingFields = {};
    };

    $scope.$on(PorticoSDEvent.TCO_EXPORT, initializeTcoReportSettings);
    $scope.$on(ViewEvent.POPUP_HIDE_MODAL, $scope.resetFields);
  }

  var app = angular.module('app'),
      requires = [
        '$scope',
        '$rootScope',
        '$routeParams',
        '$log',
        'ViewEvent',
        'ngPorticoSD.constants.PorticoSDEvent',
        'ngCartosCore.services.LevelService',
        'ngPorticoSD.services.PorticoSDExportHelperService',
        'ngCartosCore.services.NotifierService',
        'ngCartosExport.services.FileExportService',
        'ngPorticoSD.constants.PorticoSDPopupName',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'ngCartosCore.services.ProjectSettingService',
        'ngCartosUtils.services.UtilityService',
        'PorticoSDSharedService',
        'ngCartosCore.services.TaskStatusService',
        ExportTcoReportController
      ];
  app.controller('ExportTcoReportController', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  //Controller to handle MV cost functionality
  function MvCostController($log, $scope, $rootScope, ViewEvent, SharedService, Configuration,
                            PorticoSDPopupName, MultiVendorService, NotifierService, PorticoSDConfiguration, PorticoSDService) {

    /**
     * Function to fetch MV cost when fetch cost button will click
     * @param latest
     */
    $scope.fetchMvCost = function (latest) {
      function onSuccess() {
        NotifierService.success('FETCHED_MV_COST_SUCCESS');
        PorticoSDService.updateMvCostStatusInPoolManager();
        $rootScope.loader.loading = false;
      }
      function onError() {
        $rootScope.loader.loading = false;
        NotifierService.error('ERROR_FETCHING_MV_COST');
      }
      $scope.togglePopupFlag(PorticoSDPopupName.MV_COST);
      $rootScope.loader.loading = true;
      MultiVendorService.fetchMvCost(latest).then(onSuccess, onError);
    };

      /**
       * On right pane open
       * @param $event
       * @param rightPane
       */
    function onRightPaneOpen($event, rightPane) {
      if(rightPane === Configuration.RIGHT_PANE.MV_COST) {
          SharedService.getSplitter().rightPane.collapse();
          $scope.togglePopupFlag(PorticoSDPopupName.MV_COST);
      }
    }

      $scope.$on(ViewEvent.PANE_OPEN, onRightPaneOpen);
  }

  var app = angular.module('app'),
      requires = [
        '$log',
        '$scope',
        '$rootScope',
        'ViewEvent',
        'SharedService',
        'Configuration',
        'ngPorticoSD.constants.PorticoSDPopupName',
        'ngPorticoSD.services.MultiVendorService',
        'ngCartosCore.services.NotifierService',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'ngPorticoSD.services.PorticoSDService',
        MvCostController
      ];
  app.controller('MvCostController', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /**
   * Controller for handling Portico Initialisation .
   */
  function PorticoInitialisationController($q, $scope, $rootScope, ViewEvent, PorticoSDConfiguration, SharedService,
                                           PorticoSDTemplatePaths, PorticoSDPopupName, BasketService, PorticoSDService,
                                           TabularViewStateBehaviourService, PorticoSDSharedService, MultiVendorService,
                                           TabularViewDataService, TabularViewSettingService, PricingHelperService,
                                           PorticoSDEvent, ProjectHelper) {


    /**
     * On Load Method will be called when on initialisation
     */
    function onLoad() {
      PorticoSDSharedService.setTcoPricingFetched(PorticoSDConfiguration.STATES.CURRENT, false);
      PorticoSDSharedService.setTcoPricingFetched(PorticoSDConfiguration.STATES.FUTURE, false);
      PorticoSDSharedService.setPageVolumeTransferor();
      BasketService.loadBasket();
      if (SharedService.getFeatureFlagByKey(PorticoSDConfiguration.FEATURE.MULTI_VENDOR) &&
          SharedService.getRolePermissionByKey(PorticoSDConfiguration.FEATURE.MULTI_VENDOR)) {
        PorticoSDSharedService.setFeatureFlagByKey(PorticoSDConfiguration.FEATURE.MULTI_VENDOR, true);
      } else {
        PorticoSDSharedService.setFeatureFlagByKey(PorticoSDConfiguration.FEATURE.MULTI_VENDOR, false);
      }
    }

    /**
     * Create tabular pool data
     */
    function createTabularPoolData() {
      var activeState = SharedService.activeState() || PorticoSDConfiguration.STATES.CURRENT,
          tabularData = TabularViewStateBehaviourService.getDefaultDataForState(activeState);
      if (tabularData && tabularData.length) {
        $rootScope.tabularViewLoaded = true;
        $rootScope.$broadcast(PorticoSDEvent.TABULAR_VIEW_DATA_LOAD_SUCCESS);
      }
    }


    /**
     * Load tabular data
     */
   function loadTabularData() {
        if (!SharedService.isCreatedProject()) {
            $q.all([TabularViewDataService.loadProjectData(),
                ProjectHelper.getAssetDataLoadedPromise()]).then(function () {
                createTabularPoolData();
            });
        } else {
            createTabularPoolData();
        }
    }
    /**
     *
     * Event will be triggered when Data refresh will be completed and we will have all the data in UI
     * After refresh only we will be generating tabular view pool
     */
    function onRefreshComplete() {
      TabularViewSettingService.getTabularSetting().then(function () {
        loadTabularData();
      });
    }

    // loading all the required data
    onLoad();

    /**
     * Remove the data from pool manager
     */
    function onDestroy() {
      PorticoSDService.removeTabularData();
      PorticoSDSharedService.tcoAdjustmentValues(null);
      MultiVendorService.removeInstance();
      PorticoSDSharedService.setTcoPricingFetched(PorticoSDConfiguration.STATES.CURRENT, false);
      PorticoSDSharedService.setTcoPricingFetched(PorticoSDConfiguration.STATES.FUTURE, false);
      PorticoSDSharedService.setPageVolumeTransferor();
      PorticoSDSharedService.setFeatureFlagByKey(PorticoSDConfiguration.FEATURE.MULTI_VENDOR, false);
      PricingHelperService.assetDartPricingData([]);
    }

    /**
     * On logout
     */
    function onLogout() {
      PorticoSDSharedService.setTabularTemplates(null);
    }

    $rootScope.PorticoSDPopupName = PorticoSDPopupName;
    $rootScope.PorticoSDTemplatePaths = PorticoSDTemplatePaths;
    $rootScope.tabularViewLoaded = false;
    $scope.$on(ViewEvent.REFRESH_COMPLETE, onRefreshComplete);
    $scope.$on(ViewEvent.LOGOUT, onLogout);
    $scope.$on('$destroy', onDestroy);
  }

  var app = angular.module('app'),
      requires = [
        '$q',
        '$scope',
        '$rootScope',
        'ViewEvent',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'SharedService',
        'PorticoSDTemplatePaths',
        'ngPorticoSD.constants.PorticoSDPopupName',
        'BasketService',
        'ngPorticoSD.services.PorticoSDService',
        'ngPorticoSD.services.TabularViewStateBehaviourService',
        'PorticoSDSharedService',
        'ngPorticoSD.services.MultiVendorService',
        'ngPorticoSD.services.TabularViewDataService',
        'TabularViewSettingService',
        'PricingHelperService',
        'ngPorticoSD.constants.PorticoSDEvent',
        'ngCartosCore.services.ProjectHelperService',
        PorticoInitialisationController
      ];
  app.controller('PorticoInitialisationController', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  //Controller to handle Portico Report functionality
  function PorticoReportController($scope, $rootScope, PorticoSDEvent) {
    /**
     * Function to open TCO pop-up
     */
    $scope.exportTCOReport = function () {
      $rootScope.$broadcast(PorticoSDEvent.TCO_EXPORT);
    };
  }

  var app = angular.module('app'),
      requires = [
        '$scope',
        '$rootScope',
        'ngPorticoSD.constants.PorticoSDEvent',
        PorticoReportController
      ];
  app.controller('PorticoReportController', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /**
   * Controller for handling tabular view on the center pane.
   */
  function PorticoSDController($log, $timeout, $scope, $rootScope, $routeParams, ViewEvent, CanvasAsset,
                               filterFilter, PorticoSDConfiguration, OutcomeType,
                               PorticoSDTemplatePaths, SharedService, Utilis, PorticoSDHelperService, PorticoSDService,
                               TabularViewStateBehaviourService, IconService, LevelService, AssetService, PorticoSDDispositionService,
                               translateFilter, ProjectSetting, TransitionHelper, AssetProperties, LevelType, NotifierService, RequestBuilder,
                               PorticoSDSharedService, VolumeTransferHelper, safeApply, PageVolumeMapService,
                               PorticoSDEvent, PageVolumeService, PorticoSDPopupName, OutcomeService, PricingService,
                               DeviceAccessoriesService, TabularDataManagerService, uiGridConstants, TcoPricingService,
                               TcoIntegrationHelperService, SyncTcoPricingService, Configuration, RetireCommand, RetainCommand,
                               ReplaceCommand, ResetCommand, ExcludeCommand, PageVolumeApplyCommand, MultiVendorService, PorticoSdUtilityService,
                               TabularViewSettingService, TabularViewDataService, MultiVendorConfiguration) {
    var reset = {
      VIRTUAL_ASSET: 'resetVirtualAssets',
      REPLACE_ASSET: 'resetReplaceAssets',
      REPOSITION_ASSET: 'resetRepositionAssets',
      OUTCOME_ASSET: 'resetAssetsOutcome'
    };
    var dispositions = {
      "add": "New",
      "Retain(Advance Placement)": "Retain(Advance Placement)",
      "excludeDevice": "Out of Scope",
      "replaceManyToOne": "Replace (Many to One)",
      "retainAdvancePlacement": "Retain(Advance Placement)",
      "replaceOneForOne": "Replace (1 For 1)",
      "reposition": "Reposition",
      "reset": "Reset",
      "retain": "Retain",
      "retire": "Retire",
      "replaceWithUnknown": "New",
      "replaceWithExisting": "Existing",
      "replaceWithNew": "New",
      "repositionTo": "Reposition To",
      "repositionFrom": "reposition From"
    };
    var projectId = $routeParams.id,
        currentSelectedAsset = {},
        blankedColumns = {},
        isCustomFilterApplied = false,
        selectedLevels = [];
    $scope.customFilterColumns = {};
    $scope.appliedCustomFilters = {};
    $scope.flagPageVolumeEnabled = false;
    $scope.flagShowTcoPricing = false;
    $scope.dartUploadStrategy = Utilis.getArrayByKey(PorticoSDConfiguration.DART_DEAL_UPLOAD_OPTIONS);

    $scope.selectedRows = [];
    // To show the checkbox for asset
    $scope.treeLevelAsset = PorticoSDConfiguration.TREE_LEVEL_ASSET;
    $scope.treeLevelAccessory = PorticoSDConfiguration.TREE_LEVEL_ACCESSORY;
    $scope.treeLevelCountry = PorticoSDConfiguration.TREE_LEVEL_COUNTRY;
    $scope.tabularTemplate = {
      selectedTemplate: PorticoSDConfiguration.TABULAR_VIEWS.MANUAL,
      templateList: {}
    };

    /**
     * Check for tabular view.
     * @returns {boolean}
     */
    function isTabularView() {
      return $rootScope.isFloorMapView !== undefined && $rootScope.isFloorMapView === false;
    }

    /**
     * This function will execute when we delete level.
     * @param $event
     * @param level
     * */
    function onLevelDelete($event, level) {
      PorticoSDService.deleteLevelFromTabularPool(level);
      refreshDataFromPoolManager();
    }

    /**
     * This function triggered when level has selected
     * @param $event
     * @param level
     */
    function onLevelSelected($event, level) {
      if (level) {
        if (level.isLevelCreated) {
          //TODO Need to handle this with pool manager right now it is constructing the pool again
          refreshTabularViewData();
        } else if (level.isTabularViewRefresh === undefined || level.isTabularViewRefresh) {
          refreshDataFromPoolManager();
        }
        selectAndScrollToLevel(level);
        openSelectedLevel(level);
      }
    }

    /**
     /**
     * Select and Scroll to level for a given level
     * @param level
     */
    function selectAndScrollToLevel(level) {
      var tabularViewData = TabularViewStateBehaviourService.getPoolDataForState(SharedService.activeState());
        if (level.hasOwnProperty('level') && level.level.hasOwnProperty('_id')) {
            var rowNo = PorticoSDHelperService.getTabularViewRowNumber(tabularViewData, level.level._id);
            $scope.gridApi.grid.modifyRows($scope.tabularViewPorticoSD.data);
            $scope.gridApi.selection.selectRow($scope.tabularViewPorticoSD.data[rowNo]);
            $scope.gridApi.core.scrollTo($scope.tabularViewPorticoSD.data[rowNo]);
        }
    }

    /**
     * This function triggered when level is updated
     * @param $event
     * @param level
     */
    function onLevelUpdated($event, level) {
      if (level) {
        PorticoSDService.updateLevelInPoolManager(level);
        refreshDataFromPoolManager();
      }
    }

    /**
     * This function will execute when asset is mapped on floor.
     */
    function onMapAsset($event, asset) {
      if (asset) {
        PorticoSDService.addAssetInPoolManager(asset);
        refreshDataFromPoolManager();
      }
    }

    /**
     * This function will execute when asset is unplotted from Asset property window.
     */
    function onUnmapAsset($event, assetIds) {
      if (assetIds && assetIds.length) {
        PorticoSDService.setTcoPricingForRemovedAssets(assetIds);
        PorticoSDService.removeAssetInPoolManager(assetIds);
        refreshDataFromPoolManager();
      }
    }

    /**
     * This function is called when asset is manually added.
     * Manually Asset will be added in different ways from canvas so need to add that asset in tabular view
     * But few specific cases like Applying replace outcome we are also adding new asset but that need not to be added
     * in tabular view becuase that is take care by outcome apply callback
     */
    function onAssetAdd($event, canvasAsset) {
      var outcome = canvasAsset.getOutcome();
      // Asset will be added in diffrent way from canvas so need to add that asset in tabular view
      if (outcome && outcome === OutcomeType.REPLACE_WITH_UNKNOWN) {
        return;
      }
      if (canvasAsset && canvasAsset.getAssetProperties()) {
        PorticoSDService.addAssetInPoolManager(canvasAsset.getAssetProperties());
        refreshDataFromPoolManager();
      }
    }

    /**
     * This function is called when asset is deleted.
     */
    function onAssetDeleted($event, assets) {
      if (assets && assets.length) {
        var assetIds = PorticoSDHelperService.getAssetIds(assets);
        PorticoSDService.setTcoPricingForRemovedAssets(assetIds);
        PorticoSDService.removeAssetInPoolManager(assetIds);
        refreshDataFromPoolManager();
      }
    }

    /**
     * Callback which is triggered when asset is re-plotted by drag and drop.
     * @param event
     * @param data object containing asset, virtualAssetId, assetUpdateReq
     */
    function onAssetReplot($event, data) {
      var assetIds = [], tabularAsset;
      if (data && data.asset) {
        assetIds.push(data.asset._id);
        if (data.virtualAssetId) {
          if (data.asset._id !== data.virtualAssetId) {
            assetIds.push(data.virtualAssetId);
          }
        }
        PorticoSDService.removeAssetInPoolManager(assetIds);
        PorticoSDService.addAssetInPoolManager(data.asset);
      }
      if (data.assetUpdateReq && data.assetUpdateReq.length) {
        data.assetUpdateReq.forEach(function (updatedAsset) {
          tabularAsset = AssetService.getAsset(updatedAsset._id);
          PorticoSDService.updateAssetInPoolManager(tabularAsset);
        });
      }
      refreshDataFromPoolManager();
    }

    /**
     * Update Accessories Data in tabular view
     * @param $event
     * @param assetIds
     */
    function onAccessoriesDelete($event, assetIds) {
      var assets = [], asset;
      if (assetIds && assetIds.length) {
        assetIds.forEach(function (assetId) {
          asset = AssetService.getAsset(assetId);
          assets.push(asset);
        });
        // updating asset as after reposition asset will have accessories so need to update asset for accesories
        PorticoSDService.updateAssetsInPoolManager(assets);
        refreshDataFromPoolManager();
      }
    }

    /**
     * Update Accessories Data in tabular view
     * @param $event
     * @param assetId for which we need to update accessory
     */
    function updateAssetAccessories($event, assetId) {
      var asset;
      if (assetId) {
        asset = AssetService.getAsset(assetId);
        PorticoSDService.updateAssetsInPoolManager([asset]);
        refreshDataFromPoolManager();
      }
    }

    /**
     * Update Accessories Data in tabular view for Virtual asset
     * @param $event
     * @param assetId
     */
    function onRepositionApplied($event, assetId) {
      var assets = [], asset;
      if (assetId) {
        asset = AssetService.getAsset(assetId);
        assets.push(asset);
        PorticoSDService.updateAssetsInPoolManager(assets);
      }
      refreshDataFromPoolManager();
    }

    /**
     * This function will execute when asset is updated on Asset property window.
     */
    function onAssetUpdate($event, assets, isManualAssetAssign) {
      var destinationAsset, outcomes, sourceAssets = [];
      if (assets) {
        assets = assets.length ? assets : [assets];
        SyncTcoPricingService.syncTcoPricingForUpdatedAsset(assets);
        assets.forEach(function (asset) {
          destinationAsset = asset;
          PorticoSDService.updateAssetInPoolManager(asset);
          if (isManualAssetAssign && !$rootScope.isFloorMapView) {
            // Get the source asset from replaced asset.
            sourceAssets = OutcomeService.getSourceAssetsByReplacedId(asset.getId());
            // calling pvt transfer
            replaceAssetPvtTransfer(sourceAssets, destinationAsset);
            updateSourceAssetMapId(sourceAssets, destinationAsset);
          }
        });
        refreshDataFromPoolManager();
      }
    }

    /**
     * This function will execute when asset is updated on Asset property window.
     */
    function onAssetAssign($event, response) {
      if (response && response.assignedAsset && response.deletedAssetId) {
        var assetIds = [], destinationAsset, outcomes, sourceAssets = [];
        assetIds.push(response.deletedAssetId);
        PorticoSDService.addAssetAfterAssetInPoolManager(response.assignedAsset, response.deletedAssetId);
        PorticoSDService.removeAssetInPoolManager(assetIds);
        SyncTcoPricingService.syncTcoPricingForUpdatedAsset(response.assignedAsset);
        refreshDataFromPoolManager();
      }
    }


    /**
     *Calculates Average for given Row and column data
     * @param row
     * @param col
     * @returns {*}
     */
    $scope.getTotal = function (row, col) {
      var total = PorticoSDHelperService.getAggregationTotal(row, col);
      return total;
    };

    /**
     * Calculates Average value of levels for mono Ampv, color Ampv and Ampv
     * @param row
     * @param col
     * @return {number}
     */
    $scope.getLevelAverage = function (row, col) {
      var deviceQty = PorticoSDHelperService.getAggregationTotal(row, PorticoSDConfiguration.PORTICOSD_GRID_COLUMNS.deviceQty),
          sumPages = PorticoSDHelperService.getAggregationTotalForPages(row, col);
      return PorticoSdUtilityService.divideValues(sumPages, deviceQty);
    };

    /**
     * Refresh Tabular view from pool manager.
     */
    function refreshDataFromPoolManager() {
      //Refresh the tabular data based on active state
      $scope.tabularViewPorticoSD.data = TabularViewStateBehaviourService.getPoolDataForState(SharedService.activeState());
      unselectRows();
      $scope.isAllAssetsSelected = false;
      if ($scope.gridApi) {
        PorticoSDHelperService.applyBlankFilter($scope.tabularViewPorticoSD, blankedColumns);
        // Setting up the placeholder on the column if there is any custom filter exists
        if (!Utilis.isEmpty($scope.appliedCustomFilters)) {
          PorticoSDHelperService.applyPlaceholderOnColumn($scope.appliedCustomFilters, $scope.tabularViewPorticoSD, blankedColumns);
          applyPreviousCustomFilter();
        }
        $scope.gridApi.grid.refresh();
      }
    }


    /**
     * Refresh Tabular view with the current data.
     */
    function refreshTabularViewData() {
      //Refresh the tabular data based on active state
      $scope.tabularViewPorticoSD.data = TabularViewStateBehaviourService.getDefaultDataForState(SharedService.activeState());
      $scope.isAllAssetsSelected = false;
      if ($scope.gridApi) {
        $scope.gridApi.grid.refresh();
      }
    }

    /**
     * Applying previous custom filter
     */
    function applyPreviousCustomFilter() {
      // Checking if there is any custom filter remaining
      if (Object.keys($scope.appliedCustomFilters).length) {
        for (var key in $scope.appliedCustomFilters) {
          if ($scope.appliedCustomFilters.hasOwnProperty(key)) {
            $scope.appliedCustomFilters[key].popupFlag = false;
            // Applying replaced custom filter again
            $scope.applyCustomFilter($scope.appliedCustomFilters[key]);
          }
        }
      }
    }

    /**
     Show the asset property on left pane
     * @param {row}
     **/
    function showAssetProperty(row) {
      if (Utilis.isNull(row) || Utilis.isEmpty(row) || Utilis.isNull(row.entity) ||
          Utilis.isNull(row.entity.assetId)) {
        return;
      }
      var asset = AssetService.getAsset(row.entity.assetId);
      SharedService.getSplitter().leftPane.expand();
      SharedService.selectedAsset(asset);

      $scope.flagRowSelected = asset.plotted;
      //These property is required if value is there in DCA then need not to override Total field in view.
      /*  asset.hasTotalLastReading = (asset.lifeTotalLastReading || asset.lifeTotalLastReading === 0) ? true : false;
       asset.hasLifeTotalReading = asset.hasTotalLastReading;
       asset.hasTotalMonthly = (asset.lifeTotalMonthly || asset.lifeTotalMonthly === 0) ? true : false;
       asset.hasLifeTotalMonthly = asset.hasTotalMonthly;*/

      asset.hasTotalLastReading = (asset.lifeTotalLastReading || asset.lifeTotalLastReading === 0) ? true : false;
      asset.hasTotalMonthly = (asset.lifeTotalMonthly || asset.lifeTotalMonthly === 0) ? true : false;
      $rootScope.$broadcast(ViewEvent.ASSET_SELECT, asset);
    }

    /**
     * Expand the levels on Tabular View.
     * @param levelsRowNumber
     */
    function expandLevels(levelsRowNumber) {
      if ($scope.gridApi) {
        var numberOfRows = $scope.gridApi.grid.rows.length;
        if (levelsRowNumber.country >= 0 && levelsRowNumber.country <= numberOfRows &&
            levelsRowNumber.site <= numberOfRows && levelsRowNumber.building <= numberOfRows &&
            levelsRowNumber.floor <= numberOfRows) {

          // Open the level in the sequence of Project -> Country -> Site -> Building -> Floor.
          // Note : Don't change the sequence.
          $scope.gridApi.treeBase.expandRow($scope.gridApi.grid.rows[levelsRowNumber.project]);
          $scope.gridApi.treeBase.expandRow($scope.gridApi.grid.rows[levelsRowNumber.country]);
          $scope.gridApi.treeBase.expandRow($scope.gridApi.grid.rows[levelsRowNumber.site]);
          $scope.gridApi.treeBase.expandRow($scope.gridApi.grid.rows[levelsRowNumber.building]);
          $scope.gridApi.treeBase.expandRow($scope.gridApi.grid.rows[levelsRowNumber.floor]);
        }
      }
    }

    /**
     * Open the floor which is selected on breadcrumb.
     */
    function openSelectedLevel() {
      var selectedFloor = SharedService.getCurrentLevel(PorticoSDConfiguration.LEVEL_FLOOR);

      // Get the tabular data
      var tabularViewData = TabularViewStateBehaviourService.getPoolDataForState(SharedService.activeState());

      if (PorticoSDHelperService.isValidLevel(selectedFloor, tabularViewData)) {
        var levelsRowNumber = PorticoSDHelperService.getLevelsRowNumber(selectedFloor, tabularViewData);
        expandLevels(levelsRowNumber);
      }
    }

    /**
     * Update tabular view columns
     */
    function updateTabularView() {
      setupTabularViewColumns();
      if ($scope.flagShowTcoPricing) {
        setTcoCategoryStatus();
      }
    }

    /**
     * Update the changes in tabular pool and reflect on UI.
     */
    function updateTabularViewDataOnViewUpdate() {
      let pricingView = SharedService.pricingView();
      updateTabularView();
      switch (pricingView) {
        case PorticoSDConfiguration.TABULAR_VIEWS.MANUAL :
          break;
        case PorticoSDConfiguration.TABULAR_VIEWS.DART :
        case PorticoSDConfiguration.TABULAR_VIEWS.TCO :
        case PorticoSDConfiguration.TABULAR_VIEWS.ALL :
        case PorticoSDConfiguration.TABULAR_VIEWS.MULTIVENDOR :
        default :
          refreshTabularViewData();
          break;
      }
    }

    /**
     * Refresh tabular view bind ui and data from pool
     */
    function refreshTabularView() {
      try {
        $rootScope.loader.loading = true;
        setupTabularViewColumns();
        refreshDataFromPoolManager();
        $rootScope.loader.loading = false;
      } catch (ex) {
        $rootScope.loader.loading = false;
      }
    }

    /**
     * On Tabular view setting update.
     */
    function onTabularViewSettingUpdate() {
      $rootScope.loader.loading = true;
      try {
        TabularViewDataService.loadProjectData().then(function () {
          updateTabularViewDataOnViewUpdate();
          $rootScope.loader.loading = false;
        });
      } catch (ex) {
        $rootScope.loader.loading = false;
      }
    }

    /**
     * This function is called when grid row is clicked.
     * If the level is selected then Level property window will open in right pane.
     * If the asset is selected then Asset property window will open in left pane.
     * @param {row,$event} row clicked
     * */
    $scope.showProperties = function (row, col, $event) {
      var selectedAssets;

      //here we are handling touched asset plotting.if an asset is selected in MLT and we clicked on row in tabularView
      if (SharedService.getTouchedAssetId()) {
        $scope.onDropAsset($event, SharedService.getTouchedAssetId());
        SharedService.getTouchedAssetId(null);
      }

      // If clicked on 'level' column then show the asset property window or level property window
      // based on row selection else return.
      if (!row || !row.entity || !col || col.field !== 'level') {
        return;
      }
      if (row.entity.type === PorticoSDConfiguration.LEVEL || row.entity.type === PorticoSDConfiguration.PROJECT) {
        var currentLevel = LevelService.getLevel(row.entity.levelId);
        if (currentLevel) {
          // To DO :: Check the refreshing event for breadcrumb.
          $rootScope.$broadcast(ViewEvent.BREADCRUMB_UPDATE, currentLevel, function () {
          });
        }
      } else if (row.entity.type === PorticoSDConfiguration.ASSET) {
        // If more than one asset is selected then asset property window will not open.
        // And if it is open then close it.
        selectedAssets = SharedService.getSelectedAssets() || [];
        if (selectedAssets.length <= 1) {
          showAssetProperty(row);
        } else {
          PorticoSDHelperService.collapseLeftPane();
        }
      }
    };

    //TODO need to remove this function as we can bind the class name directly on HTML
    /**
     * Set the row level Id.
     * @param row
     * @returns {string}
     */
    $scope.rowLevel = function (row) {
      var rowLevelId = '';
      if (row.treeLevel !== void 0) {
        rowLevelId = 'rowLevel-' + row.treeLevel;
      }
      return rowLevelId;
    };

    /**
     * This function is called on dropping the asset from master asset lookup window.
     * @param $event
     * @param $data
     */
    $scope.onDropAsset = function ($event, $data) {
      var assetData, assetId, asset, assetOutcome,
          configMatrix = SharedService.getConfiguration().StateMatrix[SharedService.activeState()];
      if ($data) {
        // We will use the existing method so sending the data in form of method argument.
        assetId = PorticoSDHelperService.getAssetIdFromDropTargetId($data);
        asset = AssetService.getAsset(assetId);
        assetOutcome = asset.outcome ? asset.outcome : '';
        if (!SharedService.isCurrentState() && configMatrix && !configMatrix.asset[asset.state].create.dragDrop.outcome[assetOutcome]) {
          NotifierService.info('PLOTTING_IS_NOT_ALLOWED_IN_TRASITION_FUTURE_STATE');
          return;
        }

        if (assetId) {
          if (SharedService.isPlottedOnCurrentFloor(assetId) && AssetService.isAssetPlacedOnFloor(assetId)) {
            NotifierService.info('ASSET_IS_ALREADY_PLOTTED_CURRENT_FLOOR');
            return;
          }
          assetData = {
            relatedTarget: {id: $data},
            isTabularView: true
          };
          $rootScope.$broadcast(ViewEvent.ASSET_PLOT_TABULAR_DRAG_DROP, assetData);
        }
      }
    };

    $scope.TemplatePath = PorticoSDTemplatePaths;
    $scope.isAllAssetsSelected = false;
    $scope.isTransitionState = SharedService.isTransitionState();
    $scope.isFutureState = SharedService.isFutureState();
    $scope.deviceTypes = [
      'multifunctionMono',
      'multifunctionColor',
      'printMono',
      'printColor',
      'fax',
      'plotter',
      'scanner'
    ];

    $scope.tabularViewPorticoSD = {
      enableSorting: true,
      enableGridMenu: true,
      showTreeExpandNoChildren: false,
      enableFiltering: true,
      columnDefs: [],
      onRegisterApi: onRegisterApi,
      rowTemplate: PorticoSDTemplatePaths.TABULAR_ROW_TEMPLATE,
      enableRowSelection: true,
      enableRowHeaderSelection: false,
      multiSelect: false,
      noUnselect: true,
      data: [],
      excessColumns: 50
    };

    /**
     * Asset Import call back
     * it will update the assets in grid if import dca in tabular view
     */
    function onAssetImport() {
      refreshTabularViewData();
    }

    /**
     * Check the permission based on roles permission.
     */
    function isDartPricingAllowed() {
      return JSON.parse($rootScope.rolePermission && $rootScope.rolePermission.dartPricing || "true");
    }

    /**
     * Update template in project setting.
     * @param selectedTabularTemplate
     */
    function updateTemplateInProjectSetting(selectedTabularTemplate) {
      let projectSetting, request,
          setting = ProjectSetting.getProjectSettings(projectId) || {};
      if (!(setting && setting.pricingSettings)) {
        return;
      }
      $rootScope.loader.loading = true;
      projectSetting = angular.copy(setting);
      projectSetting.pricingSettings.pricingSource = selectedTabularTemplate;
      projectSetting.pricingSettings.currentPricingView = selectedTabularTemplate;
      projectSetting.pricingSettings.proposedPricingView = selectedTabularTemplate;
      request = {
        projectId: projectId,
        pricingSettings: projectSetting.pricingSettings
      };

      //success callback on updating project settings
      function onSuccess() {
        SharedService.pricingView(selectedTabularTemplate);
        $rootScope.$broadcast(ViewEvent.PRICING_SETTING_UPDATE);
        $rootScope.loader.loading = false;
        $log.info('Pricing settings update successfully.');
      }

      //error callback on updating project settings
      function onError(error) {
        Notify.error('PRICING_SETTING_UPDATE_ERROR');
        $log.info('Error updating pricing setting.');
        $rootScope.loader.loading = false;
      }

      //update project setting
      ProjectSetting.updateProjectSetting(request).then(onSuccess, onError);
    }

    $scope.showSelectedTemplate = function (selectedTemplate) {
      updateTemplateInProjectSetting(selectedTemplate);
    };


    /**
     * Function will setup the tabular view columns
     * It will get the column setting from project setting api & initiate the columns
     */
    function setupTabularViewColumns() {
      var gridColumns,
          settings = ProjectSetting.getProjectSettings(projectId),
          tabularSelectedView, templateData;

      if (!settings) {
        $log.error("Error while retrieving project settings.");
        return;
      }
      if (!settings.pricingSettings) {
        settings.pricingSettings = PorticoSDConfiguration.defaultPricingSettings;
      }

      tabularSelectedView = angular.copy(settings.pricingSettings.pricingSource ||
          PorticoSDConfiguration.defaultPricingSettings.pricingSource);
      $scope.tabularTemplate.templateList = TabularViewSettingService.getTabularTemplatesName();
      if (Utilis.isValid($scope.tabularTemplate.templateList)) {
        $scope.tabularTemplate.selectedTemplate = $scope.tabularTemplate.templateList.hasOwnProperty(tabularSelectedView) ?
            tabularSelectedView : PorticoSDConfiguration.TABULAR_VIEWS.MANUAL;
      } else {
        $scope.tabularTemplate.selectedTemplate = PorticoSDConfiguration.TABULAR_VIEWS.MANUAL;
      }
      $scope.flagPageVolumeEnabled = !settings.toolbarControlSetting ? false : !!settings.toolbarControlSetting.flagPageVolumeEnabled;
      $scope.flagShowTcoPricing = tabularSelectedView === PorticoSDConfiguration.PRICING_SOURCE.TCO;

      templateData = PorticoSDSharedService.getTemplateData(tabularSelectedView);
      $scope.tabularViewPorticoSD.columnDefs = [];
      if (templateData && templateData.length) {
        gridColumns = angular.copy(templateData);
        for (var gridColumnIndex = 0; gridColumnIndex < gridColumns.length; gridColumnIndex++) {
          gridColumns[gridColumnIndex].displayName = translateFilter(gridColumns[gridColumnIndex].name);
          //TODO: We have to handle it from the html itself in future.
          if (gridColumns[gridColumnIndex].name === PorticoSDConfiguration.PORTICOSD_GRID_COLUMNS.selection) {
            PorticoSDHelperService.setSelectionColumn(gridColumns[gridColumnIndex]);
          }
          gridColumns[gridColumnIndex].menuItems = getCustomMenuItems(gridColumns[gridColumnIndex].name);
          gridColumns[gridColumnIndex].filter = PorticoSDHelperService.getCustomRowFilter(gridColumns[gridColumnIndex].name, blankedColumns);
          if (gridColumns[gridColumnIndex].name === PorticoSDConfiguration.PORTICOSD_GRID_COLUMNS.hwDisposition) {
            gridColumns[gridColumnIndex].filters = [{
              condition: function (searchTerm, cellValue) {
                searchTerm = searchTerm.toLowerCase();
                return !!(!searchTerm ||
                    (dispositions[cellValue] && dispositions[cellValue].toLowerCase().indexOf(searchTerm) >= 0));
              }
            }
            ];
          }
          $scope.tabularViewPorticoSD.columnDefs.push(gridColumns[gridColumnIndex]);
        }
      }
    }

    /**
     * Triggered when controller is loaded.
     */
    function onLoad() {
      getTCOColumnCategories();
      SharedService.pageVolumeEnabled($scope.flagPageVolumeEnabled);
    }

    /**
     * Function on adding assets from dropdown in tabular view
     * @param assetType
     */
    $scope.plotManualAsset = function (assetType) {
      var options = IconService.getAssetIcons();
      var data = options.filter(function (option) {
        return (option.assetType === assetType);
      });
      var option = {
        icon: data[0],
        isTabularView: true,
        labelCoordinates: {
          x: 0,
          y: 0
        }
      };
      $rootScope.$broadcast(ViewEvent.MANUAL_ASSET_PLOT_ON_TABULAR_VIEW, option);
    };

    /**
     * To select all the assets
     */
    $scope.toggleCheckerAll = function () {
      if ($scope.isAllAssetsSelected) {
        // If header check box is checked then all the rows will appear in expand mode
        // "expandAllRows" method will trigger the onRowsRendered event.
        // Set the "$rootScope.loader.loading = false" in onRowsRendered event
        $rootScope.loader.loading = true;
        if ($scope.gridApi) {
          $scope.gridApi.treeBase.expandAllRows();
        }
        PorticoSDHelperService.collapseLeftPane();
      } else {
        // Remove the selection and update the selected asset.
        unselectRows();
        SharedService.setSelectedAssets();
      }
    };

    /**
     * Show toner detail for the asset.
     * @param row
     */
    $scope.showTonerDetails = function (row) {
      if (row && row.assetId) {
        var tonerDetails = TcoPricingService.getTonerDetails(row.assetId);
        if (tonerDetails && tonerDetails.length) {
          $scope.tonnerDetails = tonerDetails;
          $scope.togglePopupFlag(PorticoSDPopupName.TONER_DETAILS);
        } else {
          NotifierService.info('NO_RECORD_FOUND_FOR_TONER_DETAILS');
        }
      }
    };


    /**
     * Show multi vendor template costing
     * @param assetId
     * @param template
     * @param templateType
     */
    function showMvTemplateCosting(assetId, template, templateType) {
      var asset;

      if (!assetId) {
        $log.error('Error in getting asset row or asset id.');
        $rootScope.loader.loading = false;
        NotifierService.error('ERROR_GETTING_MULTI_VENDOR_TEMPLATE_COST');
        return;
      }

      asset = AssetService.getAsset(assetId);
      if (!asset) {
        $log.error('Error in getting asset details.');
        $rootScope.loader.loading = false;
        NotifierService.error('ERROR_GETTING_MULTI_VENDOR_TEMPLATE_COST');
        return;
      }

      function onSuccess(response) {
        $rootScope.loader.loading = false;
        if (Array.isArray(response)) {
          if (response.length) {
            $scope.mvTemplateCosts = response;
            $scope.mvPartnerName = template;
            $scope.mvCurrency = MultiVendorConfiguration.TEMPLATE_CURRENCY_MAPPING[template];
            if (asset.productId && Utilis.isString(asset.productId)) {
              $scope.mvDeviceId = asset.productId.padStart(8, PorticoSDConfiguration.ZERO);
            }
            $scope.togglePopupFlag(PorticoSDPopupName.MV_TEMPLATE_COST);
          } else {
            NotifierService.error('NO_RECORD_FOUND_FOR_MULTI_VENDOR_COST');
          }
        } else {
          NotifierService.error('ERROR_GETTING_MULTI_VENDOR_TEMPLATE_COST');
        }
      }

      function onError(error) {
        $rootScope.loader.loading = false;
        $log.error('Error in getting multi vendor template cost.', error);
        $scope.mvTemplateCosts = [];
        NotifierService.error('ERROR_GETTING_MULTI_VENDOR_TEMPLATE_COST');
      }

      MultiVendorService.getMvTemplateCost(template, asset.productId, asset.printTechnology, asset.hasColor,
        templateType).then(onSuccess, onError);
    }

    /**
     * Show costing for the service template
     * @param row
     */
    $scope.showServiceCosting = function (row) {
      if (row) {
        if (!row.mvService) {
          NotifierService.error('PLEASE_SELECT_SERVICE_TEMPLATE_FOR_ASSET');
          return;
        }
        else if (!row.mvServiceCostStatus) {
          NotifierService.error('PLEASE_FETCH_COST_FOR_THIS_ASSET');
          return;
        }
      }
      else {
        NotifierService.error('ERROR_GETTING_DATA_FOR_THIS_ROW');
        return;
      }
      $rootScope.loader.loading = true;
      $scope.templateType = PorticoSDConfiguration.MV_TEMPLATE_TYPE.SERVICE;
      showMvTemplateCosting(row.assetId, row.mvService, $scope.templateType);
    };


    /**
     * Show costing for the supply template
     * @param row
     */
    $scope.showSupplyCosting = function (row) {
      if (row) {
        if (!row.mvSupply) {
          NotifierService.error('PLEASE_SELECT_SUPPLY_TEMPLATE_FOR_ASSET');
          return;
        }
        else if (!row.mvSupplyCostStatus) {
          NotifierService.error('PLEASE_FETCH_COST_FOR_THIS_ASSET');
          return;
        }
      }
      else {
        NotifierService.error('ERROR_GETTING_DATA_FOR_THIS_ROW');
        return;
      }
      $rootScope.loader.loading = true;
      $scope.templateType = PorticoSDConfiguration.MV_TEMPLATE_TYPE.SUPPLIES;
      showMvTemplateCosting(row.assetId, row.mvSupply, $scope.templateType);
    };

    /**
     * Show replaceable parts detail for the asset.
     * @param row
     */
    $scope.showReplaceablePartsDetails = function (row) {
      if (row && row.assetId) {
        var replaceablePartsDetails = TcoPricingService.getReplaceablePartsDetails(row.assetId);
        if (replaceablePartsDetails && replaceablePartsDetails.length) {
          $scope.replaceablePartsDetails = replaceablePartsDetails;
          $scope.togglePopupFlag(PorticoSDPopupName.REPLACEABLE_PARTS_DETAILS);
        } else {
          NotifierService.info('NO_RECORD_FOUND_FOR_REPLACEABLE_PARTS_DETAILS');
        }
      }
    };

    /**
     * to destroy the page voulme transferor objects
     */
    function destroyTransferor() {
      $scope.isAllAssetsSelected = false;
      unselectRows();
      PorticoSDSharedService.setPageVolumeTransferor();
    }

    /**
     * This function mainly transfer the Full page volume from source asset to destination asset.
     * @param sourceAssets array of assets {@Code Asset}
     * @param successMessage success message to display
     * @param destinationAsset {@Code Asset}
     */
    function fullPvtTransfer(sourceAssets, destinationAsset, successMessage) {
      var request = RequestBuilder.getProposedVolumeTransferRequest(sourceAssets, destinationAsset),
          selectedAssetRequest = request.assets[destinationAsset.getId()];

      // error callback closure.
      function onError(error) {
        return function (message) {
          if (message) {
            NotifierService.error(SharedService.getErrorMessage(error, message));
          }
        };
      }

      // have transfers request.
      if (request.transfersRequest.length) {
        var pvtCommandRequest = {
          transfer: request.transfersRequest[0],
          assets: Utilis.getArrayByKey(request.assets)
        };
        var command = PageVolumeApplyCommand.getInstance(pvtCommandRequest);
        command.execute();
      } else if (selectedAssetRequest) {
        AssetService.updateAsset([selectedAssetRequest]).then(function (response) {
          $rootScope.$broadcast(ViewEvent.ASSET_UPDATE, response.first());
        }, onError('UNABLE_TO_UPDATE_AN_ASSET'));
      }
      unselectAssets();
      NotifierService.success(successMessage);
      $log.info(translateFilter(successMessage));
    }

    /**
     * function responsible for validating transfer is allowed for selected asset or not and
     * if allowed than transfer the page volume
     * @param sourceAsset
     * @param destinationAsset
     */
    function transferPageVolume(sourceAsset, destinationAsset) {
      //Validate transfer is allowed for selected assets or not
      var message = VolumeTransferHelper.validateTransfer(sourceAsset, destinationAsset);
      if (message) {
        safeApply($scope, function () {
          NotifierService.info(message);
        });
        destroyTransferor();
        return;
      }
      safeApply($scope, function () {
        //by default we transfer the 100% page volume
        fullPvtTransfer([sourceAsset.getAssetProperties()], destinationAsset.getAssetProperties(), 'PAGE_VOLUME_TRANSFERRED_SUCCESSFULLY');
      });
    }

    /**
     * function to handle asset selection option in transition state
     * @param asset
     */
    function onAssetClickInTransitionState(asset) {
      var transferAssets, transferor,
          message = 'OUTCOME_CANNOT_BE_ASSIGNED_TO_EMPTY_ASSET',
          isManual = !!asset.getAssetProperties().isManual,
          isVirtual = asset.isVirtual(),
          isPageVolumeEnabled = SharedService.pageVolumeEnabled(),
          flagNoTransfer = isManual && isPageVolumeEnabled,
          flagNoOutcome = isManual && !isVirtual;

      if (isPageVolumeEnabled) {
        message = 'EMPTY_ASSET_DOES_NOT_SUPPORT_VOLUME_TRANSFER';
      }

      if (flagNoTransfer) {
        //destroy the transferor assets
        destroyTransferor();
        NotifierService.info(message);
        $log.info('Empty asset is clicked for volume transfer.');
        return;
      }

      if (flagNoOutcome) {
        NotifierService.info(message);
        $log.info('Empty asset is clicked for outcome in transition state.');
        return;
      }

      if (!isPageVolumeEnabled) {
        return;
      }
      // get the page volume transferor it will return assets which are selected for the transfer
      transferor = PorticoSDSharedService.getPageVolumeTransferor();
      if (transferor) {
        // if source or destination asset is not selected then set them to transferor
        if (!transferor.sourceAsset || !transferor.destinationAsset) {
          PorticoSDSharedService.setPageVolumeTransferor(asset);
        }
        //again check if both Source and Destination assets are set or not if yes then transfer the page volume
        transferAssets = PorticoSDSharedService.getPageVolumeTransferor();
        if (transferAssets.sourceAsset && transferAssets.destinationAsset) {
          transferPageVolume(transferAssets.sourceAsset, transferAssets.destinationAsset);
          SharedService.setSelectedAssets();
          PorticoSDSharedService.setPageVolumeTransferor();
        }
        return;
      }
    }

    /**
     * toggle select all check box on the basis of all the assets are selected or not
     * @param row
     * @param assetCheckBoxChecked
     */
    function toggleAllRowSelected(row, assetCheckBoxChecked) {
      var selectedAssets, filterTabularData, tabularViewAssets, isAllAssetSelected, filteredLevelData,
          selectedParentLevels,
          floorData;

      // Get the data which is appear on the tabular view. If any filter is applied then it will give us filtered data.
      filterTabularData = TabularDataManagerService.getTabularFilteredData($scope.gridApi.grid.rows);
      // This will give us all the assets from the floor.
      floorData = filterFilter(filterTabularData, PorticoSDHelperService.extractFloorData);
      setSelectedRows(floorData);
      if (assetCheckBoxChecked) {
        // Get all the assets from the floor.
        tabularViewAssets = TabularDataManagerService.getAssetsBasedOnLevels(filterTabularData, row);
        // Check if all the assets are selected then we need to select the levels as well.
        isAllAssetSelected = TabularDataManagerService.isAllRowSelected(tabularViewAssets);
        if (isAllAssetSelected) {
          // This will give the specific level data
          filteredLevelData = TabularDataManagerService.getFilterDataForHeaderUnSelection(filterTabularData, row);
          if (filteredLevelData) {
            // This will give us which level will be selected.
            selectedParentLevels = TabularDataManagerService.getLevelsToBeSelected(filterTabularData, row);
            selectedLevels = TabularDataManagerService.setSelectedLevels(filteredLevelData, selectedParentLevels);
          }
        }
        $scope.isAllAssetsSelected = TabularDataManagerService.isAllRowSelected(filterTabularData);
      } else {
        $scope.isAllAssetsSelected = false;
        filteredLevelData = TabularDataManagerService.getFilterDataForHeaderUnSelection(filterTabularData, row);
        if (filteredLevelData) {
          TabularDataManagerService.setTabularDataRowSelection(filteredLevelData, false);
        }
      }

      selectedAssets = SharedService.getSelectedAssets() || [];
      // Collapse the asset property window if multiple asset selected.
      if (selectedAssets && selectedAssets.length > 1) {
        PorticoSDHelperService.collapseLeftPane();
      }
    }


    /**
     * Responsible to handle asset selection by checkbox from tabular view when single asset is selected
     * @param row which is selected
     * @param $event
     */
    function toggleAssetRow(row, $event) {
      var validationMessage,
          isPageVolumeEnabled = SharedService.pageVolumeEnabled(),
          selectedAsset = AssetService.getAsset(row.assetId), filterTabularData, floorData;
      if (row.selection) {
        validationMessage = PorticoSDDispositionService.validateAssetForDisposition(selectedAsset);
        if (validationMessage && validationMessage.length) {
          NotifierService.info(validationMessage);
          row.selection = false;
          return;
        }
      }
      //if we uncheck the row destroy the page volume transferors
      if (row && row.assetId && SharedService.isTransitionState() && isPageVolumeEnabled) {
        if (!row.selection) {
          destroyTransferor();
          return;
        }

        // Get the data which is appear on the tabular view. If any filter is applied then it will give us filtered data.
        filterTabularData = TabularDataManagerService.getTabularFilteredData($scope.gridApi.grid.rows);

        //TODO: will move the below code into a separate function
        var pvtTransferor = PorticoSDSharedService.getPageVolumeTransferor();
        //  if both source and destination assets are blank then notify message and set the source asset
        if (pvtTransferor && !pvtTransferor.sourceAsset && !pvtTransferor.destinationAsset) {

          NotifierService.info('PLEASE_SELECT_ASSET_RECEIVING_PRINT_VOLUME');
          onAssetClickInTransitionState(selectedAsset);
        } else {
          //check if both selected asset and already set source assets are same if yes then destroy the transferor
          if (pvtTransferor.sourceAsset && pvtTransferor.sourceAsset.getId() === row.assetId) {
            destroyTransferor();
          } else if (!pvtTransferor.sourceAsset) {
            NotifierService.info('PLEASE_SELECT_ASSET_RECEIVING_PRINT_VOLUME');
            onAssetClickInTransitionState(selectedAsset);
          } else { //To set the destination asset
            var sourceAsset = PorticoSDService.getTabularViewData(pvtTransferor.sourceAsset.getId());
            // check if source and destination assets are on different floor than do not allow PVT
            if (sourceAsset && sourceAsset.first().levelId !== row.levelId) {
              row.selection = false;
              NotifierService.info('TRANSFER_IS_NOT_ALLOWED_AMONG_DIFFERENT_FLOORS');
              return;
            } else {
              onAssetClickInTransitionState(selectedAsset);
            }
          }
        }
        return;
      } else {
        toggleAllRowSelected(row, $event.target.checked);
      }
    }

    /**
     * Responsible to handle level selection.
     * @param row which is selected
     * @param $event
     */
    function toggleLevelRow(row, $event) {
      try {
        var filterTabularDataByLevel, filterTabularData, tabularAssets, levelRows,
            levelCheckBox = $event.target.checked;
        $rootScope.loader.loading = true;

        filterTabularData = TabularDataManagerService.getTabularFilteredData($scope.gridApi.grid.rows);
        filterTabularDataByLevel = TabularDataManagerService.getFilterDataByLevel(filterTabularData, row);
        TabularDataManagerService.setTabularDataRowSelection(filterTabularDataByLevel, levelCheckBox);
        if (filterTabularData && filterTabularData.length) {
          tabularAssets = filterFilter(filterTabularData, PorticoSDHelperService.extractFloorData);
          setSelectedRows(tabularAssets);
          selectedLevels = selectedLevels.length ? selectedLevels.concat(PorticoSDHelperService.getSelectedLevels(filterTabularData, row)) :
              PorticoSDHelperService.getSelectedLevels(filterTabularData, row);
        }
        if (levelCheckBox) {
          if (row.type === PorticoSDConfiguration.PROJECT) {
            $scope.isAllAssetsSelected = true;
          }
        } else {
          $scope.isAllAssetsSelected = false;
          levelRows = TabularDataManagerService.getFilterDataForHeaderUnSelection(filterTabularData, row);
          if (levelRows) {
            TabularDataManagerService.setTabularDataRowSelection(levelRows, false);
          }
        }
        $rootScope.loader.loading = false;
      } catch (ex) {
        $rootScope.loader.loading = false;
        $log.error('Error in selecting/un-selecting the level ', ex);
      }
    }

    /**
     * Responsible to handle asset selection by checkbox from tabular view
     * Row could be Levels (Project, Country, Site, Building & floor) or Assets
     * if selected row is any level then we need to select all the level under the selected level
     * @param row which is selected
     * @param $event checked or unchecked
     */
    $scope.toggleChecker = function (row, $event) {
      // checking if selected row is asset then need to select that row only
      if (row.type === PorticoSDConfiguration.ASSET) {
        toggleAssetRow(row, $event);
      }
      // if selected row is not asset row i.e level row
      // then we need to select all the assets under that level as well as sublevel also.
      else if (row.type === PorticoSDConfiguration.LEVEL || row.type === PorticoSDConfiguration.PROJECT) {
        toggleLevelRow(row, $event);
      }
    };


    /**
     * To Check if any of the row is selected.
     * @returns {boolean}
     */
    function isAnyRowSelected() {
      var isAnyRowSelected = false;
      var floorData = filterFilter($scope.tabularViewPorticoSD.data, PorticoSDHelperService.extractFloorData);
      if (floorData) {
        for (var rowIndex = 0; rowIndex < floorData.length; rowIndex++) {
          if (floorData[rowIndex].selection) {
            isAnyRowSelected = true;
            break;
          }
        }
      }
      return isAnyRowSelected;
    }

    /**
     * to handle the state change event
     * @param $event
     * @param activeState
     */
    function onStateChange($event, activeState) {
      var tabularData;
      $scope.isTransitionState = SharedService.isTransitionState();
      $scope.isFutureState = SharedService.isFutureState();
      PorticoSDService.updateAssetsTcoPricingInPoolManager();
      if ($rootScope.isFloorMapView !== undefined && !$rootScope.isFloorMapView) {
        tabularData = TabularViewStateBehaviourService.getPoolDataForState(activeState);
        PorticoSDService.setTcoPricingOnLevels(tabularData);
        tabularData = TabularViewStateBehaviourService.getPoolDataForState(activeState);
        if (tabularData && tabularData.length) {
          $scope.tabularViewPorticoSD.data = tabularData;
        }
        SharedService.setSelectedAssets();
        unselectRows();
      }
      $scope.isAllAssetsSelected = false;


    }

    /*
     * To handle the disposition change in tabular view
     * @param Selected Row
     */
    $scope.onDispositionChange = function (row) {
      if (row) {
        var selectedAssets = SharedService.getSelectedAssets(),
            asset = AssetService.getAsset(row.assetId),
            selectedDisposition = row.hwDisposition;
        currentSelectedAsset = AssetService.getAsset(row.assetId);
        if (selectedDisposition === 'reposition') {
          asset = AssetService.getAsset(row.assetId);
          $rootScope.$broadcast(PorticoSDEvent.SHOW_REPOSITION_POPUP, asset);
          row.hwDisposition = '';
          row.outcome = '';
          return;
        }
        onApplyDisposition(selectedDisposition);
      }
    };

    /**
     * Validate selected multi vendor rows
     * @returns {boolean}
     */
    function validateSelectedMvRows() {
      var multiVendorAssets, assetRows, countryId,
          selectedRows = $scope.selectedRows;

      try {
        if (!(Array.isArray(selectedRows) && selectedRows.length)) {
          return false;
        }

        assetRows = selectedRows.filter(function (row) {
          return row.type === PorticoSDConfiguration.ASSET && row.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_ASSET;
        });

        if (!assetRows.length) {
          $log.error('Error in getting selected assets.');
          return false;
        }

        multiVendorAssets = assetRows.filter(function (row) {
          return row.isMultiVendorAsset === true;
        });

        if (!multiVendorAssets.length) {
          $log.error('Error in getting selected multi vendor assets.');
          return false;
        }

        countryId = multiVendorAssets.first().countryId;
        for (var index = 1; index < multiVendorAssets.length; index++) {
          if (countryId !== multiVendorAssets[index].countryId) {
            NotifierService.error('PLEASE_SELECT_ASSET_FROM_ONE_COUNTRY_ONLY');
            return false;
          }
        }
        return true;
      } catch (ex) {
        $log.error('Error while validating multi vendor asset ', ex);
        return false;
      }
    }

    /*
     * On Supply change
     * @param Selected Row
     */
    $scope.onMvZoneChange = function (row) {
      var asset;
      if (!row) {
        return;
      }

      $rootScope.loader.loading = true;
      if (!validateSelectedMvRows()) {
        asset = AssetService.getAsset(row.assetId);
        row.mvDartZone = asset.mvDartZone;
        $rootScope.loader.loading = false;
        return;
      }
      var selectedAssets = SharedService.getSelectedAssets(),
          selectedZone = row.mvDartZone, assetReq,
          assetUpdateReq = [];

      if (!(Array.isArray(selectedAssets) && selectedAssets.length)) {
        $log.error("Error in getting selected assets.");
        $rootScope.loader.loading = false;
        return;
      }

      selectedAssets = PorticoSDHelperService.getMvAssetsFromAssets(selectedAssets);
      if (!selectedAssets.length) {
        $log.error("Error in getting mv assets.");
        $rootScope.loader.loading = false;
        return;
      }

      for (var index = 0; index < selectedAssets.length; index++) {
        assetReq = {
          _id: selectedAssets[index]._id,
          mvDartZone: selectedZone
        };
        assetUpdateReq.push(assetReq);
      }

      function onAssetUpdateSuccess(response) {
        $rootScope.loader.loading = false;
        unselectRows();
        SharedService.setSelectedAssets();
        PorticoSDService.updateAssetsInPoolManager(response);
        refreshDataFromPoolManager();
        NotifierService.success('MULTI_VENDOR_ZONE_SAVED_SUCCESSFULLY');
      }

      function onAssetUpdateError(error) {
        $rootScope.loader.loading = false;
        $log.error('Error in saving multi vendor zone', error);
        NotifierService.error('ERROR_IN_SAVING_MULTI_VENDOR_ZONE');
      }

      AssetService.updateAsset(assetUpdateReq).then(onAssetUpdateSuccess, onAssetUpdateError);
    };

    /*
     * On Supply change
     * @param Selected Row
     */
    $scope.onSupplyChange = function (row) {
      var asset;
      if (!row) {
        return;
      }

      $rootScope.loader.loading = true;
      if (!validateSelectedMvRows()) {
        asset = AssetService.getAsset(row.assetId);
        row.mvSupply = asset.mvSupply;
        $rootScope.loader.loading = false;
        return;
      }
      var selectedAssets = SharedService.getSelectedAssets(),
          selectedSupply = row.mvSupply, assetReq,
          assetUpdateReq = [];

      if (!(Array.isArray(selectedAssets) && selectedAssets.length)) {
        $log.error("Error in getting selected assets.");
        $rootScope.loader.loading = false;
        return;
      }

      selectedAssets = PorticoSDHelperService.getMvAssetsFromAssets(selectedAssets);
      if (!selectedAssets.length) {
        $log.error("Error in getting mv assets.");
        $rootScope.loader.loading = false;
        return;
      }

      for (var index = 0; index < selectedAssets.length; index++) {
        assetReq = {
          _id: selectedAssets[index]._id,
          mvSupply: selectedSupply
        };
        assetUpdateReq.push(assetReq);
      }

      function onAssetUpdateSuccess(response) {
        $rootScope.loader.loading = false;
        unselectRows();
        SharedService.setSelectedAssets();
        PorticoSDService.updateAssetsInPoolManager(response);
        refreshDataFromPoolManager();
        NotifierService.success('SUPPLIES_SAVED_SUCCESSFULLY');
      }

      function onAssetUpdateError(error) {
        $rootScope.loader.loading = false;
        $log.error('Error in saving Supplies', error);
        NotifierService.error('ERROR_IN_SAVING_SUPPLIES');
      }

      AssetService.updateAsset(assetUpdateReq).then(onAssetUpdateSuccess, onAssetUpdateError);
    };

    /*
     * On Service change
     * @param Selected Row
     */
    $scope.onServiceChange = function (row) {
      var asset;
      if (!row) {
        return;
      }

      $rootScope.loader.loading = true;
      if (!validateSelectedMvRows()) {
        asset = AssetService.getAsset(row.assetId);
        row.mvService = asset.mvService;
        $rootScope.loader.loading = false;
        return;
      }
      var selectedAssets = SharedService.getSelectedAssets(),
          selectedService = row.mvService, assetReq, mvVolBreakLevel,
          assetUpdateReq = [];

      if (!(Array.isArray(selectedAssets) && selectedAssets.length)) {
        $rootScope.loader.loading = false;
        $log.error("Error in getting selected assets.");
        return;
      }

      selectedAssets = PorticoSDHelperService.getMvAssetsFromAssets(selectedAssets);
      if (!selectedAssets.length) {
        $rootScope.loader.loading = false;
        $log.error("Error in getting mv assets.");
        return;
      }

      mvVolBreakLevel = selectedService ? MultiVendorService.getVolumeBreakLevel(selectedService) : "";
      for (var index = 0; index < selectedAssets.length; index++) {
        assetReq = {
          _id: selectedAssets[index]._id,
          mvService: selectedService,
          mvVolBreakLevel: mvVolBreakLevel
        };
        assetUpdateReq.push(assetReq);
      }

      function onAssetUpdateSuccess(response) {
        $rootScope.loader.loading = false;
        unselectRows();
        SharedService.setSelectedAssets();
        PorticoSDService.updateAssetsInPoolManager(response);
        refreshDataFromPoolManager();
        NotifierService.success("SERVICE_SAVED_SUCCESSFULLY");
      }

      function onAssetUpdateError(error) {
        $rootScope.loader.loading = false;
        $log.error('Error in saving service', error);
        NotifierService.error('ERROR_IN_SAVING_SERVICES');
      }

      AssetService.updateAsset(assetUpdateReq).then(onAssetUpdateSuccess, onAssetUpdateError);
    };

    /**
     * To Initialise all the HW Disposition combo in tabular view and on disposition selection
     * @param Selected Row
     */
    function initialiseCombo(row) {
      var selectedDisposition;
      if (row.selection && $scope.isAllAssetsSelected && PorticoSDDispositionService.getHasOutcome(SharedService.getSelectedAssets())) {
        initialiseMultipleSelectedRows();
        return;
      }
      row.initHwDispositions = PorticoSDService.getDispositionList(row.hwDisposition);
    }

    /**
     * To get the disposition value of selectedDisposition
     * @param selectedDisposition
     */
    $scope.getDisposition = function (selectedDisposition, subDisposition) {
      if (selectedDisposition === OutcomeType.RETAIN && subDisposition === PorticoSDConfiguration.ADVANCE_PLACEMENT) {
        return PorticoSDConfiguration.RETAIN_ADVANCE_PLACEMENT;
      } else {
        return PorticoSDHelperService.getTranslatedOutcomeLabel(selectedDisposition);
      }
    };

    /*
     * Function to register the gridAPI object to work with grid events.
     */
    function onRegisterApi(gridApi) {
      let projectSettings = ProjectSetting.getProjectSettings(projectId);
      $scope.gridApi = gridApi;
      $scope.gridApi.grid.registerDataChangeCallback(function () {
        if (!isCustomFilterApplied) {
          $rootScope.$broadcast(PorticoSDEvent.UPDATE_TABULAR_VIEW_HEIGHT);
          PorticoSDService.setAggregations($scope.gridApi, projectId);
          refreshDataFromPoolManager();
          openSelectedLevel();
        }
        isCustomFilterApplied = false;
      });
      //Will be called whenever filtered is Appied/removed and row expanded/collapsed
      $scope.gridApi.core.on.rowsRendered($scope, onRowsRendered);

      // updating the tabular setting.
      function updateTabularTemplate(templateData) {

        if (!(projectSettings && $scope.tabularTemplate.selectedTemplate)) {
          NotifierService.Error("ERROR_WITH_TABULAR_TEMPLATE");
          return;
        }

        let updateTemplate = {
          'templateName': projectSettings.pricingSettings.pricingSource,
          'templateData': templateData
        };

        TabularViewSettingService.updateTemplate(updateTemplate, projectId).then(function () {
          //Nothing to handle
        }, function (error) {
          NotifierService.error("ERROR_ON_SAVING_USER_SETTINGS");
        });
      }

      /*
       * Function to notify the user about the error on changing the column properties
       */
      function errorOnColumnPropertyChange() {
        NotifierService.error("ERROR_ON_SAVING_USER_SETTINGS");
      }

      /*
       * Function to update the column visible property
       */
      function updateColumnVisibility(column) {
        if (column && column.name) {
          let templateData = PorticoSDSharedService.getTemplateData($scope.tabularTemplate.selectedTemplate);
          let index = $scope.gridApi.saveState.save().columns.findIndex(function (col) {
            return col.name === column.name
          });
          if (index) {
            templateData[index].visible = column.visible;
            updateTabularTemplate(templateData);
          } else {
            errorOnColumnPropertyChange()
          }
        } else {
          errorOnColumnPropertyChange();
        }
      }

      /*
       * Function to update the column width property
       */
      function updateColumnWidth(column) {
        if (column && column.name) {
          let templateData = PorticoSDSharedService.getTemplateData($scope.tabularTemplate.selectedTemplate);
          let index = $scope.gridApi.saveState.save().columns.findIndex(function (col) {
            return col.name === column.name
          });
          if (index >= 0) {
            templateData[index].width = $scope.gridApi.saveState.save().columns[index].width;
            updateTabularTemplate(templateData);
          } else {
            errorOnColumnPropertyChange()
          }
        } else {
          errorOnColumnPropertyChange();
        }
      }

      /*
       * Function to update the column position
       */
      function updateColumnPosition(column) {
        let templateData = PorticoSDSharedService.getTemplateData($scope.tabularTemplate.selectedTemplate),
            fromPosition = templateData.findIndex(function (col) {
              return col.name === column.name
            }),
            toPosition = $scope.gridApi.saveState.save().columns.findIndex(function (col) {
              return col.name === column.name
            }),
            updatedColumn = templateData.splice(fromPosition, 1),
            nonDraggableColumnsLength = Object.keys(PorticoSDConfiguration.TABULAR_VIEW_TEMPLATE_SETTINGS.NON_DRAGGABLE_COLUMNS).length;
        toPosition = toPosition <= nonDraggableColumnsLength ? nonDraggableColumnsLength : toPosition;
        toPosition = toPosition > fromPosition ? toPosition : toPosition - 1;
        templateData = templateData.slice(0, toPosition).concat(updatedColumn).concat(templateData.slice(toPosition));
        updateTabularTemplate(templateData);
      }

      $scope.gridApi.colMovable.on.columnPositionChanged($scope, updateColumnPosition);
      $scope.gridApi.colResizable.on.columnSizeChanged($scope, updateColumnWidth);
      $scope.gridApi.core.on.columnVisibilityChanged($scope, updateColumnVisibility);

      // restore the default state of tabular column.
      gridApi.grid.clearAllFilters = function () {
        this.columns.forEach(function (column) {
          column.sort = {};
          column.filters.forEach(function (filter) {
            filter.term = undefined;
          });
        });
        blankedColumns = {};
        $scope.customFilterColumns = {};
        $scope.appliedCustomFilters = {};
        isCustomFilterApplied = true;
        updateGridColumns();
      };
    }


    /**
     * Including  custom filter in Columndef of Tabular data
     * @param columnName
     */
    function getCustomMenuItems(columnName) {
      var filters = [
        {
          title: translateFilter('BLANKS'),
          icon: 'ui-grid-icon-filter',
          shown: function () {
            if (!$scope.appliedCustomFilters[columnName]) {
              return !blankedColumns[columnName];
            } else {
              return false;
            }
          },
          action: function ($event) {
            blankedColumns[columnName] = columnName;
            refreshDataFromPoolManager();
            if (!($scope.tabularViewPorticoSD.data.length)) {
              $scope.noDataFlag = true;
            }
          },
          context: $scope
        },
        {
          title: translateFilter('BLANKS_REMOVE'),
          icon: 'ui-grid-icon-cancel',
          shown: function () {
            return blankedColumns[columnName];
          },
          action: function ($event) {
            $scope.noDataFlag = false;
            delete blankedColumns[columnName];
            refreshDataFromPoolManager();
          },
          context: $scope
        },
        {
          title: translateFilter('REMOVE_CUSTOM_FILTER'),
          icon: 'ui-grid-icon-cancel',
          shown: function () {
            return $scope.customFilterColumns[columnName];
          },
          action: function ($event) {
            delete $scope.customFilterColumns[columnName];
            delete $scope.appliedCustomFilters[columnName];
            isCustomFilterApplied = true;
            refreshDataFromPoolManager();
          },
          context: $scope
        }
      ];

      switch (columnName) {
        case PorticoSDConfiguration.PORTICOSD_GRID_COLUMNS.a3Percentage:
        case PorticoSDConfiguration.PORTICOSD_GRID_COLUMNS.colorA3Percentage:
        case PorticoSDConfiguration.PORTICOSD_GRID_COLUMNS.monoA3Percentage:
        case PorticoSDConfiguration.PORTICOSD_GRID_COLUMNS.age:
        case PorticoSDConfiguration.PORTICOSD_GRID_COLUMNS.lifeTimeEngineLife:
        case PorticoSDConfiguration.PORTICOSD_GRID_COLUMNS.eoslPast:
          filters.push(setCustomFilter(columnName));
          break;
        default:
      }
      return filters;
    }

    /**
     * Setting up custom filter in Columndef of Tabular data
     * @param columnName
     * @returns {{title: string, icon: string, action: action, context: *}}
     */
    function setCustomFilter(columnName) {
      var customFilterOption = {
        title: translateFilter('CUSTOM_FILTER'),
        icon: 'glyphicon glyphicon-cog',
        shown: function () {
          if (!blankedColumns[columnName]) {
            return !$scope.customFilterColumns[columnName];
          } else {
            return false;
          }
        },
        action: function ($event) {
          $scope.filterData = {
            name: translateFilter(columnName),
            key: columnName,
            filters: PorticoSDConfiguration.COLUMN_FILTERS[columnName],
            type: 'number',
            appliedFilter: '',
            value: '',
            popupFlag: true
          };
          $scope.togglePopupFlag(PorticoSDPopupName.CUSTOM_FILTERS);
          $scope.customFilterColumns[columnName] = columnName;
        },
        context: $scope
      };
      return customFilterOption;
    }

    /**
     * Bind Tco category
     */
    function getTCOColumnCategories() {
      $scope.tcoCategories = PorticoSDConfiguration.TCO_CATEGORIES;
    }

    function updateAbbreviationData() {
      PorticoSDService.updateAssetAbbreviationInPoolManager(ProjectSetting.isAssetAbbreviationEnabled());
      refreshDataFromPoolManager();
    }

    /**
     * Set the tco category status
     */
    function setTcoCategoryStatus() {
      var categoriesActiveStatus = TcoIntegrationHelperService.getCategoriesActiveStatus($scope.tabularViewPorticoSD.columnDefs);
      $scope.tcoCategories.forEach(function (category) {
        category.active = categoriesActiveStatus[category.id];
      });
    }

    /**
     * Function is responsible for update all the setting related of tabular columns and load the portico tabular data also.
     */
    function updateGridColumns() {
      setupTabularViewColumns();
      // updating the tabular data.
      var tabularData = TabularViewStateBehaviourService.getDefaultDataForState(SharedService.activeState());
      if (tabularData && tabularData.length) {
        $scope.tabularViewPorticoSD.data = tabularData;
        $rootScope.tabularViewLoaded = true;
      }
      getTCOColumnCategories();
      setTcoCategoryStatus();
      openSelectedLevel();
    }

    /**
     * Function to Apply disposition
     * @param option
     */
    function onApplyDisposition(option) {
      outcomeOption(option);
    }

    /**
     * To handle the selected outcome option operation
     * @param option
     */
    function outcomeOption(option) {
      //TODO: Multiple feature not implemented for specific option, so till that we also need a single asset.
      var hasOutcome, selectedAssets = SharedService.getSelectedAssets();
      // handling multiple/single selected asset.
      hasOutcome = PorticoSDDispositionService.getHasOutcome(selectedAssets);
      applyOutcome(selectedAssets, option, hasOutcome);
    }

    /**
     * Applies Outcome to Single/multiple assets.
     * @param selectedAssets
     * @param outcomeOption
     * @param hasOutcome
     */
    function applyOutcome(selectedAssets, outcomeOption, hasOutcome) {

      if (!hasOutcome) {
        switch (outcomeOption) {
          case OutcomeType.RETIRE:
            retireAssets(selectedAssets);
            break;
          case OutcomeType.RETAIN:
            retainAssets(selectedAssets);
            break;
          case PorticoSDConfiguration.RETAIN_ADVANCE_PLACEMENT:
            retainAssets(selectedAssets, PorticoSDConfiguration.ADVANCE_PLACEMENT);
            break;
          case OutcomeType.REPOSITION:
            //To Do : Below commented code be uncommented to implement REPOSITION Disposition
            //  if (assets.length) {
            //  NotifierService.info('REPOSITION_NOT_ALLOWED_WITH_MULTI_SELECT');
            // } else {
            //  repositionToSameFloor(selectedAsset);
            // }
            break;
          case OutcomeType.REPOSITION_TO:
            //To Do : Below commented code be uncommented to implement REPOSITION_TO Disposition
            //  if (assets.length) {
            // NotifierService.info('REPOSITION_NOT_ALLOWED_WITH_MULTI_SELECT');
            // } else {
            // repositionToOtherFloor(selectedAsset);
            // }
            break;
          case OutcomeType.REPLACE_ONE_FOR_ONE:
            replaceAssets(PorticoSDConfiguration.OutcomeSubTypes.OneForOne, selectedAssets);
            break;
          case OutcomeType.REPLACE_MANY_TO_ONE:
            replaceAssets(PorticoSDConfiguration.OutcomeSubTypes.ManyToOne, selectedAssets);
            break;
          case OutcomeType.EXCLUDE_DEVICE:
            excludeAssetFromDesign(selectedAssets);
            break;
          default:
            // TODO: handle other outcome options click event
            break;
        }
      }
      //To Do : Below commented code be uncommented to implement RESET Disposition
      else if (TransitionHelper.isReset(outcomeOption)) {
        //  for resetting the outcomes.
        resetAssets(selectedAssets);
      }
      // We need to unselect the asset only when outcome is replace and reposition becuase in this
      // case will highlight newly added asset from replace and reposition outcome
      // if (option === OutcomeType.REPOSITION || option === OutcomeType.REPLACE) {
      //   SharedService.removeMultipleAssetsHighlights();
      //   SharedService.setSelectedAssets(null);
      // }

      unselectRows();
    }

    /**
     * To apply retire disposition for single/multiple assets.
     * @param selectedAssets
     */
    function retireAssets(selectedAssets) {
      var command = RetireCommand.getInstance({assets: selectedAssets});
      command.execute();
    }

    /**
     * To apply retain disposition for single/multiple assets.
     * @param selectedAssets
     * @param subType
     */
    function retainAssets(selectedAssets, subType) {
      var command = RetainCommand.getInstance({assets: selectedAssets, subType: subType});
      command.execute();
    }

    /**
     * To apply exclude device disposition for single/multiple assets.
     * @param assets
     */
    function excludeAssetFromDesign(assets) {
      var command = ExcludeCommand.getInstance({assets: assets});
      command.execute();
    }

    /**
     * To Apply Reset disposition on  selected single/multiple assets
     * @param selectedAssets
     */
    function resetAssets(selectedAssets) {
      var selectAssetIds = PorticoSDDispositionService.filterResetAssets(selectedAssets);
      if (selectAssetIds.length) {
        var command = ResetCommand.getInstance({assetIds: selectAssetIds});
        command.execute();
      } else {
        $log.info("Nothing to Reset");
      }
    }

    /**
     * Function is responsible for filtering the reset outcomes based on the outcome type and also define a function
     * which is responsible for resetting particular outcome.
     * @param assets {@code CanvasAsset} array of canvasAsset.
     * @return {{}} resetOutcomes, It's a key value pair object containing the particular assets and responsible function.
     */
    function filterResetOutcomes(assets) {
      var outcome, resetOutcomes = {},
          assetsWithNoOutcome = [];
      // created a key value pair of filtering the outcomes for reset.
      resetOutcomes[reset.VIRTUAL_ASSET] = {assets: [], resetOutcome: resetVirtualAssets};
      //To Do will uncomment the below lines while implementing REPLACE and REPOSITION
      resetOutcomes[reset.REPLACE_ASSET] = {assets: [], resetOutcome: resetReplaceAssets};
      resetOutcomes[reset.REPOSITION_ASSET] = {assets: [], resetOutcome: resetRepositionAssets};
      resetOutcomes[reset.OUTCOME_ASSET] = {assets: [], resetOutcome: resetAssetsOutcome};

      assets.forEach(function (asset) {
        if (asset.hasOutcome()) {
          outcome = asset.getOutcome();
          if (TransitionHelper.isReplaceWithNew(outcome)) {
            resetOutcomes[reset.REPLACE_ASSET].assets.push(asset);
          } else {
            switch (outcome) {
              case OutcomeType.ADD:
                resetOutcomes[reset.VIRTUAL_ASSET].assets.push(asset);
                break;
              case OutcomeType.REPOSITION:
              case OutcomeType.REPOSITION_FROM:
                resetOutcomes[reset.REPOSITION_ASSET].assets.push(asset);
                break;
              default:
                // In this case mainly dealing with RETIRE & RETAIN outcome as of now, but later on we can more extend.
                resetOutcomes[reset.OUTCOME_ASSET].assets.push(asset);
                break;
            }
          }
        } else {
          assetsWithNoOutcome.push(asset);
        }
      });

      if (assetsWithNoOutcome && assetsWithNoOutcome.length) {
        PorticoSDService.updateAssetsInPoolManager(assetsWithNoOutcome);
      }
      return resetOutcomes;
    }

    /**
     * Responsible to Delete accessories
     * @param assetIds for which we need to delete accessories
     */
    function deleteAccessories(assetIds) {
      var accessoriesDelRequest = {
        assetIds: assetIds
      };
      DeviceAccessoriesService.deleteAccessories(accessoriesDelRequest).then(function (response) {
        $rootScope.$broadcast(ViewEvent.ALL_ASSET_ACCESSORIES_DELETED, assetIds);
      }, function (error) {
        $log.error('unable to delete accessories', error);
      });
    }

    /**
     * Function will be triggered, when we reset the position of selected assets.
     * @param assets
     */
    function resetRepositionAssets(assets) {
      // success callback
      function onResetRepositionSuccess(response) {
        var assetsTransfers;

        // check transfers is having with the selected asset.
        if (response.transfersIds.length) {
          assetsTransfers = {
            transfersIds: response.transfersIds,
            assets: response.updatedAssets
          };
          // broadcasting the transfer delete event
          $rootScope.$broadcast(ViewEvent.TRANSFER_DELETE, assetsTransfers);
        }
        $log.info('Resetting reposition assets successfully.');
        // Update / Remove assets in pool manager
        if (response.deletedAssetIds && response.deletedAssetIds.length) {
          deleteAccessories(response.deletedAssetIds);
          PorticoSDService.setTcoPricingForRemovedAssets(response.deletedAssetIds);
          PorticoSDService.removeAssetInPoolManager(response.deletedAssetIds);
        }
        if (response.updatedAssets && response.updatedAssets.length) {
          response.updatedAssets.forEach(function (asset) {
            PorticoSDService.updateAssetInPoolManager(asset);
          });
        }
        refreshDataFromPoolManager();
      }

      // error callback
      function onResetRepositionError(error) {
        $log.error('Error while resetting reposition assets ', error);
      }

      PorticoSDDispositionService.resetRepositionAssets(assets).then(onResetRepositionSuccess, onResetRepositionError);
    }

    /**
     * Function will be triggered, when we reset the virtual assets.
     * @param virtualAssets {@Code virtualAssets} array of virtualAssets.
     */
    function resetVirtualAssets(virtualAssets) {
      //On Sucess Callback
      function onRemoveVirtualSuccess(response) {
        var assetsTransfers;
        // check transfers is having with the selected asset.
        if (response.transfersIds.length) {
          assetsTransfers = {
            transfersIds: response.transfersIds,
            assets: response.assets
          };

          // broadcasting the transfer delete event
          $rootScope.$broadcast(ViewEvent.TRANSFER_DELETE, assetsTransfers);
        }
        $rootScope.$broadcast(ViewEvent.VIRTUAL_ASSET_RESET, virtualAssets);
        // Remove assets in pool manager
        if (response.deleteAssets && response.deleteAssets.length) {
          PorticoSDService.removeAssetInPoolManager(response.deleteAssets);
        }
        refreshDataFromPoolManager();
      }

      //On Error Callback
      function onRemoveVirtualError(error) {
        $log.error('Error while resetting assets ', error);
      }

      PorticoSDDispositionService.resetVirtualAssets(virtualAssets).then(onRemoveVirtualSuccess, onRemoveVirtualError);
    }

    /**
     * Function is responsible for reset the outcome of an given assets.
     * Mainly it will reset Retain & Retire outcome.
     * @param assets {@code assets} array of assets.
     */
    function resetAssetsOutcome(assets) {
      // success callback, for updating the canvas asset after getting response.
      function onAssetsResetSuccess(response) {
        var updatedOutcomeData,
            resetOutcomes = {};
        response.assets.forEach(function (asset) {
          //As backend only returning outcome ids so we need to find it in request.
          var updateOutcomes = response.updateOutcomes;
          var outcomeByAssetId = updateOutcomes.find(function (outcome) {
            return asset.getId() === outcome.assetId;
          });

          if (outcomeByAssetId) {
            if (TransitionHelper.isExcludeFromDesign(outcomeByAssetId.outcome)) {
              asset.excludeFromDesign = false;
              AssetService.updateAsset([asset]).then(function onUpdateSuccess(assets) {
                $rootScope.$broadcast(ViewEvent.ASSET_UPDATE, asset);
              });
            }
            // creating information for broadcasting the reset outcome event.
            resetOutcomes[outcomeByAssetId._id] = {
              outcome: outcomeByAssetId.outcome,
              canvasAsset: asset
            };
          }
        });
        $log.info('Assets are successfully reset.');
        // Update assets in pool manager
        if (response.assets && response.assets.length) {
          response.assets.forEach(function (asset) {
            PorticoSDService.updateAssetInPoolManager(asset);
          });
        }
        refreshDataFromPoolManager();
        updatedOutcomeData = {
          resetOutcomes: resetOutcomes
        };
        $rootScope.$broadcast(ViewEvent.OUTCOME_RESET, updatedOutcomeData);
        SharedService.setSelectedAssets();
      }

      // error callback
      function onAssetsResetError(error) {
        $log.error('Error while resetting asset ', error);
      }

      PorticoSDDispositionService.resetAssetsOutcome(assets).then(onAssetsResetSuccess, onAssetsResetError);
    }

    /**
     * Function will be triggered, when we reset the replace assets.
     * @param assets {@Code assets} array of assets.
     */
    function resetReplaceAssets(assets) {
      // success callback
      function onResetReplaceSuccess(response) {
        var updatedOutcomeData, assetsTransfers, asset, assetLabel, assetId, resetOutcome = {};

        // updating the asset on canvas
        function updateAsset(asset) {
          var assetId = asset.getId();
          resetOutcome[assetId] = {
            outcome: asset.getOutcome(),
            canvasAsset: asset
          };
        }

        // to remove the assets from canvas whose coordinates are null or undefined
        function removeRealAssetFromCanvas(lem) {
          if (!lem.coordinates) {
            assetId = lem.entityId;
            asset = AssetService.getAsset(assetId);
            if (asset && asset.isReal()) {
              resetOutcome[assetId] = {
                outcome: asset.getOutcome(),
                canvasAsset: asset,
                isReplacedWithExisting: true
              };
            }
          }
        }

        // for updating the assets on canvas
        response.updatedAssets.forEach(updateAsset);
        if (response.updateLevelEntityMap.length) {
          response.updateLevelEntityMap.forEach(removeRealAssetFromCanvas);
        }
        updatedOutcomeData = {
          resetOutcomes: resetOutcome
        };
        //To DO :: Here OUTCOME_RESET event should be broadcasted and process with pool manager
        $rootScope.$broadcast(ViewEvent.OUTCOME_RESET, updatedOutcomeData);
        // check transfers is having with the selected asset.
        if (response.transfersIds.length) {
          assetsTransfers = {
            transfersIds: response.transfersIds,
            assets: response.updatedAssets
          };

          // broadcasting the transfer delete event
          $rootScope.$broadcast(ViewEvent.TRANSFER_DELETE, assetsTransfers);
        }
        SharedService.setSelectedAssets();
        $log.info('Resetting replace assets successfully.');
        // Update/Remove assets in pool manager
        if (response.deletedAssetIds && response.deletedAssetIds.length) {
          PorticoSDService.setTcoPricingForRemovedAssets(response.deletedAssetIds);
          PorticoSDService.removeAssetInPoolManager(response.deletedAssetIds);
        }
        if (response.updatedAssets && response.updatedAssets.length) {
          response.updatedAssets.forEach(function (asset) {
            PorticoSDService.updateAssetInPoolManager(asset);
          });
        }
        refreshDataFromPoolManager();
      }

      // error callback
      function onResetReplaceError(error) {
        $log.error('Error while resetting asset ', error);
      }

      PorticoSDDispositionService.resetReplaceAssets(assets).then(onResetReplaceSuccess, onResetReplaceError);
    }

    /**
     * function to replace assets one for many and one for one.
     * @param subType
     * @param selectedAssets
     */
    function replaceAssets(subType, selectedAssets) {
      SharedService.selectedAsset(currentSelectedAsset);
      var countryInfo = SharedService.getCurrentLevel("COUNTRY"),
          region = SharedService.getRegionFromCountrySet(countryInfo.name),
          request = {
            assetIds: SharedService.getSelectedAssetIds(),
            projectId: $routeParams.id,
            parentAssetId: (currentSelectedAsset && currentSelectedAsset.getId()),
            countryCode: region.countryCode,
            region: region.region,
            subType: subType
          };
      var command = ReplaceCommand.getInstance([request]);
      command.execute();
    }

    /**
     *Set multiple selected rows
     */
    function setSelectedRows(floorData) {
      var isAssetsInSameFloor,
          selectedAssets = [];
      try {
        SharedService.setSelectedAssets();
        $scope.selectedRows = [];
        if (floorData) {
          for (var rowIndex = 0; rowIndex < floorData.length; rowIndex++) {
            if (floorData[rowIndex].selection) {
              var asset = AssetService.getAsset(floorData[rowIndex].assetId);
              SharedService.setSelectedAssets(asset);
              selectedAssets.push(asset);
              $scope.selectedRows.push(floorData[rowIndex]);
            }
          }
        }
        //Initlaiise combo box in case of mutiple selected assets
        if (PorticoSDDispositionService.getHasOutcome(selectedAssets)) {
          initialiseMultipleSelectedRows();
        } else {
          isAssetsInSameFloor = PorticoSDDispositionService.isInSameFloor(selectedAssets);
          $scope.selectedRows.forEach(function (row) {
            initialiseCombo(row);
            if (isAssetsInSameFloor && selectedAssets.length > 1) {
              addReplaceManyToOneDisposition(row);
            } else if (!isAssetsInSameFloor) {
              removeReplaceManyToOneDisposition(row);
            }
            if (selectedAssets.length === 1) {
              addRepositionDisposition(row);
            } else if (selectedAssets.length > 1) {
              removeRepositionDisposition(row);
            }
          });
        }
      } catch (ex) {
        $log.error('Error in setting multiple selected rows & initializing combo box ', ex);
      }
    }

    /**
     * To add 'ReplaceManyToOne' option in the hwDisposition combo
     * @param selectedRows
     */
    function addReplaceManyToOneDisposition(row) {
      row.initHwDispositions.replaceManyToOne = PorticoSDHelperService.getTranslatedOutcomeLabel(OutcomeType.REPLACE_MANY_TO_ONE);
    }

    /**
     * To remove 'ReplaceManyToOne' option from the hwDisposition combo
     * @param selectedRows
     */
    function removeReplaceManyToOneDisposition(row) {
      delete row.initHwDispositions.replaceManyToOne;
    }

    /**
     * To add 'Reposition' option in the hwDisposition combo
     * @param selectedRows
     */
    function addRepositionDisposition(row) {
      row.initHwDispositions.reposition = PorticoSDHelperService.getTranslatedOutcomeLabel(OutcomeType.REPOSITION);
    }

    /**
     * To add 'Replace One For One' option in the hwDisposition combo
     * @param selectedRows
     */
    function addReplaceOneForOneDisposition(row) {
      row.initHwDispositions.replaceOneForOne = PorticoSDHelperService.getTranslatedOutcomeLabel(OutcomeType.REPLACE_ONE_FOR_ONE);
    }

    /**
     * To remove 'Reposition' option from the hwDisposition combo
     * @param selectedRows
     */
    function removeRepositionDisposition(row) {
      delete row.initHwDispositions.reposition;
    }

    /**
     * To remove 'Replace One For One' option from the hwDisposition combo
     * @param selectedRows
     */
    function removeReplaceOneForOneDisposition(row) {
      delete row.initHwDispositions.replaceOneForOne;
    }

    /**
     * Initilise combo for multiple selected rows
     * @param rows
     */
    function initialiseMultipleSelectedRows() {
      if ($scope.selectedRows && $scope.selectedRows.length) {
        var selectedRowsLength = $scope.selectedRows.length;
        for (var selectedRowsIndex = 0; selectedRowsIndex < selectedRowsLength; selectedRowsIndex++) {
          var selectedDisposition = $scope.selectedRows[selectedRowsIndex].hwDisposition;
          $scope.selectedRows[selectedRowsIndex].initHwDispositions = {};
          $scope.selectedRows[selectedRowsIndex].initHwDispositions.reset = PorticoSDHelperService.getTranslatedOutcomeLabel(OutcomeType.RESET);
          if (selectedDisposition) {
            $scope.selectedRows[selectedRowsIndex].initHwDispositions[selectedDisposition] = PorticoSDHelperService.getTranslatedOutcomeLabel(selectedDisposition);
          }
        }
      }
    }


    /**
     * to uncheck all the selected assets when PVT is enabled
     * @param $event
     * @param isPvtEnabled : true if PVT is enabled as false
     */
    function onPvtEnable($event, isPvtEnabled) {
      $scope.flagPageVolumeEnabled = isPvtEnabled;
      $scope.isAllAssetsSelected = false;
      unselectRows();
      SharedService.setSelectedAssets();
    }

    /**
     * function to handle the PVT transfer click on Pvt-in and Pvt-Out columns
     * @param pvtId
     */
    $scope.pvtTransferClick = function (pvtId) {
      var transfer = PageVolumeService.getPageVolume(pvtId);
      if (transfer) {
        var sourceAsset = AssetService.getAsset(transfer.fromAssetId),
            destinationAsset = AssetService.getAsset(transfer.toAssetId);
        var assets = {
          source: sourceAsset,
          destination: destinationAsset,
          transfer: transfer
        };
        $rootScope.$broadcast(ViewEvent.TABULAR_ASSET_PVT_CLICK, assets);
        safeApply($scope, function () {
          $scope.isPageVolumePopupVisible = true;
        });
      }
    };

    /**
     * to hide the Page Volume Popup
     */
    function onPopupHide($event, data) {
      safeApply($scope, function () {
        $scope.isPageVolumePopupVisible = false;
      });
      // Resetting all custom filters if it exists
      if (data && data === PorticoSDPopupName.CUSTOM_FILTERS && !$scope.appliedCustomFilters[Object.keys($scope.customFilterColumns).last()]) {
        delete $scope.customFilterColumns[Object.keys($scope.customFilterColumns).last()];
      }
    }

    /**
     * UnSelect selected Asset Rows
     */
    function unselectAssets() {
      $scope.selectedRows.forEach(function (data) {
        if (data.selection) {
          data.selection = false;
        }
      });
      $scope.selectedRows = [];
    }

    /**
     * UnSelect selected Levels Row
     */
    function unselectLevels() {
      selectedLevels.forEach(function (data) {
        if (data.selection) {
          data.selection = false;
        }
      });

      selectedLevels = [];
    }


    /**
     * Unselect all the row Selection checkbox from the tabular UI grid
     */
    function unselectRows() {
      if ($scope.selectedRows && $scope.selectedRows.length) {
        unselectAssets();
      }
      if (selectedLevels && selectedLevels.length) {
        unselectLevels();
      }
    }

    /*** it will take source assets and destination Asset and transfer all the page volumes.
     * @param sourceAssets
     * @param destinationAsset
     */
    function replaceAssetPvtTransfer(sourceAssets, destinationAsset) {
      var request = RequestBuilder.getReplacePageVolumeRequest(sourceAssets, destinationAsset),
          selectedAssetRequest = request.assets[destinationAsset.getId()];

      // error callback closure.
      function onError(error) {
        return function (message) {
          if (message) {
            NotifierService.error(SharedService.getErrorMessage(error, message));
          }
        };
      }

      // To prevent recursive call we are sending isManualAssetAssignFlag False and true for HighLighting
      // Asset after Asset update in case of replace
      // success page volume callback
      function onSaveTransferSuccess(response) {
        $rootScope.$broadcast(ViewEvent.TRANSFER_UPDATE, response.assets);
      }

      // have transfers request.
      if (request.transfersRequest.length) {
        // creating new transfer
        PageVolumeMapService.saveTransfer(request.transfersRequest, Utilis.getArrayByKey(request.assets))
            .then(onSaveTransferSuccess, onError('UNABLE_TO_SAVE_TRANSFER'));
      } else if (selectedAssetRequest) {
        AssetService.updateAsset([selectedAssetRequest]).then(function (response) {
          $rootScope.$broadcast(ViewEvent.ASSET_UPDATE, response.first());
        }, onError('UNABLE_TO_UPDATE_AN_ASSET'));
      }
    }

    /***
     * This function will update the Sub-EnvId of source assets based on destination mapId.After assigne of new/Existing
     * asset.
     * @param sourceAssets
     * @param destinationAsset
     */
    function updateSourceAssetMapId(sourceAssets, destinationAsset) {
      if (sourceAssets.length) {
        sourceAssets.forEach(function (asset) {
          asset.subEnvId = destinationAsset.mapID;
        });
        AssetService.updateAsset(sourceAssets).then(function (response) {
          $log.info('sub-EnvId of source assets has been updated');
        }, function (error) {
          $log.info('unable to update the sub-EnvId');
        });
      }
    }


    /**
     * CAll back on Rows rendered in grid
     */
    function onRowsRendered() {
      if ($scope.isAllAssetsSelected) {
        try {
          var tabularData, tabularAssets, selectedLevelsData;
          $rootScope.loader.loading = true;
          unselectRows();
          // Get the data which is appear on the tabular view. If any filter is applied then it will give us filtered data.
          if ($scope.gridApi) {
            tabularData = TabularDataManagerService.getTabularFilteredData($scope.gridApi.grid.rows);
          }
          if (tabularData && tabularData.length) {
            // Select all the tabular data
            TabularDataManagerService.setTabularDataRowSelection(tabularData, true);
            // This will give us all the assets from the floors.
            tabularAssets = filterFilter(tabularData, PorticoSDHelperService.extractFloorData);
            selectedLevels = PorticoSDHelperService.getSelectedLevels(tabularData, null, $scope.isAllAssetsSelected);
            setSelectedRows(tabularAssets);
          }
          $rootScope.loader.loading = false;
        } catch (ex) {
          $rootScope.loader.loading = false;
          $log.error('Error in selecting the tabular data ', ex);
        }
      }
    }

    /**
     * Updates Tabular View with pricing data
     * @param $event
     * @param dartPricingData
     */
    function populatePricingData($event, source, dartPricingData) {
      if (dartPricingData && source === PorticoSDConfiguration.PRICING_SOURCE.DART) {
        PorticoSDService.updatePricingData(dartPricingData, projectId);
        if ($scope.gridApi) {
          PorticoSDService.setAggregations($scope.gridApi, projectId);
        }
        refreshDataFromPoolManager();
      } else if (source === PorticoSDConfiguration.PRICING_SOURCE.TCO) {
        refreshTabularViewData();
      }
    }

    /**
     * Hide tco columns
     * @param selectedValue
     */
    function hideColumns(selectedValue) {
      $scope.tabularViewPorticoSD.columnDefs.forEach(function (col, index) {
        if (col.group && selectedValue[col.group] !== undefined) {
          $scope.tabularViewPorticoSD.columnDefs[index].visible = selectedValue[col.group];
        }
      });
      if ($scope.gridApi) {
        $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
        $scope.gridApi.grid.api.core.raise.columnVisibilityChanged();
      }
    }

    /**
     * On select category
     * @param itemIndex
     * @param event
     */
    $scope.selectCategoryItem = function (itemIndex, event) {
      var categoriesStatus;
      event.stopPropagation();
      $scope.tcoCategories[itemIndex].active = !$scope.tcoCategories[itemIndex].active;
      categoriesStatus = TcoIntegrationHelperService.getStatusFromCategories($scope.tcoCategories);
      hideColumns(categoriesStatus);
    };

    /**
     * Applying custom filters on the basis of user selection
     * @param filterData
     */
    $scope.applyCustomFilter = function (filterData) {
      var filterFlag = true,
          isFilterDataInValid;

      try {
        if (!filterData) {
          return;
        }

        isCustomFilterApplied = true;
        $rootScope.loader.loading = true;
        isFilterDataInValid = filterData.value === '' || filterData.value === undefined ||
            filterData.appliedFilter === '' || filterData.appliedFilter === undefined;

        if (isFilterDataInValid) {
          NotifierService.error('PLEASE_PROVIDE_REQUIRED_DATA_FOR_FILTER');
          $rootScope.loader.loading = false;
          return;
        }

        $scope.tabularViewPorticoSD.data = $scope.tabularViewPorticoSD.data.filter(function (row) {
          if (row.type === PorticoSDConfiguration.LEVEL || row.type === PorticoSDConfiguration.PROJECT) {
            return true;
          } else if (row.type === PorticoSDConfiguration.ASSET) {
            if (row[filterData.key] === "" || row[filterData.key] === '') {
              filterFlag = false;
              return filterFlag;
            }
            // Applying filter option on the basis of user selection
            filterFlag = PorticoSDHelperService.applyFilterOption(row[filterData.key], filterData);
            return filterFlag;
          } else if (row.type === PorticoSDConfiguration.ACCESSORY) {
            return filterFlag;
          }
        });
        if ($scope.gridApi) {
          $scope.gridApi.grid.refresh();
        }
        if (filterData.popupFlag) {
          $scope.togglePopupFlag(PorticoSDPopupName.CUSTOM_FILTERS);
        }
        $scope.appliedCustomFilters[filterData.key] = filterData;

        // Setting which custom filter is applied by user in the placeholder of column
        PorticoSDHelperService.applyPlaceholderOnColumn($scope.appliedCustomFilters, $scope.tabularViewPorticoSD, blankedColumns);
        $rootScope.loader.loading = false;
      } catch (ex) {
        $rootScope.loader.loading = false;
        $log.info('Error while applying the custom filter: ', ex);
      }
    };

    /**
     * Function is called when Reposition disposition is applied from map view we need to update the assets on tabular view
     * Based on outcome & outcome type
     * @param response
     */
    function onAssetRepositionFromMapView(response) {
      var outcome;
      response.updated.assets.forEach(function (asset) {
        asset = AssetService.getAsset(asset._id);
        PorticoSDService.updateAssetInPoolManager(asset);
      });
      response.added.assets.forEach(function (asset) {
        asset = AssetService.getAsset(asset._id);
        outcome = OutcomeService.getOutcomeByAssetId(asset._id) || {};
        if (TransitionHelper.isRepositionFrom(asset.outcome)) {
          PorticoSDService.addVirtualAssetInPoolManager(asset, outcome.levelId);
        } else {
          PorticoSDService.addVirtualAssetInPoolManager(asset, outcome.levelId, outcome.originalAssetId);
        }
      });
      refreshDataFromPoolManager();
    }

    /**
     * Function is called when replace disposition is applied from map view we need to update the assets on tabular view
     * Based on outcome & outcome type
     * @param response
     */
    function updateReplaceOneToOneAssetsInTabularView(response) {
      PorticoSDDispositionService.updatePoolManagerForOneForOne(response);
      refreshDataFromPoolManager();
    }


    /**
     * Function is called when replace Many To One disposition  is applied from map view we need to update the assets on
     * tabular view
     * Based on outcome & outcome type
     * @param response
     */
    function updateReplaceManyToOneAssetsInTabularView(response) {
      PorticoSDDispositionService.updatePoolManagerForManyToOne(response);
      refreshDataFromPoolManager();
    }

    /**
     * On Switch view
     * This call back will be called when we switch view from switch view icon
     */
    function onSwitchView() {
      $timeout(function () {
        $rootScope.tabularViewLoaded = true;
        if ($rootScope.isFloorMapView === false) {
          setupTabularViewColumns();
          refreshDataFromPoolManager();
        }
      }, 0);
    }

    /**
     * Function is called when we update asset outcome's (Retain, Retire, ) from map view
     * @param assets
     */
    function outcomeUpdateCallback(assets) {
      $log.info('Updating assets in tabular view....');
      PorticoSDService.updateAssetsInPoolManager(assets);
      refreshDataFromPoolManager();
    }


    /**
     * Function will be triggered when we apply outcome from map view
     * @param event
     * @param data
     */
    function onOutcomeApplyFromMapView(event, data) {
      switch (data.outcomeType) {
        case OutcomeType.RETAIN:
          outcomeUpdateCallback(data.assets);
          break;
        case OutcomeType.RETIRE:
          outcomeUpdateCallback(data.assets);
          break;
        case OutcomeType.EXCLUDE_DEVICE:
          outcomeUpdateCallback(data.assets);
          break;
        case PorticoSDConfiguration.OutcomeSubTypes.OneForOne:
          updateReplaceOneToOneAssetsInTabularView(data.response);
          break;
        case PorticoSDConfiguration.OutcomeSubTypes.ManyToOne:
          updateReplaceManyToOneAssetsInTabularView(data.response);
          break;
        case OutcomeType.REPOSITION_TO:
        case OutcomeType.REPOSITION:
          onAssetRepositionFromMapView(data.response);
          break;
      }
    }

    /**
     * Function will be triggered when we reset outcome from map view
     * @param event
     * @param response
     */
    function onOutcomeResetFromMapView(event, response) {
      PorticoSDService.setTcoPricingForRemovedAssets(response.deletedAssets);
      // Remove assets in pool manager
      PorticoSDService.removeAssetInPoolManager(response.deletedAssets);

      // Update assets in pool manager
      response.updated.assets.forEach(function (asset) {
        PorticoSDService.updateAssetInPoolManager(asset);
      });

      refreshDataFromPoolManager();
    }

    /**
     * On Pricing Setting updte
     */
    function onPricingSettingUpdate() {
      onTabularViewSettingUpdate();
    }

    /**
     * Update zone when zone is assigned to asset
     */

    function updateAssetZoneInTabularView() {
      PorticoSDService.updateAssetZoneInTabularView(AssetService.getUpdatedZoneAssetIds());
    }

    /**
     * The function will update the assets after the deal has been uploaded
     * @param $event
     * @param updatedAssets
     */
    function onDartDealUpload($event, updatedAssets) {
      if (updatedAssets && updatedAssets.length) {
        PorticoSDService.updateAssetsInPoolManager(updatedAssets);
        refreshDataFromPoolManager();
      }
    }

    /**
     * On Tabular view data load success
     */
    function onTabularViewDataLoadSuccess() {
      setupTabularViewColumns();
      setTcoCategoryStatus();
      refreshDataFromPoolManager();
    }

    $scope.openTcoAdjustmentPopup = function () {
      $rootScope.$broadcast(PorticoSDEvent.SHOW_TCO_ADJUSTMENT_POPUP);
    };

    /**
     * This function will broadcast an event to open tabular template settings pop-up
     */
    $scope.openTabularTemplateSettingPopUp = function () {
      $rootScope.$broadcast(PorticoSDEvent.SHOW_TABULAR_TEMPLATE_VIEW_SETTINGS_POPUP);
    };

    /**
     * To send DART Deal strategy change to PorticoSDService for storage
     * @param Selected Row
     */
    $scope.onUploadStrategyChange = function (countryRowData) {
      PorticoSDHelperService.saveDartUploadStrategy(countryRowData);
    };

    onLoad();

    $scope.$on(ViewEvent.ASSET_IMPORTED, onAssetImport);
    $scope.$on(ViewEvent.LEVEL_SELECTED, onLevelSelected);
    $scope.$on(ViewEvent.LEVEL_DELETE, onLevelDelete);
    $scope.$on(ViewEvent.LEVEL_UPDATE, onLevelUpdated);
    $scope.$on(ViewEvent.ASSET_MAP, onMapAsset);
    $scope.$on(ViewEvent.ASSET_UNMAP, onUnmapAsset);
    $scope.$on(ViewEvent.ASSET_UPDATE, onAssetUpdate);
    $scope.$on(ViewEvent.ASSET_ASSIGN, onAssetAssign);
    $scope.$on(ViewEvent.ASSET_ADD, onAssetAdd);
    $scope.$on(ViewEvent.ASSET_DELETED, onAssetDeleted);
    $scope.$on(ViewEvent.ASSET_REPLOT, onAssetReplot);
    $scope.$on(ViewEvent.ALL_ASSET_ACCESSORIES_DELETED, onAccessoriesDelete);
    $scope.$on(ViewEvent.ACCESSORY_ADDED, updateAssetAccessories);
    $scope.$on(ViewEvent.ACCESSORY_DELETED, updateAssetAccessories);
    $scope.$on(ViewEvent.STATE_CHANGE, onStateChange);
    $scope.$on(ViewEvent.PVT_ENABLE, onPvtEnable);
    $scope.$on(ViewEvent.POPUP_HIDE_MODAL, onPopupHide);
    $scope.$on(PorticoSDEvent.REFRESH_TABULAR_VIEW_POOL, refreshDataFromPoolManager);
    $scope.$on(ViewEvent.OUT_OF_SCOPE_SUCCESS, refreshTabularViewData);
    $scope.$on(ViewEvent.OUTCOME_APPLY, onOutcomeApplyFromMapView);
    $scope.$on(ViewEvent.OUTCOME_RESET, onOutcomeResetFromMapView);
    $scope.$on(ViewEvent.ON_SWITCH_VIEW, onSwitchView);
    $scope.$on(ViewEvent.IN_SCOPE_SUCCESS, refreshTabularViewData);
    $scope.$on(ViewEvent.UNPLOT_ASSET_SUCCESS, refreshTabularViewData);
    $scope.$on(ViewEvent.MATCH_TO_ALL, refreshTabularViewData);
    $scope.$on(ViewEvent.CDF_IMPORTED, refreshTabularViewData);
    $scope.$on(PorticoSDEvent.REPOSITION_APPLIED, onRepositionApplied);
    $scope.$on(ViewEvent.PRICING_SETTING_UPDATE, onPricingSettingUpdate);
    $scope.$on(ViewEvent.TABULAR_PRICING, populatePricingData);
    $scope.$on(ViewEvent.ZONE_ASSETS_UPDATE, updateAssetZoneInTabularView);
    $scope.$on(ViewEvent.ASSET_ZONE_UPDATED, updateAssetZoneInTabularView);
    $scope.$on(ViewEvent.UPDATE_TAG, updateAssetZoneInTabularView);
    $scope.$on(ViewEvent.DART_DEAL_UPLOAD, onDartDealUpload);
    $scope.$on(PorticoSDEvent.UPDATE_TABULAR_VIEW, refreshTabularView);
    $scope.$on(PorticoSDEvent.TABULAR_VIEW_DATA_LOAD_SUCCESS, onTabularViewDataLoadSuccess);
    $scope.$on(ViewEvent.ASSET_ABBREVIATION_SYNC, updateAbbreviationData);
  }

  var app = angular.module('app'),
      requires = [
        '$log',
        '$timeout',
        '$scope',
        '$rootScope',
        '$routeParams',
        'ViewEvent',
        'CanvasAsset',
        'filterFilter',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'ngCartosCanvas.constants.OutcomeType',
        'PorticoSDTemplatePaths',
        'SharedService',
        'ngCartosUtils.services.UtilityService',
        'ngPorticoSD.services.PorticoSDHelperService',
        'ngPorticoSD.services.PorticoSDService',
        'ngPorticoSD.services.TabularViewStateBehaviourService',
        'ngCartosCore.services.IconService',
        'ngCartosCore.services.LevelService',
        'ngCartosCore.services.AssetService',
        'ngPorticoSD.services.PorticoSDDispositionService',
        'translateFilter',
        'ngCartosCore.services.ProjectSettingService',
        'TransitionHelperService',
        'ngCartosCore.constants.AssetProperties',
        'ngCartosCore.constants.LevelType',
        'ngCartosCore.services.NotifierService',
        'RequestBuilder',
        'PorticoSDSharedService',
        'VolumeTransferHelperService',
        'ngCartosUtils.services.SafeApplyService',
        'ngCartosCore.services.PageVolumeMapService',
        'ngPorticoSD.constants.PorticoSDEvent',
        'ngCartosCore.services.PageVolumeService',
        'ngPorticoSD.constants.PorticoSDPopupName',
        'ngCartosCore.services.OutcomeService',
        'PricingService',
        'ngCartosCore.services.DeviceAccessoriesService',
        'ngPorticoSD.services.TabularDataManagerService',
        'uiGridConstants',
        'TcoPricingService',
        'TcoIntegrationHelperService',
        'SyncTcoPricingService',
        'Configuration',
        'ngCartosUndoRedo.commands.RetireCommand',
        'ngCartosUndoRedo.commands.RetainCommand',
        'ngCartosUndoRedo.commands.ReplaceCommand',
        'ngCartosUndoRedo.commands.ResetCommand',
        'ngCartosUndoRedo.commands.ExcludeCommand',
        'ngCartosUndoRedo.commands.PageVolumeApplyCommand',
        'ngPorticoSD.services.MultiVendorService',
        'PorticoSdUtilityService',
        'TabularViewSettingService',
        'ngPorticoSD.services.TabularViewDataService',
        'ngPorticoSD.constants.MultiVendorConfiguration',
        PorticoSDController
      ];
  app.controller('PorticoSDController', requires);
}());

/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /***
   * This controller is to show Report after Asset Import Success.
   * @param $scope
   * @param ViewEvent
   * @constructor
   */
  function PrescriptiveColMappingController($log, $rootScope, $routeParams, $scope, $window, ViewEvent, ColumnMappingService,
                                            ColumnMappingHtmlFactory, AssetService, UtilityService, SharedService,
                                            NotifierService, Config, translate, PorticoSDEvent,
                                            PrescriptiveService, PorticoSDPopupName, DeviceAccessoriesService) {

    var queryParams,
        TYPE = 'PURSUIT_PRESCRIPTIVE_FILE',
        unassigned = "unassigned",
        request = {},
        file,
        score = {}, highScore = {}, dcaHeaderMap = {}, mappedFields = {}, isTemplatesLoaded = false;



    /**
     * function to select the template with most number of matching headers.
     * sets a score object and a highScore object
     * @returns {*}
     */
    function getTemplateScores() {
      highScore = {};
      score = {};
      if ($scope.templates) {
        var index = $scope.templates.length;
        while (index--) {
          score[$scope.templates[index].name] =
              ColumnMappingService.getTemplateScore($scope.templates[index], dcaHeaderMap);
          if (!highScore.score) {
            highScore.score = score[$scope.templates[index].name];
            highScore.template = $scope.templates[index];
          } else {
            if (highScore.score < score[$scope.templates[index].name]) {
              highScore.score = score[$scope.templates[index].name];
              highScore.template = $scope.templates[index];
            }
          }
        }
      }
      return highScore.template;
    }

    /**
     * create an object from an array, helps with better performance.
     * @param fields
     * @param mapReference
     */
    function createMap(fields, mapReference) {
      var index = fields.length;
      while (index--) {
        mapReference[UtilityService.getLowerCaseWithoutSpaces(fields[index])] = fields[index].trim();
      }
    }

    /**
     * function to get default template.
     * which will be what the tenant specific drop down values will show.
     * @returns {*}
     */
    function getDefaultTemplate() {
      if ($scope.templates) {
        var index = $scope.templates.length;
        while (index--) {
          if ($scope.templates[index].name === Config.defaultTemplate) {
            return $scope.templates[index].mappings;
          }
        }
      }
      return {};
    }

    /**
     * function called when different template is selected.
     * reset's the scroll bar of the table
     * called by watcher of templateName
     */
    function resetScrollable() {
      angular.element("body, html, #columnMapping").scrollTop(0);
    }

    /**
     * function called when file is selected and column mapping begins.
     * @param $event
     * @param params
     */
    function onColumnMappingCb($event, params) {
      $scope.templates = [];
      $scope.selectedTemplate = {};
      $scope.templateName = '';
      $scope.dcaHeaders = [];
      dcaHeaderMap = {};
      highScore = {};
      score = {};


      queryParams = params.queryParams;
      file = params.file;
      request = params.request;

      //Resetting the column mapping window
      $scope.isEdit = false;

      //Parsing file using papa parse.
      Papa.parse(params.file, {
        header: true,
        preview: Config.previewHeaders,
        complete: function (results) {
          $scope.dcaHeaders = results.meta.fields;
          createMap($scope.dcaHeaders, dcaHeaderMap);
          $scope.dca = results.data;

          //get view based on headers.
          $scope.tableView = ColumnMappingHtmlFactory.getMappingTable(results.meta.fields);

          //getting templates.
          if (isTemplatesLoaded) {
            $scope.templates = ColumnMappingService.getAllTemplates();
            $scope.selectedTemplate = getTemplateScores();
            $scope.assetFields = getDefaultTemplate();
            $scope.assetFields.unassigned = '';
            $scope.onChangeTemplate();
            angular.copy($scope.mappedKeys, $scope.checkKeys);
          } else {
            var projectType = SharedService.getProjectType();
            ColumnMappingService.loadTemplates(projectType).then(function (response) {
              isTemplatesLoaded = true;
              $scope.templates = response;
              $scope.selectedTemplate = getTemplateScores();
              $scope.assetFields = getDefaultTemplate();
              $scope.assetFields.unassigned = '';
              $scope.onChangeTemplate();
              angular.copy($scope.mappedKeys, $scope.checkKeys);

            }, function (err) {
              $log.error('error getting templates: ', err);
              NotifierService.error('UNABLE_TO_PROCESS_REQUEST');
            });
          }
        },
        error: function (error) {
          NotifierService.error('UNABLE_TO_PROCESS_REQUEST');
          $log.error(error.message);
        }
      });
    }

    /**
     * function callback for when template is changed from drop down.
     */
    $scope.onChangeTemplate = function () {
      if (!$scope.selectedTemplate) {
        return;
      }
      $scope.mappedKeys = {};
      $scope.checkKeys = {};
      mappedFields = {};
      $scope.selectedTemplate.mappings.unassigned = '';
      for (var key in $scope.selectedTemplate.mappings) {
        if ($scope.selectedTemplate.mappings.hasOwnProperty(key)) {
          if (dcaHeaderMap[UtilityService.getLowerCaseWithoutSpaces($scope.selectedTemplate.mappings[key])]) {
            $scope.mappedKeys[UtilityService.getLowerCaseWithoutSpaces($scope.selectedTemplate.mappings[key])] = key;
            $scope.onMap($scope.selectedTemplate.mappings[key]);
          }
        }
      }
      for (var key in dcaHeaderMap) {
        if (!$scope.mappedKeys[key]) {
          $scope.mappedKeys[key] = unassigned;
        }
      }
      $scope.templateName = $scope.selectedTemplate.name;
    };

    /**
     * function will keep track of mapped values and validation for enable button
     * also responsible for allowing unique values to be selected.
     * @param head
     */
    $scope.onMap = function (head) {
      var header = UtilityService.getLowerCaseWithoutSpaces(head);
      if (mappedFields[$scope.mappedKeys[header]]) {
        $scope.mappedKeys[UtilityService.getLowerCaseWithoutSpaces(mappedFields[$scope.mappedKeys[header]])] = unassigned;
        $scope.checkKeys[UtilityService.getLowerCaseWithoutSpaces(mappedFields[$scope.mappedKeys[header]])] = unassigned;
      }
      delete mappedFields[$scope.checkKeys[header]];
      $scope.checkKeys[header] = $scope.mappedKeys[header];
      mappedFields[$scope.mappedKeys[header]] = head;
    };

    /**
     * function to enable/disable import button.
     * @returns {boolean}
     */
    $scope.enableImport = function () {
      return !mappedFields.model;
    };
    /**
     * function that makes the input field editable to make a new template
     */
    $scope.enableEdit = function () {
      $scope.isEdit = true;
    };

    /**
     * function to cancel an edit.
     */
    $scope.onCancel = function () {
      if ($window.confirm(translate('CONFIRM_UNDO_TEMPLATE_CHANGE'))) {
        $scope.onChangeTemplate();
        $scope.isEdit = false;
        $scope.templateName = $scope.selectedTemplate.name;
      } else {
        $scope.enableEdit();
      }
    };

    /**
     * funciton to delete a template.
     */
    $scope.onDelete = function () {
      if (!$scope.selectedTemplate.userId) {
        NotifierService.error('DELETE_DEFAULT_TEMPLATE_ERROR');
        $log.info('trying to delete default template.');
        return;
      }

      function onSuccess() {
        $scope.templates = $scope.templates.filter(function (template) {
          return template._id !== $scope.selectedTemplate._id;
        });
        $scope.selectedTemplate = getTemplateScores();
        $scope.templateName = $scope.selectedTemplate.name;
        ColumnMappingService.updateTemplates($scope.templates);
        NotifierService.success('DELETE_TEMPLATE_SUCCESS');
        $log.info('template deleted.');
      }

      function onError() {
        NotifierService.error('DELETE_TEMPLATE_ERROR');
        $log.info('template could not be deleted.');
      }

      if ($window.confirm(translate('CONFIRM_DELETE_TEMPLATE'))) {
        ColumnMappingService.deleteTemplate($scope.selectedTemplate).then(onSuccess, onError);
      }
    };

    /**
     * function to confirm on closing dca import.
     */
    $scope.onClose = function () {
      //getting confirmation before closing the dialog
      if ($window.confirm(translate('CONFIRM_CANCEL_DCA_IMPORT'))) {
        angular.element('#btnCancelColumnMapping').attr('data-dismiss', 'modal');
      } else {
        angular.element('#btnCancelColumnMapping').removeAttr('data-dismiss', 'modal');
      }
    };

    /**
     * function to create request for import DCA.
     */
    $scope.createRequest = function () {
     var projectType = SharedService.getProjectType();
      // error callback
      function onError(error) {
        var message = SharedService.getErrorMessage(error, 'DCA File upload error.');
        NotifierService.error(message);
        $log.error('Failed to import assets');
        $rootScope.loader.loading = false;
      }

      // success callback
      function onSuccess(response) {
        isTemplatesLoaded = !$scope.isEdit;
        $scope.isEdit = false;
        $scope.popupFlags[PorticoSDPopupName.PRESCRIPTIVE_COLUMN_MAPPING] = false;
        $rootScope.$broadcast(ViewEvent.ASSET_IMPORTED, response);
        $rootScope.loader.loading = false;
      }

      function onImportSuccess(importResponse) {
        if (importResponse.data.addedAccessories) {
          DeviceAccessoriesService.loadProjectAccessories($routeParams.id)
              .then(function (response) {
                onSuccess(importResponse);
              }, function (error) {
                onSuccess(error, importResponse);
              });
        }
        else {
          onSuccess(importResponse);
        }
      }

      for (var key in $scope.assetFields) {
        if (mappedFields[key]) {
          mappedFields[key] = dcaHeaderMap[UtilityService.getLowerCaseWithoutSpaces(mappedFields[key])];
        } else {
          mappedFields[key] = "";
        }
      }

      delete mappedFields.unassigned;
      //Note: here Json.stringify is necessary because we are sending data as formdata.
      //all data must be appended with formdata and formdata takes only string values.
      request.columnMapTemplate = JSON.stringify({
        mappings: mappedFields,
        name: $scope.templateName,
        type: projectType
      });
      $rootScope.loader.loading = true;
      PrescriptiveService.importFutureAssets(UtilityService.getFileData(file, TYPE, request), queryParams)
          .then(onImportSuccess, onError);
    };

    $scope.templates = [];
    $scope.selectedTemplate = {};
    $scope.templateName = '';
    $scope.dcaHeaders = [];
    $scope.mappedKeys = {};
    $scope.checkKeys = {};
    $scope.isEdit = false;
    $scope.assetFields = {};
    $scope.mandatoryDcaColumns = Config.mandatoryDcaColumns;

    //listener
    $scope.$on(PorticoSDEvent.PRESCRIPTIVE_COLUMN_MAPPING, onColumnMappingCb);

    //watcher
    $scope.$watch('templateName', resetScrollable);
  }

  var app = angular.module('app'),
      requires = [
        '$log',
        '$rootScope',
        '$routeParams',
        '$scope',
        '$window',
        'ViewEvent',
        'ColumnMappingService',
        'ColumnMappingHtmlFactory',
        'ngCartosCore.services.AssetService',
        'ngCartosUtils.services.UtilityService',
        'SharedService',
        'ngCartosCore.services.NotifierService',
        'Configuration',
        'translateFilter',
        'ngPorticoSD.constants.PorticoSDEvent',
        'PrescriptiveService',
        'ngPorticoSD.constants.PorticoSDPopupName',
        'ngCartosCore.services.DeviceAccessoriesService',
        PrescriptiveColMappingController
      ];
  app.controller('PrescriptiveColMappingController', requires);
}());

/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  function PrescriptiveImportController($q, $log, $http, $scope, $rootScope, $routeParams, Config, ViewEvent, PorticoSDPopupName, LevelService,
                                        AssetMapService, NotifierService, UtilityService, FileReaderService, SharedService, ProjectSettingService,
                                        AssetService, PrescriptiveService, PorticoSDEvent) {

    var queryParams,
        TYPE = 'PURSUIT_PRESCRIPTIVE_FILE',
        unassigned = "unassigned",
        request = {},
        file,
        score = {}, highScore = {}, dcaHeaderMap = {}, mappedFields = {}, isTemplatesLoaded = false;
    $scope.mappedKeys = {};


    /**
     * This callback function  is responsible to initialize  file-model empty string.
     * @param $event
     * @param title
     * */
    function onImportPopupHide($event, title) {
      if (title === PorticoSDPopupName.PRESCRIPTIVE_IMPORT) {
        $scope.fileModels.dcaData = '';
      }
    }

    /**
     * Check for file field. If input file field  contain value then change the submit flag and enable submit button.
     * @param {boolean} newValue New value of file modal
     */
    function fileInputWatcher(newValue) {
      $scope.flagDisableSubmit = !newValue.length;
    }

    /**
     * Use this function where you get error callback to handle error case
     * @param {message} message related to error
     * */
    function onError(message) {
      return function (error) {
        if (message) {
          $log.error(message);
          NotifierService.error(SharedService.getErrorMessage(error, message));
        }
        $log.error(error);
        $rootScope.loader.loading = false;
      };
    }

    /**
     * This function responsible for importing DCA file.
     */
    $scope.importAssets = function () {
      if (!UtilityService.validateFileName($scope.fileModels.dcaData[0].name)) {
        return NotifierService.showHtmlTemplatePopup(SharedService.getFileTypeValidationMessage());
      }
      var projectSettingData = ProjectSettingService.getProjectSettings($routeParams.id);
      var levelData = LevelService.getLevel($routeParams.id);
      var file = $scope.fileModels.dcaData[0],
          fileValidationMessage = 'PLEASE_SELECT_A_VALID_FILE_TO_IMPORT_ONLY_CSV_FILES_ARE_ALLOWED',
          message = SharedService.getFileValidationMessage(file), // CSV
          request = {
            projectId: $routeParams.id
          };
      queryParams = {
        type: TYPE
      }
      $scope.flagDisableSubmit = true;

      //checking the file size
      if (message) {
        NotifierService.error(message);
        return;
      }
      $scope.popupFlags[PorticoSDPopupName.PRESCRIPTIVE_IMPORT] = false;
      $scope.popupFlags[PorticoSDPopupName.PRESCRIPTIVE_COLUMN_MAPPING] = true;
      $rootScope.$broadcast(PorticoSDEvent.PRESCRIPTIVE_COLUMN_MAPPING, {
        "file": file,
        "queryParams": queryParams,
        "request": request
      });
    };

     $scope.fileModels = {
      dcaData: ''
    };

    // Watches input file value
    $scope.$watch('fileModels.dcaData', fileInputWatcher);

    $scope.$on(ViewEvent.POPUP_HIDE_MODAL, onImportPopupHide);
  }

  var app = angular.module('app'),
      requires = [
        '$q',
        '$log',
        '$http',
        '$scope',
        '$rootScope',
        '$routeParams',
        'Configuration',
        'ViewEvent',
        'ngPorticoSD.constants.PorticoSDPopupName',
        'ngCartosCore.services.LevelService',
        'ngCartosCore.services.AssetMapService',
        'ngCartosCore.services.NotifierService',
        'ngCartosUtils.services.UtilityService',
        'ngCartosUtils.services.FileReaderService',
        'SharedService',
        'ngCartosCore.services.ProjectSettingService',
        'ngCartosCore.services.AssetService',
        'PrescriptiveService',
        'ngPorticoSD.constants.PorticoSDEvent',
        PrescriptiveImportController
      ];
  app.controller('PrescriptiveImportController', requires);

}());

/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /**
   * Controller for handling proxy device button.
   */
  function ProxyDeviceButtonController($scope, $rootScope, PorticoSDEvent, SyncTcoPricingService, PorticoSDService,
                                       ViewEvent, SharedService) {

    /**
     * Check whether we need to allow for tco proxy device or not.
     * @returns {*}
     */
    function isTcoProxyDeviceEligible() {
      var isEnable, tabularAsset, selectedTabularAsset, isFutureState;

      isFutureState = SharedService.isFutureState();
      if (isFutureState) {
        return false;
      }
      isEnable = SyncTcoPricingService.isEligibleForTcoPricingFetch();
      if (!isEnable) {
        return false;
      }
      tabularAsset = PorticoSDService.getAssetFromTabularPool();
      if (tabularAsset && tabularAsset.length) {
        selectedTabularAsset = tabularAsset.first();
        return selectedTabularAsset.value.isEligibleForTcoRemap || selectedTabularAsset.value.isAssetProxy;
      }
      return false;
    }

    /**
     * On Asset select
     */
    function onAssetSelect() {
      $scope.isProxyDeviceEligible = isTcoProxyDeviceEligible();
    }

    /**
     * Function is responsible to show proxy device list
     */
    $scope.showProxyDevices = function () {
      $rootScope.$broadcast(PorticoSDEvent.SHOW_TCO_PROXY_DEVICE_POPUP);
    };

    $scope.isProxyDeviceEligible = false;

    $scope.$on(ViewEvent.ASSET_SELECT, onAssetSelect);
  }

  var app = angular.module('app'),
      requires = [
        '$scope',
        '$rootScope',
        'ngPorticoSD.constants.PorticoSDEvent',
        'SyncTcoPricingService',
        'ngPorticoSD.services.PorticoSDService',
        'ViewEvent',
        'SharedService',
        ProxyDeviceButtonController
      ];
  app.controller('ProxyDeviceButtonController', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /**
   * Controller for handling switch button.
   */
  function RepositionController($log, $scope, $rootScope, $window, SharedService, ViewEvent, PorticoSDEvent,
                                PorticoSDService, LevelEntityMapService, UtilityService, RequestBuilder, Configuration,
                                AssetMapService, OutcomeType, AssetService, translateFilter, PorticoSDHelperService,
                                PorticoSDPopupName, AssetRepositionRequestBuilder, DeviceAccessoriesService, RepositionCommand) {

    /**
     * Show Reposition popup.
     * @param $event
     * @param data
     */
    function showRepositionPopup($event, data) {
      var levels = PorticoSDService.getLevelData(),
          assetLocation = PorticoSDHelperService.getAssetLocation(data);

      $scope.building = assetLocation.building;
      $scope.site = assetLocation.site;
      $scope.floor = assetLocation.floor;

      $scope.buildings = levels.buildings.filter(function (building) {
        return building.parentId === assetLocation.building.parentId;
      });
      $scope.sites = levels.sites.filter(function (site) {
        return site.parentId === assetLocation.site.parentId;
      });
      $scope.floors = levels.floors.filter(function (floor) {
        return floor.parentId === assetLocation.floor.parentId;
      });

      $scope.selectedAsset = data;
      $scope.togglePopupFlag(PorticoSDPopupName.APPLY_REPOSITION_DISPOSITION);
    }

    /**
     * Apply reposition disposition for same or different floor.
     */
    $scope.applyReposition = function () {
      var message = AssetRepositionRequestBuilder.getRepositionMessage($scope.selectedAsset);
      var flag = $window.confirm(message);
      if (!flag) {
        $log.info('Reposition to other floor cancelled.');
        return;
      }

      var request = {},
          entityMap = LevelEntityMapService.getLevelEntityMapByEntityId($scope.selectedAsset._id);
      if (entityMap) {
        request._id = $scope.selectedAsset._id;
        request.coordinates = entityMap.coordinates ? entityMap.coordinates : null;
        request.levelId = $scope.floor._id;
      }
      if (entityMap.levelId !== $scope.floor._id) { // reposition to different floor.
        request.originalLevelId = entityMap.levelId;
      }

      var command = RepositionCommand.getInstance([request]);
      command.execute().then(function () {
        $scope.togglePopupFlag(PorticoSDPopupName.APPLY_REPOSITION_DISPOSITION);
      });

    };

    /**
     * Cancel reposition.
     */
    $scope.cancelReposition = function () {
      if ($scope.selectedAsset) {
        PorticoSDService.updateAssetInPoolManager($scope.selectedAsset);
      }
      $rootScope.$broadcast(PorticoSDEvent.REFRESH_TABULAR_VIEW_POOL);
      $scope.togglePopupFlag(PorticoSDPopupName.APPLY_REPOSITION_DISPOSITION);
    };

    /**
     * On building change
     */
    $scope.onBuildingChange = function (building) {
      var subLevels, floors;
      if (building) {
        subLevels = PorticoSDService.getLevelData(building._id);
        if (subLevels) {
          $scope.subEnvId = '';
          floors = subLevels.floors.filter(function (floor) {
            return floor.parentId === building._id;
          });
          $scope.floors = floors;
          $scope.floor = floors[0];
        }
      }
    };

    /**
     * On site change
     */
    $scope.onSiteChange = function (site) {
      var subLevels, floors;
      if (site) {
        subLevels = PorticoSDService.getLevelData(site._id);
        if (subLevels && subLevels.buildings && subLevels.buildings.length) {
          $scope.buildings = subLevels.buildings;
          $scope.building = subLevels.buildings[0];
          $scope.subEnvId = '';
          floors = subLevels.floors.filter(function (floor) {
            return floor.parentId === subLevels.buildings[0]._id;
          });
          $scope.floors = floors;
          $scope.floor = floors[0];
        }
      }
    };

    $scope.$on(PorticoSDEvent.SHOW_REPOSITION_POPUP, showRepositionPopup);
  }

  var app = angular.module('app'),
      requires = [
        '$log',
        '$scope',
        '$rootScope',
        '$window',
        'SharedService',
        'ViewEvent',
        'ngPorticoSD.constants.PorticoSDEvent',
        'ngPorticoSD.services.PorticoSDService',
        'ngCartosCore.services.LevelEntityMapService',
        'ngCartosUtils.services.UtilityService',
        'RequestBuilder',
        'Configuration',
        'ngCartosCore.services.AssetMapService',
        'ngCartosCanvas.constants.OutcomeType',
        'ngCartosCore.services.AssetService',
        'translateFilter',
        'ngPorticoSD.services.PorticoSDHelperService',
        'ngPorticoSD.constants.PorticoSDPopupName',
        'ngCartosRequest.services.AssetRepositionRequestBuilder',
        'ngCartosCore.services.DeviceAccessoriesService',
        'ngCartosUndoRedo.commands.RepositionCommand',
        RepositionController
      ];
  app.controller('RepositionController', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /**
   * Controller for handling switch button.
   */
  function SwitchViewController($scope, $rootScope, $routeParams, PorticoSDTemplatePaths, ProjectSetting, ViewEvent) {

    var projectId = $routeParams.id;
    /**
     * Update 'isFloorMapView' to switch b/w the map & tabular view
     */
    $scope.switchView = function () {

      $rootScope.isFloorMapView = !$rootScope.isFloorMapView;
      if (!$rootScope.isFloorMapView) {
        $rootScope.tabularViewLoaded = false;
      }
      $rootScope.$broadcast(ViewEvent.ON_SWITCH_VIEW);
      ProjectSetting.retainView(projectId, $rootScope.isFloorMapView);
    };

    $scope.TemplatePath = PorticoSDTemplatePaths;
  }

  var app = angular.module('app'),
      requires = [
        '$scope',
        '$rootScope',
        '$routeParams',
        'PorticoSDTemplatePaths',
        'ngCartosCore.services.ProjectSettingService',
        'ViewEvent',
        SwitchViewController
      ];
  app.controller('SwitchViewController', requires);
}());
/*global angular*/
(function () {
  'use strict';

  /**
   * Controller to handle tabular view pop-up setting
   */
  function TabularViewSettingController($window, $log, $scope, $timeout, $rootScope, $routeParams, SharedService, Notify, PorticoSDPopupName,
                                        TabularViewSettingService, ViewEvent, PorticoSDSharedService, PorticoSDEvent,
                                        PorticoSDConfig, translateFilter) {

    /*this function will get executed on initialization of this controller
    */
    function onLoad() {
      $scope.templates = {};
      $scope.copyTemplateFrom = PorticoSDConfig.TABULAR_VIEW_TEMPLATE_SETTINGS.ALL_COLUMNS_TEMPLATE_NAME;
      $scope.selectedTemplate = '';
      $scope.templatesList = [];
      $scope.customTemplateName = PorticoSDConfig.TABULAR_VIEW_TEMPLATE_SETTINGS.CUSTOM;
      $scope.templateData = [];
    }

    /**
     * Get selected View
     * @returns {*|string|string}
     */
    function getSelectedView() {
      return SharedService.pricingView() || PorticoSDConfig.TABULAR_VIEWS.MANUAL;
    }

    /**
     * Update display name
     * @param templateData
     * @return {*}
     */
    function updateDisplayName(templateData) {
      if (Array.isArray((templateData))) {
        for (let index = 0; index < templateData.length; index++) {
          templateData[index].displayName = translateFilter(templateData[index].name);
        }
      }
      return templateData;
    }


    /**
     * Get template data
     * @param templateName
     * @returns {*|Array}
     */
    function getTemplateData(templateName) {
      let templateDetails;
      if ($scope.templates) {
        templateDetails = $scope.templates[templateName] || [];
      }
      return updateDisplayName(templateDetails);
    }

    /**
     * Set template data
     * @param templateName
     */
    function setTemplateData(templateName) {
      $scope.templateData = getTemplateData(templateName);
      $scope.selectedTemplate = templateName;
      $scope.copyTemplateFrom = PorticoSDConfig.TABULAR_VIEW_TEMPLATE_SETTINGS.ALL_COLUMNS_TEMPLATE_NAME;
    }

    /**
     * Update the template details on the pop-up
     */
    function updateTemplateSettings() {
      let tabularTemplates = PorticoSDSharedService.getTemplates(),
          selectedView = getSelectedView();
      if (tabularTemplates) {
        $scope.templates = tabularTemplates;
        $scope.templatesList = Object.keys(tabularTemplates);
        $scope.customTemplateName = PorticoSDConfig.TABULAR_VIEW_TEMPLATE_SETTINGS.CUSTOM;
        setTemplateData(selectedView);
      }
    }

    /**
     * This function will bring the user settings pop-up
     */
    function openViewSettingPopup() {
      updateTemplateSettings();
      $scope.togglePopupFlag(PorticoSDPopupName.TABULAR_TEMPLATE_VIEW_SETTING_POPUP);
    }

    /**
     * Copy the template from the selected template
     */
    $scope.copyTemplate = function () {
      var templateData = TabularViewSettingService.updateColumnsFromSelectedTemplate($scope.copyTemplateFrom);
      $scope.templateData = updateDisplayName(templateData);
      $scope.templates = PorticoSDSharedService.getTemplates();
    };

    /**
     * return boolean value based on the given template name
     */
    $scope.isDefaultTemplate = function (template) {
      return PorticoSDConfig.TABULAR_VIEW_TEMPLATE_SETTINGS.DEFAULT_TEMPLATES[template];
    };

    $scope.isDraggableColumn = function (columnName) {
      return PorticoSDConfig.TABULAR_VIEW_TEMPLATE_SETTINGS.NON_DRAGGABLE_COLUMNS[columnName];
    };

      /**
       * return true or false based on existing custom templates length
       */
      function isMaxTemplateLimitExceed() {
          //Since new template("ALL") is also a property in templates object,
          //so, We need to subtract one from the templates object length
          let totalTemplates = Object.keys($scope.templates).length - 1,
              defaultTemplates = Object.keys(PorticoSDConfig.TABULAR_VIEW_TEMPLATE_SETTINGS.DEFAULT_TEMPLATES).length,
              customTemplatesLength = totalTemplates - defaultTemplates;
          return (customTemplatesLength < PorticoSDConfig.TABULAR_VIEW_TEMPLATE_SETTINGS.MAX_CUSTOM_TEMPLATES);
      }

    /**
     * Update pop-up with the selected template data
     * @param templateName: string
     */
    $scope.updateTab = function (templateName) {
      if (templateName === PorticoSDConfig.TABULAR_VIEW_TEMPLATE_SETTINGS.ALL_COLUMNS_TEMPLATE_NAME && !isMaxTemplateLimitExceed()) {
        Notify.info("CUSTOM_VIEW_MAXIMUM_LIMIT");
      } else {
        setTemplateData(templateName);
      }
    };

    /**
     * This function will close the pop-up and update the templates with saved data
     */
    $scope.onCancel = function () {
      $scope.popupFlags[PorticoSDPopupName.TABULAR_TEMPLATE_VIEW_SETTING_POPUP] = false;
    };

    /**
     * This function will return the selected items
     * @param list array containing all the columns from the selected template
     * @param item selected item from the list array
     */
    $scope.getSelectedItems = function (list, item) {
      item.selected = true;
      return list.filter(function (item) {
        return item.selected;
      });
    };

    /**
     * This function will show an image on drag start
     * @param list array containing all the columns from the selected template
     * @param event
     */
    $scope.onDragstart = function (list, event) {
      TabularViewSettingService.showImageOnDragStart(list, event);
    };

    /**
     * This function will get executed on drag end, it will update the columns to either to display or hidden
     * @param list array containing all the columns from the selected template
     * @param items array containing selected items
     * @param index position to include the dropped items
     * @param isVisible boolean, to show or hide the dropped items
     */
    $scope.onDrop = function (list, items, index, isVisible) {
      $scope.templateData = TabularViewSettingService.updateColumnsOnDrop(list, items, index, isVisible);
      return true;
    };

    $scope.selectItem = function (item) {
      if ($scope.isDraggableColumn(item.name)) {
        item.selected = false;
      } else {
        item.selected = !item.selected;
      }
    };

    /**
     * This function is used to save the changes made by the user
     */
    $scope.updateTemplate = function () {
      if ($scope.selectedTemplate === PorticoSDConfig.TABULAR_VIEW_TEMPLATE_SETTINGS.ALL_COLUMNS_TEMPLATE_NAME &&
          $scope.templatesList.includes($scope.customTemplateName)) {
        Notify.error("NAME_EXISTS");
        return;
      }

      let templateName = $scope.selectedTemplate === PorticoSDConfig.TABULAR_VIEW_TEMPLATE_SETTINGS.ALL_COLUMNS_TEMPLATE_NAME ?
          $scope.customTemplateName : $scope.selectedTemplate,
          updateTemplate = {
            'templateName': templateName,
            'templateData': $scope.templateData
          };
      delete updateTemplate.templateData.dragging;

      function onError(data) {
        Notify.error("ERROR_ON_SAVING_USER_SETTINGS");
      }

      function onSuccess(response) {
        $rootScope.$broadcast(PorticoSDEvent.UPDATE_TABULAR_VIEW);
        $scope.popupFlags[PorticoSDPopupName.TABULAR_TEMPLATE_VIEW_SETTING_POPUP] = false;
        Notify.success("CHANGES_UPDATED");
      }

      //TODO: remove route params once the backend portion is implemented
      TabularViewSettingService.updateTemplate(updateTemplate, $routeParams.id).then(onSuccess, onError);
    };

    /**
     * Delete custom template
     * @param templateName
     */
    $scope.deleteCustomTemplate = function (templateName) {
      event.stopPropagation();

      function onSuccess() {
        Notify.success("DELETE_CUSTOM_TEMPLATE");
        updateTemplateSettings();
        $rootScope.$broadcast(PorticoSDEvent.UPDATE_TABULAR_VIEW);
      }

      function onError() {
        Notify.error("UNSUCCESSFUL_CUSTOM_TEMPLATE_DELETE");
      }

      let deleteConfirm = $window.confirm(translateFilter("DELETE_TABULAR_TEMPLATE_CONFIRMATION"));
      if (deleteConfirm) {
        TabularViewSettingService.deleteTabularTemplate(templateName).then(onSuccess, onError);
      }
    };

    onLoad();

    $scope.$on(PorticoSDEvent.SHOW_TABULAR_TEMPLATE_VIEW_SETTINGS_POPUP, openViewSettingPopup);
  }

  let app = angular.module('app'),
      requires = [
        '$window',
        '$log',
        '$scope',
        '$timeout',
        '$rootScope',
        '$routeParams',
        'SharedService',
        'ngCartosCore.services.NotifierService',
        'ngPorticoSD.constants.PorticoSDPopupName',
        'TabularViewSettingService',
        'ViewEvent',
        'PorticoSDSharedService',
        'ngPorticoSD.constants.PorticoSDEvent',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'translateFilter',
        TabularViewSettingController
      ];
  app.controller('TabularViewSettingController', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /**
   * Controller for handling Tco Adjustment button.
   */
  function TcoAdjustmentController($log, $scope, $rootScope, $routeParams, ViewEvent, SharedService, NotifierService,
                                   TcoPricingService, PorticoSDPopupName, PorticoSDConfiguration, UtilityService,
                                   PorticoSDEvent, TcoAdjustmentService, PorticoSDSharedService) {

    $scope.tcoAdjustmentData = {'current': {}, 'future': {}};
    var defaultTcoAdjustmentData = {};

    /**
     * Set Tco adjustment values
     * @param tcoAdjustmentValues
     */
    function setTcoAdjustmentData(tcoAdjustmentValues) {
      $scope.tcoAdjustmentData.current = tcoAdjustmentValues.current ?
          tcoAdjustmentValues.current : defaultTcoAdjustmentData.current;
      $scope.tcoAdjustmentData.future = tcoAdjustmentValues.future ?
          tcoAdjustmentValues.future : defaultTcoAdjustmentData.future;
    }

    /**
     * Set Default Tco Adjustment
     */
    function setDefaultTcoAdjustmentData() {
      defaultTcoAdjustmentData.current = angular.copy(PorticoSDConfiguration.DefaultTcoAdjustment);
      defaultTcoAdjustmentData.future = angular.copy(PorticoSDConfiguration.DefaultTcoAdjustment);
    }

    /**
     * Function to initialize field values in TCO Adjustment pop-up
     */
    function initializeTcoAdjustmentSettings() {
      var tcoAdjustmentValues;

      setDefaultTcoAdjustmentData();

      function onSuccess(response) {
        tcoAdjustmentValues = angular.copy(PorticoSDSharedService.tcoAdjustmentValues());
        if (tcoAdjustmentValues) {
          setTcoAdjustmentData(tcoAdjustmentValues);
        } else {
          $scope.tcoAdjustmentData = defaultTcoAdjustmentData;
        }
        $scope.togglePopupFlag(PorticoSDPopupName.TCO_ADJUSTMENT);
        $log.info("Tco Adjustment values are loaded successfully");
      }

      function onError(error) {
        NotifierService.error("ERROR_LOADING_TCO_ADJUSTMENT_VALUES");
        $log.error("Error Getting Tco Adjustment", error);
      }

      tcoAdjustmentValues = angular.copy(PorticoSDSharedService.tcoAdjustmentValues());
      if (tcoAdjustmentValues) {
        setTcoAdjustmentData(tcoAdjustmentValues);
        $scope.togglePopupFlag(PorticoSDPopupName.TCO_ADJUSTMENT);
      }
      else {
        TcoAdjustmentService.loadTcoAdjustment().then(onSuccess, onError);
      }
    }

    //Function to validate the numeric data for project
    function validateTcoAdjustmentData() {
      var tcoAdjustmentCurrent = $scope.tcoAdjustmentData.current,
          tcoAdjustmentFuture = $scope.tcoAdjustmentData.future;
      if (tcoAdjustmentCurrent.tonerRate && (!UtilityService.isValidNumber(tcoAdjustmentCurrent.tonerRate) ||
              tcoAdjustmentCurrent.tonerRate < -100 || tcoAdjustmentCurrent.tonerRate >= 100)) {
        return 'CURRENT_TONER_SHOULD_BE_NUMBER_BETWEEN_MINUS_100_AND_100';
      } else if ((!tcoAdjustmentCurrent.tonerRate && tcoAdjustmentCurrent.tonerRate !== 0) || UtilityService.isEmptyString(tcoAdjustmentCurrent.tonerRate)) {
        return 'CURRENT_TONER_IS_REQUIRED';
      }
      else if (tcoAdjustmentCurrent.tonerRate || tcoAdjustmentCurrent.tonerRate === 0) {
        tcoAdjustmentCurrent.tonerRate = parseFloat(tcoAdjustmentCurrent.tonerRate);
      }

      if (tcoAdjustmentCurrent.hardwareRate && (!UtilityService.isValidNumber(tcoAdjustmentCurrent.hardwareRate) ||
              tcoAdjustmentCurrent.hardwareRate < -100 || tcoAdjustmentCurrent.hardwareRate >= 100)) {
        return 'CURRENT_HARDWARE_SHOULD_BE_NUMBER_BETWEEN_MINUS_100_AND_100';
      } else if ((!tcoAdjustmentCurrent.hardwareRate && tcoAdjustmentCurrent.hardwareRate !== 0) ||
          UtilityService.isEmptyString(tcoAdjustmentCurrent.hardwareRate)) {
        return 'CURRENT_HARDWARE_IS_REQUIRED';
      }
      else if (tcoAdjustmentCurrent.hardwareRate || tcoAdjustmentCurrent.hardwareRate === 0) {
        tcoAdjustmentCurrent.hardwareRate = parseFloat(tcoAdjustmentCurrent.hardwareRate);
      }

      if (tcoAdjustmentCurrent.replaceablePartsRate && (!UtilityService.isValidNumber(tcoAdjustmentCurrent.replaceablePartsRate) ||
              tcoAdjustmentCurrent.replaceablePartsRate < -100 || tcoAdjustmentCurrent.replaceablePartsRate >= 100)) {
        return 'CURRENT_REPLACEABLE_PARTS_SHOULD_BE_NUMBER_BETWEEN_MINUS_100_AND_100';
      } else if ((!tcoAdjustmentCurrent.replaceablePartsRate && tcoAdjustmentCurrent.replaceablePartsRate !== 0) ||
          UtilityService.isEmptyString(tcoAdjustmentCurrent.replaceablePartsRate)) {
        return 'CURRENT_REPLACEABLE_PARTS_IS_REQUIRED';
      } else if (tcoAdjustmentCurrent.replaceablePartsRate || tcoAdjustmentCurrent.replaceablePartsRate === 0) {
        tcoAdjustmentCurrent.replaceablePartsRate = parseFloat(tcoAdjustmentCurrent.replaceablePartsRate);
      }

      if (tcoAdjustmentCurrent.serviceRate && (!UtilityService.isValidNumber(tcoAdjustmentCurrent.serviceRate) ||
              tcoAdjustmentCurrent.serviceRate < -100 || tcoAdjustmentCurrent.serviceRate >= 100)) {
        return 'CURRENT_SERVICE_SHOULD_BE_NUMBER_BETWEEN_MINUS_100_AND_100';
      } else if ((!tcoAdjustmentCurrent.serviceRate && tcoAdjustmentCurrent.serviceRate !== 0) ||
          UtilityService.isEmptyString(tcoAdjustmentCurrent.serviceRate)) {
        return 'CURRENT_SERVICE_IS_REQUIRED';
      } else if (tcoAdjustmentCurrent.serviceRate || tcoAdjustmentCurrent.serviceRate === 0) {
        tcoAdjustmentCurrent.serviceRate = parseFloat(tcoAdjustmentCurrent.serviceRate);
      }

      if (tcoAdjustmentFuture.tonerRate && (!UtilityService.isValidNumber(tcoAdjustmentFuture.tonerRate) ||
              tcoAdjustmentFuture.tonerRate < -100 || tcoAdjustmentFuture.tonerRate >= 100)) {
        return 'FUTURE_TONER_SHOULD_BE_NUMBER_BETWEEN_MINUS_100_AND_100';
      } else if ((!tcoAdjustmentFuture.tonerRate && tcoAdjustmentFuture.tonerRate !== 0) ||
          UtilityService.isEmptyString(tcoAdjustmentFuture.tonerRate)) {
        return 'FUTURE_TONER_IS_REQUIRED';
      } else if (tcoAdjustmentFuture.tonerRate || tcoAdjustmentFuture.tonerRate === 0) {
        tcoAdjustmentFuture.tonerRate = parseFloat(tcoAdjustmentFuture.tonerRate);
      }

      if (tcoAdjustmentFuture.hardwareRate && (!UtilityService.isValidNumber(tcoAdjustmentFuture.hardwareRate) ||
              tcoAdjustmentFuture.hardwareRate < -100 || tcoAdjustmentFuture.hardwareRate >= 100)) {
        return 'FUTURE_HARDWARE_SHOULD_BE_NUMBER_BETWEEN_MINUS_100_AND_100';
      } else if ((!tcoAdjustmentFuture.hardwareRate && tcoAdjustmentFuture.hardwareRate !== 0) ||
          UtilityService.isEmptyString(tcoAdjustmentFuture.hardwareRate)) {
        return 'FUTURE_HARDWARE_IS_REQUIRED';
      } else if (tcoAdjustmentFuture.hardwareRate || tcoAdjustmentFuture.hardwareRate === 0) {
        tcoAdjustmentFuture.hardwareRate = parseFloat(tcoAdjustmentFuture.hardwareRate);
      }

      if (tcoAdjustmentFuture.replaceablePartsRate && (!UtilityService.isValidNumber(tcoAdjustmentFuture.replaceablePartsRate) ||
              tcoAdjustmentFuture.replaceablePartsRate < -100 || tcoAdjustmentFuture.replaceablePartsRate >= 100)) {
        return 'FUTURE_REPLACEABLE_PARTS_SHOULD_BE_NUMBER_BETWEEN_MINUS_100_AND_100';
      } else if ((!tcoAdjustmentFuture.replaceablePartsRate && tcoAdjustmentFuture.replaceablePartsRate !== 0) ||
          UtilityService.isEmptyString(tcoAdjustmentFuture.replaceablePartsRate)) {
        return 'FUTURE_REPLACEABLE_PARTS_IS_REQUIRED';
      } else if (tcoAdjustmentFuture.replaceablePartsRate || tcoAdjustmentFuture.replaceablePartsRate === 0) {
        tcoAdjustmentFuture.replaceablePartsRate = parseFloat(tcoAdjustmentFuture.replaceablePartsRate);
      }

      if (tcoAdjustmentFuture.serviceRate && (!UtilityService.isValidNumber(tcoAdjustmentFuture.serviceRate) ||
              tcoAdjustmentFuture.serviceRate < -100 || tcoAdjustmentFuture.serviceRate >= 100)) {
        return 'FUTURE_SERVICE_SHOULD_BE_NUMBER_BETWEEN_MINUS_100_AND_100';
      } else if ((!tcoAdjustmentFuture.serviceRate && tcoAdjustmentFuture.serviceRate !== 0) ||
          UtilityService.isEmptyString(tcoAdjustmentFuture.serviceRate)) {
        return 'FUTURE_SERVICE_IS_REQUIRED';
      } else if (tcoAdjustmentFuture.serviceRate || tcoAdjustmentFuture.serviceRate === 0) {
        tcoAdjustmentFuture.serviceRate = parseFloat(tcoAdjustmentFuture.serviceRate);
      }
    }

    /**
     * Updates the tco adjustment property.
     */
    function updateProperties() {
      //creating request body from changed properties...
      var changes = $scope.tcoAdjustmentData;
      //initializing all the keys which have undefined values .
      UtilityService.initUndefinedProperty(changes);

      function onSuccess(response) {
        function onSuccess(response) {
          $rootScope.$broadcast(ViewEvent.TABULAR_PRICING, PorticoSDConfiguration.PRICING_SOURCE.TCO);
          NotifierService.success("TCO_ADJUSTMENT_VALUES_SAVED_SUCCESSFULLY");
          $log.info("TCO_ADJUSTMENT_PRICING_LOADED_SUCCESSFULLY", response);
          $rootScope.loader.loading = false;
        }

        function onError(error) {
          $rootScope.loader.loading = false;
          $log.error("ERROR_LOADING_TCO_ADJUSTMENT_VALUES", error);
          NotifierService.error("ERROR_LOADING_TCO_ADJUSTMENT_VALUES");
        }

        TcoPricingService.getTcoPricing().then(onSuccess, onError);
        $log.info("TCO_ADJUSTMENT_VALUES_SAVED_SUCCESSFULLY", response);
        $scope.togglePopupFlag(PorticoSDPopupName.TCO_ADJUSTMENT);
      }

      function onError(error) {
        $rootScope.loader.loading = false;
        $log.error("ERROR_SAVING_TCO_ADJUSTMENT_VALUES", error);
        NotifierService.error("ERROR_SAVING_TCO_ADJUSTMENT_VALUES");
        $scope.togglePopupFlag(PorticoSDPopupName.TCO_ADJUSTMENT);
      }

      if (!UtilityService.isEmpty(changes)) {
        TcoAdjustmentService.saveTcoAdjustment(changes).then(onSuccess, onError);
        $rootScope.loader.loading = true;
      } else {
        NotifierService.notify('NOTHING_TO_SAVE_IN_TCO_ADJUSTMENT', NotifierService.INFO);
      }
    }

    /**
     * Apply Tco Adjustment Settings
     */
    $scope.applyTcoAdjustment = function () {
      var errMsg = validateTcoAdjustmentData();
      if (errMsg) {
        NotifierService.error(errMsg);
        return;
      }
      else {
        updateProperties();
      }
    };

    $scope.$on(PorticoSDEvent.SHOW_TCO_ADJUSTMENT_POPUP, initializeTcoAdjustmentSettings);
  }

  var app = angular.module('app'),
      requires = [
        '$log',
        '$scope',
        '$rootScope',
        '$routeParams',
        'ViewEvent',
        'SharedService',
        'ngCartosCore.services.NotifierService',
        'TcoPricingService',
        'ngPorticoSD.constants.PorticoSDPopupName',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'ngCartosUtils.services.UtilityService',
        'ngPorticoSD.constants.PorticoSDEvent',
        'TcoAdjustmentService',
        'PorticoSDSharedService',
        TcoAdjustmentController
      ];
  app.controller('TcoAdjustmentController', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /**
   * Controller for handling switch button.
   */
  function TcoPricingController($log, $scope, $rootScope, ViewEvent, SharedService, NotifierService,
                                TcoPricingService, PorticoSDPopupName, PorticoSDConfiguration, LevelType, LevelService, Configuration, translateFilter) {

    /**
     * Get error message based on error code
     * @param error
     * @param message
     */
    function getErrorMessage(error, message) {
      if (error && error.data && error.data.error && error.data.error.errorCode) {
        switch (error.data.error.errorCode) {
          case  'CARTOS_ERROR_INVALID_REQUEST':
          case 'CARTOS_ERROR_EXCEPTION':
            message = 'ERROR_FETCHING_PRICING';
            break;
          case 'CARTOS_SDSS_MISSING_AUTHORIZATION_HEADER':
          case 'CARTOS_SDSS_ERROR_PARSE_AUTH_HEADER':
          case 'CARTOS_SDSS_EMPTY_AUTHORIZATION_HEADER':
            message = 'UNABLE_TO_LOGIN_TO_SOLD';
            break;
          case 'CARTOS_ERROR_NO_RECORD':
            message = 'NO_PRICING_RECORD_FOUND';
            break;
          default:
            message = error.data.error.errorCode;
            break;
        }
      }
    }

    /**
     * To validate the countries
     * returns the list of invalid countries
     */
    function countryValidate(countries) {
      var regions = [],
          invalidCountries = [];
      for (let index = 0; index < countries.length; index++) {
        var region = SharedService.getRegionFromCountrySet(countries[index].name);
        if (!region) {
          invalidCountries.push(countries[index].name);
        }
      }
      return invalidCountries;
    }

    /**
     * On right pane open
     * @param $event
     * @param openedPane
     */
    function onRightPaneOpen($event, openedPane) {
      if (openedPane === Configuration.RIGHT_PANE.TCO_PRICING && !$rootScope.token.featureMatrix.cartosTcoPricing) {
        SharedService.getSplitter().rightPane.collapse();
        $rootScope.loader.loading = true;
        var countries = LevelService.getLevelsByType(LevelType.COUNTRY);
        var invalidCountries = countryValidate(countries);
        var invalidCountriesList = "";
        //'fetchPricing' flag is used to disable 'fetchPricing' button when all countries are invalid
        $scope.fetchPricing = true;

        //if all the plotted countries are invalid
        if (countries.length === invalidCountries.length) {
          $scope.tcoPricingDescription = translateFilter('INVALID_COUNTRIES_PRICING_CANNOT_FETCHED');
          $scope.invalidCountries = invalidCountriesList;
          $scope.fetchPricing = false;
          //if some countries are invalid
        } else if (invalidCountries.length) {
          $scope.tcoPricingDescription = translateFilter('PRICING_OF_INVALID_COUNTRIES_WILL_NOT_DISPLAYED');
          $scope.invalidCountries = invalidCountries.join(", ");
          // if all countries are valid
        } else {
          $scope.invalidCountries = invalidCountriesList;
          $scope.tcoPricingDescription = translateFilter('DO_YOU_WANT_TO_PROCEED_WITH_TCO_PRICING');
        }
        $rootScope.loader.loading = false;
        $scope.togglePopupFlag(PorticoSDPopupName.TCO_PRICING);
      }
    }

    $scope.fetchTcoPricing = function () {
      $scope.togglePopupFlag(PorticoSDPopupName.TCO_PRICING);
      $rootScope.loader.loading = true;

      function onSuccess(response) {
        $rootScope.loader.loading = false;
        var isEmptyResponse = SharedService.noContentResponse(response);
        if (isEmptyResponse || !response) {
          NotifierService.error('ERROR_FETCHING_PRICING');
          return;
        }
        NotifierService.success('IMPORT_TCO_PRICING_SUCCESS');
        $rootScope.$broadcast(ViewEvent.TABULAR_PRICING, PorticoSDConfiguration.PRICING_SOURCE.TCO);
      }

      function onError(error) {
        $rootScope.loader.loading = false;
        var message = 'ERROR_FETCHING_PRICING';
        getErrorMessage(error, message);
        NotifierService.error(message);
        $log.error(error);
      }

      TcoPricingService.fetchTcoPricing().then(onSuccess, onError);
    };
    $scope.invalidCountries = "";
    $scope.tcoPricingDescription = "";
    $scope.fetchPricing = true;
    $scope.$on(ViewEvent.PANE_OPEN, onRightPaneOpen);
  }

  var app = angular.module('app'),
      requires = [
        '$log',
        '$scope',
        '$rootScope',
        'ViewEvent',
        'SharedService',
        'ngCartosCore.services.NotifierService',
        'TcoPricingService',
        'ngPorticoSD.constants.PorticoSDPopupName',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'ngCartosCore.constants.LevelType',
        'ngCartosCore.services.LevelService',
        'Configuration',
        'translateFilter',
        TcoPricingController
      ];
  app.controller('TcoPricingController', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  function TcoProxyDeviceController($log, $scope, $rootScope, NotifierService, PorticoSDHelperService,
                                    TcoProxyDeviceService, PorticoSDEvent, PorticoSDPopupName, PorticoSDConfiguration,
                                    SharedService) {

    /**
     * Check feature flag for tcoProxyDevice
     * @returns {boolean}
     */
    function isTcoProxyDeviceEnabled() {
      if ($rootScope.token && $rootScope.token.featureMatrix) {
        return $rootScope.token.featureMatrix.tcoProxyDevice || PorticoSDConfiguration.TRUE;
      }
      return false;
    }

    /**
     * Initialize grid properties
     */
    function initializeGrid() {
      $scope.tcoProxyDeviceGrid = {
        enableRowSelection: true,
        enableRowHeaderSelection: false,
        enableHiding: false,
        enableFiltering: true
      };
      $scope.tcoProxyDeviceGrid.columnDefs = PorticoSDConfiguration.TCO_PROXY_GRID_COLUMNS;
      $scope.tcoProxyDeviceGrid.multiSelect = false;
      $scope.tcoProxyDeviceGrid.modifierKeysToMultiSelect = false;
      $scope.tcoProxyDeviceGrid.noUnselect = true;
      $scope.tcoProxyDeviceGrid.onRegisterApi = function (gridApi) {
        $scope.gridApi = gridApi;
      };
      $scope.tcoProxyDeviceGrid.data = [];
    }

    /**
     * On Controller load this function will be called.
     */
    function onLoad() {
      if (isTcoProxyDeviceEnabled()) {
        initializeGrid();
      }
    }

    /**
     * Get tco proxy devices
     * @param latest
     * true : Will fetch the latest data from hp server and override existing db
     * false : Wil fetch the data from db
     */
    function getTcoProxyDevices(latest) {
      function onSuccess(response) {
        $rootScope.loader.loading = false;
        if (latest) {
          $log.info("Tco proxy device list synced successfully");
          NotifierService.success('TCO_PROXY_DEVICE_SYNCED_LIST_SUCCESSFULLY');
        }
        else {
          $log.info("Tco proxy device list loaded successfully");
        }

        $scope.tcoProxyDeviceGrid.data = response.data;
      }

      function onError(error) {
        $rootScope.loader.loading = false;
        $scope.tcoProxyDeviceGrid.data = [];
        if (latest) {
          $log.error("Error in syncing Tco proxy device list", error);
          NotifierService.error('TCO_PROXY_DEVICE_ERROR_SYNCING_DEVICE');
        }
        else {
          $log.error("Error in loading Tco proxy device list", error);
          NotifierService.error('TCO_PROXY_DEVICE_ERROR_LOADING_DEVICE_LIST');
        }
      }

      $rootScope.loader.loading = true;
      TcoProxyDeviceService.getTcoProxyDevices(PorticoSDConfiguration.STRING_TCO.toLowerCase(),
          latest).then(onSuccess, onError);
    }

    /**
     * Show tco proxy device popup
     */
    function showTcoProxyDevicePopup() {
      var selectedAsset = SharedService.selectedAsset();
      if (!selectedAsset) {
        $log.info('Asset not found');
        return;
      }
      if (!selectedAsset.productNumber) {
        NotifierService.error('TCO_PROXY_DEVICE_ERROR_SKU_NOT_AVAILABLE');
        return;
      }
      if (isTcoProxyDeviceEnabled()) {
        getTcoProxyDevices(false);
        $scope.togglePopupFlag(PorticoSDPopupName.TCO_PROXY_DEVICE);
      }
    }

    /**
     * Sync the tco proxy device list with Hp.
     */
    $scope.syncTcoProxyDevice = function () {
      getTcoProxyDevices(true);
    };

    /**
     * Set the proxy device for the assets which has same aku as selected asset.
     */
    $scope.setProxyDevice = function () {
      var selectedProxyDevice,
          selectedRows = $scope.gridApi.selection.getSelectedRows();
      if (!(Array.isArray(selectedRows) && selectedRows.length)) {
        NotifierService.error('TCO_PROXY_DEVICE_ERROR_SELECTING_FROM_DEVICE_LIST');
        return;
      }
      selectedProxyDevice = selectedRows.first();

      function onSuccess(response) {
        $rootScope.loader.loading = false;
        $scope.togglePopupFlag(PorticoSDPopupName.TCO_PROXY_DEVICE);
        NotifierService.success('TCO_PROXY_DEVICE_UPDATED_SUCCESSFULLY');
      }

      function onError(error) {
        $rootScope.loader.loading = false;
        $scope.togglePopupFlag(PorticoSDPopupName.TCO_PROXY_DEVICE);
        NotifierService.error('TCO_PROXY_DEVICE_ERROR_UPDATING_DEVICE');
      }

      $rootScope.loader.loading = true;
      PorticoSDHelperService.collapseLeftPane();
      TcoProxyDeviceService.updateProxyDevicePricing(selectedProxyDevice).then(onSuccess, onError);
    };

    onLoad();

    $scope.$on(PorticoSDEvent.SHOW_TCO_PROXY_DEVICE_POPUP, showTcoProxyDevicePopup);
  }

  var app = angular.module('app'),
      requires = [
        '$log',
        '$scope',
        '$rootScope',
        'ngCartosCore.services.NotifierService',
        'ngPorticoSD.services.PorticoSDHelperService',
        'ngPorticoSD.services.TcoProxyDeviceService',
        'ngPorticoSD.constants.PorticoSDEvent',
        'ngPorticoSD.constants.PorticoSDPopupName',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'SharedService',
        TcoProxyDeviceController
      ];
  app.controller('TcoProxyDeviceController', requires);
}());
(function () {
    'use strict';

    /**
     * Directive to remove the html element based on project type
     * @param Configuration
     * @returns {{restrict: string, scope: {prescriptive: string}, link: linkFn}}
     */
    function ngPorticoPrescriptive(Configuration) {

        /**
         * linkFn
         * @param scope
         * @param element
         * @param attrs
         */
        function linkFn(scope, element, attrs) {
            var uiElement = element[0];
            //Remove the elements based on project type
            if (uiElement) {
                if (scope.$root.projectType === Configuration.projectType.STANDARD && scope.prescriptive) {
                    uiElement.parentNode.removeChild(uiElement);
                    return;
                }
                if (scope.$root.projectType === Configuration.projectType.PRESCRIPTIVE && !scope.prescriptive) {
                    uiElement.parentNode.removeChild(uiElement);
                    return;
                }
            }
        }

        return {
            restrict: 'A',
            scope: {
                prescriptive: '='
            },
            link: linkFn
        };
    }


    var app = angular.module('app'),
        requires = [
            'Configuration',
            ngPorticoPrescriptive
        ];
    app.directive('ngPorticoPrescriptive', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /**
   * Directive: ngPorticoSDStageDimension.
   * It will calculate the dimension of the PorticoSD grid.
   * @param $window
   * @param Utils
   * @param ViewEvent
   * @returns {{restrict: string, link: linkFn}}
   */
  function ngPorticoSDStageDimension($window, ViewEvent, Utils, PorticoSDEvent) {

    /**
     * linkFn
     * @param scope
     */
    function linkFn(scope) {

      /**
       *  Calculate the stage dimension
       */
      function setStageDimension() {
        // Set the height of the porticoSD grid.
        var mainMenu = Utils.toNumber(angular.element('#mainMenu').height()),
            toolBar = Utils.toNumber(angular.element('div[ng-controller="ToolBarController"]').height()),
            navBar = Utils.toNumber(angular.element('#primaryNavContainer').height()),
            deviceHeight = $window.innerHeight - (mainMenu + toolBar + navBar + 2);
        angular.element('.porticoSdGridContainer').height(deviceHeight - 49);
      }

      /**
       * Set the stage dimension on window resize
       */
      function onReSize() {
        setStageDimension();
      }

      angular.element($window).bind('resize', onReSize);
      scope.$on(ViewEvent.BIRD_VIEW_LOADED, setStageDimension);
      scope.$on(PorticoSDEvent.UPDATE_TABULAR_VIEW_HEIGHT, setStageDimension);
    }

    //Directive Definition Object
    return {
      restrict: 'A',
      link: linkFn
    };
  }

  var app = angular.module('app'),
      requires = [
        '$window',
        'ViewEvent',
        'ngCartosUtils.services.UtilityService',
        'ngPorticoSD.constants.PorticoSDEvent',
        ngPorticoSDStageDimension
      ];
  app.directive('ngPorticoSdStageDimension', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  function BasketOfGoodsService(PorticoSDApiService, $routeParams, $q, Configuration) {
    var bogDataPool = {};

    /**
     * load data into the pool when.
     * @returns {*}
     */
    function loadData() {
      bogDataPool = {};
      var projectId = $routeParams.id, deferred = $q.defer();
      PorticoSDApiService.getBasketOfGoods(projectId).then(function (response) {
        bogDataPool[projectId] = {
          headers: response.data.data.headers,
          bogData: response.data.data.data
        };
        deferred.resolve(bogDataPool[projectId]);
      }, function (error) {
        deferred.reject(error);
      });
      return deferred.promise;
    }

    /**
     * get the data from the pool if not try to get from the bog service.
     * @param projectId
     * @returns {*}
     */
    function getBasketOfGoods(projectId) {
      var deferred = $q.defer();
      if (bogDataPool[projectId]) {
        deferred.resolve(bogDataPool[projectId]);
      } else {
        loadData().then(function (data) {
          deferred.resolve(bogDataPool[projectId]);
        }, function (error) {
          deferred.reject(error);
        })
      }
      return deferred.promise;
    }

    /**
     * function to sync the basket of goods.
     * @param projectId
     */
    function syncBasketOfGoods(projectId) {
      var deferred = $q.defer();
      PorticoSDApiService.syncBasketOfGoods(projectId).then(function (response) {
        bogDataPool[projectId] = {
          headers: response.data.data.headers,
          bogData: response.data.data.data
        };
        deferred.resolve(bogDataPool[projectId]);
      }, function (error) {
        deferred.reject(error);
      });
      return deferred.promise;
    }

    /**
     * function to import the basket of goods.
     * @param projectId
     * @param ttProjectId
     * @returns {*}
     */
    function importBasketOfGoods(projectId, ttProjectId) {
      var deferred = $q.defer();
      PorticoSDApiService.importBasketOfGoods(projectId, ttProjectId).then(function (response) {
        if (response.status === Configuration.HTTP_STATUS_OK) {
          bogDataPool[projectId] = {
            headers: response.data.data.headers,
            bogData: response.data.data.data
          };
          deferred.resolve(bogDataPool[projectId]);
        } else if (response.status === Configuration.HTTP_STATUS_NO_CONTENT) {
          deferred.resolve(response);
        } else {
          deferred.reject("Api status is not as expected");
        }
      }, function (error) {
        deferred.reject(error);
      });
      return deferred.promise;
    }

    return {
      importBasketOfGoods: importBasketOfGoods,
      loadBasketOfGods: loadData,
      getBasketOfGoods: getBasketOfGoods,
      syncBasketOfGoods: syncBasketOfGoods

    }
  }

  var app = angular.module('app'),
      requries = [
        'PorticoSDApiService',
        '$routeParams',
        '$q',
        'Configuration',
        BasketOfGoodsService
      ];
  app.factory('BasketOfGoodsService', requries);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  function BasketService($q, $log, $routeParams, Config, OutcomeType, BasketHelperService, PorticoSDApiService,
                         DeviceDataSetService, SharedService, translateFilter, Utils, BasketOfGoodsService,
                         OutcomeService, Notify, AssetService, AccessoriesDataService, TcoPricingService,
                         ProjectSettingService, CustomizedBasketService) {

    var CONFIDENCE = {
      HIGH: 'Confidence 1-High',
      MEDIUM: 'Confidence 2-Medium',
      LOW: 'Confidence 3-Low'
    }, PRECISION = {TWO: 2, FIVE: 5, ZERO: 0};

    var hpStateHeads = Config.HpMpsSalable.stateColumns,
        bogStateHeads = Config.BasketOfGoods.stateColumns,
        actualMonthlyPageVolumeMono = 0, actualMonthlyPageVolumeColor = 0,
        globalAssignee = {};

    /**
     * get the list for hp salable list
     * @returns {*}
     */
    function getHpSalableList() {
      var countryInfo = SharedService.getCurrentLevel('COUNTRY'),
          region = SharedService.getRegionFromCountrySet(countryInfo.name), deferred = $q.defer();
      DeviceDataSetService.getHpMpsSalableDevices(region.countryCode).then(function (data) {
        deferred.resolve({
          source: Config.HpMpsSalable.name,
          data: data
        });
      }, function (error) {
        deferred.reject(error);
      });
      return deferred.promise;
    }

    /**
     * To populate current pricing pricing
     * @param isTco
     * @param isBog
     * @param model
     * @param asset
     */
    function populatePricing(source, model, asset) {
      if (source === Config.PRICING_SOURCE.TCO) {
        var tcoPricing = TcoPricingService.getBasicAssetPricing(asset._id);
        var tcoPricingCurrentBasic = tcoPricing ? tcoPricing.currentBasic : null;
        if (tcoPricingCurrentBasic) {
          model.baseFee = tcoPricingCurrentBasic.baseFee;
          model.monoCPP = tcoPricingCurrentBasic.monoCPP;
          model.colorCPP = tcoPricingCurrentBasic.colorCPP;
          model.colorProCPP = tcoPricingCurrentBasic.colorProCPP;
          model.monthlyCost = tcoPricingCurrentBasic.totalProposedMonthlyCost;

        }
      }
      else if (source === Config.PRICING_SOURCE.MANUAL) {
        model.baseFee = asset.currentBaseMonthlyCost || 0;
        model.monoCPP = Utils.toNumber(asset.currentMonoCPP, PRECISION.FIVE);
        model.colorCPP = Utils.toNumber(asset.currentColorCPP, PRECISION.FIVE);
        model.colorProCPP = Utils.toNumber(asset.currentColorProCPP, PRECISION.TWO);
        model.monthlyCost = Utils.toNumber(asset.totalCurrentMonthlyCost, PRECISION.FIVE);
      }
    }

    /**
     * function to populate current state table for mps basket list.
     * @param assets
     * @param models
     */
    function populateCurrentMpsSalable(assets, models) {
      try {
        actualMonthlyPageVolumeMono = 0;
        actualMonthlyPageVolumeColor = 0;
        var currentBaseMonthlyCost = 0,
            currentMonoCPP = 0, currentColorCPP = 0, currentColorProCPP = 0,
            colorTotalMonthly = 0, monoTotalMonthly = 0, lifeTotalMonthly = 0,
            source,
            pricingSetting = (function () {
              return ProjectSettingService.getProjectSettings($routeParams.id) || {};
            }()).pricingSettings || {};
        source = pricingSetting ? pricingSetting.pricingSource : null;
        assets.forEach(function (asset, index) {
          models[index] = {};
          populatePricing(source, models[index], asset);
          currentBaseMonthlyCost = isNaN(asset.currentBaseMonthlyCost) ? 0.00 : Utils.toNumber(asset.currentBaseMonthlyCost, PRECISION.FIVE);
          monoTotalMonthly = isNaN(asset.monoTotalMonthly) ? 0 : Utils.toFloor(asset.monoTotalMonthly);
          currentMonoCPP = isNaN(asset.currentMonoCPP) ? 0.00 : Utils.toNumber(asset.currentMonoCPP, PRECISION.FIVE);
          colorTotalMonthly = isNaN(asset.colorTotalMonthly) ? 0 : Utils.toFloor(asset.colorTotalMonthly);
          currentColorCPP = isNaN(asset.currentColorCPP) ? 0.00 : Utils.toNumber(asset.currentColorCPP, PRECISION.FIVE);
          currentColorProCPP = isNaN(asset.currentColorProCPP) ? 0.00 : Utils.toNumber(asset.currentColorProCPP, PRECISION.TWO);
          lifeTotalMonthly = asset.lifeTotalMonthly ? Utils.toFloor(asset.lifeTotalMonthly) : Utils.toFloor(colorTotalMonthly + monoTotalMonthly);
          models[index].category = translateFilter(asset.deviceType);
          models[index].model = (asset.make || '') + ' ' + (asset.model || '');
          models[index].sku = asset.productNumber;
          models[index].totalPages = lifeTotalMonthly;
          actualMonthlyPageVolumeMono += monoTotalMonthly;
          actualMonthlyPageVolumeColor += colorTotalMonthly;
        });
      } catch (ex) {
        $log.error(ex);
      }
    }

    /**
     * function to populate the future table for the mps list.
     * @param properties
     * @param model
     */
    function populateFutureMpsSalable(properties, model) {
      try {
        var proposedMonthlyPageVolumeMono = 0, proposedMonthlyPageVolumeColor = 0;
        globalAssignee = {};
        model.category = translateFilter(properties.deviceType) || '';
        model.model = (properties.make || '') + ' ' + (properties.model || '');
        model.sku = properties.productNumber || '';
        globalAssignee.proposedBaseMonthlyCost = model.baseFee = isNaN(properties.proposedBaseMonthlyCost) ? 0.00 : Utils.toNumber(properties.proposedBaseMonthlyCost, PRECISION.TWO);
        globalAssignee.proposedMonoCPP = model.monoCPP = isNaN(properties.proposedMonoCPP) ? 0.00 : Utils.toNumber(properties.proposedMonoCPP, PRECISION.FIVE);
        globalAssignee.proposedColorCPP = model.colorCPP = isNaN(properties.proposedColorCPP) ? 0.00 : Utils.toNumber(properties.proposedColorCPP, PRECISION.FIVE);
        globalAssignee.proposedColorProCPP = model.colorProCPP = isNaN(properties.proposedColorProCPP) ? 0.00 : Utils.toNumber(properties.proposedColorProCPP, PRECISION.TWO);
        if (properties.outcome) {//outcome undefined when row selected
          if (properties.productNumber) {
            model.totalPages = proposedMonthlyPageVolumeMono = isNaN(properties.proposedMonthlyPageVolumeMono) ? 0 : Utils.toFloor(properties.proposedMonthlyPageVolumeMono);
            if (properties.hasColor) {
              model.totalPages += proposedMonthlyPageVolumeColor = isNaN(properties.proposedMonthlyPageVolumeColor) ? 0 : Utils.toFloor(properties.proposedMonthlyPageVolumeColor)
            }
          } else if (properties.outcome !== OutcomeType.REPLACE_WITH_NEW) {
            model.totalPages = proposedMonthlyPageVolumeMono = actualMonthlyPageVolumeMono;
            if (properties.hasColor) {
              model.totalPages += proposedMonthlyPageVolumeColor = actualMonthlyPageVolumeColor;
            }
          }
        } else {
          model.totalPages = proposedMonthlyPageVolumeMono = actualMonthlyPageVolumeMono;
          if (properties.hasColor) {
            model.totalPages += proposedMonthlyPageVolumeColor = actualMonthlyPageVolumeColor;
          }
        }
        globalAssignee.proposedMonthlyPageVolumeMono = Utils.toFloor(proposedMonthlyPageVolumeMono);
        globalAssignee.proposedMonthlyPageVolumeColor = properties.hasColor ? Utils.toFloor(proposedMonthlyPageVolumeColor) : 0;
        globalAssignee.totalProposedMonthlyCost = model.monthlyCost = Utils.toNumber(properties.totalProposedMonthlyCost, PRECISION.FIVE);
      } catch (ex) {
        $log.info(ex);
      }
    }

    /**
     * function to calculate totals for mps list.
     * @param model
     * @param future
     * @param current
     */
    function calculateTotals(model, future, current) {
      var totalCurrentMonthly = 0;
      for (var index = 0; index < current.length; index++) {
        totalCurrentMonthly += Utils.toNumber(current[index].monthlyCost, PRECISION.FIVE);
      }
      model.upgradeCost = Utils.toNumber(future.monthlyCost, PRECISION.FIVE);
      model.monthlySavings = Utils.toNumber((totalCurrentMonthly - model.upgradeCost), PRECISION.FIVE);
      model.paybackPeriod = !!model.monthlySavings ? Utils.toNumber(model.upgradeCost / model.monthlySavings, PRECISION.FIVE) : 0;
    }


    /**
     * callback for when row selected on mps list basket.
     * @param asset
     * @returns {*}
     */
    function onRowSelectCb(asset) {
      var countryInfo = SharedService.getCurrentLevel('COUNTRY'),
          region = SharedService.getRegionFromCountrySet(countryInfo.name), deferred = $q.defer();

      //required for pricing.
      asset.inventory = true;
      asset.countryCode = region.countryCode;
      asset.country = region.country;
      asset.region = region.region;

      function onSuccess(response) {
        var data = response.data.data[asset._id];
        var result = {
          hasColor: asset.hasColor,
          deviceType: asset.deviceType,
          productNumber: asset.productNumber,
          model: asset.model,
          make: asset.make,
          proposedBaseMonthlyCost: data.basic.baseFee,
          proposedMonoCPP: data.basic.monoCPP,
          proposedColorCPP: data.basic.colorCPP,
          totalProposedMonthlyCost: data.basic.totalProposedMonthlyCost,
          proposedMonthlyPageVolumeMono: asset.proposedMonthlyPageVolumeMono,
          proposedMonthlyPageVolumeColor: asset.proposedMonthlyPageVolumeColor
        };
        deferred.resolve(result);
      }

      function onError(error) {
        Notify.error("ERROR_FETCHING_PRICING");
        var result = {
          deviceType: asset.deviceType,
          productNumber: asset.productNumber,
          model: asset.model,
          make: asset.make,
          proposedMonthlyPageVolumeMono: asset.proposedMonthlyPageVolumeMono,
          proposedMonthlyPageVolumeColor: asset.proposedMonthlyPageVolumeColor
        };
        deferred.reject(result);
      }

      asset.state = Config.VIRTUAL_STATE;
      asset.proposedMonthlyPageVolumeMono = actualMonthlyPageVolumeMono;
      asset.proposedMonthlyPageVolumeColor = asset.hasColor ? actualMonthlyPageVolumeColor : 0;

      PorticoSDApiService.fetchTcoAssetPricing(asset).then(onSuccess, onError);
      return deferred.promise;
    }

    /**
     * bog row select callback
     * @param row
     * @returns {*}
     */
    function onBogRowSelectCb(row) {
      var deferred = $q.defer();
      deferred.resolve(row);
      return deferred.promise;
    }

    /**
     * function to populate current state table basket of goods.
     * @param assets
     * @param models
     */
    function populateCurrentBog(assets, models) {
      var source, tcoPricing, tcoPricingCurrentBasic, pricingSetting = (function () {
        return ProjectSettingService.getProjectSettings($routeParams.id) || {};
      }()).pricingSettings || {};
      source = pricingSetting ? pricingSetting.pricingSource : null;
      try {
        assets.forEach(function (asset, index) {
          models[index] = {};
          populatePricing(source, models[index], asset);
          models[index].category = translateFilter(asset.deviceType);
          models[index].model = (asset.make || '') + ' ' + (asset.model || '');
          models[index].sku = asset.productNumber;
          models[index].totalPages = asset.lifeTotalMonthly;
          if (!models[index].totalPages) {
            models[index].totalPages = Utils.toFloor(asset.monoTotalMonthly) +
                Utils.toFloor(asset.colorTotalMonthly);
          }
        });
      } catch (ex) {
        $log.error(ex);
      }
    }

    /**
     * function to populate the future state table.
     * @param properties
     * @param model
     */
    function populateFutureBog(properties, model) {
      //TODO: error handling task CAR-10173
      var totalVolume = 0;
      if (properties.deviceSolId) {
        model.category = properties.printerType || '';
        model.model = properties.description || '';
        model.sku = properties.deviceSolId || '';
        model.baseFee = isNaN(properties.baseFee) ? 0.00 : Utils.toNumber(properties.baseFee, PRECISION.TWO);
        model.monoCPP = isNaN(properties.monoClick) ? 0.00 : Utils.toNumber(properties.monoClick, PRECISION.FIVE);
        model.colorCPP = isNaN(properties.colorClick) ? 0.00 : Utils.toNumber(properties.colorClick, PRECISION.FIVE);
        model.colorProCPP = isNaN(properties.bestColor) ? 0.00 : Utils.toNumber(properties.bestColor, PRECISION.TWO);
        totalVolume = parseFloat(properties.monoPages) + parseFloat(properties.colorPages);
        model.totalPages = isNaN(totalVolume) ? '' : Utils.toFloor(totalVolume);
        model.monthlyCost = isNaN(properties.monthlyCost) ? 0.00 : Utils.toNumber(properties.monthlyCost, PRECISION.FIVE);
      } else {
        model.category = translateFilter(properties.deviceType) || '';
        model.model = (properties.make || '') + ' ' + (properties.model || '');
        model.sku = properties.productNumber || '';
        model.baseFee = isNaN(properties.proposedBaseMonthlyCost) ? 0.00 : Utils.toNumber(properties.proposedBaseMonthlyCost, PRECISION.TWO);
        model.monoCPP = isNaN(properties.proposedMonoCPP) ? 0.00 : Utils.toNumber(properties.proposedMonoCPP, PRECISION.FIVE);
        model.colorCPP = isNaN(properties.proposedColorCPP) ? 0.00 : Utils.toNumber(properties.proposedColorCPP, PRECISION.FIVE);
        model.colorProCPP = isNaN(properties.proposedColorProCPP) ? 0.00 : Utils.toNumber(properties.proposedColorProCPP, PRECISION.TWO);
        totalVolume = parseFloat(properties.proposedMonthlyPageVolumeMono) + parseFloat(properties.proposedMonthlyPageVolumeColor);
        model.totalPages = isNaN(totalVolume) ? '' : Utils.toFloor(totalVolume);
        model.monthlyCost = Utils.toNumber(properties.totalProposedMonthlyCost, PRECISION.FIVE);
      }

    }

    /**
     * function to get the basket of goods.
     * @returns {*}
     */
    function getBasketOfGoods() {
      var projectId = $routeParams.id, deferred = $q.defer();

      function onSuccess(data) {
        deferred.resolve({
          source: Config.BasketOfGoods.name,
          data: data.bogData,
          headers: data.headers
        });
      }

      function onError(error) {
        deferred.reject(error);
        SharedService.getErrorMessageByCode(error, 'BOG_HAS_NOT_BEEN_IMPORTED_FOR_THIS_PROJECT');
      }

      BasketOfGoodsService.getBasketOfGoods(projectId).then(onSuccess, onError);
      return deferred.promise;
    }

    /**
     * get acceorries update request.
     * @param accessories
     * @returns {Array}
     */
    function getAccessoriesRequest(accessories, assetId) {
      try {
        var accessoriesList = accessories.split(";") || [],
            accessoriesRequest = [], accessoryObjects = {};
        if (accessoriesList.length) {
          var index = accessoriesList.length;
          while (index--) {
            if (accessoriesList[index]) {
              if (!accessoryObjects[accessoriesList[index]]) {
                accessoryObjects[accessoriesList[index]] = {
                  assetId: assetId,
                  projectId: $routeParams.id,
                  CMO: 0,
                  FMO: 1,
                  accessoryNumber: accessoriesList[index],
                  description: (function () {
                    return AccessoriesDataService.getAccessoriesByAccessoriesNumber(accessoriesList[index].trim()) || {};
                  }()).description || ''
                };
                accessoriesRequest.push(accessoryObjects[accessoriesList[index]]);
              } else {
                accessoryObjects[accessoriesList[index]].FMO++;
              }
            }
          }
        }
        return accessoriesRequest;
      } catch (ex) {
        $log.error(ex);
        return [];
      }
    }

    /**
     * function to get the assignment request for bog list.
     * @param original
     * @param rowSelection
     * @returns {{outcomeUpdateReq: *, assigneeUpdates: *}}
     */
    function getBogAssignRequest(original, rowSelection) {
      var selectedAsset, basketAsset, outcome, updateOutcome, assignee,
          accessoriesRequest = [], accessories;
      if (!Utils.isValid(original) || !Utils.isValid(rowSelection)) {
        return;
      }
      try {
        selectedAsset = angular.copy(original);
        basketAsset = {};
        basketAsset.productNumber = rowSelection.sku || '';
        outcome = !!selectedAsset ? OutcomeService.getOutcomeByAssetId(selectedAsset._id) : {};
        updateOutcome = {
          _id: outcome._id,
          coordinates: outcome.coordinates,
          outcome: (outcome.outcome === OutcomeType.REPLACE_WITH_UNKNOWN) ? OutcomeType.REPLACE_WITH_NEW : outcome.outcome
        };
        assignee = Utils.getChanges(selectedAsset, basketAsset);
        var dataSet = DeviceDataSetService.getDeviceDataSetFromProductNumber(basketAsset.productNumber);
        if (dataSet._id) {
          assignee.confidence = CONFIDENCE.HIGH;
        } else {
          //settting the confidence as low since not found in DDS
          assignee.confidence = CONFIDENCE.LOW;
          //get device type based on bog device type.
          assignee.deviceType = SharedService.getDeviceType(rowSelection);
          assignee.model = rowSelection.description || '';
          assignee.make = '';
        }
        delete dataSet._id;
        for (var key in dataSet) {
          if (dataSet.hasOwnProperty(key)) {
            if ((!assignee[key] && !Utils.isBoolean(assignee[key]))) {
              assignee[key] = dataSet[key];
            }
          }
        }
        //delete co-ordinates if in tabular view and not plotted.
        if (!SharedService.isFloorMapView() && !AssetService.isAssetPlacedOnFloor(updateOutcome._id)) {
          delete updateOutcome.coordinates;
        }
        assignee._id = original._id;
        assignee.outcome = updateOutcome.outcome;
        if (updateOutcome.outcome !== OutcomeType.REPLACE_WITH_NEW || updateOutcome.outcome !== OutcomeType.ADD) {
          delete updateOutcome.outcome;
        }
        assignee.state = Config.VIRTUAL_STATE;
        assignee.isManual = false;
        if (rowSelection.accessoryList) {
          accessories = angular.copy(rowSelection.accessoryList);
        }
        if (rowSelection.customAccessories && rowSelection.customAccessories.length) {
          let customAccessories = rowSelection.customAccessories,
            customAccessoriesLength = customAccessories.length;
          for (let index = 0; index < customAccessoriesLength; index++) {
            if (customAccessories[index].accessoryList) {
              accessories += customAccessories[index].accessoryList;
            }
          }
        }
        accessoriesRequest = getAccessoriesRequest(accessories, assignee._id);
      } catch (ex) {
        $log.error("error in getting assign request", ex);
        assignee = {};
        outcome = null;
      }
      return {
        outcomeUpdateReq: !!outcome ? [updateOutcome] : [],
        assigneeUpdates: assignee,
        accessoriesRequest: accessoriesRequest
      };
    }

    /**
     * function to update the basket of goods
     * @returns {*}
     */
    function syncBogData() {
      var projectId = $routeParams.id, deferred = $q.defer();

      function onSuccess(data) {
        deferred.resolve({
          source: Config.BasketOfGoods.name,
          data: data.bogData,
          headers: data.headers
        });
        Notify.success("BOG_SYNC_SUCCESS");
      }

      function onError(error) {
        deferred.reject(error);
        Notify.error("BOG_SYNC_ERROR");
      }

      BasketOfGoodsService.syncBasketOfGoods(projectId).then(onSuccess, onError);
      return deferred.promise;
    }

    /**
     * function to get the assignment req for the hp salable assignment request.
     * @param original
     * @param rowData
     * @returns {{outcomeUpdateReq: *, assigneeUpdates: *}}
     */
    function getHpSalableAssignRequest(original, rowData) {
      var currentAsset, rowSelection, outcome, updateOutcome, assignee;
      if (!Utils.isValid(original) || !Utils.isValid(rowData)) {
        return;
      }
      try {
        currentAsset = angular.copy(original);
        rowSelection = angular.copy(rowData);
        delete rowData._id;
        outcome = !!currentAsset ? OutcomeService.getOutcomeByAssetId(currentAsset._id) : {};
        //updating the outcome of the asset.
        updateOutcome = {
          _id: outcome._id,
          coordinates: outcome.coordinates,
          outcome: (outcome.outcome === OutcomeType.REPLACE_WITH_UNKNOWN) ? OutcomeType.REPLACE_WITH_NEW : outcome.outcome
        };
        //getting the changes to update.
        assignee = Utils.getChanges(currentAsset, rowSelection);
		
		//removing proposed page volumes as they will get updated after page volume will be transferred.
        if (assignee.hasOwnProperty('proposedMonthlyPageVolumeColor')) {
          delete assignee.proposedMonthlyPageVolumeColor;
        }
        if (assignee.hasOwnProperty('proposedMonthlyPageVolumeMono')) {
          delete assignee.proposedMonthlyPageVolumeMono;
        }
		
        //remove coordinaates if in tabular view and assets not plotted.
        if (!SharedService.isFloorMapView() && !AssetService.isAssetPlacedOnFloor(updateOutcome._id)) {
          delete updateOutcome.coordinates;
        }
        assignee._id = currentAsset._id;
        assignee.outcome = updateOutcome.outcome;
        //deleting outocome if it hasn't changed.
        if (updateOutcome.outcome !== OutcomeType.REPLACE_WITH_NEW || updateOutcome.outcome !== OutcomeType.ADD) {
          delete updateOutcome.outcome;
        }
        assignee.state = Config.VIRTUAL_STATE;
        assignee.confidence = CONFIDENCE.HIGH;
        assignee.isManual = false;
      } catch (ex) {
        $log.error("error in getting assign request", ex);
        outcome = null;
        assignee = {};
      }
      return {
        outcomeUpdateReq: !!outcome ? [updateOutcome] : [],
        assigneeUpdates: assignee
      };
    }

    /**
     * function to load the initial configuration for the basket.
     */
    function loadBasket() {
      //hp mps salable list configuration.
      var configObject = {
        source: Config.HpMpsSalable.name,
        columns: Config.HpMpsSalable.columns,
        primaryKey: Config.HpMpsSalable.primaryKey,
        translate: [Config.HpMpsSalable.columns[3]],
        stateColumns: hpStateHeads,
        titles: {
          "productNumber": "sku"
        },
        dataProvider: getHpSalableList,
        updateData: {
          enabled: false
        },
        callbacks: {
          rowSelected: onRowSelectCb,
          assignReqCallback: getHpSalableAssignRequest
        },
        populate: {
          current: populateCurrentMpsSalable,
          future: populateFutureMpsSalable,
          totals: calculateTotals
        }
      };
      BasketHelperService.setSourceConfig(configObject);

      //basket of goods configuration.
      configObject = {
        source: Config.BasketOfGoods.name,
        columns: Config.BasketOfGoods.columns,
        primaryKey: Config.BasketOfGoods.primaryKey,
        stateColumns: bogStateHeads,
        numberFields: {},
        dataProvider: getBasketOfGoods,
        updateData: {
          enabled: true,
          sync: syncBogData,
          getFavouriteAssets: CustomizedBasketService.getFavouriteAssets
        },
        callbacks: {
          rowSelected: onBogRowSelectCb,
          assignReqCallback: getBogAssignRequest
        },
        populate: {
          current: populateCurrentBog,
          future: populateFutureBog,
          totals: calculateTotals
        },
      };
      configObject.numberFields[Config.BasketOfGoods.baseFee] = PRECISION.TWO;
      configObject.numberFields[Config.BasketOfGoods.monoClick] = PRECISION.FIVE;
      configObject.numberFields[Config.BasketOfGoods.colorClick] = PRECISION.FIVE;
      configObject.numberFields[Config.BasketOfGoods.bestColor] = PRECISION.FIVE;
      BasketHelperService.setSourceConfig(configObject);
    }

    return {
      loadBasket: loadBasket
    };
  }

  var app = angular.module('app'),
      requries = [
        '$q',
        '$log',
        '$routeParams',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'ngCartosCanvas.constants.OutcomeType',
        'ngCartosIntegration.services.BasketHelperService',
        'PorticoSDApiService',
        'ngCartosCore.services.DeviceDataSetService',
        'SharedService',
        'translateFilter',
        'ngCartosUtils.services.UtilityService',
        'BasketOfGoodsService',
        'ngCartosCore.services.OutcomeService',
        'ngCartosCore.services.NotifierService',
        'ngCartosCore.services.AssetService',
        'ngCartosCore.services.AccessoriesDataMapService',
        'TcoPricingService',
        'ngCartosCore.services.ProjectSettingService',
        'ngPorticoSD.services.CustomizedBasketService',
        BasketService
      ];
  app.factory('BasketService', requries);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  function CurrencyRateService($q, PorticoSDApiService, SharedService) {

    /**
     * fetch currency conversion rates
     */
    function fetchCurrencyRates() {
      var deferred = $q.defer();

      function onSuccess(response) {
        SharedService.currencyRatesData(response.data.data);
        deferred.resolve({currencyRates: response.data});
      }

      function onError(error) {
        deferred.reject(error);
      }

      PorticoSDApiService.fetchCurrencyRates().then(onSuccess, onError);
      return deferred.promise;
    }


    return {
      fetchCurrencyRates: fetchCurrencyRates
    };
  }

  var app = angular.module('app'),
      requires = [
        '$q',
        'PorticoSDApiService',
        'SharedService',
        CurrencyRateService
      ];
  app.factory('CurrencyRateService', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  function CustomizedBasketService($q, $log, PorticoSDApiService, $routeParams, Configuration, NotifierService) {

    /**
     * To find the common accessories by comparing the bog accessories and add-ons accessories
     * @param assetAccessories
     * @param accessoriesFromBog
     * @param accessoryList of the the device
     * @returns commonAccessories
     */
    function getCommonAccessories(assetAccessories, accessoriesFromBog, accessoryList) {
      let commonAccessories = [];

      assetAccessories.filter(function (assetAcc) {
        accessoriesFromBog.filter(function (bogAcc) {
          if (assetAcc.accessoryNumber === bogAcc.accessoryList.replace(';', '').trim()) {
            commonAccessories.push(bogAcc);
          }
        });
      });

      return getStandAloneAccessories(commonAccessories, accessoryList);
    }

    /**
     * To find common and stand alone accessories
     * Removing common accessories which are already part of the device
     * @param commonAccessories
     * @param accessoryList
     * @returns commonAccessories
     */
    function getStandAloneAccessories(commonAccessories, accessoryList) {

      var deviceAcc = accessoryList.split(";");
      deviceAcc.pop();

      for (let commonAccessoryIndex = 0; commonAccessoryIndex < commonAccessories.length; commonAccessoryIndex++) {
        for (let deviceAccIndex = 0; deviceAccIndex < deviceAcc.length; deviceAccIndex++) {
          if (deviceAcc[deviceAccIndex] === commonAccessories[commonAccessoryIndex].accessoryList.replace(';', '').trim()) {
            commonAccessories.splice(commonAccessoryIndex, 1);
          }
        }
      }
      return commonAccessories;
    }

    /**
     * To get the selected accessories's number's array
     * @param selectedAccessories
     * @returns Selected accessories's number's List from the popup
     */
    function getAccessories(selectedAccessories) {
      return selectedAccessories.map(accessory => accessory.accessoryList.replace(';', '').trim());
    }

    /**
     * Prepare request
     * @param selectedAccFromBog
     * @returns accList
     */
    function getRequest(selectedAccFromBog) {
      let projectId = $routeParams.id,
        accProductNumber = getAccessories(selectedAccFromBog.accessoriesSelected);
      return {
        "porticoProjectId": projectId,
        "customObject": {"modelName": selectedAccFromBog.favModelName, "customAcc": JSON.stringify(accProductNumber)},
        "deviceSolutionId": selectedAccFromBog.row.deviceSolId
      };
    }

    /**
     * Prepare request
     * @param selectedAccFromBog
     * @returns accList
     */
    function getUpdateRequest(selectedAccFromBog) {
      let projectId = $routeParams.id,
        accProductNumber = getAccessories(selectedAccFromBog.accessoriesSelected);
      return {
        "porticoProjectId": projectId,
        "customObject": {"modelName": selectedAccFromBog.favModelName, "customAcc": JSON.stringify(accProductNumber)},
        "deviceSolutionId": selectedAccFromBog.row.deviceSolId,
        "_id": selectedAccFromBog.row._id
      };
    }

    /**
     * To save "BYOBOG" into the db
     * @param selectedAccFromBog
     * @returns deferred.promise
     */
    function addAssetInFavouriteList(selectedAccFromBog) {
      let deferred = $q.defer(),
        request = getRequest(selectedAccFromBog);

      function onSuccess(response) {
        if (response && response.status === Configuration.HTTP_STATUS_OK) {
          deferred.resolve(response);
        } else {
          onError('No data found in response');
        }
      }

      function onError(error) {
        $log.error('Error while adding favourite asset from MY FAVOURITE table', error);
        deferred.reject();
      }

      PorticoSDApiService.addAssetInFavouriteList(request).then(onSuccess, onError);
      return deferred.promise;
    }

    function deleteFavouriteBogAsset(favouriteBogAssetId) {
      let deferred = $q.defer();

      function onSuccess(response) {
        if (response) {
          deferred.resolve(response);
        } else {
          onError('No data found in response');
        }
      }

      function onError(error) {
        $log.error('Error while deleting favourite asset from MY FAVOURITE table', error);
        deferred.reject();
      }

      PorticoSDApiService.deleteFavouriteBogAsset(favouriteBogAssetId).then(onSuccess, onError);
      return deferred.promise;
    }

    function updateFavouriteBogAsset(selectedAccFromBog) {
      let deferred = $q.defer(),
        request = getUpdateRequest(selectedAccFromBog);

      function onSuccess(response) {
        if (response) {
          deferred.resolve(response);
        } else {
          onError('No data found in response');
        }
      }
      function onError(error) {
        $log.error('Error while updating favourite asset from MY FAVOURITE table', error);
        deferred.reject();
      }
      PorticoSDApiService.updateFavouriteBogAsset(request).then(onSuccess, onError);
      return deferred.promise;
    }

    function getFavouriteAssets() {
      let deferred = $q.defer(),
        projectId = $routeParams.id;

      function onSuccess(response) {
        if (response && response.status === Configuration.HTTP_STATUS_OK) {
          deferred.resolve(response);
        } else {
          onError('No data found in response');
        }
      }
      function onError(error) {
        $log.error('Error in getting MY FAVOURITE table data', error);
        deferred.reject(error);
      }
      PorticoSDApiService.getFavouriteAssets(projectId).then(onSuccess, onError);
      return deferred.promise;
    }

    /**
     * Function to update stand alone accessories for my favourite popup.
     * @param customAccessories
     * @param commonAccessories
     */
    function getLastCheckedAccessories(customAccessories, commonAccessories) {
      let customAccessoriesLength = customAccessories.length,
        commonAccessoriesLength = commonAccessories.length;
      for (let customAccessoryIndex = 0; customAccessoryIndex < customAccessoriesLength; customAccessoryIndex++) {
        for (let index = 0; index < commonAccessoriesLength; index++) {
          if (customAccessories[customAccessoryIndex].accessoryList === commonAccessories[index].accessoryList) {
            commonAccessories[index].checked = true;
            break;
          }
        }
      }
    }

    function notifySuccess(apiCallMethod) {
      if (apiCallMethod === "POST") {
        NotifierService.success('ASSET_ADDED_FAVORITE_BASKET_SUCCESSFULLY');
      }
      if (apiCallMethod === "PUT") {
        NotifierService.success('ASSET_UPDATED_FAVORITE_BASKET_SUCCESSFULLY');
      }
      if (apiCallMethod === "DELETE") {
        NotifierService.success('ASSET_DELETED_FAVORITE_BASKET_SUCCESSFULLY');
      }
    }

    return {
      getCommonAccessories: getCommonAccessories,
      addAssetInFavouriteList: addAssetInFavouriteList,
      getFavouriteAssets: getFavouriteAssets,
      updateFavouriteBogAsset: updateFavouriteBogAsset,
      deleteFavouriteBogAsset: deleteFavouriteBogAsset,
      getLastCheckedAccessories: getLastCheckedAccessories,
      notifySuccess: notifySuccess
    }
  }

  var app = angular.module('app'),
      requries = [
        '$q',
        '$log',
        'PorticoSDApiService',
        '$routeParams',
        'Configuration',
        'ngCartosCore.services.NotifierService',
        CustomizedBasketService
      ];
  app.factory('ngPorticoSD.services.CustomizedBasketService', requries);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /**
   * Constructor of mv cost data. It should be used to create the instance of an mv cost data
   * @param {object} data properties of mv cost.
   */
  function MvCostData(data) {
    if (data !== undefined) {
      this.setData(data);
    }
  }

  MvCostData.prototype = {
    /**
     * Set properties to mv cost data. Uses angular.extend to assign the values.
     * @param {object} data properties to be set.
     */
    setData: function (data) {
      angular.extend(this, data);
    }
  };

  function MultiVendorService($q, $log, PorticoSDApiService, PorticoSDSharedService, AppSettingService,
                              PorticoSDConfiguration, PorticoSDPoolManager) {

    var mvCostPool = [],
        retrieveInstance = PorticoSDPoolManager.retrieveInstance.bind(null, MvCostData, mvCostPool),
        removeInstance = PorticoSDPoolManager.remove.bind(null, mvCostPool);

    /**
     * Function returns true or false on the basis of mv cost fetched or not
     * @returns {boolean}
     */
    function isMvCostFetched() {
      return !!(mvCostPool && mvCostPool.length);
    }

    /**
     * Make call to get multi vendor cost
     * @param latest
     */
    function fetchMvCost(latest) {
      var deferred = $q.defer();
      function onSuccess(response) {
        if (response && Array.isArray(response.data)) {
          $log.info('Successfully loaded multi vendor cost');
          storeMvCostInPool(response.data);
          deferred.resolve();
        }
        else {
          onError('Response is not in array format');
        }
      }

      function onError(error) {
        $log.error('Error in loading multi vendor cost', error);
        deferred.reject();
      }

      PorticoSDApiService.fetchMvCost(latest).then(onSuccess, onError);
      return deferred.promise;
    }

    /**
     * Function to store device cost into mvCost Pool
     * @param devices
     */
    function storeMvCostInPool(devices) {
      var key, valueOk = {status: PorticoSDConfiguration.OK}, valueEmpty = {status: PorticoSDConfiguration._EMPTY},
          devicesLength = devices.length || 0;
      for (var index = 0; index < devicesLength; index++){
        if (devices[index].productId) {
          devices[index].productId = devices[index].productId.padStart(8, PorticoSDConfiguration.ZERO);
          if (devices[index].mvSupply) {
            if (devices[index].supplyCostStatus) {
              key = devices[index].productId + PorticoSDConfiguration.PLUS + devices[index].mvSupply;
              retrieveInstance(key, valueOk);
            }
            else {
              key = devices[index].productId + PorticoSDConfiguration.PLUS + devices[index].mvSupply;
              retrieveInstance(key, valueEmpty);
            }
          }
          if (devices[index].mvService) {
            if (devices[index].serviceCostStatus) {
              key = devices[index].productId + PorticoSDConfiguration.PLUS + devices[index].mvService;
              retrieveInstance(key, valueOk);
            }
            else {
              key = devices[index].productId + PorticoSDConfiguration.PLUS + devices[index].mvService;
              retrieveInstance(key, valueEmpty);
            }
          }
        }
      }
    }

    /**
     * Function to update mv cost status in tabular data
     * @param productId
     * @param mvTemplateName
     */
    function updateMvCostStatus(productId, mvTemplateName) {
      var mvCostStatus = "";
      if (mvCostPool && mvCostPool.length && productId && mvTemplateName) {
        productId = productId.padStart(8, PorticoSDConfiguration.ZERO);
        mvCostStatus = retrieveInstance(productId + PorticoSDConfiguration.PLUS + mvTemplateName);
        mvCostStatus = mvCostStatus.value.status || "";
      }
      return mvCostStatus;
    }

    /**
     * Make call to get multi vendor templates
     * @param latest
     */
    function getMvTemplates(latest) {
      var deferred = $q.defer();

      function onSuccess(response) {
        if (response && Array.isArray(response.data)) {
          $log.info('Successfully loaded multi vendor templates');
          PorticoSDSharedService.mvTemplates(response.data);
          deferred.resolve();
        }
        else {
          $log.error('Error in loading multi vendor templates');
          deferred.reject();
        }
      }

      function onError(error) {
        $log.error('Error in loading multi vendor templates', error);
        deferred.reject(error);
      }

      PorticoSDApiService.getMvTemplates(latest).then(onSuccess, onError);
      return deferred.promise;
    }

    /**
     * Make call to get multi vendor template cost
     * @param mvTemplate
     * @param deviceId
     * @param printTechnology
     * @param hasColor
     * @param templateType
     */
    function getMvTemplateCost(mvTemplate, deviceId, printTechnology, hasColor, templateType) {
      var deferred = $q.defer();

      function onSuccess(response) {
        if (response && Array.isArray(response.data)) {
          $log.info('Successfully loaded multi vendor template cost');
          deferred.resolve(response.data);
        }
        else {
          $log.error('Error in loading multi vendor template cost');
          deferred.reject();
        }
      }

      function onError(error) {
        $log.error('Error in loading multi vendor template cost', error);
        deferred.reject(error);
      }

      PorticoSDApiService.getMvTemplateCost(mvTemplate, deviceId, printTechnology, hasColor,
        templateType).then(onSuccess, onError);
      return deferred.promise;
    }

    /**
     * Get multi vendor template based on template type and country code.
     * @param countryCode
     * @returns {Array}
     */
    function getMvTemplatesForCountry(countryCode) {
      var mvTemplates = {supply: [], service: []},
          templates = PorticoSDSharedService.mvTemplates();
      if (Array.isArray(templates) && templates.length) {
        templates.forEach(function (template) {
          if (template.templateType && template.countryCode && template.countryCode === countryCode) {
            if (template.templateType === PorticoSDConfiguration.MV_TEMPLATE_TYPE.SERVICE) {
              mvTemplates.service.push(template.templateName);
            }
            else if (template.templateType === PorticoSDConfiguration.MV_TEMPLATE_TYPE.SUPPLIES) {
              mvTemplates.supply.push(template.templateName);
            }
          }
        });
      }
      return mvTemplates;
    }

    /**
     * Set multi vendor template details for country.
     * @param country
     */
    function setMvDetailsForCountry(country) {
      var countrySet, mvDetails = {};

      if (!country) {
        return;
      }

      try {
        countrySet = AppSettingService.getRegionFromCountrySet(country.name);
        if (!(countrySet && countrySet.countryCode)) {
          return;
        }

        mvDetails = getMvTemplatesForCountry(countrySet.countryCode);
        PorticoSDSharedService.setMvDetailsForCountry(country._id, mvDetails);
      }
      catch (ex) {
        $log.error('Error in setting mv details for country', ex);
      }
    }

    /**
     * Get volume break level
     * @param mvTemplateName
     */
    function getVolumeBreakLevel(mvTemplateName) {
      var volumeBreakLevel = "", mvTemplate = [],
          mvTemplates;
      try {
        mvTemplates = PorticoSDSharedService.mvTemplates();
        if (Array.isArray(mvTemplates) && mvTemplates.length) {
          mvTemplate = mvTemplates.filter(function (template) {
            return template.templateName === mvTemplateName &&
                template.templateType === PorticoSDConfiguration.MV_TEMPLATE_TYPE.SERVICE;
          });
        }

        if (mvTemplate.length) {
          volumeBreakLevel = mvTemplate.first().volumeBreakLevel;
        }
        return volumeBreakLevel;
      }
      catch (ex) {
        $log.error('Error in getting volume break level', ex);
        return volumeBreakLevel;
      }
    }

    return {
      isMvCostFetched: isMvCostFetched,
      fetchMvCost: fetchMvCost,
      updateMvCostStatus: updateMvCostStatus,
      getMvTemplates: getMvTemplates,
      getMvTemplateCost: getMvTemplateCost,
      getVolumeBreakLevel: getVolumeBreakLevel,
      setMvDetailsForCountry: setMvDetailsForCountry,
      removeInstance: removeInstance
    };
  }

  var app = angular.module('app'),
      requires = [
        '$q',
        '$log',
        'PorticoSDApiService',
        'PorticoSDSharedService',
        'AppSettingService',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'ngPorticoSD.services.PorticoSDPoolManager',
        MultiVendorService
      ];
  app.factory('ngPorticoSD.services.MultiVendorService', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  function PorticoSDApi($http, $window, $routeParams, Configuration, $q, PorticoApiConfig) {

    var baseUrl = $window.location.protocol + '//' + $window.location.host,
        apiVersions = Configuration.API_VERSIONS,
        version1ApiUrl = baseUrl + apiVersions.V1,
        cdfExportApi = version1ApiUrl + '/cdfReport/',
        pricingApi = version1ApiUrl + '/pricing/project/',
        pricingServiceApi = baseUrl + '/portico/pricing/',
        bogApi = version1ApiUrl + '/basketofgoods/',
        currencyRatesApi = version1ApiUrl + '/currencyRates/',
        hpcsServiceUrl = baseUrl,   // Note : 'http://127.0.0.1:9004' is for local machine, Ngx will handle this.
        hpcsMultiVendorUrl = hpcsServiceUrl + '/api/portico/mv',
        reportUrl = baseUrl + '/api/portico/reports/', // Note : use 'http://127.0.0.1:9007' instead of baseUrl for report generation locally.
        tcoReportUrl = reportUrl + 'tco?project_id=',
        rfqReportUrl = reportUrl + 'mv?project_id=',
        tabularView = version1ApiUrl + '/tabularView/userSettings';

    /**
     * function to save the updated template in db
     * @param data : template data
     * @returns {*}
     */
    function saveTabularTemplate(data) {
      var url = version1ApiUrl + '/tabularView/userSettings';
      return $http.post(url, [data]);
    }

    /**
     * function to delete the selected template
     * @param data : template data
     * @returns {*}
     */
    function deleteTabularTemplate(data) {
      let url = version1ApiUrl + '/tabularView/userSettings?templateName=' + data;
      return $http.delete(url);
    }

    /**
     * function to get the tabular view user settings
     * @returns {*}
     */
    function getTabularSetting() {
      return $http.get(tabularView);
    }

    /**
     * Export CDF Report for the project.
     * @returns {object} promise.
     */
    function exportCDFReport() {
      var projectId = $routeParams.id;
      return $http.get(cdfExportApi + projectId);
    }

    /**
     * Export TCO Report for the project.
     * @returns {object} promise.
     */
    function exportTCOReport() {
      var projectId = $routeParams.id;
      return $http.get(tcoReportUrl + projectId);
    }

    /**
     * Export RFQ Report for the project.
     * @returns {object} promise.
     */
    function exportRfqReport() {
      var projectId = $routeParams.id;
      return $http.get(rfqReportUrl + projectId);
    }

    /**
     * Make a get call to fetch tco pricing data
     * @param state
     * @param pricingSource
     * @returns {object} promise.
     */
    function fetchTcoPricing(state, pricingSource) {
      var projectId = $routeParams.id,
          url = pricingApi + projectId + '?state=' + state + '&source=' + pricingSource + '&latest=true';
      return $http.get(url);
    }

    /**
     * Make a get call to get tco pricing from db.
     * @param pricingSource
     * @returns {object} promise.
     */
    function getTcoPricing(pricingSource) {
      var projectId = $routeParams.id,
          url = pricingApi + projectId + '?source=' + pricingSource + '&state=all&latest=false';
      return $http.get(url);
    }

    /**
     * function to get the TCO price for the asset.
     * @param asset
     * @returns {*}
     */
    function fetchTcoAssetPricing(asset) {
      var url = version1ApiUrl + '/pricing/asset';
      return $http.post(url, [asset]);
    }

    /**
     * function to get the bog.
     * @param projectId
     * @returns {*}
     */
    function getBasketOfGoods(projectId) {
      var url = bogApi + projectId;
      return $http.get(url);
    }

    /**
     * function to get the latest bog.
     * @param projectId
     * @returns {*}
     */
    function syncBasketOfGoods(projectId) {
      var url = bogApi + projectId + '?latest=true';
      return $http.get(url);
    }

    /**
     * import Basket of Good for the project.
     * @returns {object} promise.
     */
    function importBasketOfGoods(projectId, ttProjectId) {
      var url = bogApi + projectId;
      return $http.post(url, [ttProjectId]);
    }

    /**
     * function to save tco adjustment.
     * @param changes
     * @returns
     */
    function saveTcoAdjustment(changes) {
      var projectId = $routeParams.id,
          adjustmentChanges = {
            "projectId": projectId,
            "current": changes.current,
            "future": changes.future
          };
      var url = pricingApi + 'CMOCostSettings/' + projectId;
      return $http.post(url, [adjustmentChanges]);
    }

    /**
     * function to save tco adjustment.
     * @returns
     */
    function getTcoAdjustment() {
      var projectId = $routeParams.id,
          url = pricingApi + 'CMOCostSettings/' + projectId;
      return $http.get(url);
    }

    /**
     * Make a call to fetch currency conversion rates
     * @returns {*}
     */
    function fetchCurrencyRates() {
      return $http.get(currencyRatesApi);
    }

    /**
     * Make a put call to get asset tco pricing from HP and store in db as well.
     * @returns {object} promise.
     */
    function updateAssetsTcoPricing(state, assets) {
      var projectId = $routeParams.id,
          url = pricingApi + projectId + '?state=' + state + '&pricingSource=tco&latest=true';
      return $http.put(url, assets);
    }

    /**
     * Get proxy device
     * @param source tco
     * @param latest
     * true : Will fetch the latest data from hp server and override existing db
     * false : Wil fetch the data from db
     */
    function getProxyDevices(source, latest) {
      var url = pricingServiceApi + 'proxyDevices?source=' + source + '&latest=' + latest;
      return $http.get(url);
    }

    /**
     * Update proxy devices pricing
     * @param assetsRequest
     */
    function updateProxyDevicesPricing(assetsRequest) {
      var url = baseUrl + '/pricing/printers/project/proxyDevices/' + assetsRequest.projectId + '?source=' + assetsRequest.source + '&state=' +
          assetsRequest.state;
      return $http.put(url, assetsRequest);
    }


    /**
     * Get multi vendor templates
     * @param latest
     */
    function getMvTemplates(latest) {
      var url = hpcsMultiVendorUrl + '/templates?latest=' + latest;
      return $http.get(url);
    }

    /**
     *
     * @param mvTemplate
     * @param deviceId
     * @param printTechnology
     * @param hasColor
     * @param templateType
     */
    function getMvTemplateCost(mvTemplate, deviceId, printTechnology, hasColor, templateType) {
      var costType = PorticoApiConfig.DEVICE,
        query = `?project_id=${$routeParams.id}&device_id=${deviceId}&template_name=${mvTemplate}` +
          `&template_type=${templateType}&print_technology=${printTechnology}&has_color=${hasColor}&cost_type=${costType}`,
        url = `${hpcsMultiVendorUrl}/comp-cost${query}`;
      return $http.get(url);
    }

    /**
     * Fetch MV Cost for the project.
     * @returns {object} promise.
     */
    function fetchMvCost(latest) {
      var projectId = $routeParams.id,
          url = hpcsMultiVendorUrl + '/comp-cost-status?project_id=' + projectId + '&latest=' + latest;
      return $http.get(url);
    }

    function addAssetInFavouriteList(data) {
      let url = version1ApiUrl + "/byobog";
      return $http.post(url, data);
    }

    function getFavouriteAssets(projectId) {
      var url = version1ApiUrl + "/byobog?porticoProjectId=" + projectId;
      return $http.get(url);
    }

    function deleteFavouriteBogAsset(id) {
      var url = version1ApiUrl + "/byobog?_id=" + id;
      return $http.delete(url);
    }

    function updateFavouriteBogAsset(request) {
      var url = version1ApiUrl + "/byobog";
      return $http.put(url, request);
    }

    return {
      exportCDFReport: exportCDFReport,
      exportTCOReport: exportTCOReport,
      exportRfqReport: exportRfqReport,
      fetchTcoPricing: fetchTcoPricing,
      getTcoPricing: getTcoPricing,
      fetchTcoAssetPricing: fetchTcoAssetPricing,
      getBasketOfGoods: getBasketOfGoods,
      syncBasketOfGoods: syncBasketOfGoods,
      importBasketOfGoods: importBasketOfGoods,
      fetchCurrencyRates: fetchCurrencyRates,
      saveTcoAdjustment: saveTcoAdjustment,
      getTcoAdjustment: getTcoAdjustment,
      updateAssetsTcoPricing: updateAssetsTcoPricing,
      getProxyDevices: getProxyDevices,
      updateProxyDevicesPricing: updateProxyDevicesPricing,
      getMvTemplates: getMvTemplates,
      getMvTemplateCost: getMvTemplateCost,
      fetchMvCost: fetchMvCost,
      getTabularSetting: getTabularSetting,
      saveTabularTemplate: saveTabularTemplate,
      deleteTabularTemplate: deleteTabularTemplate,
      addAssetInFavouriteList: addAssetInFavouriteList,
      getFavouriteAssets: getFavouriteAssets,
      deleteFavouriteBogAsset: deleteFavouriteBogAsset,
      updateFavouriteBogAsset: updateFavouriteBogAsset
    };
  }

  var app = angular.module('app'),
      requires = [
        '$http',
        '$window',
        '$routeParams',
        'Configuration',
        '$q',
        'ngPorticoSD.constants.PorticoApiConfig',
        PorticoSDApi
      ];
  app.factory('PorticoSDApiService', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  //Service to Handle tabular view disposition
  function PorticoSDDispositionService($rootScope, $routeParams, $q, $log, OutcomeType, AssetType, OutcomeProperties, Configuration,
                                       PorticoSDConfiguration, SharedService, AssetService, OutcomeService, StageHelper,
                                       LevelEntityMapService, AssetMapService, SchemeSetService, PorticoSDHelperService,
                                       AssetProperties, NotifierService, UtilityService, RequestBuilder, TransitionHelper,
                                       PorticoSDService, AssetReplaceRequestBuilder, AssetRepositionRequestBuilder, OutcomeHandler,
                                       SyncTcoPricingService) {

    /**
     * To Apply MAIKeep disposition on  selected single/multiple assets
     * @param assets
     */
    function retainAssets(assets, outcomeType) {
      var assetsRequest = [],
          outcomesRequest = [],
          deferred = $q.defer();

      // creating multiple asset and outcome update request.
      function createRequest(asset) {
        var updateAsset, updateOutcome,
            levelEntityMapData = LevelEntityMapService.getLevelEntityMapByEntityId(asset.getId());
        updateAsset = {
          _id: asset.getId(),
          outcome: OutcomeType.RETAIN
        };
        updateOutcome = {
          transitionName: Configuration.TRANSITION_NAME,
          assetId: asset.getId(),
          levelId: levelEntityMapData.levelId,
          outcome: OutcomeType.RETAIN
        };
        if (outcomeType === PorticoSDConfiguration.RETAIN_ADVANCE_PLACEMENT) {
          updateAsset.subDispositions = PorticoSDConfiguration.ADVANCE_PLACEMENT;
        }
        assetsRequest.push(updateAsset);
        outcomesRequest.push(updateOutcome);
      }

      assets.forEach(createRequest);
      $log.info('Retaining assets...');

      //call back on Retain success
      function onRetainSuccess(data) {
        deferred.resolve(data);
      }

      //call back on Retain error
      function onRetainError(error) {
        deferred.reject(error);
      }

      applyDispositionToAssets(assetsRequest, outcomesRequest).then(onRetainSuccess, onRetainError);
      return deferred.promise;
    }

    /**
     * Notify Cannot apply Disposition on empty Asset
     */
    function notifyEmptyAsset(asset) {
      NotifierService.info(PorticoSDConfiguration.OUTCOME_CANNOT_APPLY_MESSAGE);
      $log.info('Empty asset is clicked for outcome in transition state.');
    }

    /**
     *  To check any of the selected asset is having outcome or not
     * @param selectedAssets
     */
    function getHasOutcome(selectedAssets) {
      var hasOutcome = selectedAssets.find(function (asset) {
        return asset.hasOutcome();
      });
      return hasOutcome;
    }

    /**
     *  To filter only those assets which are having outcomes
     * @param selectedAssets
     */
    function filterResetAssets(selectedAssets) {
      var resetAssetIds = [], assetsWithNoOutcome = [];
      var result = selectedAssets.filter(function (asset) {
        if (asset.hasOutcome()) {
          resetAssetIds.push(asset._id);
        } else {
          assetsWithNoOutcome.push(asset);
        }
        return asset.hasOutcome();
      });
      if (assetsWithNoOutcome && assetsWithNoOutcome.length) {
        PorticoSDService.updateAssetsInPoolManager(assetsWithNoOutcome);
      }
      return resetAssetIds;
    }

    /**
     * To Apply Eliminate disposition on  selected single/multiple assets
     * @param assets
     */
    function retireAssets(assets) {
      var assetsRequest = [],
          outcomesRequest = [],
          deferred = $q.defer();

      // creating multiple asset and outcome update request.
      function createRequest(asset) {
        var updateAsset, updateOutcome,
            levelEntityMapData = LevelEntityMapService.getLevelEntityMapByEntityId(asset.getId());
        updateAsset = {
          _id: asset.getId(),
          outcome: OutcomeType.RETIRE
        };
        updateOutcome = {
          transitionName: Configuration.TRANSITION_NAME,
          assetId: asset.getId(),
          levelId: levelEntityMapData.levelId,
          outcome: OutcomeType.RETIRE
        };
        assetsRequest.push(updateAsset);
        outcomesRequest.push(updateOutcome);
      }

      assets.forEach(createRequest);
      $log.info('Retiring assets...');

      //call back on Retire success
      function onRetireSuccess(updatedAssets) {
        deferred.resolve(updatedAssets);
      }

      //call back on Retire error
      function onRetireError(error) {
        deferred.reject(error);
      }

      applyDispositionToAssets(assetsRequest, outcomesRequest).then(onRetireSuccess, onRetireError);
      return deferred.promise;
    }

    /**
     * Function is responsible for reset the outcome of an given assets.
     * Mainly it will reset Retain & Retire outcome.
     * @param assets {@code CanvasAsset} array of canvas assets.
     */
    function resetAssetsOutcome(assets) {
      var updateAssets = [],
          updateOutcomeIds = [],
          updateOutcomes = [],
          deferred = $q.defer();

      // creating the request for resetting the outcomes.
      function createRequest(asset) {
        var outcome = OutcomeService.getOutcomeByAssetId(asset.getId());
        updateAssets.push({
          _id: asset.getId(),
          subDispositions: '',
          outcome: '' //to reset the outcome in Asset set the outcome field as empty string
        });
        updateOutcomes.push(outcome);
        updateOutcomeIds.push(outcome._id);
      }

      assets.forEach(createRequest);

      // success callback, for updating the  asset after getting response.
      function onAssetsResetSuccess(response) {
        response.updateOutcomes = updateOutcomes;
        deferred.resolve(response);
      }

      // error callback on Asset reset
      function onAssetsResetError(error) {
        deferred.reject(error);
      }

      $log.info('Resetting an assets...');
      AssetMapService.resetOutcome(updateAssets, updateOutcomeIds).then(onAssetsResetSuccess, onAssetsResetError);
      return deferred.promise;
    }

    /**
     * Function will be triggered, when we reset the selected assets.
     * @param assets
     */
    function resetRepositionAssets(assets) {
      var request = AssetRepositionRequestBuilder.getResetRepositionRequest(assets),
          deferred = $q.defer();

      // reset reposition success callback
      function onResetRepositionSuccess(response) {
        deferred.resolve(response);
      }

      //reset reposition error callback
      function onResetRepositionError(error) {
        deferred.reject(error);
      }

      $log.info('Resetting reposition assets...');
      AssetMapService.resetReposition(request).then(onResetRepositionSuccess, onResetRepositionError);
      return deferred.promise;
    }

    /**
     * Function will be triggered, when we reset the virtual assets.
     * @param virtualAssets {@Code virtualAssets} array of virtualAssets.
     */
    function resetVirtualAssets(virtualAssets) {
      var transferRequest, assets = [],
          request = {
            deleteOutcomes: [],
            deleteAssets: []
          },
          deferred = $q.defer();

      // creating virtual asset request.
      function createRequest(virtualAsset) {
        assets.push(virtualAsset);
        request.deleteAssets.push(virtualAsset._id);
        request.deleteOutcomes.push(OutcomeService.getOutcomeByAssetId(virtualAsset._id)._id);
      }

      virtualAssets.forEach(createRequest);
      transferRequest = RequestBuilder.getMultipleAssetsDeleteRequest(assets);
      // TODO: Need to refactor the above function so that we can easily use extend function.
      request.updatedAssets = transferRequest.affectedAssets;
      request.transfersIds = transferRequest.deletedTransfers;

      // Reset virtual asset succes callback
      function onResetVirtualSuccess(response) {
        deferred.resolve(response);
      }

      // Reset virtual asset error callback
      function onError(error) {
        deferred.reject(error);
      }

      $log.info('Resetting virtual assets...');
      AssetMapService.removeVirtualAsset(request).then(onResetVirtualSuccess, onResetVirtualSuccess);
      return deferred.promise;
    }

    /**
     * Apply excludeAssetFromDesign outcome on asset
     * @param asset
     */
    function excludeAssetFromDesign(assets) {
      var assetsRequest = [],
          outcomesRequest = [],
          deferred = $q.defer();

      // creating multiple asset and outcome update request.
      function createRequest(asset) {
        var updateAsset, updateOutcome,
            levelEntityMapData = LevelEntityMapService.getLevelEntityMapByEntityId(asset.getId());
        updateAsset = {
          _id: asset.getId(),
          outcome: OutcomeType.EXCLUDE_DEVICE,
          excludeFromDesign: true
        };
        updateOutcome = {
          transitionName: Configuration.TRANSITION_NAME,
          assetId: asset.getId(),
          levelId: levelEntityMapData.levelId,
          outcome: OutcomeType.EXCLUDE_DEVICE
        };
        assetsRequest.push(updateAsset);
        outcomesRequest.push(updateOutcome);
      }

      assets.forEach(createRequest);

      //Exclude device success callback
      function onExcludeDeviceSucess(response) {
        var data = {};
        data.outcome = response.outcome;
        data.updatedAssets = response.updatedAssets;
        deferred.resolve(data);
      }

      //Exclude device error callback
      function onExcludeDeviceError(error) {
        deferred.reject(error);
      }

      applyDispositionToAssets(assetsRequest, outcomesRequest).then(onExcludeDeviceSucess, onExcludeDeviceError);
      return deferred.promise;
    }

    /**
     * Function will be triggered, when we reset the replace assets.
     * @param assets {@Code assets} array of  assets.
     */
    function resetReplaceAssets(assets) {
      var request = RequestBuilder.getResetReplaceRequest(assets),
          deferred = $q.defer();

      // reset replace success callback
      function onResetReplaceSuccess(response) {
        deferred.resolve(response);
      }

      //reset replace error callback
      function onResetReplaceError(error) {
        deferred.reject(error);
      }

      $log.info('Resetting replace assets...');
      AssetMapService.resetReplaceOutcome(request).then(onResetReplaceSuccess, onResetReplaceError);
      return deferred.promise;
    }

    /**
     * Function will update the pool data after reset.
     * @param response
     */
    function updatePoolAfterReset(response) {
      PorticoSDService.setTcoPricingForRemovedAssets(response.deletedAssets);
      // Remove assets in pool manager
      PorticoSDService.removeAssetInPoolManager(response.deletedAssets);

      // Update assets in pool manager
      response.updated.assets.forEach(function (asset) {
        PorticoSDService.updateAssetInPoolManager(asset);
      });
    }

    /**
     * Function will reset the given assetIds passed.
     * @param selectedAssetIds
     */
    function resetAssets(selectedAssetIds) {
      var deferred = $q.defer(),
          request = {
            assetIds: selectedAssetIds,
            projectId: $routeParams.id
          };

      OutcomeHandler.resetAssets(request).then(function (response) {
        updatePoolAfterReset(response);
        deferred.resolve(response);
      }, function (error) {
        $log.error(error);
        deferred.reject(error);
      });
      return deferred.promise;
    }

    /*To Retain Asets in Tabular View
     * @param assetsRequest
     * @param outcomesRequest
     */
    function applyDispositionToAssets(assetsRequest, outcomesRequest) {
      var data = {},
          deferred = $q.defer();

      //error callback.
      function onError(error) {
        deferred.reject(error);
      }

      if (assetsRequest.length > 0) {
        AssetService.updateAsset(assetsRequest).then(function (updatedAssets) {
          var levelIds = UtilityService.getMappedValuesByKey(outcomesRequest, OutcomeProperties.LEVEL_ID);
          data.updatedAssets = updatedAssets;
          OutcomeService.saveOutcomes(outcomesRequest, levelIds).then(function (outcome) {
            data.outcome = outcome;
            deferred.resolve(data);
          }, onError);
        }, onError);
      }
      return deferred.promise;
    }

    /**
     * Get Replaced asset map data
     * Get the asset who has coordinates.
     * @param assets
     * @returns {*}
     */
    function getReplacedAssetMapData(assets) {
      var assetMapData = {asset: '', coordinates: ''};
      if (assets && assets.length) {
        assets.forEach(function (asset) {
          var levelEntityMap = LevelEntityMapService.getLevelEntityMapByEntityId(asset.getId());
          if (levelEntityMap && levelEntityMap.coordinates) {
            assetMapData.asset = asset;
            assetMapData.coordinates = levelEntityMap.coordinates;
          }
        });
        if (assetMapData && assetMapData.asset !== "" && UtilityService.isValid(assetMapData.asset)) {
          return assetMapData;
        }
      }
      return null;
    }

    /**
     * Get asset map request
     * @param entityMap
     * @param assetCoordinates
     * @returns {{corrdinates: {x, y: *}, entityMapReq: ({_id, entityType: string, coordinates: {x, y: *}}|*)}}
     */
    function getAssetMapRequest(entityMap, assetCoordinates) {
      if (entityMap && assetCoordinates) {
        var entityMapReq,
            xCoordinate = assetCoordinates.x,
            yCoordinate = assetCoordinates.y;

        if (entityMap.coordinates) {
          xCoordinate = entityMap.coordinates.x;
          yCoordinate = entityMap.coordinates.y;
        }
        else {
          xCoordinate = assetCoordinates.x;
          yCoordinate = assetCoordinates.y + 75;
          assetCoordinates.y = assetCoordinates.y + 75;
        }

        entityMapReq = {
          _id: entityMap.getId(),
          entityType: 'asset',
          coordinates: {x: xCoordinate, y: yCoordinate}
        };

        return {
          coordinates: {
            x: xCoordinate,
            y: yCoordinate
          },
          entityMapReq: entityMapReq
        };
      }
      return null;
    }


    /***
     * This function will take multiple assets and process with Replace outcome.it create multiple outcomes update Request and
     * plot one empty asset on top of selected asset.
     */
    function replaceAssets(assets, currentSelectedAsset, outcomeType) {
      var assetCoordinates, assetMapData, replaceRequest,
          assetsRequest = [],
          updatedAssets = [],
          selectedAsset = currentSelectedAsset,
          deferred = $q.defer(),
          entityMaps = [];

      assetMapData = getReplacedAssetMapData(assets);
      if (assetMapData) {
        assetCoordinates = assetMapData.coordinates;
      }

      // creating multiple asset and outcome update request.
      function createRequest(asset) {
        var entityMap, updateAsset, assetMapRequest;
        if (assetMapData) {
          entityMap = LevelEntityMapService.getLevelEntityMapByEntityId(asset.getId());
          if (entityMap) {
            entityMap = angular.copy(entityMap);
            assetMapRequest = getAssetMapRequest(entityMap, assetCoordinates);
            if (assetMapRequest) {
              entityMaps.push(assetMapRequest.entityMapReq);
              updateAsset = setUpdateAsset(asset, currentSelectedAsset, outcomeType, assetMapRequest.coordinates);
            }
          }
          else {
            updateAsset = setUpdateAsset(asset, currentSelectedAsset, outcomeType, null);
          }
        }
        else {
          updateAsset = setUpdateAsset(asset, currentSelectedAsset, outcomeType, null);
        }
        assetsRequest.push(updateAsset);
      }

      assets.forEach(createRequest);

      replaceRequest = [
        AssetService.updateAsset(assetsRequest)
      ];

      // For Many to one disposition, if one asset has coordinates then we have to show the other asset as well.
      // So level entity map will be updated.
      if (assetMapData) {
        replaceRequest.push(LevelEntityMapService.updateLevelEntityMap(entityMaps));
      }

      //Replace Asset error callback.
      function OnReplaceAssetError(error) {
        deferred.reject(error);
      }

      //Update Asset error callback.
      function onUpdateAssetError(error) {
        deferred.reject(error);
      }

      function onUpdateAssetSuccess(response) {
        var asset = selectedAsset,
            outcomeReplaceRequest = {},
            replaceAssetRequest = {},
            outcomeRequest = [],
            lem = LevelEntityMapService.getLevelEntityMapByEntityId(asset.getId());
        updatedAssets = response;
        replaceAssetRequest = setReplaceAssetRequest(lem, asset);
        //adding default properties to new plotted unknown state asset.
        PorticoSDHelperService.addDefaultProperties(replaceAssetRequest.asset);
        outcomeReplaceRequest = setOutcomeReplaceRequest(lem, replaceAssetRequest);
        outcomeRequest.push(outcomeReplaceRequest);

        //create outcome request
        function createOutcomeRequest(asset) {
          var updateOutcome = setUpdateOutcome(lem, asset, outcomeType);
          // In case of many to one replace, to get position of virtual asset 'isActualSelectedAsset' flag is maintained.
          if (asset.getId() === currentSelectedAsset.getId()) {
            updateOutcome.isActualSelectedAsset = true;
          }
          outcomeRequest.push(updateOutcome);
        }

        response.forEach(createOutcomeRequest);
        AssetMapService.replaceAsset(replaceAssetRequest, outcomeRequest).then(function (response) {
          deferred.resolve(response);
        }, OnReplaceAssetError);

      }

      $log.info('Asset is replacing....');

      $q.all(replaceRequest).then(function (response) {
        onUpdateAssetSuccess(response[0]);
      }, onUpdateAssetError);

      return deferred.promise;
    }

    /**
     * Function is responsible to replace the assets based on given outcome type.
     * @param assets
     * @param outcomeType
     */
    function replaceAssetsOneForOne(assets, outcomeType) {
      var deferred = $q.defer(),
          assetsUpdateRequest = [];

      // creating multiple asset and outcome update request.
      function createRequest(asset) {
        var request = {
          _id: asset.getId(),
          outcome: outcomeType
        };

        // adding subEnvId
        if (asset.mapID) {
          request.subEnvId = asset.mapID;
        }
        assetsUpdateRequest.push(request);
      }

      assets.forEach(createRequest);

      // error callback
      function onError(error) {
        $log.error("Error while replacing one for one assets", error);
        deferred.reject(error);
      }

      // success callback
      function onSuccess(updatedAssets) {
        var replaceRequest = AssetReplaceRequestBuilder.getReplaceAssetsOutcomesRequest(updatedAssets);

        $log.info('Assets are replacing...');
        AssetMapService.replaceAssets(replaceRequest.assetsReplaceRequest, replaceRequest.outcomesReplaceRequest,
            outcomeType).then(function (response) {
          deferred.resolve(response);
        }, onError);
      }

      $log.info('Assets are updating...');
      AssetService.updateAsset(assetsUpdateRequest).then(onSuccess, onError);
      return deferred.promise;
    }

    //create replaceAssetRequest object
    function setReplaceAssetRequest(lem, asset) {
      var mapId, coordinates, labelCoordinates,
          replaceAssetRequest = {};

      if (lem.coordinates) {
        coordinates = {
          x: lem.coordinates.x + 35,
          y: lem.coordinates.y + 35
        };
        labelCoordinates = {
          x: lem.coordinates.x + 100,
          y: lem.coordinates.y + 100
        };
      }
      else {
        coordinates = null;
        labelCoordinates = null;
      }
      replaceAssetRequest.levelEntityMap = {
        _id: lem._id,
        levelId: lem.levelId,
        entityType: 'asset',
        coordinates: coordinates
      };

      if (asset.mapID) {
        mapId = asset.mapID;
      }
      replaceAssetRequest.asset = {
        inventory: true,
        plotted: true,
        labelCoordinates: labelCoordinates,
        state: AssetProperties.UNKNOWN_STATE,

        //Setting the de\ault pricing source for the asset as 'Project'.
        pricingSource: Configuration.DEFAULT_PRICING,
        deviceType: AssetType.UNKNOWN,
        outcome: OutcomeType.REPLACE_WITH_UNKNOWN,
        name: Configuration.TRANSITION_NAME,
        isManual: true,
        mapID: mapId
      };
      return replaceAssetRequest;
    }

    //Create outcome object to add into outcome collection
    function setOutcomeReplaceRequest(lem, replaceAssetRequest) {
      var outcomeReplaceRequest = {
        transitionName: Configuration.TRANSITION_NAME,
        levelId: lem.levelId,
        outcome: OutcomeType.REPLACE_WITH_UNKNOWN,
        coordinates: replaceAssetRequest.levelEntityMap.coordinates,
        replacementAsset: Configuration.REPLACED_ASSETS_LOCATION.atCurrentState
      };
      return outcomeReplaceRequest;
    }

    //Set update asset object to add to assetsRequest collection
    function setUpdateAsset(asset, currentSelectedAsset, outcomeType, labelCoordinates) {
      var updateAsset = {
        _id: asset.getId(),
        outcome: outcomeType
      };

      //currentSelectedAsset (Asset on which disposition has been applied) having mapId then assign it as sub-EnvID to updated assets.
      if (currentSelectedAsset.mapID) {
        updateAsset.subEnvId = currentSelectedAsset.mapID;
      }

      if (labelCoordinates) {
        updateAsset.labelCoordinates = {
          //TODO: Scale it with icon scale.
          x: labelCoordinates.x - 125,
          y: labelCoordinates.y - 125
        };
      }
      return updateAsset;
    }

    //Set update outcome object to add to outcomeRequest
    function setUpdateOutcome(lem, asset, outcomeType) {
      var updateOutcome = {
        transitionName: Configuration.TRANSITION_NAME,
        levelId: lem.levelId,
        outcome: outcomeType,
        assetId: asset.getId()
      };
      return updateOutcome;
    }

    /**
     * Checks if the selected assets are in same floor
     * @param selectedAssets
     * @returns {boolean}
     */
    function isInSameFloor(selectedAssets) {
      var isInSameFloor = true,
          isNotFromSameFloor;
      if (selectedAssets.length) {
        var anyAsset = selectedAssets.first(),
            entityMap = LevelEntityMapService.getLevelEntityMapByEntityId(anyAsset._id);
        if (selectedAssets.length > 1) {
          isNotFromSameFloor = selectedAssets.find(function (asset) {
            var assetEntityMap = LevelEntityMapService.getLevelEntityMapByEntityId(asset._id);
            return assetEntityMap.levelId !== entityMap.levelId;
          });
        }
        isInSameFloor = isNotFromSameFloor ? false : true;
      }
      return isInSameFloor;
    }

    /**
     * function to check asset is allowed to apply disposition or not
     * @param Asset
     * @returns {{message: string, isError: boolean}}
     */
    function validateAssetForDisposition(asset) {
      var errorMessage,
          isTrivial = SharedService.isTrivialAsset(asset.getType()),
          isEmpty = SharedService.isEmptyAsset(asset),
          isTransitionState = SharedService.isTransitionState();
      if (isTransitionState) {
        if (isEmpty && !TransitionHelper.isReplaceWithNew(asset.outcome)) {
          errorMessage = 'OUTCOME_CANNOT_BE_ASSIGNED_TO_EMPTY_ASSET';
        }
        else if (isTrivial) {
          errorMessage = 'OUTCOME_CANNOT_BE_ASSIGNED_TO_TRIVIAL_ASSET';
        }
      }
      return errorMessage;
    }

    /**
     * Update Pool Manager For One To One Disposition
     * @param response
     */
    function updatePoolManagerForOneForOne(response) {
      try {
        var addedOutcomes = response && response.added && response.added.outcomes, asset,
            assets = [];
        for (var index = 0; index < addedOutcomes.length; index++) {
          PorticoSDService.updateAssetInPoolManager(AssetService.getAsset(addedOutcomes[index].assetId));
          if (addedOutcomes[index].replacedByAssetId) {
            asset = AssetService.getAsset(addedOutcomes[index].replacedByAssetId);
            if (asset) {
              PorticoSDService.addVirtualAssetInPoolManager(asset,
                  addedOutcomes[index].levelId, addedOutcomes[index].assetId);
              assets.push(asset);
            }
          }
        }
        SyncTcoPricingService.syncTcoPricingForReplacedAsset(assets);
      } catch (ex) {
        $log.error("error updating pools after replace", ex);
      }
    }

    /**
     * Update Pool Manager For Many To One Disposition
     * @param virtualAssetDetails contains {asset, levelEntityMap, outcome}
     * @param outcomeDetails contains old assets
     */
    function updatePoolManagerForManyToOne(response) {
      try {
        // The replaced assets will appear together. So in pool manager,
        // we first add the virtual asset and then remove those assets which are replaced.
        // And then add the replaced assets after the virtual asset.

        var assetsToupdate = [], oldAssetId = response.updated.assets.first()._id, newAsset,
            virtualAsset = response.added.assets.first();

        // Adding Virtual Asset
        if (response.added.outcomes) {
          var virtualAssetMap = response.added.outcomes.find(function (outcome) {
            return outcome.assetId === virtualAsset._id
          });
          newAsset = AssetService.getAsset(virtualAsset._id);
          if (newAsset) {
            PorticoSDService.addVirtualAssetInPoolManager(newAsset, virtualAssetMap.levelId, oldAssetId);
            oldAssetId = virtualAsset._id;
            SyncTcoPricingService.syncTcoPricingForReplacedAsset([newAsset]);
          }
        }


        // Adding Real Asset
        if (response.updated.assets) {
          var updatedIndex, updatedAssets = response.updated.assets,
              updatedAssetsLength = updatedAssets.length;
          for (updatedIndex = 0; updatedIndex < updatedAssetsLength; updatedIndex++) {
            assetsToupdate.push(updatedAssets[updatedIndex]._id);
          }
        }
        if (assetsToupdate && assetsToupdate.length) {
          // Remove the asset Ids.
          PorticoSDService.removeAssetInPoolManager(assetsToupdate);
          // Add the asset again after the virtual asset.
          assetsToupdate.forEach(function (assetId) {
            var asset = AssetService.getAsset(assetId);
            PorticoSDService.addAssetAfterAssetInPoolManager(asset, oldAssetId);
            oldAssetId = assetId;
          });
        }
      } catch (ex) {
        $log.error("error updating pools after replace", ex);
      }
    }


    /**
     * execute the outcome. calls the new api to deal with all outcomes in backend
     * @param subType
     * @returns {*}
     */
    function executeOutcome(subType) {
      var deferred = $q.defer(), selectedAssetIds = SharedService.getSelectedAssetIds(),
          countryInfo = SharedService.getCurrentLevel('COUNTRY'),
          region = SharedService.getRegionFromCountrySet(countryInfo.name),
          asset = SharedService.selectedAsset(),
          request = {
            assetIds: selectedAssetIds,
            subType: subType,
            projectId: $routeParams.id,
            parentAssetId: (asset && asset.getId()),
            countryCode: region ? region.countryCode : '',
            region: region ? region.region : ''
          };
      OutcomeHandler.replaceAssets(request).then(function (response) {
        if (subType === PorticoSDConfiguration.OutcomeSubTypes.ManyToOne) {
          updatePoolManagerForManyToOne(response);
        } else if (subType === PorticoSDConfiguration.OutcomeSubTypes.OneForOne) {
          updatePoolManagerForOneForOne(response);
        }
        deferred.resolve(response);
      }, function (error) {
        $log.error(error);
        deferred.reject(error)
      });
      return deferred.promise;
    }

    /**
     * Function will be triggered when we reset Reposition/Replace asset's outcome from map view
     * Here we will update actual assets and wil remove virtual assets
     * @param response
     */
    function updateReplaceRepositionAssetsInPool(response) {
      if ($rootScope.token.featureFlag.oldReset) {
        // Update / Remove assets in pool manager
        if (response.deletedAssetIds && response.deletedAssetIds.length) {
          PorticoSDService.removeAssetInPoolManager(response.deletedAssetIds);
        }
        if (response.updatedAssets && response.updatedAssets.length) {
          response.updatedAssets.forEach(function (asset) {
            PorticoSDService.updateAssetInPoolManager(asset);
          });
        }
      } else {
        // Update / Remove assets in pool manager
        PorticoSDService.removeAssetInPoolManager(response.deletedAssets);
        response.updated.assets.forEach(function (asset) {
          PorticoSDService.updateAssetInPoolManager(asset);
        });

      }
    }

    return {
      getHasOutcome: getHasOutcome,
      notifyEmptyAsset: notifyEmptyAsset,
      retireAssets: retireAssets,
      retainAssets: retainAssets,
      excludeAssetFromDesign: excludeAssetFromDesign,
      resetVirtualAssets: resetVirtualAssets,
      resetReplaceAssets: resetReplaceAssets,
      resetAssetsOutcome: resetAssetsOutcome,
      resetRepositionAssets: resetRepositionAssets,
      resetAssets: resetAssets,
      replaceAssets: replaceAssets,
      replaceAssetsOneForOne: replaceAssetsOneForOne,
      executeOutcome: executeOutcome,
      isInSameFloor: isInSameFloor,
      validateAssetForDisposition: validateAssetForDisposition,
      updatePoolManagerForOneForOne: updatePoolManagerForOneForOne,
      updatePoolManagerForManyToOne: updatePoolManagerForManyToOne,
      updateReplaceRepositionAssetsInPool: updateReplaceRepositionAssetsInPool,
      filterResetAssets: filterResetAssets
    };
  }

  var app = angular.module('app'),
      requires = [
        '$rootScope',
        '$routeParams',
        '$q',
        '$log',
        'ngCartosCanvas.constants.OutcomeType',
        'ngCartosCanvas.constants.AssetType',
        'ngCartosCore.constants.OutcomeProperties',
        'Configuration',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'SharedService',
        'ngCartosCore.services.AssetService',
        'ngCartosCore.services.OutcomeService',
        'StageHelperService',
        'ngCartosCore.services.LevelEntityMapService',
        'ngCartosCore.services.AssetMapService',
        'ngCartosCore.services.SchemeSetService',
        'ngPorticoSD.services.PorticoSDHelperService',
        'ngCartosCore.constants.AssetProperties',
        'ngCartosCore.services.NotifierService',
        'ngCartosUtils.services.UtilityService',
        'RequestBuilder',
        'TransitionHelperService',
        'ngPorticoSD.services.PorticoSDService',
        'ngCartosRequest.services.AssetReplaceRequestBuilder',
        'ngCartosRequest.services.AssetRepositionRequestBuilder',
        'ngCartosCore.services.OutcomeHandler',
        'SyncTcoPricingService',
        PorticoSDDispositionService
      ];
  app.factory('ngPorticoSD.services.PorticoSDDispositionService', requires);
}());

/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  function PorticoSDExportHelperService($log, $q, PorticoSDApiService) {

    /**
     *Makes use of PorticoSDApiService to export CDf Report
     * @returns promise
     */
    function exportCdfReport() {
      var deferred = $q.defer();
      PorticoSDApiService.exportCDFReport().then(function (response) {
        deferred.resolve(response.data.data);
      }, function (error) {
        deferred.reject(error);
      });
      return deferred.promise;
    }

    /**
     * Makes use of PorticoSDApiService to export TCO Report
     * @return promise
     */
    function exportTcoReport() {
      var deferred = $q.defer();
      PorticoSDApiService.exportTCOReport().then(function (response) {
        deferred.resolve(response.data);
      }, function (error) {
        deferred.reject(error);
      });
      return deferred.promise;
    }

    /**
     * Makes use of PorticoSDApiService to export RFQ Report
     * @return promise
     */
    function exportRfqReport() {
      var deferred = $q.defer();
      PorticoSDApiService.exportRfqReport().then(function (response) {
        deferred.resolve(response.data);
      }, function (error) {
        deferred.reject(error);
      });
      return deferred.promise;
    }

    /**
     * Returns the full name of the file with datetime and extension.
     * @param   {String} project  name for which the file need to be exported.
     * @param   {String} name    name of the file
     * @param   {String} fileType type of file
     * @returns {String} fileType   File extension. It can be jpg, png, pdf, xlsx etc.
     */
    function getExportFileNameWithTimestamp(project, name, fileType) {
      try {
        return (project.replace(/\s+/g, '-') + '_export_' + name + '-' +
            new Date().toISOString().replace(/T/, '_').replace(/\..+/, '') + '.' + fileType);
      }
      catch (ex) {
        $log.error('Error while naming the file name', ex);
        return ('Report.' + fileType);
      }
    }

    return {
      exportCdfReport: exportCdfReport,
      exportTcoReport: exportTcoReport,
      exportRfqReport: exportRfqReport,
      getExportFileNameWithTimestamp: getExportFileNameWithTimestamp
    };
  }

  var app = angular.module('app'),
      requires = [
        '$log',
        '$q',
        'PorticoSDApiService',
        PorticoSDExportHelperService
      ];
  app.factory('ngPorticoSD.services.PorticoSDExportHelperService', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  function PorticoSDHelperService($log, PorticoSDConfiguration, OutcomeSetService, translateFilter, SchemeSetService,
                                  PorticoSdUtilityService, PorticoSDTemplatePaths, LevelService, LevelEntityMapService, AssetProperties,
                                  OutcomeService, SharedService, PricingService, Utilis, OutcomeType) {

    var dartUploadStrategy = [];


    /**
     *  function To Extract floor data*
     */
    function extractFloorData(element) {
      return element.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_ASSET ? true : false;
    }

    /**
     * Get the row number from the tabular view data based on level.
     * @param tabularViewData
     * @param levelId
     * @returns {number|*} rowNumber
     * @constructor
     */
    function getTabularViewRowNumber(tabularViewData, levelId) {
      return tabularViewData.findIndex(function (elementLevel) {
        return elementLevel.levelId === levelId;
      });
    }

    /**
     * Get the levels row number.
     * @param selectedFloor
     * @param tabularViewData
     * @returns {{country: number, site: number, building: number, floor: number}}
     */
    function getLevelsRowNumber(selectedFloor, tabularViewData) {
      var floorId, siteId, buildingId, countryId, projectId,
          levelsRowNumber = {project: 0, country: 0, site: 0, building: 0, floor: 0};

      // Get the level Id's from the ancestors.
      projectId = selectedFloor.ancestors[0];
      countryId = selectedFloor.ancestors[1];
      siteId = selectedFloor.ancestors[2];
      buildingId = selectedFloor.ancestors[3];
      floorId = selectedFloor._id;

      // Get the project row number
      levelsRowNumber.project = getTabularViewRowNumber(tabularViewData, projectId);
      // Get the selected country row number.
      levelsRowNumber.country = getTabularViewRowNumber(tabularViewData, countryId);
      // Get the selected site row number.
      levelsRowNumber.site = getTabularViewRowNumber(tabularViewData, siteId);
      // Get the selected building row number.
      levelsRowNumber.building = getTabularViewRowNumber(tabularViewData, buildingId);
      // Get the selected floor row number.
      levelsRowNumber.floor = getTabularViewRowNumber(tabularViewData, floorId);

      return levelsRowNumber;
    }

    /**
     * Validate level information.
     * @param selectedFloor
     * @param tabularViewData
     * @returns {boolean}
     */
    function isValidLevel(selectedFloor, tabularViewData) {
      if (selectedFloor && tabularViewData &&
          selectedFloor.ancestors.length === PorticoSDConfiguration.TREE_LEVEL_FLOOR) {
        return true;
      }
      return false;
    }

    /**
     * To get outcome label for a given outcome
     * @param disposition
     * @returns {*|exports.models.OutcomeSet.properties.outcomeLabel|{type, description}}
     */
    function getTranslatedOutcomeLabel(disposition) {
      return translateFilter(OutcomeSetService.getOutcomeLabel(disposition));
    }

    // Set default boolean asset properties
    function addDefaultProperties(assetData) {
      // get assetSchemeSet for the particular device type
      var assetSchemeSet = SchemeSetService.getAssetScheme(assetData.deviceType);
      if (!assetSchemeSet.defaultBoolean) {
        return;
      }
      // Iterate through boolean fields of all device types
      // set the boolean fields false by default
      assetSchemeSet.defaultBoolean.forEach(function (property) {
        assetData[property] = false;
      });
    }

    /**
     * Get the array of asset ids
     * @param assets
     */
    function getAssetIds(assets) {
      return assets.map(function (item) {
        return item._id;
      });
    }

    /**
     * function to filter the assets in Tabular on blanks
     * @param grid
     * @param blankedColumns
     */
    function applyBlankFilter(grid, blankedColumns) {
      //Will check the empty columns and based on that
      //it filter and give the results.
      if (!grid.data) {
        return;
      }

      if (Object.keys(blankedColumns).length === 0) {
        grid.columnDefs.forEach(function (column) {
          if (column.filter) {
            column.filter.placeholder = '';
          }
        });
        return;
      }

      for (var col in blankedColumns) {
        var falseyValue = true;
        if (blankedColumns.hasOwnProperty(col)) {
          grid.data = grid.data.filter(function (asset) {
            // Showing/hiding assets on the basis of blank field
            if (asset.type === PorticoSDConfiguration.ASSET) {
              falseyValue = SharedService.isFalseyValue(asset[blankedColumns[col]]);
              return falseyValue;
            }
            // Checking for accessories for all those assets which are visible after applying blank filter
            else if (asset.type === PorticoSDConfiguration.ACCESSORY && !falseyValue) {
              return false;
            }
            return true;
          });
        }
      }
    }

    /**
     * Applying filter on placeholder
     * @param customFilter
     * @param grid
     * @param blankFilter
     */
    function applyPlaceholderOnColumn(customFilter, grid, blankFilter) {
      try {
        grid.columnDefs.forEach(function (column) {
          if (blankFilter.hasOwnProperty(column.field)) {
            column.filter.placeholder = '(' + translateFilter('BLANKS') + ')';
          }
          else if (customFilter[column.field] !== undefined && customFilter[column.field].key === column.field) {
            column.filter.placeholder = '(' + customFilter[column.field].appliedFilter + ' ' + customFilter[column.field].value + ')';
          }
          else {
            column.filter.placeholder = '';
          }
        });
      }
      catch (ex) {
        $log.info('Error while applying the placeholder: ', ex);
      }
    }

    /**
     * Method for applying filtering option
     * @param rowPropertyValue
     * @param filterData
     * @returns {boolean}
     */
    function applyFilterOption(rowPropertyValue, filterData) {
      var outputValue = false;
      switch (filterData.appliedFilter) {
        case PorticoSDConfiguration.CUSTOM_FILTERS.EQUAL:
          outputValue = parseFloat(rowPropertyValue) === parseFloat(filterData.value);
          break;
        case PorticoSDConfiguration.CUSTOM_FILTERS.LESS_THAN:
          outputValue = parseFloat(rowPropertyValue) < parseFloat(filterData.value);
          break;
        case PorticoSDConfiguration.CUSTOM_FILTERS.GREATER_THAN:
          outputValue = parseFloat(rowPropertyValue) > parseFloat(filterData.value);
          break;
      }
      return outputValue;
    }

    /**
     * Retrieve Asset location (Site, building and floor)
     * @param asset
     * @returns {*}
     */
    function getAssetLocation(asset) {
      var location, floor, lem;
      if (asset.isVirtual()) {
        lem = OutcomeService.getOutcomeByAssetId(asset.getId());
      }
      else {
        lem = LevelEntityMapService.getLevelEntityMapByEntityId(asset.getId());
      }
      if (!lem) {
        $log.debug('No level entity map found for asset while locating it.');
        return 'Error while locating asset.';
      }
      floor = LevelService.getLevel(lem.levelId);
      location = floor.ancestors.map(function (levelId) {
        return LevelService.getLevel(levelId);
      });

      location.push(floor);
      return {
        country: location[1],
        site: location[2],
        building: location[3],
        floor: location[4]
      };
    }

    /**
     * Get asset properties
     * @returns {[*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*]}
     */
    function getAssetProperties() {
      var assetProperties = [
        AssetProperties.ID,
        AssetProperties.COLOR_A3_MONTHLY,
        AssetProperties.COLOR_A4_MONTHLY,
        AssetProperties.COLOR_TOTAL_MONTHLY,
        AssetProperties.CONNECTION_TYPE,
        AssetProperties.LIFE_TOTAL_MONTHLY,
        AssetProperties.LOCATION,
        AssetProperties.DEPARTMENT,
        AssetProperties.EXCLUDE_FROM_DESIGN,
        AssetProperties.HOST_NAME,
        AssetProperties.IP_ADDRESS,
        AssetProperties.MONO_A3_MONTHLY,
        AssetProperties.MONO_A4_MONTHLY,
        AssetProperties.MONO_TOTAL_MONTHLY,
        AssetProperties.NOTES,
        AssetProperties.PROPOSED_MONTHLY_PAGE_VOLUME_COLOR,
        AssetProperties.PROPOSED_MONTHLY_PAGE_VOLUME_MONO
      ];
      return assetProperties;
    }

    /**
     * Collapse left pane
     */
    function collapseLeftPane() {
      var splitter = SharedService.getSplitter();
      if (splitter) {
        splitter.leftPane.collapse();
      }
    }

    /**
     *Calculates Average for given Row and column data
     * @param row
     * @param col
     * @returns {*}
     */
    function aggregationsAvg(row, col) {
      try {
        if (row && col) {
          var children = row.children ? row.children : row.treeNode.children, sum = 0, count = 0, avg = 0;
          if (children && children.length) {
            children.forEach(function (child) {
              if (child.row.entity[col] && parseFloat(child.row.entity[col]) > 0) {
                count++;
              }
              sum += (child.row.entity[col] && parseFloat(child.row.entity[col]) > 0) ? parseFloat(child.row.entity[col]) : aggregationsAvg(child.row, col);
            });
            avg = count > 0 ? (sum / count) : sum;
            if (!isNaN(avg)) {
              return avg;
            }
          }
        }
        return 0;
      }
      catch (ex) {
        $log.error('ERROR_WHILE_CALCULATING_LEVEL_SUMMARY', ex);
        return 0;
      }
    }

    /**
     *Calculates Total for given Row and column data
     * @param row
     * @param col
     * @returns sum
     */
    function aggregationsTotal(row, col) {
      try {
        if (row && col) {
          var children = row.children ? row.children : row.treeNode.children, sum = 0;
          if (children && children.length) {
            children.forEach(function (child) {
              sum += (child.row.entity[col] && parseFloat(child.row.entity[col]) > 0) ?
                  parseFloat(child.row.entity[col]) : aggregationsTotal(child.row, col);
            });
            if (!isNaN(sum)) {
              return sum;
            }
          }
        }
        return 0;
      }
      catch (ex) {
        $log.error('ERROR_WHILE_CALCULATING_LEVEL_SUMMARY', ex);
        return 0;
      }
    }

    /**
     *Calculates Total for given Row and column data with device qty
     * @param row
     * @param col
     * @returns sum
     */
    function getAggregationTotalForPages(row, col) {
      try {
        if (row && col) {
          var children = row.children ? row.children : row.treeNode.children, sum = 0;
          if (children && children.length) {
            children.forEach(function (child) {
              if (child.row.entity[col] && Math.floor(child.row.entity[col]) && Math.floor(child.row.entity.deviceQty)) {
                sum += (Math.floor(child.row.entity[col]) * Math.floor(child.row.entity.deviceQty));
              }
              else {
                sum += getAggregationTotalForPages(child.row, col);
              }
            });
            if (!isNaN(sum)) {
              return sum;
            }
          }
        }
        return 0;
      }
      catch (ex) {
        $log.error('ERROR_WHILE_CALCULATING_LEVEL_SUMMARY', ex);
        return 0;
      }
    }

    /**
     * Gets Aggregated Total/Avg for a Building,site and floor levels
     * @param row
     */
    function getAggregatedPricingData(row) {
      var colsTotal = PorticoSDConfiguration.COLUMNS_TOTAL,
          colsAvg = PorticoSDConfiguration.COLUMNS_AVG,
          pricingData = [],
          aggregationValue;

      colsAvg.forEach(function (col) {
        aggregationValue = aggregationsAvg(row, col);
        aggregationValue = (aggregationValue || aggregationValue >= 0) ?
            Utilis.toFloat(aggregationValue, PorticoSDConfiguration.NUMBER_FIVE) : '';
        pricingData[col] = aggregationValue;
      });
      colsTotal.forEach(function (col) {
        aggregationValue = aggregationsTotal(row, col);
        if (col === PorticoSDConfiguration.PORTICOSD_GRID_COLUMNS.upfrontChargeUsd ||
            col === PorticoSDConfiguration.PORTICOSD_GRID_COLUMNS.baseFeeUsd ||
            col === PorticoSDConfiguration.PORTICOSD_GRID_COLUMNS.baseFeeLc ||
            col === PorticoSDConfiguration.PORTICOSD_GRID_COLUMNS.upfrontChargeLc) {
          aggregationValue = (aggregationValue || aggregationValue >= 0) ?
              Utilis.toFloat(aggregationValue, PorticoSDConfiguration.NUMBER_TWO) : '';
        }
        else if (col === PorticoSDConfiguration.PORTICOSD_GRID_COLUMNS.totalRevenueUsd ||
            col === PorticoSDConfiguration.PORTICOSD_GRID_COLUMNS.totalRevenueLc) {
          aggregationValue = (aggregationValue || aggregationValue >= 0) ? Utilis.truncateDecimalValue(aggregationValue) : '';
        } else {
          aggregationValue = (aggregationValue || aggregationValue >= 0) ? Utilis.toFloat(aggregationValue, PorticoSDConfiguration.NUMBER_FIVE) : '';
        }
        pricingData[col] = aggregationValue;
      });
      return pricingData;
    }

    /**
     * Including  row filter to set placeholder as (Blanks) while applying blanks filtering
     * @param columnName
     * @param blankedColumns
     */
    function getCustomRowFilter(columnName, blankedColumns) {
      return {
        placeholder: blankedColumns[columnName] ? '(' + translateFilter('BLANKS') + ')' : ''
      };
    }

    /**
     *
     * @param gridSelectionColumn
     */
    function setSelectionColumn(gridSelectionColumn) {
      gridSelectionColumn.headerCellTemplate = '<div class="selectCheckboxHeader">' +
          '<input ng-disabled="grid.appScope.hasViewPermission || !grid.appScope.isTransitionState || grid.appScope.flagPageVolumeEnabled" ' +
          'ng-model="grid.appScope.$parent.$parent.isAllAssetsSelected"' +
          'ng-click="grid.appScope.toggleCheckerAll()" type="checkbox"/>' +
          '</div>';
    }

    /**
     * Returns the id of the item which is dragged and dropped on tabular view.
     * @param assetDropId dome element which is dropped on the canvas.
     * @returns {*}
     */
    function getAssetIdFromDropTargetId(assetDropId) {
      if (!assetDropId) {
        return null;
      }

      if (assetDropId.includes('_')) {
        return assetDropId.split('_').last();
      } else {
        return assetDropId;
      }
    }

    /**
     * To get the HW disposition value of asset
     * @param asset
     */
    function getHwDisposition(asset) {
      if (asset.outcome === OutcomeType.RETAIN && asset.subDispositions === PorticoSDConfiguration.ADVANCE_PLACEMENT) {
        return translateFilter(PorticoSDConfiguration.RETAIN_ADVANCE_PLACEMENT);
      }
      else {
        return asset.outcome;
      }
    }

    /**
     * function to check whether the column is a dart column
     * @param columnName
     * @returns {boolean}
     */
    function isDartColumn(columnName) {
      if (Utilis.isUndefined(columnName)) {
        return false;
      }
      var isColumnExist = PorticoSDConfiguration.DART_PRICING_COLUMNS.find(function (dartColumn) {
        return columnName === dartColumn;
      });

      return !!isColumnExist;
    }

    /**
     * function to check whether the column is a multi-vendor column
     * @param columnName
     * @returns {boolean}
     */
    function isMultiVendorColumn(columnName) {
      if (Utilis.isUndefined(columnName)) {
        return false;
      }
      var isColumnExist = PorticoSDConfiguration.MULTI_VENDOR_VIEW.find(function (multiVendorColumn) {
        return columnName === multiVendorColumn;
      });

      return !!isColumnExist;
    }

    /**
     * Check the column is of respective view or not.
     * @param pricingSource
     * @param field
     * @returns {boolean}
     */
    function isPricingSourceColumn(pricingSource, field) {
      var isExist = false;
      if (pricingSource) {
        switch (pricingSource) {
          case PorticoSDConfiguration.PRICING_SOURCE.DART:
            isExist = isDartColumn(field);
            break;
          case PorticoSDConfiguration.PRICING_SOURCE.TCO:
            isExist = isTcoColumn(field);
            break;
          case PorticoSDConfiguration.PRICING_SOURCE.MULTI_VENDOR:
            isExist = isMultiVendorColumn(field);
            break;
        }
      }
      return isExist;
    }

    /**
     * Get sites in country
     * @param countryId
     * @returns {Function}
     */
    function getSitesInCountry(countryId) {
      return function (elementLevel) {
        return elementLevel.value && elementLevel.value.countryId === countryId &&
            elementLevel.value.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_SITE;
      };
    }

    /**
     * Get buildings in Site
     * @param siteId
     * @returns {Function}
     */
    function getBuildingsInSite(siteId) {
      return function (elementLevel) {
        return elementLevel.value && elementLevel.value.siteId === siteId &&
            elementLevel.value.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_BUILDING;
      };
    }

    /**
     * Get floors in building
     * @param buildingId
     * @returns {Function}
     */
    function getFloorsInBuilding(buildingId) {
      return function (elementLevel) {
        return elementLevel.value && elementLevel.value.buildingId === buildingId &&
            elementLevel.value.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_FLOOR;
      };
    }

    /**
     * Get assets in floor
     * @param floorId
     * @returns {Function}
     */
    function getAssetsInFloor(floorId) {
      return function (elementLevel) {
        return elementLevel.floorId === floorId;
      };
    }

    /**
     * Update asset data based on state change
     * @param tabularViewAssetRow
     * @param activeState
     */
    function updateAssetBasedOnState(tabularViewAssetRow, activeState) {
      if (activeState === PorticoSDConfiguration.STATES.CURRENT) {
        tabularViewAssetRow.lifeTimeEngineLife = tabularViewAssetRow.currentLifeTimeEngineLife;
        tabularViewAssetRow.eoslPast = tabularViewAssetRow.currentEoslPast;
      }
      else {
        tabularViewAssetRow.lifeTimeEngineLife = tabularViewAssetRow.proposedLifeTimeEngineLife;
        tabularViewAssetRow.eoslPast = tabularViewAssetRow.proposedEoslPast;
      }
    }

    /**
     * Function to get level name
     * @param levelId
     */
    function getLevelName(levelId) {
      if (levelId) {
        var level = LevelService.getLevel(levelId);
        return level ? level.getName() : '';
      }
      return '';
    }

    /**
     * This function will return all the levels inclucing parent & childrens
     * @param tabularViewData tabular data from here we will be filtering selected levels & sub levels
     * @param row which is selected
     * @param isAllAssetsSelected if all levels are selected then it will return all the levels
     * @returns filteredTabularData array of selected levels
     */
    function getSelectedLevels(tabularViewData, row, isAllAssetsSelected) {
      var filteredTabularData = [];
      // if all assets selected then we need to returns all the levels
      if (isAllAssetsSelected) {
        filteredTabularData = tabularViewData.filter(function (element) {
          return (element.type === PorticoSDConfiguration.LEVEL) ||
              (element.type === PorticoSDConfiguration.PROJECT);
        });
        return filteredTabularData;
      }
      try {
        switch (row.$$treeLevel) {
          case PorticoSDConfiguration.TREE_LEVEL_PROJECT :
            filteredTabularData = tabularViewData.filter(function (element) {
              return element.projectId === row.projectId && (element.type === PorticoSDConfiguration.LEVEL) ||
                  (element.type === PorticoSDConfiguration.PROJECT);
            });
            break;
          case PorticoSDConfiguration.TREE_LEVEL_COUNTRY :
            filteredTabularData = tabularViewData.filter(function (element) {
              return element.countryId === row.countryId && element.type === PorticoSDConfiguration.LEVEL;
            });
            break;
          case PorticoSDConfiguration.TREE_LEVEL_SITE :
            filteredTabularData = tabularViewData.filter(function (element) {
              return element.siteId === row.siteId && element.type === PorticoSDConfiguration.LEVEL;
            });
            break;
          case PorticoSDConfiguration.TREE_LEVEL_BUILDING :
            filteredTabularData = tabularViewData.filter(function (element) {
              return element.buildingId === row.buildingId && element.type === PorticoSDConfiguration.LEVEL;
            });
            break;
          case PorticoSDConfiguration.TREE_LEVEL_FLOOR  :
            filteredTabularData = tabularViewData.filter(function (element) {
              return element.floorId === row.floorId && element.type === PorticoSDConfiguration.LEVEL;
            });
            break;
        }
        return filteredTabularData;
      }
      catch (ex) {
        $log.error('Error in getting in finding sublevels while selecting rows', ex);
        return null;
      }

    }

    /**
     * Check whether asset is multi vendor asset or not
     * @param asset
     * @returns {boolean}
     */
    function isMultiVendorAsset(asset) {
      var isMultiVendor = false;
      try {
        if (asset && asset.make) {
          isMultiVendor = ((asset.make.toLowerCase() !== PorticoSDConfiguration.HP) &&
          (asset.outcome === OutcomeType.RETAIN || asset.outcome === OutcomeType.ADD));
        }
        return isMultiVendor;
      }
      catch (ex) {
        return isMultiVendor;
      }
    }

    /**
     * Get Monthly pages
     * @param pages
     * @param proposedPages
     * @param qty
     * @return {string}
     */
    function getMonthlyPages(pages, proposedPages, qty) {
      var monthlyPages = '';
      if (SharedService.isCurrentState()) {
        monthlyPages = PorticoSdUtilityService.multiplyValues(pages, qty);
      }
      else {
        monthlyPages = PorticoSdUtilityService.multiplyValues(proposedPages, qty);
      }
      return monthlyPages;
    }

    /**
     * Get multi vendor assets from assets
     * @param assets
     * @returns {Array}
     */
    function getMvAssetsFromAssets(assets) {
      var mvAssets = [],
          assetsLength;
      if (Array.isArray(assets) && assets.length) {
        assetsLength = assets.length;
        for (var index = 0; index < assetsLength; index++) {
          if (isMultiVendorAsset(assets[index])) {
            mvAssets.push(assets[index]);
          }
        }
      }
      return mvAssets;
    }

    /**
     * Save DART upload strategy objects
     * @param country row data
     */
    function saveDartUploadStrategy(countryRowData){
      var countryUploadStrategy = {CountryName: countryRowData.level, UploadStrategy: countryRowData.dartUploadStrategy},
          countryIndex = Utilis.indexOfObjectArray(dartUploadStrategy, countryUploadStrategy.CountryName, PorticoSDConfiguration.COUNTRY_NAME);

      countryIndex === -1 ? dartUploadStrategy.push(countryUploadStrategy) : dartUploadStrategy[countryIndex] = countryUploadStrategy;
    }

    /**
     * Retrieves DART upload data when DART deal is saved or submitted
     * @returns {Array}
     */
    function getDartDealUploadStrategies(){
      return dartUploadStrategy;
    }

    return {
      extractFloorData: extractFloorData,
      getSelectedLevels: getSelectedLevels,
      getLevelsRowNumber: getLevelsRowNumber,
      isValidLevel: isValidLevel,
      getTranslatedOutcomeLabel: getTranslatedOutcomeLabel,
      addDefaultProperties: addDefaultProperties,
      getAssetIds: getAssetIds,
      getAssetLocation: getAssetLocation,
      getAssetProperties: getAssetProperties,
      collapseLeftPane: collapseLeftPane,
      getAggregatedPricingData: getAggregatedPricingData,
      getAssetIdFromDropTargetId: getAssetIdFromDropTargetId,
      applyBlankFilter: applyBlankFilter,
      getHwDisposition: getHwDisposition,
      getCustomRowFilter: getCustomRowFilter,
      isDartColumn: isDartColumn,
      isMultiVendorColumn : isMultiVendorColumn,
      setSelectionColumn: setSelectionColumn,
      applyFilterOption: applyFilterOption,
      getSitesInCountry: getSitesInCountry,
      getBuildingsInSite: getBuildingsInSite,
      getFloorsInBuilding: getFloorsInBuilding,
      getAssetsInFloor: getAssetsInFloor,
      applyPlaceholderOnColumn: applyPlaceholderOnColumn,
      updateAssetBasedOnState: updateAssetBasedOnState,
      getLevelName: getLevelName,
      getAggregationTotal: aggregationsTotal,
      isMultiVendorAsset: isMultiVendorAsset,
      getAggregationTotalForPages: getAggregationTotalForPages,
      getMonthlyPages: getMonthlyPages,
      getMvAssetsFromAssets: getMvAssetsFromAssets,
      getTabularViewRowNumber: getTabularViewRowNumber,
      saveDartUploadStrategy: saveDartUploadStrategy,
      getDartDealUploadStrategies: getDartDealUploadStrategies
    };
  }

  var app = angular.module('app'),
      requires = [
        '$log',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'ngCartosCore.services.OutcomeSetService',
        'translateFilter',
        'ngCartosCore.services.SchemeSetService',
        'PorticoSdUtilityService',
        'PorticoSDTemplatePaths',
        'ngCartosCore.services.LevelService',
        'ngCartosCore.services.LevelEntityMapService',
        'ngCartosCore.constants.AssetProperties',
        'ngCartosCore.services.OutcomeService',
        'SharedService',
        'PricingService',
        'ngCartosUtils.services.UtilityService',
        'ngCartosCanvas.constants.OutcomeType',
        'TcoIntegrationHelperService',
        PorticoSDHelperService
      ];
  app.factory('ngPorticoSD.services.PorticoSDHelperService', requires);
}());

/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /**
   * We need a service to manage the data pools which we have in our data model services.
   * PorticoSDPoolManager will do this job for us. It will have useful methods which are required.
   * @returns {object} service object.
   */
  function PorticoSDPoolManager() {

    //hold the methods and data members which will be exposed by PoolManager.
    var poolManager;

    /**
     * It returns the instance of item with id and value.
     * If item is not present then it creates it and adds it to the pool.
     * @param {function} Constructor  constructor of the item stored in data pool.
     * @param {object} dataPool     object which stores item using id as key.
     * @param {string} poolItemId   id of the item.
     * @param {object} poolItemData properties of item.
     */
    function retrieveInstance(Constructor, dataPool, poolItemId, poolItemData) {
      if (angular.isArray(dataPool)) {
        var itemIndex = dataPool.findIndex(function (element) {
          return element.id === poolItemId;
        });

        if (itemIndex >= 0) {
          angular.extend(dataPool[itemIndex].value, poolItemData);
          return dataPool[itemIndex];
        } else {
          var newItemData = new Constructor(poolItemData),
              newItem = {id: poolItemId, value: newItemData};
          dataPool.push(newItem);
          return newItem;
        }
      }
    }

    /**
     * Update the instance in pool manager
     * @param {function} Constructor  constructor of the item stored in data pool.
     * @param {object} dataPool     object which stores item using id as key.
     * @param {string} poolItemId   id of the item.
     * @param {object} poolItemData properties of item.
     */
    function updateInstance(Constructor, dataPool, poolItemId, poolItemData) {
      if (angular.isArray(dataPool)) {
        var itemIndex = dataPool.findIndex(function (element) {
          return element.id === poolItemId;
        });

        if (itemIndex >= 0) {
          angular.extend(dataPool[itemIndex].value, poolItemData);
          return dataPool[itemIndex];
        }
        return null;
      }
    }


    /**
     * It returns the instance of item with id and value.
     * If item is not present then it creates it and adds the item at the position in the pool.
     * @param {function} Constructor  constructor of the item stored in data pool.
     * @param {object} dataPool     object which stores item using id as key.
     * @param {string} poolItemId   id of the item.
     * @param {object} poolItemData properties of item.
     * @param position where you want to add the item in pool.
     * @returns {*}
     */
    function addInstanceAtPosition(Constructor, dataPool, poolItemId, poolItemData, position) {
      if (angular.isArray(dataPool)) {
        var itemIndex = dataPool.findIndex(function (element) {
          return element.id === poolItemId;
        });

        if (itemIndex >= 0) {
          angular.extend(dataPool[itemIndex].value, poolItemData);
          return dataPool[itemIndex];
        } else {
          var newItemData = new Constructor(poolItemData),
              newItem = {id: poolItemId, value: newItemData};
          dataPool.insert(position, newItem);
          return newItem;
        }
      }
    }

    /**
     * Removes elements from the pool on the basis of parameters. If Id is passed as a parameter then an individual element
     * will be removed else whole pool will be removed.
     * @param pool collection of items
     * @param ids unique identifier of item.
     * @returns {boolean} indicator to detect remove is successful or not.
     */
    function remove(pool, ids) {
      if (arguments.length === 2) {
        if (angular.isArray(ids)) {
          ids.forEach(function (id) {
            var rowNumber = pool.findIndex(function (element) {
              return element.id === id;
            });
            if (rowNumber >= 0) {
              pool.splice(rowNumber, 1);
            }
          });
        }
      }
      else if (pool && pool.length > 0) {
        pool.splice(0, pool.length);
      }
    }

    /**
     * Get the data array.
     * @param pool data pool
     * @returns {Array} resultant array.
     */
    function toDataArray(pool) {
      var dataArray = [],
          poolLength = pool.length;
      if (pool) {
        for (var poolIndex = 0; poolIndex < poolLength; poolIndex++) {
          dataArray.push(pool[poolIndex].value);
        }
      }
      return dataArray;
    }


    poolManager = {
      retrieveInstance: retrieveInstance,
      remove: remove,
      addInstanceAtPosition: addInstanceAtPosition,
      updateInstance: updateInstance,
      toDataArray: toDataArray
    };
    return poolManager;
  }

  var app = angular.module('app'),
      requires = [
        PorticoSDPoolManager
      ];
  app.factory('ngPorticoSD.services.PorticoSDPoolManager', requires);
}());

/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /**
   * Constructor of Outcome. It should be used to create the instance of an Outcome
   * @param {object} data properties of the Outcome.
   */
  function TabularData(data) {
    if (data !== undefined) {
      this.setData(data);
    }
  }

  TabularData.prototype = {
    /**
     * Set properties to Outcome. Uses angular.extend to assign the values.
     * @param {object} data properties to be set.
     */
    setData: function (data) {
      angular.extend(this, data);
    }
  };

  function PorticoSDService($log, $routeParams, $rootScope, $filter, $q, translateFilter, PorticoSDConfiguration, OutcomeType, LevelType, LevelService, AssetMapService,
                            Utilis, PorticoSDPoolManager, PorticoSDHelperService, TransitionHelperService,
                            OutcomeService, AssetService, LevelEntityMapService, PageVolumeService, DeviceAccessoriesService,
                            SharedService, PricingService, PricingHelperService, ZoneService, TcoPricingService, TcoPricingHelperService,
                            MultiVendorService, PorticoSDSharedService, PorticoSDUtilityService, RGSharedLocaleService) {

    var tabularPool = [],
        levels = {},
        retrieveInstance = PorticoSDPoolManager.retrieveInstance.bind(null, TabularData, tabularPool),
        addInstanceAtPosition = PorticoSDPoolManager.addInstanceAtPosition.bind(null, TabularData, tabularPool),
        updateInstance = PorticoSDPoolManager.updateInstance.bind(null, TabularData, tabularPool),
        removeTabularData = PorticoSDPoolManager.remove.bind(null, tabularPool),
        levelPricing, locale = null;

    var tabularViewProperties = PorticoSDConfiguration.TABULAR_PROPERTIES,
        tabularPricingProperties = angular.copy(tabularViewProperties);
    TcoPricingHelperService.setDefaultTcoPricingForTabularRow(tabularPricingProperties);

    /**
     * function to get all the floor assets on the basis of floor id in descending order by plotted date
     * @param levelId floor Id
     * @returns {*} return an array
     */
    function getFloorAssets(levelId) {
      var realAssets = AssetMapService.getAssetMaps(levelId),
          virtualAssets = AssetMapService.getVirtualAssetMap(levelId),
          floorAssets = realAssets.concat(virtualAssets),
          dartPricingData = PricingHelperService.assetDartPricingData();
      if (dartPricingData && dartPricingData.length) {
        floorAssets.forEach(function (item) {
          var assetPricingData = dartPricingData.filter(function (data) {
            return data.assetId === item.properties._id;
          });
          if (assetPricingData.length) {
            item.properties.dartPricingData = assetPricingData[0];
          }
        });
      }
      if (floorAssets.length > 1) {
        // if floor has more than one asset than sort the assets in descending order on the basis of plotted date
        floorAssets.sort(function (a, b) {
          return b.properties.plottedDate - a.properties.plottedDate;
        });
      }
      return floorAssets;
    }

    /**
     * function will returns an object which contains replace with asset and an array of replaced assets
     * function to get the replaced with Asset data based on the replacedByAssetId
     * @param replacedAssetId
     * @returns {{replaceWithAsset: (*|null|*), sourceReplacedAssets: Array}|*}
     */
    function getReplaceAssetsCollection(replacedAssetId) {
      var outcome, replaceWithAsset, sourceReplacedAssets, replaceAssets = {
        replaceWithAsset: null,
        sourceReplacedAssets: []
      };
      //to get the outcome of replaced asset to identify the replacedWithAssetId
      outcome = OutcomeService.getOutcomeByAssetId(replacedAssetId);
      if (outcome && outcome.replacedByAssetId) {
        // get the replaced with asset (new lease or replaceWithExisting asset)
        replaceWithAsset = AssetService.getAsset(outcome.replacedByAssetId);
        //to get all the source assets based on the replacedByAssetId
        sourceReplacedAssets = OutcomeService.getSourceAssetsByReplacedId(outcome.replacedByAssetId);
        replaceAssets = {
          replaceWithAsset: replaceWithAsset,
          sourceReplacedAssets: sourceReplacedAssets
        };
      }
      return replaceAssets;
    }

    /**
     * function to add all the countries and their child(Sites, Building, Floors and assets) for Project
     * @param countries array of all the countries under a project
     * @param levelSite array of all the Sites
     * @param levelBuilding array of all the buildings
     * @param levelFloor array of all the floors
     */
    function addCountriesData(countries, levelSite, levelBuilding, levelFloor) {
      var countryIndex, levelsDetail,
          dartPricingData = PricingHelperService.assetDartPricingData(),
          countryPricingData = PricingService.getCountryPricingData(dartPricingData) || [],
          isMvFeatureEnable = PorticoSDSharedService.getFeatureFlagByKey(PorticoSDConfiguration.FEATURE.MULTI_VENDOR);
      for (countryIndex = 0; countryIndex < countries.length; countryIndex++) {
        if (countryPricingData.length) {
          countryPricingData.forEach(function (pricing) {
            if (pricing.countryId === countries[countryIndex]._id) {
              countries[countryIndex].dartPricingData = pricing;
            }
          });
        }
        levelsDetail = {countryId: "", siteId: "", buildingId: "", floorId: ""};
        levelsDetail.countryId = countries[countryIndex]._id;
        if (isMvFeatureEnable) {
          MultiVendorService.setMvDetailsForCountry(countries[countryIndex]);
        }
        addLevel(countries[countryIndex], PorticoSDConfiguration.TREE_LEVEL_COUNTRY, levelsDetail); // Add Country

        // Get sites for the country
        var sites = levelSite.filter(function (site) {
          return site.parentId === countries[countryIndex]._id;
        });

        if (sites) {
          //To add all the sites data associated to each country
          addSitesData(sites, levelBuilding, levelFloor, levelsDetail);
        }
      }
    }

    /**
     *  function to add all the sites and their child(Building, Floors and assets) for each Country
     * @param sites array of all the sites under each country
     * @param levelBuilding array of all the buildings
     * @param levelFloor array of all the floors
     * @param levelsDetail {{countryId: "", siteId: "", buildingId: "", floorId: ""}}
     */
    function addSitesData(sites, levelBuilding, levelFloor, levelsDetail) {
      var siteIndex;
      for (siteIndex = 0; siteIndex < sites.length; siteIndex++) {
        levelsDetail.siteId = sites[siteIndex]._id;
        levelsDetail.buildingId = "";
        levelsDetail.floorId = "";
        addLevel(sites[siteIndex], PorticoSDConfiguration.TREE_LEVEL_SITE, levelsDetail); // Add Site
        // Get building for the site
        var buildings = levelBuilding.filter(function (building) {
          return building.parentId === sites[siteIndex]._id;
        });
        if (buildings) {
          //To add all the building data associated to each Site
          addBuildingData(buildings, levelFloor, levelsDetail);
        }
      }
    }

    /**
     * function to add all the buildings and their child(Floors and assets) for each Site
     * @param buildings all the buildings
     * @param levelFloor
     * @param levelsDetail {{countryId: "", siteId: "", buildingId: "", floorId: ""}}
     */
    function addBuildingData(buildings, levelFloor, levelsDetail) {
      var buildingIndex;
      for (buildingIndex = 0; buildingIndex < buildings.length; buildingIndex++) {
        levelsDetail.buildingId = buildings[buildingIndex]._id;
        levelsDetail.floorId = "";
        addLevel(buildings[buildingIndex], PorticoSDConfiguration.TREE_LEVEL_BUILDING, levelsDetail); // Add Building
        // Get floor for the site
        var floors = levelFloor.filter(function (floor) {
          return floor.parentId === buildings[buildingIndex]._id;
        });
        if (floors) {
          //To add all the floors data associated to each building
          addFloorData(floors, levelsDetail);
        }
      }
    }

    /**
     * add all the floors and their assets for each building
     * @param floors array of all the floors of a building
     * @param levelsDetail {{countryId: "", siteId: "", buildingId: "", floorId: ""}}
     */
    function addFloorData(floors, levelsDetail) {
      var floorIndex;
      for (floorIndex = 0; floorIndex < floors.length; floorIndex++) {
        levelsDetail.floorId = floors[floorIndex]._id;
        addLevel(floors[floorIndex], PorticoSDConfiguration.TREE_LEVEL_FLOOR, levelsDetail); // Add Floor
        // Get all the assets based on the floorId
        var floorAssets = getFloorAssets(floors[floorIndex]._id);
        if (floorAssets) {
          //To add all the assets specific to each floor
          addTabularAssets(floorAssets, floors[floorIndex]._id, PorticoSDConfiguration.TREE_LEVEL_ASSET, levelsDetail);
        }
      }
    }

    /**
     * return true if asset already exist in tabular view data else false
     * @param tabularData tabular data
     * @param assetId asset Id
     * @returns {boolean}
     */
    function isAssetExist(tabularData, assetId) {
      var found = tabularData.find(function (item) {
        return item.assetId === assetId;
      });
      return found ? true : false;
    }

    /**
     * To add the group of replaced assets in tabular data
     * @param replaceAssetCollection object which contains replace with asset and an array of replaced assets
     * @param floorId floor Level Id
     * @param treeLevel asset tree level in tabular data default tree level for asset is 4
     * @param levelsDetail {{countryId: "", siteId: "", buildingId: "", floorId: ""}}
     */
    function addReplacedAssetsCollection(replaceAssetCollection, floorId, treeLevel, levelsDetail) {
      var indexReplace;
      // To add replaced with asset first check replacedWithAsset already exist or not
      if (replaceAssetCollection.replaceWithAsset && !isAssetExist(tabularPool, replaceAssetCollection.replaceWithAsset._id)) {
        addAsset(replaceAssetCollection.replaceWithAsset, floorId, treeLevel, false, levelsDetail);
      }
      if (replaceAssetCollection.sourceReplacedAssets) {
        // To add group of all the replaced assets
        for (indexReplace = 0; indexReplace < replaceAssetCollection.sourceReplacedAssets.length; indexReplace++) {
          if (!isAssetExist(tabularPool, replaceAssetCollection.sourceReplacedAssets[indexReplace]._id)) {
            addAsset(replaceAssetCollection.sourceReplacedAssets[indexReplace], floorId, treeLevel, true, levelsDetail);
          }
        }
      }
    }

    /**
     * function to add reposition assets collection
     * @param asset repositioned asset
     * @param floorId floor Level Id
     * @param treeLevel
     * @param levelsDetail {{countryId: "", siteId: "", buildingId: "", floorId: ""}}
     */
    function addRepositionAssets(asset, floorId, treeLevel, levelsDetail) {
      // to get the reposition (New) asset get the outcome based on the original asset id
      var repositionedAssetOutcome = OutcomeService.getOutcomeByOriginalAssetId(asset._id);
      if (repositionedAssetOutcome) {
        // To get the reposition (new) asset
        var repositionAsset = AssetService.getAsset(repositionedAssetOutcome.assetId);
        if (repositionAsset) {
          //To add the reposition asset (new asset)
          addAsset(repositionAsset, floorId, treeLevel, false, levelsDetail);
          //To add the repositioned (original) asset
          addAsset(asset, floorId, treeLevel, true, levelsDetail);
        }
      }
    }

    /**
     * Get the levels based on ProjectId / provided level ID
     * if level id is not provided then it will retrieve levels from ProjectId
     * @param levelId
     * @returns {*}
     */
    function getLevelData(levelId) {
      var subLevels, sublevelLength,
          levelCountry = [], levelSite = [], levelBuilding = [], levelFloor = [], levelProject = [];

      // Get all the levels.
      levelId = levelId ? levelId : $routeParams.id;

      subLevels = LevelService.getSubLevels(levelId);
      sublevelLength = subLevels.length;

      if (Utilis.isNull(subLevels) || Utilis.isEmpty(subLevels)) {
        return null;
      }
      // Get the project level
      levelProject = LevelService.getLevel($routeParams.id);


      // filtering level based on type
      for (var levelLength = 0; levelLength < sublevelLength; levelLength++) {
        switch (subLevels[levelLength].type) {
          case PorticoSDConfiguration.LEVEL_COUNTRY:
            levelCountry.push(subLevels[levelLength]);
            break;
          case PorticoSDConfiguration.LEVEL_SITE:
            levelSite.push(subLevels[levelLength]);

            break;
          case PorticoSDConfiguration.LEVEL_BUILDING:
            levelBuilding.push(subLevels[levelLength]);

            break;
          case PorticoSDConfiguration.LEVEL_FLOOR:
            levelFloor.push(subLevels[levelLength]);
            break;
        }
      }
      levels = {
        project: levelProject,
        countries: levelCountry,
        sites: levelSite,
        buildings: levelBuilding,
        floors: levelFloor
      };
      return levels;
    }

    /**
     * Add project in tabular view.
     * @param level
     * @param treeLevel
     */
    function addProject(level, treeLevel) {
      var tabularViewLevelRow = angular.copy(tabularViewProperties);
      tabularViewLevelRow.level = level.name;
      tabularViewLevelRow.levelId = level._id;
      tabularViewLevelRow.type = PorticoSDConfiguration.PROJECT;
      tabularViewLevelRow.comments = level.notes;
      tabularViewLevelRow.$$treeLevel = treeLevel;
      retrieveInstance(level._id, tabularViewLevelRow);
    }

    /**
     * Get Tabular Data
     */
    function loadTabularData() {
      //clear the pool manager
      removeTabularData();
      var levels = getLevelData();

      if (levels) {
        if (levels.project) {
          addProject(levels.project, PorticoSDConfiguration.TREE_LEVEL_PROJECT);
        }
        if (levels.countries) {
          //To add all the countries data associated to Project
          addCountriesData(levels.countries, levels.sites, levels.buildings, levels.floors);
        }
      }
      if (tabularPool && tabularPool.length) {
        return PorticoSDPoolManager.toDataArray(tabularPool);
      }
      return null;
    }

    /**
     * function to add floor specific assets to the tabularView
     * @param floorAssets all the assets of a floor
     * @param floorId
     * @param treeLevel tree level for assets
     * @param levelsDetail {{countryId: "", siteId: "", buildingId: "", floorId: ""}}
     */
    function addTabularAssets(floorAssets, floorId, treeLevel, levelsDetail) {
      try {
        var index,
            configMatrix = SharedService.getConfiguration().StateMatrix[SharedService.activeState()];
        if (floorAssets) {
          for (index = 0; index < floorAssets.length; index++) {
            //to add replaceWithAsset on the top of replacedAsset check the outcome isReplace
            if (TransitionHelperService.isReplace(floorAssets[index].properties.outcome)) {
              //get the object which contains all the replaced and replacedWithAsset
              var replaceAssetCollection = getReplaceAssetsCollection(floorAssets[index].properties._id);
              if (replaceAssetCollection) {
                // To add replaced with asset first check replacedWithAsset already exist or not
                addReplacedAssetsCollection(replaceAssetCollection, floorId, treeLevel, levelsDetail);
              }
            } else if (floorAssets[index].properties.isReal() && TransitionHelperService.isReposition(floorAssets[index].properties.outcome)) {
              //To add the Reposition and Reposition assets in a group
              addRepositionAssets(floorAssets[index].properties, floorId, treeLevel, levelsDetail);
            }
            //Add real assets if it's not a replaced asset
            else if (floorAssets[index].properties.isReal() && !TransitionHelperService.isReplaceWithExisting(floorAssets[index].properties.outcome) && !TransitionHelperService.isReplace(floorAssets[index].properties.outcome)) {
              addAsset(floorAssets[index].properties, floorId, treeLevel, true, levelsDetail);
            }
            // to add the virtual assets other than replace and reposition assets as they are already added by above condition
            else if ((!TransitionHelperService.isReplaceWithNew(floorAssets[index].properties.outcome) ||
                (configMatrix && configMatrix.asset[floorAssets[index].properties.state].show.outcome[floorAssets[index].properties.outcome] &&
                    OutcomeService.getOutcomeByAssetId(floorAssets[index].properties._id).outcome !== OutcomeType.REPLACE_WITH_NEW)) &&
                !TransitionHelperService.isReposition(floorAssets[index].properties.outcome)) {
              addAsset(floorAssets[index].properties, floorId, treeLevel, false, levelsDetail); // Add Asset
            }
          }
        }
      } catch (ex) {
        $log.error('Error in adding asset in tabular pool : ', ex);
      }
    }

    /**
     * to get the tabular view data from pool manager
     * @param id
     * @returns {*}
     */
    function getTabularViewData(id) {
      if (!arguments.length) {
        return PorticoSDPoolManager.toDataArray(tabularPool);
      }
      var TabularData = [];
      angular.forEach(tabularPool, function (tabularData) {
        if (tabularData.id === id) {
          TabularData.push(tabularData.value);
        }
      });
      return TabularData;
    }

    /**
     * to get the tabular view assets from pool manager based on provided level ids
     * @param floorIds (level ids for which wee need assets)
     * @param isUpdateSelection flag to update selection value
     * @param selectionValue {true/false} value to update in selection
     * @returns {*}
     */
    function getTabularViewAssetsForFloors(floorIds, isUpdateSelection, selectionValue) {
      var tabularViewAssets = [];
      for (var i = 0; i < tabularPool.length; i++) {
        if (tabularPool[i].value.type === PorticoSDConfiguration.ASSET && floorIds.indexOf(tabularPool[i].value.levelId) > -1) {
          if (isUpdateSelection) {
            tabularPool[i].value.selection = selectionValue;
          }
          tabularViewAssets.push(tabularPool[i].value);
        }
      }
      return tabularViewAssets;
    }

    /**
     * Add level in tabular view
     * @param level
     * @param treeLevel
     * @param levelsDetail {{countryId: "", siteId: "", buildingId: "", floorId: ""}}
     */
    function addLevel(level, treeLevel, levelsDetail) {
      var tabularViewLevelRow = getNewLevel(level, treeLevel, levelsDetail);
      retrieveInstance(level._id, tabularViewLevelRow);
    }

    /**
     * Get new level
     * @param level
     * @param treeLevel
     * @param levelsDetail {{countryId: "", siteId: "", buildingId: "", floorId: ""}}
     */
    function getNewLevel(level, treeLevel, levelsDetail) {
      var tabularViewLevelRow = angular.copy(tabularViewProperties);
      tabularViewLevelRow.level = level.name;
      tabularViewLevelRow.levelId = level._id;
      tabularViewLevelRow.comments = level.notes;
      tabularViewLevelRow.type = PorticoSDConfiguration.LEVEL;
      tabularViewLevelRow.isRealAsset = true;
      tabularViewLevelRow.$$treeLevel = treeLevel;
      setLevelDetails(tabularViewLevelRow, levelsDetail);
      if (level.dartPricingData) {
        PricingService.setPricingData(tabularViewLevelRow, level.dartPricingData);
      }
      return tabularViewLevelRow;
    }

    /**
     * @desc regions settings
     * @param levelsDetail
     */
    function setCurrentRowLocale(levelsDetail) {
      if (levelsDetail.countryId) {
        RGSharedLocaleService.selectedRowLevel(levelsDetail);
      }
    }

    /**
     * Update existing level.
     * @param level
     * @param treeLevel
     * @param levelsDetail {{countryId: "", siteId: "", buildingId: "", floorId: ""}}
     * @param tabularViewLevelRow
     */
    function updateLevel(level, treeLevel, levelsDetail, tabularViewLevelRow) {
      //regions settings
      setCurrentRowLocale(levelsDetail);
      tabularViewLevelRow.level = level.name;
      tabularViewLevelRow.levelId = level._id;
      tabularViewLevelRow.comments = level.notes;
      tabularViewLevelRow.type = PorticoSDConfiguration.LEVEL;
      tabularViewLevelRow.isRealAsset = true;
      tabularViewLevelRow.$$treeLevel = treeLevel;
      setLevelDetails(tabularViewLevelRow, levelsDetail);
      if (level.dartPricingData) {
        PricingService.setPricingData(tabularViewLevelRow, level.dartPricingData);
      } else {
        PricingService.resetDartPricingDataForTabularRow(tabularViewLevelRow);
      }
      return tabularViewLevelRow;
    }

    /**
     * Set the levesls id in tabular row
     * @param tabularRow
     * @param levelsDetail
     */
    function setLevelDetails(tabularRow, levelsDetail) {
      var levelsId = angular.copy(levelsDetail);
      if (levelsId) {
        tabularRow.countryId = levelsId.countryId;
        tabularRow.siteId = levelsId.siteId;
        tabularRow.buildingId = levelsId.buildingId;
        tabularRow.floorId = levelsId.floorId;
      }
    }

    /**
     * Get the asset level name
     * @param model
     * @param serialNumber
     * @param make
     * @returns {string}
     */
    function getAssetLevelName(model, serialNumber, make) {
      if (model === undefined) {
        model = '';
      }
      if (serialNumber === undefined) {
        serialNumber = '';
      }
      if (make === undefined) {
        make = '';
      }

      if (model === '' && serialNumber === '' && make === '') {
        return 'New Empty Asset ()';
      } else {
        return model + ' (' + serialNumber + ') ' + make;
      }
    }

    /**
     * Add the asset in tabular view.
     * @param asset
     * @param levelId
     * @param treeLevel
     * @param isReal
     * @param levelsDetail {{countryId: "", siteId: "", buildingId: "", floorId: ""}}
     */
    function addAsset(asset, levelId, treeLevel, isReal, levelsDetail) {
      //regionSettings
      setCurrentRowLocale(levelsDetail);
      var tabularViewAssetRow = getNewAsset(asset, levelId, treeLevel, isReal, levelsDetail);
      retrieveInstance(asset._id, tabularViewAssetRow);
      // Add accessories.
      addAccessoryAfterAssetInPoolManager(levelId, asset, levelsDetail);
    }

    /**
     * Get the new accessory object.
     * @param accessory
     * @param levelId
     * @param asset
     * @param treeLevel
     * @param levelsDetail {{countryId: "", siteId: "", buildingId: "", floorId: ""}}
     */
    function getNewAccessory(accessory, levelId, asset, treeLevel, levelsDetail) {
      var tabularViewAccessoryRow = angular.copy(tabularViewProperties);
      tabularViewAccessoryRow.level = '[ ' + accessory.CMO + ' => ' + accessory.FMO + ' ] : ' + accessory.accessoryNumber + ' - ' + accessory.description;
      tabularViewAccessoryRow.levelId = levelId;
      tabularViewAccessoryRow.assetId = asset._id;
      tabularViewAccessoryRow.accessoryId = accessory._id;
      tabularViewAccessoryRow.type = PorticoSDConfiguration.ACCESSORY;
      tabularViewAccessoryRow.isRealAsset = asset.isReal();
      tabularViewAccessoryRow.outcome = asset.outcome;
      tabularViewAccessoryRow.$$treeLevel = treeLevel;
      setLevelDetails(tabularViewAccessoryRow, levelsDetail);
      return tabularViewAccessoryRow;
    }

    /**
     * Get valid pages
     * If the pages are null, empty and undefined then return 0 else return the pages.
     * @param pages
     * @returns {*}
     */
    function getValidPages(pages) {
      // To Do : Move this method in PorticoSD utility service.
      if (!Utilis.isValid(pages) || pages === "") {
        pages = 0;
      } else {
        pages = Utilis.toFloor(pages);
      }
      return pages;
    }

    /**
     * Get the new asset object.
     * @param asset
     * @param levelId
     * @param treeLevel
     * @param isReal
     * @param levelsDetail {{countryId: "", siteId: "", buildingId: "", floorId: ""}}
     */
    function getNewAsset(asset, levelId, treeLevel, isReal, levelsDetail) {
      var mvDetails,
          tabularViewAssetRow = angular.copy(tabularViewProperties);
      tabularViewAssetRow.level = getAssetLevelName(asset.model, asset.serialNumber, asset.make);
      tabularViewAssetRow.levelId = levelId;
      tabularViewAssetRow.subEnvID = asset.subEnvId;
      tabularViewAssetRow.assetAbbreviation = asset.assetAbbreviation;
      tabularViewAssetRow.mapId = asset.mapID;
      tabularViewAssetRow.deviceModel = asset.model;
      tabularViewAssetRow.deviceType = translateFilter(asset.deviceType);
      tabularViewAssetRow.deviceQty = asset.qty ? asset.qty : 1;
      tabularViewAssetRow.sku = asset.productNumber;
      tabularViewAssetRow.floor = PorticoSDHelperService.getLevelName(levelsDetail.floorId);
      tabularViewAssetRow.department = asset.department;
      tabularViewAssetRow.monoPages = getValidPages(asset.monoTotalMonthly);
      tabularViewAssetRow.colorPages = getValidPages(asset.colorTotalMonthly);
      tabularViewAssetRow.colorPro = getValidPages(asset.colorProMonthlyVolume);
      tabularViewAssetRow.proposedMonthlyPageVolumeMono = getValidPages(asset.proposedMonthlyPageVolumeMono);
      tabularViewAssetRow.proposedMonthlyPageVolumeColor = getValidPages(asset.proposedMonthlyPageVolumeColor);
      tabularViewAssetRow.proposedColorProMonthlyVolume = getValidPages(asset.proposedColorProMonthlyVolume);
      tabularViewAssetRow.ampv = getAmpv(tabularViewAssetRow.colorPages, tabularViewAssetRow.monoPages, tabularViewAssetRow.colorPro);
      tabularViewAssetRow.monthlyMonoPages = PorticoSDHelperService.getMonthlyPages(tabularViewAssetRow.monoPages,
          tabularViewAssetRow.proposedMonthlyPageVolumeMono, tabularViewAssetRow.deviceQty);
      tabularViewAssetRow.monthlyColorPages = PorticoSDHelperService.getMonthlyPages(tabularViewAssetRow.colorPages,
          tabularViewAssetRow.proposedMonthlyPageVolumeColor, tabularViewAssetRow.deviceQty);
      tabularViewAssetRow.monthlyColorProfPages = PorticoSDHelperService.getMonthlyPages(tabularViewAssetRow.colorPro,
          tabularViewAssetRow.proposedColorProMonthlyVolume, tabularViewAssetRow.deviceQty);
      tabularViewAssetRow.proposedAmpv = getAmpv(tabularViewAssetRow.proposedMonthlyPageVolumeColor,
          tabularViewAssetRow.proposedMonthlyPageVolumeMono, tabularViewAssetRow.proposedColorProMonthlyVolume);
      tabularViewAssetRow.monthlyPages = PorticoSDHelperService.getMonthlyPages(tabularViewAssetRow.ampv,
          tabularViewAssetRow.proposedAmpv, tabularViewAssetRow.deviceQty);
      tabularViewAssetRow.colorTotalPagesPercentage = asset.colorPercentageOfTotalPages ? asset.colorPercentageOfTotalPages + '%'
          : getColorTotalPagesPercentage(tabularViewAssetRow.colorPages, tabularViewAssetRow.monoPages, tabularViewAssetRow.colorPro);
      tabularViewAssetRow.colorProTotalPagesPercentage = asset.profColorPercentageOfTotalPages ?
          asset.profColorPercentageOfTotalPages + '%' : getColorProTotalPagesPercentage(tabularViewAssetRow.colorPages,
              tabularViewAssetRow.monoPages, tabularViewAssetRow.colorPro);
      tabularViewAssetRow.proposedColorTotalPagesPercentage = getColorTotalPagesPercentage(tabularViewAssetRow.proposedMonthlyPageVolumeColor,
          tabularViewAssetRow.proposedMonthlyPageVolumeMono, tabularViewAssetRow.proposedColorProMonthlyVolume);
      tabularViewAssetRow.proposedColorProTotalPagesPercentage = getColorProTotalPagesPercentage(tabularViewAssetRow.proposedMonthlyPageVolumeColor,
          tabularViewAssetRow.proposedMonthlyPageVolumeMono, tabularViewAssetRow.proposedColorProMonthlyVolume);
      tabularViewAssetRow.a3Percentage = getA3Percentage(asset.a3Percentage);
      tabularViewAssetRow.colorA3Percentage = getA3Percentage(asset.colorA3Percentage);
      tabularViewAssetRow.monoA3Percentage = getA3Percentage(asset.monoA3Percentage);
      tabularViewAssetRow.age = asset.age;
      tabularViewAssetRow.lifeTimeEngineLife = getLifetimeEngineLifePercentage(asset.lifeTimeEngineLife, asset.proposedLifeTimeEngineLife);
      tabularViewAssetRow.currentLifeTimeEngineLife = getValidPercentage(asset.lifeTimeEngineLife);
      tabularViewAssetRow.proposedLifeTimeEngineLife = getValidPercentage(asset.proposedLifeTimeEngineLife);
      tabularViewAssetRow.eoslPast = getEOSLPast(asset.eoslPast, asset.proposedEoslPast);
      tabularViewAssetRow.currentEoslPast = asset.eoslPast;
      tabularViewAssetRow.proposedEoslPast = asset.proposedEoslPast;
      tabularViewAssetRow.comments = asset.notes;
      tabularViewAssetRow.assetId = asset._id;
      tabularViewAssetRow.$$treeLevel = treeLevel;
      tabularViewAssetRow.type = PorticoSDConfiguration.ASSET;
      tabularViewAssetRow.isRealAsset = isReal;
      tabularViewAssetRow.outcome = asset.outcome;
      tabularViewAssetRow.hwDisposition = PorticoSDHelperService.getHwDisposition(asset);
      tabularViewAssetRow.pageSize = asset.hasA3 ? PorticoSDConfiguration.PAGE_SIZE_A3 : PorticoSDConfiguration.PAGE_SIZE_A4;
      tabularViewAssetRow.pvtIn = getPvtInByAssetId(asset._id, asset.mapID);
      tabularViewAssetRow.pvtOut = getPvtOutByAssetId(asset._id, asset.mapID);
      tabularViewAssetRow.initHwDispositions = getDispositionList(asset.outcome);
      if (PorticoSDSharedService.getFeatureFlagByKey(PorticoSDConfiguration.FEATURE.MULTI_VENDOR)) {
        tabularViewAssetRow.isMultiVendorAsset = PorticoSDHelperService.isMultiVendorAsset(asset);
        if (tabularViewAssetRow.isMultiVendorAsset) {
          tabularViewAssetRow.productId = asset.productId;
          tabularViewAssetRow.mvSupply = asset.mvSupply;
          tabularViewAssetRow.mvService = asset.mvService;
          tabularViewAssetRow.mvSupplyCostStatus = MultiVendorService.updateMvCostStatus(asset.productId, asset.mvSupply);
          tabularViewAssetRow.mvServiceCostStatus = MultiVendorService.updateMvCostStatus(asset.productId, asset.mvService);
          tabularViewAssetRow.mvDartZone = asset.mvDartZone;
          tabularViewAssetRow.initMvDartZone = PorticoSDConfiguration.DART_ZONES;
          mvDetails = PorticoSDSharedService.getMvDetailsForCountry(levelsDetail.countryId);
          if (mvDetails) {
            tabularViewAssetRow.initService = mvDetails.service;
            tabularViewAssetRow.initSupply = mvDetails.supply;
          }
        }
      }
      tabularViewAssetRow.zone = ZoneService.getZoneName(asset._id);
      setLevelDetails(tabularViewAssetRow, levelsDetail);
      if (asset.dartPricingData) {
        PricingService.setPricingData(tabularViewAssetRow, asset.dartPricingData);
      }
      if (TcoPricingService.isTcoPricingFetched()) {
        PricingService.setTcoPricingData(tabularViewAssetRow);
      }
      setAssetAdditionalFields(tabularViewAssetRow, asset);
      return tabularViewAssetRow;
    }

    function setAssetAdditionalFields(tabularViewAssetRow, asset) {
      tabularViewAssetRow.macAddress = asset.macAddress;
      tabularViewAssetRow.hostName = asset.hostName;
      tabularViewAssetRow.subnetMask = asset.subnetMask;
      tabularViewAssetRow.iPAddress = asset.iPAddress;
      tabularViewAssetRow.gateway = asset.gateway;
      tabularViewAssetRow.dateIntroduced = PorticoSDUtilityService.formatDate(asset.dateIntroduced);
      tabularViewAssetRow.dateManufactured = PorticoSDUtilityService.formatDate(asset.dateManufactured);
      tabularViewAssetRow.dateInstalled = PorticoSDUtilityService.formatDate(asset.dateInstalled);
      tabularViewAssetRow.serviceID = asset.serviceID;
      tabularViewAssetRow.location = asset.location;
      tabularViewAssetRow.placement = translateFilter(asset.placement);
      tabularViewAssetRow.connectionType = translateFilter(asset.connectionType);
      tabularViewAssetRow.printQueue = asset.printQueue;
      tabularViewAssetRow.faxNumber = asset.faxNumber;
      tabularViewAssetRow.serialNumber = asset.serialNumber;
      return tabularViewAssetRow;
    }

    /**
     * Round A3%, Color A3% and Mono A3% value of an asset
     * @param a3Percentage
     * @returns {string}
     */
    function getA3Percentage(a3Percentage) {
      var a3PercentageValues = '';
      if (a3Percentage && !isNaN(a3Percentage)) {
        a3PercentageValues = Math.round(a3Percentage) + "%";
      }
      return a3PercentageValues;
    }

    /**
     * Getting Lifetime Engine Life % of an asset on the basis of active state
     * @param currentLifeTimeEngineLife
     * @param proposedLifeTimeEngineLife
     * @returns {string}
     */
    function getLifetimeEngineLifePercentage(currentLifeTimeEngineLife, proposedLifeTimeEngineLife) {
      var lifeTimeEngineLifePercentage = '',
          lifeTimeEngineLife;
      if (SharedService.isCurrentState()) {
        lifeTimeEngineLife = currentLifeTimeEngineLife;
      } else {
        lifeTimeEngineLife = proposedLifeTimeEngineLife;
      }

      if (lifeTimeEngineLife && !isNaN(lifeTimeEngineLife)) {
        lifeTimeEngineLifePercentage = Math.round(lifeTimeEngineLife) + "%";
      }
      return lifeTimeEngineLifePercentage;
    }

    /**
     * Get valid percentage
     * @param value
     * @returns {string}
     */
    function getValidPercentage(value) {
      var percentage = '';
      if (value && !isNaN(value)) {
        percentage = Math.round(value) + "%";
      }
      return percentage;
    }

    /**
     * Getting Past EOSL(months) value of an asset on the basis of active state
     * @param eoslPast
     * @param proposedEoslPast
     * @returns {*}
     */
    function getEOSLPast(eoslPast, proposedEoslPast) {
      if (SharedService.isCurrentState())
        return eoslPast;
      else {
        return proposedEoslPast;
      }
    }

    /**
     * Calculates ampv of an asset
     * @param colorPages
     * @param monoPages
     * @param colorPro
     * @returns {string} calculated ampv
     */
    function getAmpv(colorPages, monoPages, colorPro) {
      colorPages = Utilis.isValid(colorPages) ? Utilis.toNumber(colorPages) : 0;
      monoPages = Utilis.isValid(monoPages) ? Utilis.toNumber(monoPages) : 0;
      colorPro = Utilis.isValid(colorPro) ? Utilis.toNumber(colorPro) : 0;
      return colorPages + monoPages + colorPro;
    }


    /**
     * Calculates colorPagesPercentage of an asset
     * @param colorPages
     * @param monoPages
     * @param colorPro
     * @returns {string} calculated colorPagesPercentage
     */
    function getColorTotalPagesPercentage(colorPages, monoPages, colorPro) {
      var colorPagesPercentage = PorticoSDConfiguration.ZERO_PERCENT;
      if (Utilis.isValid(colorPages)) {
        colorPages = Utilis.toNumber(colorPages);
        monoPages = Utilis.isValid(monoPages) ? Utilis.toNumber(monoPages) : 0;
        colorPro = Utilis.isValid(colorPro) ? Utilis.toNumber(colorPro) : 0;
        if (colorPages > 0 || monoPages > 0 || colorPro > 0) {
          colorPagesPercentage = Math.round((colorPages / (colorPages + monoPages + colorPro)) * 100) + "%";
        }
      }
      return colorPagesPercentage;
    }

    /**
     * Calculates colorProPagesPercentage of an asset
     * @param colorPages
     * @param monoPages
     * @param colorPro
     * @returns {string} calculated colorPagesPercentage
     */
    function getColorProTotalPagesPercentage(colorPages, monoPages, colorPro) {
      var colorPagesPercentage = PorticoSDConfiguration.ZERO_PERCENT;
      if (Utilis.isValid(colorPages)) {
        colorPages = Utilis.toNumber(colorPages);
        monoPages = Utilis.isValid(monoPages) ? Utilis.toNumber(monoPages) : 0;
        colorPro = Utilis.isValid(colorPro) ? Utilis.toNumber(colorPro) : 0;
        if (colorPages > 0 || monoPages > 0 || colorPro > 0) {
          colorPagesPercentage = Math.round((colorPro / (colorPages + monoPages + colorPro)) * 100) + "%";
        }
      }
      return colorPagesPercentage;
    }

    /**
     * Calculates Percentage color pages on color device
     * @param colorPages
     * @returns {string} calculated colorPercentage
     */
    function getColorPercentage(colorPages) {
      var colorPercentage = PorticoSDConfiguration.ZERO_PERCENT;
      if (Utilis.isValid(colorPages)) {
        colorPages = Utilis.toNumber(colorPages);
        if (colorPages > 0) {
          colorPercentage = Math.round(colorPages / 100) + "%";
        }
      }
      return colorPercentage;
    }

    /**
     *  To get the PorticoSDDispositionList
     */
    function getPorticoSdDispositionList() {
      var porticoSdDispositionList = {
        add: PorticoSDHelperService.getTranslatedOutcomeLabel(OutcomeType.ADD),
        reset: PorticoSDHelperService.getTranslatedOutcomeLabel(OutcomeType.RESET),
        retain: PorticoSDHelperService.getTranslatedOutcomeLabel(OutcomeType.RETAIN),
        retire: PorticoSDHelperService.getTranslatedOutcomeLabel(OutcomeType.RETIRE),
        excludeDevice: PorticoSDHelperService.getTranslatedOutcomeLabel(OutcomeType.EXCLUDE_DEVICE),
        replaceOneForOne: PorticoSDHelperService.getTranslatedOutcomeLabel(OutcomeType.REPLACE_ONE_FOR_ONE),
        replaceManyToOne: PorticoSDHelperService.getTranslatedOutcomeLabel(OutcomeType.REPLACE_MANY_TO_ONE),
        reposition: PorticoSDHelperService.getTranslatedOutcomeLabel(OutcomeType.REPOSITION),
        retainAdvancePlacement: PorticoSDHelperService.getTranslatedOutcomeLabel(PorticoSDConfiguration.RETAIN_ADVANCE_PLACEMENT)
      };
      return porticoSdDispositionList;
    }

    /**
     * Get the accessory Ids from pool manager by Asset Id.
     * @param assetId
     * @returns {*}
     */
    function getAccessoryIdsFromPoolManagerByAssetId(assetId) {
      var assetAccessories,
          accessoryIds = [];

      if (tabularPool) {
        assetAccessories = tabularPool.filter(function (elementLevel) {
          return elementLevel.value.assetId === assetId &&
              elementLevel.value.type === PorticoSDConfiguration.ACCESSORY;
        });

        if (assetAccessories.length) {
          assetAccessories.forEach(function (accessory) {
            accessoryIds.push(accessory.id);
          });
        }
      }
      return accessoryIds;
    }

    /**
     * Get levels detail by floor id.
     * @param floorId
     * @returns {{countryId: string, siteId: string, buildingId: string, floorId: string}}
     */
    function getLevelsDetailByFloorId(floorId) {
      var levelsDetail = {countryId: "", siteId: "", buildingId: "", floorId: ""},
          levelFloor = LevelService.getLevel(floorId);

      if (levelFloor && levelFloor.ancestors && levelFloor.ancestors.length) {
        // Get the level Id's from the ancestors.
        levelsDetail.countryId = levelFloor.ancestors[1];
        levelsDetail.siteId = levelFloor.ancestors[2];
        levelsDetail.buildingId = levelFloor.ancestors[3];
        levelsDetail.floorId = levelFloor._id;
      }
      return levelsDetail;
    }

    /**
     * Update asset in pool manager.
     * @param asset
     */
    function updateAssetInPoolManager(asset) {
      // To update the asset in pool manager. Now we also need to update the accessories as well.
      // So first, I updated the asset and then remove all the accessories of the asset and then
      // get the accessories again for the asset and add them after the asset. It will give us updated accessories.

      var accessoryIds = [];
      if (asset) {
        var tabularViewAssetRow, entityMap, levelsDetail;
        if (asset.isVirtual() || asset.isUnknownState()) {
          entityMap = OutcomeService.getOutcomeByAssetId(asset.getId());
        } else {
          entityMap = LevelEntityMapService.getLevelEntityMapByEntityId(asset.getId());
        }
        if (entityMap) {
          levelsDetail = getLevelsDetailByFloorId(entityMap.levelId);
          tabularViewAssetRow = getNewAsset(asset, entityMap.levelId, PorticoSDConfiguration.TREE_LEVEL_ASSET, asset.isReal(), levelsDetail);
          updateInstance(asset._id, tabularViewAssetRow);
          // Remove the accessories
          accessoryIds = getAccessoryIdsFromPoolManagerByAssetId(asset._id);
          if (accessoryIds.length) {
            PorticoSDPoolManager.remove(tabularPool, accessoryIds);
          }
          // Add the accessories
          addAccessoryAfterAssetInPoolManager(entityMap.levelId, asset, levelsDetail);
        }
      }
    }

    /**
     * Update asset in Pool Manager.
     * @param selectedAssets
     */
    function updateAssetsInPoolManager(selectedAssets) {
      if (selectedAssets && selectedAssets.length) {
        selectedAssets.forEach(function (asset) {
          updateAssetInPoolManager(asset);
        });
      }
    }

    /**
     * Is Tco pricing need to update.
     * @returns {boolean}
     */
    function isTcoPricingUpdate() {
      var activeState = SharedService.activeState(),
          previousState = SharedService.previousState();

      // Transition state and future state show the Tco pricing for the future state. So no need to update.
      return !((activeState === PorticoSDConfiguration.STATES.TRANSITION && previousState === PorticoSDConfiguration.STATES.FUTURE) ||
          (activeState === PorticoSDConfiguration.STATES.FUTURE && previousState === PorticoSDConfiguration.STATES.TRANSITION));
    }

    /**
     * Update pool assets tco pricing
     */
    function updateAssetsTcoPricingInPoolManager() {
      var tabularViewAssetRow, tabularPoolLength,
          treeLevelAsset = PorticoSDConfiguration.TREE_LEVEL_ASSET,
          activeState = SharedService.activeState();
      try {
        if (isTcoPricingUpdate() && tabularPool && tabularPool.length) {
          tabularPoolLength = tabularPool.length;
          for (var index = 0; index < tabularPoolLength; index++) {
            if (tabularPool[index].value) {
              tabularViewAssetRow = tabularPool[index].value;
              TcoPricingHelperService.resetTcoPricingDataForTabularRow(tabularViewAssetRow);
              if (tabularPool[index].value.$$treeLevel === treeLevelAsset) {
                PricingService.setTcoPricingData(tabularViewAssetRow);
                PorticoSDHelperService.updateAssetBasedOnState(tabularViewAssetRow, activeState);
              }
              updateInstance(tabularPool[index].id, tabularViewAssetRow);
            }
          }
        }
      } catch (ex) {
        $log.error('Error in updating the tco pricing in tabular pool : ', ex);
      }
    }

    /**
     * Get Pool Id's from Tabular Pool.
     * @param tabularPoolData
     */
    function getTabularPoolIds(tabularPoolData) {
      return tabularPoolData.map(function (data) {
        return data.id;
      });
    }

    /**
     * Update tco pricing on levels if level is deleted.
     * @param level
     */
    function updateTcoLevelPricingOnLevelDelete(level) {
      try {
        if (TcoPricingService.isTcoPricingFetched() && tabularPool && tabularPool.length) {
          // If the level is removed from tabular pool then we need to update the tco pricing on other levels.
          var tabularLevel, filteredLevel;

          // Get the level from tabular pool as it will contain tco pricing values.
          filteredLevel = tabularPool.filter(function (elementLevel) {
            return (elementLevel.id === level._id && elementLevel.value.type === PorticoSDConfiguration.LEVEL);
          });

          if (filteredLevel.length) {
            tabularLevel = filteredLevel.first();
            if (tabularLevel.value) {
              switch (level.type) {
                case PorticoSDConfiguration.LEVEL_COUNTRY:
                  subtractLevelPricing(tabularLevel, $routeParams.id); // Project
                  break;
                case PorticoSDConfiguration.LEVEL_SITE:
                  subtractLevelPricing(tabularLevel, tabularLevel.value.countryId); // Country
                  subtractLevelPricing(tabularLevel, $routeParams.id); // Project
                  break;
                case PorticoSDConfiguration.LEVEL_BUILDING:
                  subtractLevelPricing(tabularLevel, tabularLevel.value.siteId); // Site
                  subtractLevelPricing(tabularLevel, tabularLevel.value.countryId); // Country
                  subtractLevelPricing(tabularLevel, $routeParams.id); // Project
                  break;
                case PorticoSDConfiguration.LEVEL_FLOOR:
                  subtractLevelPricing(tabularLevel, tabularLevel.value.buildingId); // Building
                  subtractLevelPricing(tabularLevel, tabularLevel.value.siteId); // Site
                  subtractLevelPricing(tabularLevel, tabularLevel.value.countryId); // Country
                  subtractLevelPricing(tabularLevel, $routeParams.id); // Project
                  break;
              }
            }
          }
        }
      } catch (ex) {
        $log.error('Error while deleting level from tabular pool : ', ex);
      }
    }

    /**
     * Update Tabular Pool by removing level.
     * @param level
     */
    function deleteLevelFromTabularPool(level) {
      var removeDataPoolId,
          removeDataPool = [];
      try {
        updateTcoLevelPricingOnLevelDelete(level);
        switch (level.type) {
          case PorticoSDConfiguration.LEVEL_COUNTRY:
            removeDataPool = tabularPool.filter(function (row) {
              return row.value.countryId === level._id;
            });
            break;
          case PorticoSDConfiguration.LEVEL_SITE:
            removeDataPool = tabularPool.filter(function (row) {
              return row.value.siteId === level._id;
            });
            break;
          case PorticoSDConfiguration.LEVEL_BUILDING:
            removeDataPool = tabularPool.filter(function (row) {
              return row.value.buildingId === level._id;
            });
            break;
          case PorticoSDConfiguration.LEVEL_FLOOR:
            removeDataPool = tabularPool.filter(function (row) {
              return row.value.floorId === level._id;
            });
            break;
        }
        if (removeDataPool && removeDataPool.length) {
          removeDataPoolId = getTabularPoolIds(removeDataPool);
          PorticoSDPoolManager.remove(tabularPool, removeDataPoolId);
        }
      } catch (ex) {
        $log.error('Error while deleting level from tabular pool : ', ex);
      }
    }

    /**
     * Get level details
     * @param level
     * @returns {{countryId: string, siteId: string, buildingId: string, floorId: string}}
     */
    function getLevelsDetail(level) {
      var levelsDetail = {countryId: "", siteId: "", buildingId: "", floorId: ""};
      switch (level.type) {
        case PorticoSDConfiguration.LEVEL_COUNTRY:
          levelsDetail.countryId = level._id;
          break;
        case PorticoSDConfiguration.LEVEL_SITE:
          levelsDetail.countryId = level.ancestors[1];
          levelsDetail.siteId = level._id;
          break;
        case PorticoSDConfiguration.LEVEL_BUILDING:
          levelsDetail.countryId = level.ancestors[1];
          levelsDetail.siteId = level.ancestors[2];
          levelsDetail.buildingId = level._id;
          break;
        case PorticoSDConfiguration.LEVEL_FLOOR:
          levelsDetail.countryId = level.ancestors[1];
          levelsDetail.siteId = level.ancestors[2];
          levelsDetail.buildingId = level.ancestors[3];
          levelsDetail.floorId = level._id;
          break;
      }
      return levelsDetail;
    }

    /**
     * Update Assets for a Level
     * @param level
     */
    function updateAssetsForLevel(level) {
      var filteredAssets, asset, assets = [];
      if (tabularPool && tabularPool.length) {
        if (level.type === PorticoSDConfiguration.LEVEL_COUNTRY) {
          filteredAssets = tabularPool.filter(function (elementLevel) {
            return (elementLevel.value.countryId === level._id && elementLevel.value.isMultiVendorAsset === true &&
                elementLevel.value.type === PorticoSDConfiguration.ASSET);
          });
        }
        if (filteredAssets.length) {
          filteredAssets.forEach(function (element) {
            asset = AssetService.getAsset(element.value.assetId);
            assets.push(asset);
          });
          updateAssetsInPoolManager(assets);
        }
      }
    }

    /**
     * Update level in pool manager.
     * @param level
     */
    function updateLevelInPoolManager(level) {
      var filteredLevels, tabularLevel, levelsDetail, tabularViewLevelRow;
      try {
        if (level && tabularPool && tabularPool.length) {
          if (level.type === PorticoSDConfiguration.LEVEL_FLOOR) {
            filteredLevels = tabularPool.filter(function (elementLevel) {
              if (elementLevel.value.floorId === level._id && elementLevel.value.type === PorticoSDConfiguration.ASSET) {
                elementLevel.value.floor = level.name;
              }
              return elementLevel.id === level._id && (elementLevel.value.type === PorticoSDConfiguration.LEVEL ||
                  elementLevel.value.type === PorticoSDConfiguration.PROJECT);
            });
          } else {
            filteredLevels = tabularPool.filter(function (elementLevel) {
              return elementLevel.id === level._id && (elementLevel.value.type === PorticoSDConfiguration.LEVEL ||
                  elementLevel.value.type === PorticoSDConfiguration.PROJECT);
            });
          }
          if (filteredLevels.length) {
            tabularLevel = filteredLevels.first();
            if (tabularLevel.value) {
              levelsDetail = getLevelsDetail(level);
              //regions settings
              setCurrentRowLocale(levelsDetail);
              tabularViewLevelRow = updateLevel(level, getTreeLevel(level.type), levelsDetail, tabularLevel.value);
              updateInstance(level._id, tabularViewLevelRow);

              if (level.type === PorticoSDConfiguration.LEVEL_COUNTRY) {
                MultiVendorService.setMvDetailsForCountry(level);
                updateAssetsForLevel(level);
              }
            }
          }
        }
      } catch (ex) {
        $log.error('Error in updating level in tabular Pool : ', ex);
      }
    }

    /**
     * Remove asset from pool manager.
     * @param assetIds
     */
    function removeAssetInPoolManager(assetIds) {
      var accessoryIds = [],
          removeAssetAccessoryIds = [];
      if (assetIds && assetIds.length > 0 && tabularPool) {
        // Get the accessories for each asset and delete all of them.
        assetIds.forEach(function (assetId) {
          accessoryIds = accessoryIds.concat(getAccessoryIdsFromPoolManagerByAssetId(assetId));
        });
        if (accessoryIds.length) {
          removeAssetAccessoryIds = assetIds.concat(accessoryIds);
        } else {
          removeAssetAccessoryIds = assetIds;
        }
        PorticoSDPoolManager.remove(tabularPool, removeAssetAccessoryIds);
      }
    }

    /**
     * Add asset in pool manager at a particular position.
     * @param asset
     */
    function addAssetInPoolManager(asset) {
      if (asset && tabularPool) {
        var newAsset, levelRowNumber, entityMap, levelsDetail;
        if (asset.isVirtual() || asset.isUnknownState()) {
          entityMap = OutcomeService.getOutcomeByAssetId(asset.getId());
        } else {
          entityMap = LevelEntityMapService.getLevelEntityMapByEntityId(asset.getId());
        }

        if (entityMap) {
          levelsDetail = getLevelsDetailByFloorId(entityMap.levelId);
          newAsset = getNewAsset(asset, entityMap.levelId, PorticoSDConfiguration.TREE_LEVEL_ASSET, asset.isReal(), levelsDetail);
          levelRowNumber = tabularPool.findIndex(function (elementLevel) {
            return elementLevel.value.levelId === entityMap.levelId &&
                elementLevel.value.type === PorticoSDConfiguration.LEVEL;
          });
          if (newAsset && levelRowNumber >= 0) {
            levelRowNumber++;
            addInstanceAtPosition(asset._id, newAsset, levelRowNumber);
            // Add accessories.
            addAccessoryAfterAssetInPoolManager(entityMap.levelId, asset, levelsDetail);
          }
        }
      }
    }

    /**
     * Add asset after asset in pool manager.
     * @param asset
     * @param oldAssetId
     */
    function addAssetAfterAssetInPoolManager(asset, oldAssetId) {
      if (asset && tabularPool) {
        var newAsset, levelRowNumber, accessories, entityMap, levelsDetail;
        if (asset.isVirtual() || asset.isUnknownState()) {
          entityMap = OutcomeService.getOutcomeByAssetId(asset.getId());
        } else {
          entityMap = LevelEntityMapService.getLevelEntityMapByEntityId(asset.getId());
        }

        if (entityMap) {
          levelsDetail = getLevelsDetailByFloorId(entityMap.levelId);
          newAsset = getNewAsset(asset, entityMap.levelId, PorticoSDConfiguration.TREE_LEVEL_ASSET, asset.isReal(), levelsDetail);
          levelRowNumber = tabularPool.findIndex(function (elementLevel) {
            return elementLevel.value.assetId === oldAssetId &&
                elementLevel.value.type === PorticoSDConfiguration.ASSET;
          });
          accessories = tabularPool.filter(function (elementLevel) {
            return elementLevel.value.assetId === oldAssetId &&
                elementLevel.value.type === PorticoSDConfiguration.ACCESSORY;
          });

          if (newAsset && levelRowNumber >= 0) {
            if (accessories.length > 0) {
              levelRowNumber = levelRowNumber + accessories.length;
            }
            levelRowNumber++;
            addInstanceAtPosition(asset._id, newAsset, levelRowNumber);
            // Add accessories.
            addAccessoryAfterAssetInPoolManager(entityMap.levelId, asset, levelsDetail);
          }
        }
      }
    }

    /**
     * Add asset in pool manager at a particular position.
     * @param asset
     * @param levelId
     * @param oldAssetId
     */
    function addVirtualAssetInPoolManager(asset, levelId, oldAssetId) {
      var newAsset, levelRowNumber,
          levelsDetail = getLevelsDetailByFloorId(levelId);
      // Add the virtual asset above the old asset.
      if (arguments.length === 3) {
        if (asset && levelId && tabularPool) {
          newAsset = getNewAsset(asset, levelId, PorticoSDConfiguration.TREE_LEVEL_ASSET, asset.isReal(), levelsDetail);
          levelRowNumber = tabularPool.findIndex(function (elementLevel) {
            return elementLevel.value.assetId === oldAssetId &&
                elementLevel.value.type === PorticoSDConfiguration.ASSET;
          });
        }
      }
      // Add the virtual asset just below floor.
      else if (arguments.length === 2) {
        if (asset && levelId && tabularPool) {
          newAsset = getNewAsset(asset, levelId, PorticoSDConfiguration.TREE_LEVEL_ASSET, asset.isReal(), levelsDetail);
          levelRowNumber = tabularPool.findIndex(function (elementLevel) {
            return elementLevel.value.levelId === levelId &&
                elementLevel.value.type === PorticoSDConfiguration.LEVEL;
          });
          levelRowNumber = levelRowNumber >= 0 ? levelRowNumber + 1 : levelRowNumber;
        }
      }

      if (newAsset && levelRowNumber >= 0) {
        addInstanceAtPosition(asset._id, newAsset, levelRowNumber);
        // Add accessories.
        addAccessoryAfterAssetInPoolManager(levelId, asset, levelsDetail);
      }
    }

    /**
     * Get tree level
     * @param levelType
     * @returns {*}
     */
    function getTreeLevel(levelType) {
      var treeLevel;
      switch (levelType) {
        case PorticoSDConfiguration.LEVEL_PROJECT:
          treeLevel = PorticoSDConfiguration.TREE_LEVEL_PROJECT;
          break;
        case PorticoSDConfiguration.LEVEL_COUNTRY:
          treeLevel = PorticoSDConfiguration.TREE_LEVEL_COUNTRY;
          break;
        case PorticoSDConfiguration.LEVEL_SITE:
          treeLevel = PorticoSDConfiguration.TREE_LEVEL_SITE;
          break;
        case PorticoSDConfiguration.LEVEL_BUILDING:
          treeLevel = PorticoSDConfiguration.TREE_LEVEL_BUILDING;
          break;
        case PorticoSDConfiguration.LEVEL_FLOOR:
          treeLevel = PorticoSDConfiguration.TREE_LEVEL_FLOOR;
          break;
      }
      return treeLevel;
    }

    /**
     * function to get the PVT In data by AssetId
     * @param assetId
     * @param mapID
     * @returns {Array}
     */
    function getPvtInByAssetId(assetId, mapID) {
      var pvtInDetail = [];
      var pvtToAssets = PageVolumeService.getPageVolumesTo(assetId),
          pvtInString = '', pvtInA3String = '', isA3Pvt = false;
      if (pvtToAssets) {
        for (var index = 0; index < pvtToAssets.length; index++) {
          var fromAsset = AssetService.getAsset(pvtToAssets[index].fromAssetId);
          isA3Pvt = !!(pvtToAssets[index].monoA3 || pvtToAssets[index].colorA3);
          var objPvtIn = {
            pvtId: pvtToAssets[index]._id,
            isA3Pvt: isA3Pvt,
            mono: pvtToAssets[index].mono ? pvtToAssets[index].mono : 0,
            color: pvtToAssets[index].color ? pvtToAssets[index].color : 0,
            monoA3: pvtToAssets[index].monoA3 ? pvtToAssets[index].monoA3 : 0,
            colorA3: pvtToAssets[index].colorA3 ? pvtToAssets[index].colorA3 : 0,
            fromMapId: fromAsset.mapID ? fromAsset.mapID : '',
            toMapId: mapID ? mapID : ''
          };
          pvtInDetail.push(objPvtIn);
          pvtInString += Utilis.isEmptyString(pvtInString) ? pvtInString : ', ';
          pvtInA3String += Utilis.isEmptyString(pvtInA3String) ? pvtInA3String : ', ';
          pvtInString += objPvtIn.fromMapId + "->" + objPvtIn.toMapId + " ( C: " + objPvtIn.color + ", M :" + objPvtIn.mono + ")" + " ";
          pvtInA3String += objPvtIn.fromMapId + "->" + objPvtIn.toMapId + " ( C: " + objPvtIn.colorA3 + ", M :" + objPvtIn.monoA3 + ")" + " ";
          if ($rootScope.token.featureFlag.regionSettings && false) {
            locale = RGSharedLocaleService.getSelectedColumnLocale().region;
            pvtInString += objPvtFrom.fromMapId + "->" + objPvtFrom.toMapId + " ( C: " + $filter('rgTabColNum')
            (objPvtIn.color, locale) + ", M :" + $filter('rgTabColNum')(objPvtIn.mono, locale) + ")" + " ";
            pvtInA3String += objPvtFrom.fromMapId + "->" + objPvtFrom.toMapId + " ( C: " + $filter('rgTabColNum')
            (objPvtIn.colorA3, locale) + ", M :" + $filter('rgTabColNum')(objPvtIn.monoA3, locale) + ")" + " ";
          }
        }
      }
      return {
        pvtInDetail: pvtInDetail,
        pvtInString: pvtInString,
        pvtInA3String: pvtInA3String
      };
    }

    /**
     * function to get the PVT Out data by AssetId
     * @param assetId
     * @param mapID
     * @returns {Array}
     */
    function getPvtOutByAssetId(assetId, mapID) {
      var pvtOutDetail = [];
      var pvtFromAssets = PageVolumeService.getPageVolumesFrom(assetId),
          pvtOutString = '', pvtOutA3String = '', isA3Pvt = false;
      if (pvtFromAssets) {
        for (var index = 0; index < pvtFromAssets.length; index++) {
          var toAsset = AssetService.getAsset(pvtFromAssets[index].toAssetId);
          if (toAsset) {
            isA3Pvt = !!(pvtFromAssets[index].monoA3 || pvtFromAssets[index].colorA3);
            var objPvtFrom = {
              pvtId: pvtFromAssets[index]._id,
              isA3Pvt: isA3Pvt,
              mono: pvtFromAssets[index].mono ? pvtFromAssets[index].mono : 0,
              color: pvtFromAssets[index].color ? pvtFromAssets[index].color : 0,
              monoA3: pvtFromAssets[index].monoA3 ? pvtFromAssets[index].monoA3 : 0,
              colorA3: pvtFromAssets[index].colorA3 ? pvtFromAssets[index].colorA3 : 0,
              fromMapId: mapID ? mapID : '',
              toMapId: toAsset.mapID ? toAsset.mapID : ''
            };
            pvtOutDetail.push(objPvtFrom);
            pvtOutString += Utilis.isEmptyString(pvtOutString) ? pvtOutString : ', ';
            pvtOutA3String += Utilis.isEmptyString(pvtOutA3String) ? pvtOutA3String : ', ';
            if ($rootScope.token.featureFlag.regionSettings) {
              locale = RGSharedLocaleService.getSelectedColumnLocale().region;
              pvtOutString += objPvtFrom.fromMapId + "->" + objPvtFrom.toMapId + " ( C: " + $filter('rgTabColNum')(objPvtFrom.color, locale) + ", M :" + $filter('rgTabColNum')(objPvtFrom.mono, locale) + ")" + " ";
              pvtOutA3String += objPvtFrom.fromMapId + "->" + objPvtFrom.toMapId + " ( C: " + $filter('rgTabColNum')(objPvtFrom.colorA3, locale) + ", M :" + $filter('rgTabColNum')(objPvtFrom.monoA3, locale) + ")" + " ";
            } else {
              pvtOutString += objPvtFrom.fromMapId + "->" + objPvtFrom.toMapId + " ( C: " + objPvtFrom.color + ", M :" + objPvtFrom.mono + ")" + " ";
              pvtOutA3String += objPvtFrom.fromMapId + "->" + objPvtFrom.toMapId + " ( C: " + objPvtFrom.colorA3 + ", M :" + objPvtFrom.monoA3 + ")" + " ";
            }
          }
        }
      }
      return {
        pvtOutDetail: pvtOutDetail,
        pvtOutString: pvtOutString,
        pvtOutA3String: pvtOutA3String
      };
    }

    /**
     * Get disposition list.
     * @param selectedDisposition
     * @returns {Array}
     */
    function getDispositionList(selectedDisposition) {
      var dispositionList = angular.copy(getPorticoSdDispositionList());
      if (!selectedDisposition || selectedDisposition === OutcomeType.RESET) {
        delete dispositionList.reset;
        delete dispositionList.add;
        delete dispositionList.replaceManyToOne;
        delete dispositionList.reposition;
      } else if (selectedDisposition && selectedDisposition !== OutcomeType.RESET) {
        dispositionList = {};
        dispositionList.reset = PorticoSDHelperService.getTranslatedOutcomeLabel(OutcomeType.RESET);
        dispositionList[selectedDisposition] = PorticoSDHelperService.getTranslatedOutcomeLabel(selectedDisposition);
      }
      return dispositionList;
    }

    /**
     * Add accessory after asset in pool manager
     * @param levelId level Id
     * @param asset asset
     * @param levelsDetail {{countryId: "", siteId: "", buildingId: "", floorId: ""}}
     */
    function addAccessoryAfterAssetInPoolManager(levelId, asset, levelsDetail) {
      var levelRowNumber,
          assetId = asset._id,
          assetAccessories = getAccessoriesArrayByAssetId(assetId);
      if (assetAccessories.length) {
        levelRowNumber = tabularPool.findIndex(function (elementLevel) {
          return elementLevel.value.assetId === assetId &&
              elementLevel.value.type === PorticoSDConfiguration.ASSET;
        });
        if (levelRowNumber > 0) {
          levelRowNumber++;
          assetAccessories.forEach(function (accessory) {
            var newAccessory = getNewAccessory(accessory, levelId, asset, PorticoSDConfiguration.TREE_LEVEL_ACCESSORY, levelsDetail);
            addInstanceAtPosition(accessory._id, newAccessory, levelRowNumber);
            levelRowNumber++;
          });
        }
      }
    }

    /**
     * Is valid accessory data
     * @param data
     * @returns {boolean}
     */
    function isValidAccessoryData(data) {
      try {
        if (Utilis.isValid(data) && data !== "" && Utilis.toNumber(data) > 0) {
          return true;
        }
        return false;
      } catch (ex) {
        $log.error('Error in accessory data - ', ex);
        return false;
      }
    }

    /**
     * Get accessories array by asset Id
     * @param assetId
     * @returns {Array}
     */
    function getAccessoriesArrayByAssetId(assetId) {
      var index, accessory, isAccessoryValid,
          accessoriesArray = [],
          assetAccessories = DeviceAccessoriesService.getAccessoriesByAssetId(assetId),
          accessoriesLength = assetAccessories.length;

      if (accessoriesLength) {
        for (index = 0; index < accessoriesLength; index++) {
          if (assetAccessories[index]) {
            accessory = angular.copy(assetAccessories[index]);
            isAccessoryValid = isValidAccessoryData(accessory.CMO) || isValidAccessoryData(accessory.FMO);

            if (isAccessoryValid) {
              if (!Utilis.isValid(accessory.accessoryNumber) || accessory.accessoryNumber === "") {
                accessory.accessoryNumber = PorticoSDConfiguration.NOT_APPLICABLE;
              }

              if (!Utilis.isValid(accessory.description) || accessory.description === "") {
                accessory.description = PorticoSDConfiguration.NOT_APPLICABLE;
              }

              if (!Utilis.isValid(accessory.CMO) || accessory.CMO === "") {
                accessory.CMO = PorticoSDConfiguration.ZERO;
              }

              if (!Utilis.isValid(accessory.FMO) || accessory.FMO === "") {
                accessory.FMO = PorticoSDConfiguration.ZERO;
              }
              accessoriesArray.push(accessory);
            }
          }
        }
      }
      return accessoriesArray;
    }

    /**
     * This function binds the pricing columns in tabular view if pricing data is avaialable for the project
     * @param pricingData
     * @param projectId
     */
    function updatePricingData(pricingData, projectId) {
      var levelData = getLevelData(projectId),
          countries = (levelData && levelData.countries) ? levelData.countries : null,
          countryPricingData = PricingService.getCountryPricingData(pricingData),
          asset = {};
      clearPricingData();
      if (countryPricingData && countries) {
        countryPricingData.forEach(function (pricing) {
          countries.forEach(function (country) {
            if (country._id === pricing.countryId) {
              country.dartPricingData = pricing;
              updateLevelInPoolManager(country);
            }
          });
        });
      }
      pricingData.forEach(function (pricing) {
        asset = AssetService.getAsset(pricing.assetId);
        if (asset) {
          asset.dartPricingData = pricing;
          updateAssetInPoolManager(asset);
        }
      });
    }

    /**
     * Performs aggregations[Total/Avg]
     * @param gridApi
     * @param projectId
     */
    function setAggregations(gridApi, projectId) {
      if (gridApi) {
        var levelData = levels,
            level = (levelData) ? levelData.buildings.concat(levelData.sites, levelData.floors) : [],
            filteredRows = (gridApi.grid && gridApi.grid.rows && gridApi.grid.rows.length) ? gridApi.grid.rows : [];
        if (filteredRows.length) {
          filteredRows.forEach(function (row) {
            if (row.entity.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_SITE || row.entity.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_BUILDING || row.entity.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_FLOOR) {
              level.forEach(function (data) {
                if (data._id === row.entity[PorticoSDConfiguration.LEVEL_ID]) {
                  data.dartPricingData = PorticoSDHelperService.getAggregatedPricingData(row);
                  updateLevelInPoolManager(data);
                }
              });
            }
          });
        }
      }
    }

    /**
     * Clear the pricing data for a project before setting updated pricing data to the project
     * @param projectId
     */
    function clearPricingData(projectId) {
      var levelData = getLevelData(projectId),
          tabularData = getTabularViewData(),
          asset,
          level = (levelData && levelData.countries &&
              levelData.floors && levelData.buildings && levelData.sites) ?
              levelData.countries.concat(levelData.buildings, levelData.floors, levelData.sites) : null;
      if (level) {
        level.forEach(function (data) {
          data.dartPricingData = [];
          updateLevelInPoolManager(data);
        });
      }
      if (tabularData) {
        tabularData.forEach(function (data) {
          asset = AssetService.getAsset(data.assetId);
          if (asset) {
            asset.dartPricingData = [];
            updateAssetInPoolManager(asset);
          }
        });
      }
    }

    /**
     * Update tco pricing of level in tabular pool
     * @param levelId
     */
    function updateLevelTcoPricingInTabularPool(levelId) {
      // Update the tco pricing of level with the tco pricing value in "levelPricing".
      var filteredLevels, level, tabularViewLevelRow;
      try {
        if (tabularPool && tabularPool.length) {
          filteredLevels = tabularPool.filter(function (elementLevel) {
            return elementLevel.id === levelId && (elementLevel.value.type === PorticoSDConfiguration.LEVEL ||
                elementLevel.value.type === PorticoSDConfiguration.PROJECT);
          });

          if (filteredLevels.length) {
            level = filteredLevels.first();
            if (level.value) {
              tabularViewLevelRow = level.value;
              TcoPricingHelperService.updateTcoPricingForLevel(tabularViewLevelRow, levelPricing);
              updateInstance(level.id, tabularViewLevelRow);
            }
          }
        }
      } catch (ex) {
        $log.error('Error in updating Tco pricing of Level in Tabular Pool : ', ex);
      }
    }


    /**
     * Update Tco pricing of floors
     * @param tabularData
     */
    function updateFloorsTcoPricing(tabularData) {
      // "tabularData" will contain the assets of active state. We will calculate the floor level tco pricing with the
      // active state assets and update the tco pricing of level in tabular pool as well.
      var floors, assets, floorsLength, assetsLength, tabularRow, activeStateAssets;
      try {
        if (tabularPool && tabularPool.length) {
          activeStateAssets = tabularData.filter(function (elementLevel) {
            return elementLevel.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_ASSET;
          });

          floors = tabularPool.filter(function (elementLevel) {
            return elementLevel.value.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_FLOOR;
          });
          floorsLength = floors.length;
          if (floorsLength && activeStateAssets && activeStateAssets.length) {
            for (var indexFloors = 0; indexFloors < floorsLength; indexFloors++) {
              assets = activeStateAssets.filter(PorticoSDHelperService.getAssetsInFloor(floors[indexFloors].value.floorId));
              assetsLength = assets.length;
              levelPricing = angular.copy(tabularPricingProperties);
              if (assetsLength) {
                for (var indexAssets = 0; indexAssets < assetsLength; indexAssets++) {
                  tabularRow = assets[indexAssets];
                  TcoPricingHelperService.sumTcoPricingOfLevels(tabularRow, levelPricing);
                }
              }
              updateLevelTcoPricingInTabularPool(floors[indexFloors].id);
            }
          }
        }
      } catch (ex) {
        $log.error('Error in updating Tco pricing of floors : ', ex);
      }
    }


    /**
     * Update Tco pricing of building
     */
    function updateBuildingsTcoPricing() {
      // To update the building tco pricing, we will need floor tco pricing. So we will get the floors tco pricing from
      // tabular pool. We will sum the floors tco pricing and update the pricing for building in tabular pool.
      var buildings, floors, buildingsLength, floorsLength, tabularRow;
      try {
        if (tabularPool && tabularPool.length) {
          buildings = tabularPool.filter(function (elementLevel) {
            return elementLevel.value.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_BUILDING;
          });
          buildingsLength = buildings.length;
          if (buildingsLength) {
            for (var indexBuildings = 0; indexBuildings < buildingsLength; indexBuildings++) {
              floors = tabularPool.filter(PorticoSDHelperService.getFloorsInBuilding(buildings[indexBuildings].value.buildingId));
              floorsLength = floors.length;
              if (floorsLength) {
                levelPricing = angular.copy(tabularPricingProperties);
                for (var indexFloors = 0; indexFloors < floorsLength; indexFloors++) {
                  tabularRow = floors[indexFloors].value;
                  TcoPricingHelperService.sumTcoPricingOfLevels(tabularRow, levelPricing);
                }
                updateLevelTcoPricingInTabularPool(buildings[indexBuildings].id);
              }
            }
          }
        }
      } catch (ex) {
        $log.error('Error in updating Tco pricing of buildings : ', ex);
      }
    }


    /**
     * Update Tco pricing of sites
     */
    function updateSitesTcoPricing() {
      // To update the sites tco pricing, we will need buildings tco pricing. So we will get the buildings tco pricing from
      // tabular pool. We will sum the buildings tco pricing and update the pricing for site in tabular pool.
      var sites, buildings, sitesLength, buildingsLength, tabularRow;
      try {
        if (tabularPool && tabularPool.length) {
          sites = tabularPool.filter(function (elementLevel) {
            return elementLevel.value.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_SITE;
          });
          sitesLength = sites.length;
          if (sitesLength) {
            for (var indexSites = 0; indexSites < sitesLength; indexSites++) {
              buildings = tabularPool.filter(PorticoSDHelperService.getBuildingsInSite(sites[indexSites].value.siteId));
              buildingsLength = buildings.length;
              if (buildingsLength) {
                levelPricing = angular.copy(tabularPricingProperties);
                for (var indexBuildings = 0; indexBuildings < buildingsLength; indexBuildings++) {
                  tabularRow = buildings[indexBuildings].value;
                  TcoPricingHelperService.sumTcoPricingOfLevels(tabularRow, levelPricing);
                }
                updateLevelTcoPricingInTabularPool(sites[indexSites].id);
              }
            }
          }
        }
      } catch (ex) {
        $log.error('Error in updating Tco pricing of sites : ', ex);
      }
    }

    /**
     * Update countries Tco pricing
     */
    function updateCountriesTcoPricing() {
      // To update the countries tco pricing, we will need sites tco pricing. So we will get the sites tco pricing from
      // tabular pool. We will sum the sites tco pricing and update the pricing for building in tabular pool.
      var countries, sites, countriesLength, sitesLength, tabularRow;
      try {
        if (tabularPool && tabularPool.length) {
          countries = tabularPool.filter(function (elementLevel) {
            return elementLevel.value.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_COUNTRY;
          });
          countriesLength = countries.length;
          if (countriesLength) {
            for (var indexCountries = 0; indexCountries < countriesLength; indexCountries++) {
              sites = tabularPool.filter(PorticoSDHelperService.getSitesInCountry(countries[indexCountries].value.countryId));
              sitesLength = sites.length;
              if (sitesLength) {
                levelPricing = angular.copy(tabularPricingProperties);
                for (var indexSites = 0; indexSites < sitesLength; indexSites++) {
                  tabularRow = sites[indexSites].value;
                  TcoPricingHelperService.sumTcoPricingOfLevels(tabularRow, levelPricing);
                }
                updateLevelTcoPricingInTabularPool(countries[indexCountries].id);
              }
            }
          }
        }
      } catch (ex) {
        $log.error('Error in updating Tco pricing of countries : ', ex);
      }
    }

    /**
     * Update Tco pricing of project
     */
    function updateProjectTcoPricing() {
      // To update the project tco pricing, we will need countries tco pricing. So we will get the countries tco pricing from
      // tabular pool. We will sum the countries tco pricing and update the pricing for project in tabular pool.
      var projects, project, countries, countriesLength, tabularRow;
      try {
        if (tabularPool && tabularPool.length) {
          projects = tabularPool.filter(function (elementLevel) {
            return elementLevel.value.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_PROJECT;
          });
          if (projects.length) {
            project = projects.first();
            countries = tabularPool.filter(function (elementLevel) {
              return elementLevel.value.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_COUNTRY;
            });
            countriesLength = countries.length;
            if (countriesLength) {
              levelPricing = angular.copy(tabularPricingProperties);
              for (var indexCountries = 0; indexCountries < countriesLength; indexCountries++) {
                tabularRow = countries[indexCountries].value;
                TcoPricingHelperService.sumTcoPricingOfLevels(tabularRow, levelPricing);
              }
              updateLevelTcoPricingInTabularPool(project.id);
            }
          }
        }
      } catch (ex) {
        $log.error('Error in updating Tco pricing of project : ', ex);
      }
    }

    /**
     * Set Tco pricing on levels
     * @param tabularData
     */
    function setTcoPricingOnLevels(tabularData) {
      try {
        if (TcoPricingService.isTcoPricingFetched && tabularPool && tabularPool.length) {
          updateFloorsTcoPricing(tabularData);
          updateBuildingsTcoPricing();
          updateSitesTcoPricing();
          updateCountriesTcoPricing();
          updateProjectTcoPricing();
        }
      } catch (ex) {
        $log.error('Error in setting Tco pricing on levels : ', ex);
      }
    }

    /**
     * Subtract level pricing
     * @param tabularAsset
     * @param levelId
     */
    function subtractLevelPricing(tabularAsset, levelId) {
      // This will subtract the asset pricing from level pricing.
      var filteredLevels, tabularLevel;
      filteredLevels = tabularPool.filter(function (elementLevel) {
        return elementLevel.id === levelId && (elementLevel.value.type === PorticoSDConfiguration.LEVEL ||
            elementLevel.value.type === PorticoSDConfiguration.PROJECT);
      });
      if (filteredLevels.length) {
        tabularLevel = filteredLevels.first();
        if (tabularLevel.value) {
          TcoPricingHelperService.subtractTcoPricingOfLevel(tabularLevel.value, tabularAsset.value);
          updateInstance(tabularLevel.id, tabularLevel.value);
        }
      }
    }

    /**
     * Update zone when zone is assigned to asset
     */

    function updateAssetZoneInTabularView(assetsIds) {
      var asset;
      for (var index = 0; index < assetsIds.length; index++) {
        asset = AssetService.getAsset(assetsIds[index]);
        updateAssetInPoolManager(asset);
      }
    }

    /**
     * Set tco pricing for removed assets.
     * @param assetIds
     */
    function setTcoPricingForRemovedAssets(assetIds) {
      if (TcoPricingService.isTcoPricingFetched()) {
        // If the asset is removed from tabular pool then we need to update the tco pricing on all the levels of asset.
        var filteredAssets, tabularAsset;
        if (assetIds && assetIds.length && tabularPool && tabularPool.length) {
          for (var assetIndex = 0; assetIndex < assetIds.length; assetIndex++) {
            filteredAssets = tabularPool.filter(function (elementLevel) {
              return elementLevel.id === assetIds[assetIndex] && (elementLevel.value.type === PorticoSDConfiguration.ASSET);
            });
            if (filteredAssets.length) {
              tabularAsset = filteredAssets.first();
              if (tabularAsset.value) {
                subtractLevelPricing(tabularAsset, tabularAsset.value.floorId); // Floor
                subtractLevelPricing(tabularAsset, tabularAsset.value.buildingId); // Building
                subtractLevelPricing(tabularAsset, tabularAsset.value.siteId); // Site
                subtractLevelPricing(tabularAsset, tabularAsset.value.countryId); // Country
                subtractLevelPricing(tabularAsset, $routeParams.id); // Project
              }
            }
          }
        }
      }
    }

    /**
     * Update tco pricing on level
     * @param tabularAsset
     * @param levelId
     * @param adjustType
     */
    function updateTcoLevelPricing(tabularAsset, levelId, adjustType) {
      var filteredLevels, tabularLevel;
      filteredLevels = tabularPool.filter(function (elementLevel) {
        return elementLevel.id === levelId && (elementLevel.value.type === PorticoSDConfiguration.LEVEL ||
            elementLevel.value.type === PorticoSDConfiguration.PROJECT);
      });
      if (filteredLevels.length) {
        tabularLevel = filteredLevels.first();
        if (tabularLevel.value) {
          switch (adjustType) {
            case PorticoSDConfiguration.ADJUST_PRICING.ADD :
              TcoPricingHelperService.addAssetTcoPricingToLevel(tabularLevel.value, tabularAsset.value);
              break;
            case PorticoSDConfiguration.ADJUST_PRICING.SUBTRACT :
              TcoPricingHelperService.subtractTcoPricingOfLevel(tabularLevel.value, tabularAsset.value);
              break;
          }
          updateInstance(tabularLevel.id, tabularLevel.value);
        }
      }
    }

    /**
     * Update TCO pricing on levels.
     * @param assetIds
     * @param adjustType
     */
    function updateLevelsTcoPricing(assetIds, adjustType) {
      if (TcoPricingService.isTcoPricingFetched()) {
        // If the asset is removed or added in tabular pool then we need to update the tco pricing on all the levels of asset.
        var filteredAssets, tabularAsset;
        if (assetIds && assetIds.length && tabularPool && tabularPool.length) {
          for (var assetIndex = 0; assetIndex < assetIds.length; assetIndex++) {
            filteredAssets = tabularPool.filter(function (elementLevel) {
              return elementLevel.id === assetIds[assetIndex] && (elementLevel.value.type === PorticoSDConfiguration.ASSET);
            });
            if (filteredAssets.length) {
              tabularAsset = filteredAssets.first();
              if (tabularAsset.value) {
                updateTcoLevelPricing(tabularAsset, tabularAsset.value.floorId, adjustType); // Floor
                updateTcoLevelPricing(tabularAsset, tabularAsset.value.buildingId, adjustType); // Building
                updateTcoLevelPricing(tabularAsset, tabularAsset.value.siteId, adjustType); // Site
                updateTcoLevelPricing(tabularAsset, tabularAsset.value.countryId, adjustType); // Country
                updateTcoLevelPricing(tabularAsset, $routeParams.id, adjustType); // Project
              }
            }
          }
        }
      }
    }

    /**
     * This function will give the sku matched plotted assets
     * @param sku
     * @returns {Array}
     */
    function getSkuMatchedAssets(sku) {
      var skuMatchedAssets = [];
      if (sku && tabularPool && tabularPool.length) {
        skuMatchedAssets = tabularPool.filter(function (row) {
          return (row.value.sku === sku && row.value.type === PorticoSDConfiguration.ASSET);
        });
      }
      return skuMatchedAssets;
    }

    /**
     * Get asset info from tabular pool.
     * @returns {Array}
     */
    function getAssetFromTabularPool() {
      var selectedAsset = SharedService.selectedAsset(),
          matchedAssets = [];
      if (selectedAsset && tabularPool && tabularPool.length) {
        matchedAssets = tabularPool.filter(function (row) {
          return (row.id === selectedAsset._id && row.value.type === PorticoSDConfiguration.ASSET);
        });
      }
      return matchedAssets;
    }

    /**
     * This method will check whether fleet has multi vendor asset or not.
     * @returns {boolean}
     */
    function hasMultiVendorAsset() {
      var filteredAsset = [];
      if (tabularPool && tabularPool.length) {
        filteredAsset = tabularPool.filter(function (element) {
          return element.value.type === PorticoSDConfiguration.ASSET &&
              element.value.isMultiVendorAsset === true;
        });
      }
      return !!filteredAsset.length;
    }

    /**
     * Update assetAbbreviations in tabular pool
     */
    function updateAssetAbbreviationInPoolManager(isAssetAbbreviation) {
      var tabularViewAssetRow, tabularPoolLength, asset,
          treeLevelAsset = PorticoSDConfiguration.TREE_LEVEL_ASSET;
      try {
        if (tabularPool && tabularPool.length) {
          tabularPoolLength = tabularPool.length;
          for (var index = 0; index < tabularPoolLength; index++) {
            if (tabularPool[index].value && tabularPool[index].value.$$treeLevel === treeLevelAsset) {
              tabularViewAssetRow = tabularPool[index].value;
              if (isAssetAbbreviation) {
                asset = AssetService.getAsset(tabularPool[index].id);
                tabularViewAssetRow.assetAbbreviation = asset ? asset.assetAbbreviation : "";
              } else {
                tabularViewAssetRow.assetAbbreviation = "";
              }
              updateInstance(tabularPool[index].id, tabularViewAssetRow);
            }
          }
        }
      } catch (ex) {
        $log.error('Error in updating the asset abbreviation in tabular pool : ', ex);
      }
    }

    /**
     * Update Mv cost status in tabular pool
     */
    function updateMvCostStatusInPoolManager() {
      var tabularViewAssetRow, tabularPoolLength,
          treeLevelAsset = PorticoSDConfiguration.TREE_LEVEL_ASSET;
      try {
        if (tabularPool && tabularPool.length) {
          tabularPoolLength = tabularPool.length;
          for (var index = 0; index < tabularPoolLength; index++) {
            if (tabularPool[index].value && tabularPool[index].value.$$treeLevel === treeLevelAsset) {
              tabularViewAssetRow = tabularPool[index].value;
              tabularViewAssetRow.mvSupplyCostStatus = MultiVendorService.updateMvCostStatus(tabularViewAssetRow.productId, tabularViewAssetRow.mvSupply);
              tabularViewAssetRow.mvServiceCostStatus = MultiVendorService.updateMvCostStatus(tabularViewAssetRow.productId, tabularViewAssetRow.mvService);
              updateInstance(tabularPool[index].id, tabularViewAssetRow);
            }
          }
        }
      } catch (ex) {
        $log.error('Error in updating the mv cost in tabular pool : ', ex);
      }
    }

    return {
      getLevelData: getLevelData,
      loadTabularData: loadTabularData,
      getTabularViewData: getTabularViewData,
      getTabularViewAssetsForFloors: getTabularViewAssetsForFloors,
      addAssetInPoolManager: addAssetInPoolManager,
      addAssetAfterAssetInPoolManager: addAssetAfterAssetInPoolManager,
      addVirtualAssetInPoolManager: addVirtualAssetInPoolManager,
      updateAssetInPoolManager: updateAssetInPoolManager,
      updateAssetsInPoolManager: updateAssetsInPoolManager,
      updateAssetsTcoPricingInPoolManager: updateAssetsTcoPricingInPoolManager,
      updateLevelInPoolManager: updateLevelInPoolManager,
      removeAssetInPoolManager: removeAssetInPoolManager,
      getPvtInByAssetId: getPvtInByAssetId,
      getPvtOutByAssetId: getPvtOutByAssetId,
      updatePricingData: updatePricingData,
      setAggregations: setAggregations,
      clearPricingData: clearPricingData,
      removeTabularData: removeTabularData,
      deleteLevelFromTabularPool: deleteLevelFromTabularPool,
      updateAssetZoneInTabularView: updateAssetZoneInTabularView,
      setTcoPricingOnLevels: setTcoPricingOnLevels,
      setTcoPricingForRemovedAssets: setTcoPricingForRemovedAssets,
      updateLevelsTcoPricing: updateLevelsTcoPricing,
      getSkuMatchedAssets: getSkuMatchedAssets,
      getAssetFromTabularPool: getAssetFromTabularPool,
      getDispositionList: getDispositionList,
      hasMultiVendorAsset: hasMultiVendorAsset,
      updateMvCostStatusInPoolManager: updateMvCostStatusInPoolManager,
      updateAssetAbbreviationInPoolManager: updateAssetAbbreviationInPoolManager
    };
  }

  var app = angular.module('app'),
      requires = [
        '$log',
        '$routeParams',
        '$rootScope',
        '$filter',
        '$q',
        'translateFilter',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'ngCartosCanvas.constants.OutcomeType',
        'ngCartosCore.constants.LevelType',
        'ngCartosCore.services.LevelService',
        'ngCartosCore.services.AssetMapService',
        'ngCartosUtils.services.UtilityService',
        'ngPorticoSD.services.PorticoSDPoolManager',
        'ngPorticoSD.services.PorticoSDHelperService',
        'TransitionHelperService',
        'ngCartosCore.services.OutcomeService',
        'ngCartosCore.services.AssetService',
        'ngCartosCore.services.LevelEntityMapService',
        'ngCartosCore.services.PageVolumeService',
        'ngCartosCore.services.DeviceAccessoriesService',
        'SharedService',
        'PricingService',
        'PricingHelperService',
        'ngCartosCore.services.ZoneService',
        'TcoPricingService',
        'TcoPricingHelperService',
        'ngPorticoSD.services.MultiVendorService',
        'PorticoSDSharedService',
        'PorticoSdUtilityService',
        'RGSharedLocaleService',
        PorticoSDService
      ];
  app.factory('ngPorticoSD.services.PorticoSDService', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /**
   *  PorticoSD shared service
   * @param $log
   * @constructor
   */
  function PorticoSDSharedService($log) {
    var tcoAdjustmentValues = null,
        mvTemplates = null,
        mvCountryDetails = {},
        pageVolume = {
          sourceAsset: "",
          destinationAsset: ""
        },
        isTcoPricingFetched = {
          current: false,
          future: false
        },
        featureFlag = {
          multiVendor: false
        },
        tabularTemplates;

    /**
     * Set the tco pricing fetched for the state true.
     * @param state
     * @param isFetched
     */
    this.setTcoPricingFetched = function (state, isFetched) {
      isTcoPricingFetched[state] = isFetched;
    };

    /**
     * Get the tco pricing fetched for the states
     * @returns {{current: boolean, future: boolean}}
     */
    this.getTcoPricingFetched = function () {
      return isTcoPricingFetched;
    };


    /**
     * to set the source and destination assets for PVT if asset is null then set source and destination as blank
     * @param asset
     */
    this.setPageVolumeTransferor = function (asset) {
      if (!asset) {
        pageVolume.sourceAsset = "";
        pageVolume.destinationAsset = "";
      } else if (asset && pageVolume.sourceAsset) {
        pageVolume.destinationAsset = asset;
      } else {
        pageVolume.sourceAsset = asset;
      }
    };

    /**
     * To get the source and destination assets for page volume transfer
     * @returns {{sourceAsset: string, destinationAsset: string}} return and object of source and destination assets
     */
    this.getPageVolumeTransferor = function () {
      return pageVolume;
    };

    /**
     * Set mv details for country
     * @param countryId
     * @param mvDetails
     */
    this.setMvDetailsForCountry = function (countryId, mvDetails) {
      if (countryId && mvDetails) {
        mvCountryDetails[countryId] = mvDetails;
      }
    };

    /**
     * Get mv details for country
     * @param countryId
     * @returns {*}
     */
    this.getMvDetailsForCountry = function (countryId) {
      var mvDetails = {supply: [], service: []};
      if (mvCountryDetails) {
        return mvCountryDetails[countryId] || mvDetails;
      }
      return mvDetails;
    };


    /**
     * Get mv details for all countries
     */
    this.getMvDetailsForAllCountry = function () {
      return mvCountryDetails;
    };

    /**
     * Private function to have setter getter to the
     * variable.
     * @param variable
     * @returns {Function}
     */
    function setterGetter(variable) {
      return function (value) {
        variable = arguments.length > 0 ? value : variable;
        return variable;
      };
    }

    this.tcoAdjustmentValues = setterGetter(tcoAdjustmentValues);
    this.mvTemplates = setterGetter(mvTemplates);

    this.setFeatureFlagByKey = function (key, value) {
      if (featureFlag && key) {
        featureFlag[key] = value;
      }
    };

    this.getFeatureFlagByKey = function (key) {
      if (featureFlag && key) {
        return featureFlag.hasOwnProperty(key) ? featureFlag[key] : true;
      }
      return true;
    };

    /**
     * save the tabular templates
     * @param templateData
     * @returns {*}
     */
    this.setTabularTemplates = function (templateData) {
      tabularTemplates = templateData;
    };

    /**
     * update the tabular templates
     * @param templateData
     * @returns {*}
     */
    this.updateTemplate = function (templateData) {
      if (templateData && templateData.templateName) {
        tabularTemplates[templateData.templateName] = templateData.templateData;
      }
    };

    /**
     * get the tabular templates
     * @param templateName
     * @returns {*}
     */
    this.getTemplateData = function (templateName) {
      if (tabularTemplates[templateName]) {
        return angular.copy(tabularTemplates[templateName]);
      }
    };

    /**
     * save the tabular templates
     * @returns {*}
     */
    this.getTemplates = function () {
      return angular.copy(tabularTemplates);
    };

    /**
     * remove the tabular template from tabularTemplates
     * @param templateName
     * @returns {*}
     */
    this.removeTemplate = function (templateName) {
      delete tabularTemplates[templateName];
    };
  }

  var app = angular.module('app'),
      requires = [
        '$log',
        PorticoSDSharedService];
  app.service('PorticoSDSharedService', requires);
}());
/*global angular*/
(function () {
  'use strict';

  /**
   * Contains utility function which will be used throughout application.
   */
  function PorticoSDUtilityService() {

    var ZERO = 0,
        FIVE = 5;

    /**
     * This function is to check given value is number or not
     * @param value
     * @return boolean true if value is string otherwise else false
     **/
    function isNumber(value) {
      var reg = /\d/g;
      return reg.test(value);
    }

    /**
     * Check number is valid or not
     * If number is valid then return number else return 0
     * @param value
     * @returns {number}
     */
    function isValidNumber(value) {
      return isNumber(value) ? value : ZERO;
    }


    /**
     * Converts the value with the precision. Here we are converting return result into parseFloat,Because
     * we need result with the precision set. like if result is 85 then it will return 85.00
     * @param   {Number} value         value to be converted
     * @param   {Number} [precision=0] number of decimal places
     * @returns {string}
     */
    function toFloat(value, precision) {
      return parseFloat(value).toFixed(precision);
    }

    /**
     * Sum values
     * @param value1
     * @param value2
     * @returns {string}
     */
    function sumValues(value1, value2) {
      return toFloat((parseFloat(isValidNumber(value1)) + parseFloat(isValidNumber(value2))), FIVE);
    }

    /**
     * Subtract values
     * @param value1
     * @param value2
     * @returns {string}
     */
    function subtractValues(value1, value2) {
      return toFloat((parseFloat(isValidNumber(value1)) - parseFloat(isValidNumber(value2))), FIVE);
    }

    /**
     * Divide values
     * @param value1
     * @param value2
     * @return {number}
     */
    function divideValues(value1, value2) {
      value2 = Math.floor(isValidNumber(value2));
      if (!value2) {
        return 0;
      }
      return Math.floor(Math.floor(isValidNumber(value1)) / value2);
    }

    /**
     * Multiply values
     * @param value1
     * @param value2
     * @return {number}
     */
    function multiplyValues(value1, value2) {
      return Math.floor(Math.floor(isValidNumber(value1)) * Math.floor(isValidNumber(value2)));
    }

    /**
     * Format for Short date (01/05/2019)
     * @param ISODate
     * @return {string}
     */
    function formatDate(ISOdate){
      let dateOutput;
      if(ISOdate) {
        let UTCDate = new Date(ISOdate);
        let year = UTCDate.getUTCFullYear();
        let month = UTCDate.getUTCMonth();
        let day = UTCDate.getUTCDate();

        if (day < 10) {
          day = '0' + day;
        }
        if (month < 10) {
          month = '0' + month;
        }
        dateOutput = month + '/' + day + '/' + year;
      }
      return dateOutput;
    }

    return {
      isNumber: isNumber,
      isValidNumber: isValidNumber,
      toFloat: toFloat,
      sumValues: sumValues,
      subtractValues: subtractValues,
      divideValues: divideValues,
      multiplyValues: multiplyValues,
      formatDate: formatDate
    };
  }

  var app = angular.module('app');
  app.factory('PorticoSdUtilityService', PorticoSDUtilityService);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  function PrescriptiveService($routeParams, $q, LevelEntityMapService, OutcomeService, LevelService, ApiService,
                               AssetService, EntityType, Configuration) {

    /**
     * Imports the asset as csv file.
     * @param req {FormData}
     * @param queryParams {Object}
     * @returns {*}
     */
    function importFutureAssets(req, queryParams) {

      var deferred = $q.defer();

      function onImportSuccess(response) {
        // Load Asset Level and loadLevelEntityMap to update pools (AssetPool & LevelEntityMapPool and levels)
        $q.all([
          AssetService.getVirtualAssets($routeParams.id, Configuration.TRANSITION_NAME),
          OutcomeService.loadOutcomes($routeParams.id, Configuration.TRANSITION_NAME),
          LevelService.loadLevel($routeParams.id, true),
          AssetService.loadAssets($routeParams.id, true),
          LevelEntityMapService.loadLevelEntityMap($routeParams.id, EntityType.ASSET, true)
        ]).then(function () {
          deferred.resolve(response.data);
        }, onError);
      }

      function onError(error) {
        deferred.reject(error);
      }

      ApiService.importFutureFleet(req, queryParams).then(onImportSuccess, onError);
      return deferred.promise;
    }

    return {
      importFutureAssets: importFutureAssets
    };
  }

  var app = angular.module('app'),
      requries = [
        '$routeParams',
        '$q',
        'ngCartosCore.services.LevelEntityMapService',
        'ngCartosCore.services.OutcomeService',
        'ngCartosCore.services.LevelService',
        'ngCartosCore.services.ApiService',
        'ngCartosCore.services.AssetService',
        'ngCartosCore.constants.EntityType',
        'Configuration',
        PrescriptiveService
      ];
  app.factory('PrescriptiveService', requries);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  function PricingService($log, $filter, $rootScope, UtilityService, PorticoSDConfiguration, PorticoSdUtilityService,
                          TcoPricingService, RGSharedLocaleService) {
    var locale = null;

    /**
     * To Convert local price to USD using currency rate
     * @param localPrice
     * @param currencyRate
     */
    function convertToUsd(localPrice, currencyRate) {
      try {
        var usdPrice = 0;
        localPrice = UtilityService.toNumberWithZero(localPrice);
        currencyRate = UtilityService.toNumberWithZero(currencyRate);
        if (currencyRate !== PorticoSDConfiguration.ZERO) {
          usdPrice = localPrice / currencyRate;
        }
        if (!isNaN(usdPrice)) {
          return usdPrice;
        }
        return 0;
      }
      catch (ex) {
        $log.error('Error while converting Pricing to USD', ex);
        return 0;
      }
    }

    /**
     * This function sets the binding for pricing related columns in tabular view
     * @param tabularRow
     * @param pricingData
     */
    function setPricingData(tabularRow, pricingData) {
      try {
        tabularRow.totalRevenueUsd = pricingData.totalRevenueUsd ? pricingData.totalRevenueUsd : (pricingData.totalRevenue && pricingData.currencyRate && PorticoSdUtilityService.isNumber(pricingData.totalRevenue)) ? UtilityService.truncateDecimalValue(convertToUsd(pricingData.totalRevenue, pricingData.currencyRate)) : '';
        tabularRow.totalCPPUsd = pricingData.totalCPPUsd ? pricingData.totalCPPUsd : (pricingData.totalCPP && pricingData.currencyRate && PorticoSdUtilityService.isNumber(pricingData.totalCPP)) ? UtilityService.toFloat(convertToUsd(pricingData.totalCPP, pricingData.currencyRate), PorticoSDConfiguration.NUMBER_FIVE) : '';
        tabularRow.monoCPPUsd = pricingData.monoCPPUsd ? pricingData.monoCPPUsd : (pricingData.monoCPP && pricingData.currencyRate && PorticoSdUtilityService.isNumber(pricingData.monoCPP)) ? UtilityService.toFloat(convertToUsd(pricingData.monoCPP, pricingData.currencyRate), PorticoSDConfiguration.NUMBER_FIVE) : '';
        tabularRow.colorCPPUsd = pricingData.colorCPPUsd ? pricingData.colorCPPUsd : (pricingData.colorCPP && pricingData.currencyRate && PorticoSdUtilityService.isNumber(pricingData.colorCPP)) ? UtilityService.toFloat(convertToUsd(pricingData.colorCPP, pricingData.currencyRate), PorticoSDConfiguration.NUMBER_FIVE) : '';
        tabularRow.baseFeeUsd = pricingData.baseFeeUsd ? pricingData.baseFeeUsd : (pricingData.baseFee && pricingData.currencyRate && PorticoSdUtilityService.isNumber(pricingData.baseFee)) ? UtilityService.toFloat(convertToUsd(pricingData.baseFee, pricingData.currencyRate), PorticoSDConfiguration.NUMBER_TWO) : '';
        tabularRow.monoClickUsd = pricingData.monoClickUsd ? pricingData.monoClickUsd : (pricingData.monoClick && pricingData.currencyRate && PorticoSdUtilityService.isNumber(pricingData.monoClick)) ? UtilityService.toFloat(convertToUsd(pricingData.monoClick, pricingData.currencyRate), PorticoSDConfiguration.NUMBER_FIVE) : '';
        tabularRow.colorClickUsd = pricingData.colorClickUsd ? pricingData.colorClickUsd : (pricingData.colorClick && pricingData.currencyRate && PorticoSdUtilityService.isNumber(pricingData.colorClick)) ? UtilityService.toFloat(convertToUsd(pricingData.colorClick, pricingData.currencyRate), PorticoSDConfiguration.NUMBER_FIVE) : '';
        tabularRow.bestColorClickUsd = pricingData.bestColorClickUsd ? pricingData.bestColorClickUsd : (pricingData.bestColorClick && pricingData.currencyRate && PorticoSdUtilityService.isNumber(pricingData.bestColorClick)) ? UtilityService.toFloat(convertToUsd(pricingData.bestColorClick, pricingData.currencyRate), PorticoSDConfiguration.NUMBER_FIVE) : '';
        tabularRow.netPriceUsd = pricingData.netPriceUsd ? pricingData.netPriceUsd : (pricingData.netPrice && pricingData.currencyRate && PorticoSdUtilityService.isNumber(pricingData.netPrice)) ? UtilityService.toFloat(convertToUsd(pricingData.netPrice, pricingData.currencyRate), PorticoSDConfiguration.NUMBER_FIVE) : '';
        tabularRow.upfrontChargeUsd = pricingData.upfrontChargeUsd ? pricingData.upfrontChargeUsd : (pricingData.upfrontCharge && pricingData.currencyRate && PorticoSdUtilityService.isNumber(pricingData.upfrontCharge)) ? UtilityService.toFloat(convertToUsd(pricingData.upfrontCharge, pricingData.currencyRate), PorticoSDConfiguration.NUMBER_TWO) : '';
        tabularRow.totalRevenueLc = pricingData.totalRevenueLc ? pricingData.totalRevenueLc : (pricingData.totalRevenue && PorticoSdUtilityService.isNumber(pricingData.totalRevenue)) ? UtilityService.truncateDecimalValue(pricingData.totalRevenue) : '';
        tabularRow.totalCPPLc = pricingData.totalCPPLc ? pricingData.totalCPPLc : (pricingData.totalCPP && PorticoSdUtilityService.isNumber(pricingData.totalCPP)) ? UtilityService.toFloat(pricingData.totalCPP, PorticoSDConfiguration.NUMBER_FIVE) : '';
        tabularRow.monoCPPLc = pricingData.monoCPPLc ? pricingData.monoCPPLc : (pricingData.monoCPP && PorticoSdUtilityService.isNumber(pricingData.monoCPP)) ? UtilityService.toFloat(pricingData.monoCPP, PorticoSDConfiguration.NUMBER_FIVE) : '';
        tabularRow.colorCPPLc = pricingData.colorCPPLc ? pricingData.colorCPPLc : (pricingData.colorCPP && PorticoSdUtilityService.isNumber(pricingData.colorCPP)) ? UtilityService.toFloat(pricingData.colorCPP, PorticoSDConfiguration.NUMBER_FIVE) : '';
        tabularRow.baseFeeLc = pricingData.baseFeeLc ? pricingData.baseFeeLc : (pricingData.baseFee && PorticoSdUtilityService.isNumber(pricingData.baseFee)) ? UtilityService.toFloat(pricingData.baseFee, PorticoSDConfiguration.NUMBER_TWO) : '';
        tabularRow.monoClickLc = pricingData.monoClickLc ? pricingData.monoClickLc : (pricingData.monoClick && PorticoSdUtilityService.isNumber(pricingData.monoClick)) ? UtilityService.toFloat(pricingData.monoClick, PorticoSDConfiguration.NUMBER_FIVE) : '';
        tabularRow.colorClickLc = pricingData.colorClickLc ? pricingData.colorClickLc : (pricingData.colorClick && PorticoSdUtilityService.isNumber(pricingData.colorClick)) ? UtilityService.toFloat(pricingData.colorClick, PorticoSDConfiguration.NUMBER_FIVE) : '';
        tabularRow.bestColorClickLc = pricingData.bestColorClickLc ? pricingData.bestColorClickLc : (pricingData.bestColorClick && PorticoSdUtilityService.isNumber(pricingData.bestColorClick)) ? UtilityService.toFloat(pricingData.bestColorClick, PorticoSDConfiguration.NUMBER_FIVE) : '';
        tabularRow.netPriceLc = pricingData.netPriceLc ? pricingData.netPriceLc : (pricingData.netPrice && PorticoSdUtilityService.isNumber(pricingData.netPrice)) ? UtilityService.toFloat(pricingData.netPrice, PorticoSDConfiguration.NUMBER_FIVE) : '';
        tabularRow.upfrontChargeLc = pricingData.upfrontChargeLc ? pricingData.upfrontChargeLc : (pricingData.upfrontCharge && PorticoSdUtilityService.isNumber(pricingData.upfrontCharge)) ? UtilityService.toFloat(pricingData.upfrontCharge, PorticoSDConfiguration.NUMBER_TWO) : '';

        if ($rootScope.token.featureFlag.regionSettings) {
          locale = RGSharedLocaleService.getSelectedColumnLocale().region;
          tabularRow.totalRevenueUsd = $filter('rgTabColNum')(tabularRow.totalRevenueUsd, locale);
          tabularRow.totalCPPUsd = $filter('rgTabColNum')(tabularRow.totalCPPUsd, locale);
          tabularRow.monoCPPUsd = $filter('rgTabColNum')(tabularRow.monoCPPUsd, locale);
          tabularRow.colorCPPUsd = $filter('rgTabColNum')(tabularRow.colorCPPUsd, locale);
          tabularRow.baseFeeUsd = $filter('rgTabColNum')(tabularRow.baseFeeUsd, locale);
          tabularRow.monoClickUsd = $filter('rgTabColNum')(tabularRow.monoClickUsd, locale);
          tabularRow.colorClickUsd = $filter('rgTabColNum')(tabularRow.colorClickUsd, locale);
          tabularRow.bestColorClickUsd = $filter('rgTabColNum')(tabularRow.bestColorClickUsd, locale);
          tabularRow.netPriceUsd = $filter('rgTabColNum')(tabularRow.netPriceUsd, locale);
          tabularRow.upfrontChargeUsd = $filter('rgTabColNum')(tabularRow.upfrontChargeUsd, locale);
          tabularRow.totalRevenueLc = $filter('rgTabColNum')(tabularRow.totalRevenueLc, locale);
          tabularRow.totalCPPLc = $filter('rgTabColNum')(tabularRow.totalCPPLc, locale);
          tabularRow.monoCPPLc = $filter('rgTabColNum')(tabularRow.monoCPPLc, locale);
          tabularRow.colorCPPLc = $filter('rgTabColNum')(tabularRow.colorCPPLc, locale);
          tabularRow.baseFeeLc = $filter('rgTabColNum')(tabularRow.baseFeeLc, locale);
          tabularRow.monoClickLc = $filter('rgTabColNum')(tabularRow.monoClickLc, locale);
          tabularRow.colorClickLc = $filter('rgTabColNum')(tabularRow.colorClickLc, locale);
          tabularRow.bestColorClickLc = $filter('rgTabColNum')(tabularRow.bestColorClickLc, locale);
          tabularRow.netPriceLc = $filter('rgTabColNum')(tabularRow.netPriceLc, locale);
          tabularRow.upfrontChargeLc = $filter('rgTabColNum')(tabularRow.upfrontChargeLc, locale);
        }
      }
      catch (ex) {
        $log.error('Error while binding dart pricing data', ex);
        return 0;
      }
    }

    /**
     * This function filters the countryPricing data from entire pricing data
     * @param pricingData
     */
    function getCountryPricingData(pricingData) {
      var countryPricingData = [];
      if (pricingData) {
        countryPricingData = pricingData.filter(function (data) {
          return data.countryId;
        });
      }
      return countryPricingData;
    }

    /**
     * Set toner pricing
     * @param tabularRow
     * @param toner
     */
    function setTonerPricing(tabularRow, toner) {
      tabularRow.totalCppToner = toner.totalCPP;
      tabularRow.monoCppToner = toner.monoCPP;
      tabularRow.colorCppToner = toner.colorCPP;
      tabularRow.totalTcoToner = toner.totalTCO;
      tabularRow.monoTcoToner = toner.monoTCO;
      tabularRow.colorTcoToner = toner.colorTCO;

      if ($rootScope.token.featureFlag.regionSettings) {
        locale = RGSharedLocaleService.getSelectedColumnLocale().region;
        tabularRow.totalCppToner = $filter('rgTabColNum')(tabularRow.totalCppToner, locale);
        tabularRow.monoCppToner = $filter('rgTabColNum')(tabularRow.monoCppToner, locale);
        tabularRow.colorCppToner = $filter('rgTabColNum')(tabularRow.colorCppToner, locale);
        tabularRow.totalTcoToner = $filter('rgTabColNum')(tabularRow.totalTcoToner, locale);
        tabularRow.monoTcoToner = $filter('rgTabColNum')(tabularRow.monoTcoToner, locale);
        tabularRow.colorTcoToner = $filter('rgTabColNum')(tabularRow.colorTcoToner, locale);
      }
    }

    /**
     * Set replaceableParts pricing
     * @param tabularRow
     * @param replaceableParts
     */
    function setReplaceablePartsPricing(tabularRow, replaceableParts) {
      tabularRow.laborRateRP = replaceableParts.laborRate;
      tabularRow.laborTravelHoursRP = replaceableParts.laborTravelHours;
      tabularRow.laborRepairHoursRP = replaceableParts.laborRepairHours;
      tabularRow.totalCppRP = replaceableParts.totalCPP;
      tabularRow.monoCppRP = replaceableParts.monoCPP;
      tabularRow.colorCppRP = replaceableParts.colorCPP;
      tabularRow.totalTcoRP = replaceableParts.totalTCO;
      tabularRow.monoTcoRP = replaceableParts.monoTCO;
      tabularRow.colorTcoRP = replaceableParts.colorTCO;


      if ($rootScope.token.featureFlag.regionSettings) {
        locale = RGSharedLocaleService.getSelectedColumnLocale().region;
        tabularRow.laborRateRP = $filter('rgTabColNum')(tabularRow.laborRateRP, locale);
        tabularRow.laborTravelHoursRP = $filter('rgTabColNum')(tabularRow.laborTravelHoursRP, locale);
        tabularRow.laborRepairHoursRP = $filter('rgTabColNum')(tabularRow.laborRepairHoursRP, locale);
        tabularRow.totalCppRP = $filter('rgTabColNum')(tabularRow.totalCppRP, locale);
        tabularRow.monoCppRP = $filter('rgTabColNum')(tabularRow.monoCppRP, locale);
        tabularRow.colorCppRP = $filter('rgTabColNum')(tabularRow.colorCppRP, locale);
      }
    }

    /**
     * Set service pricing
     * @param tabularRow
     * @param service
     */
    function setServicePricing(tabularRow, service) {
      tabularRow.avgPwcService = service.avgPostWarrantyCost;
      tabularRow.totalCppService = service.totalCPP;
      tabularRow.monoCppService = service.monoCPP;
      tabularRow.colorCppService = service.colorCPP;
      tabularRow.totalTcoService = service.totalTCO;
      tabularRow.monoTcoService = service.monoTCO;
      tabularRow.colorTcoService = service.colorTCO;

      if ($rootScope.token.featureFlag.regionSettings) {
        locale = RGSharedLocaleService.getSelectedColumnLocale().region;
        tabularRow.avgPwcService = $filter('rgTabColNum')(tabularRow.avgPwcService, locale);
        tabularRow.totalCppService = $filter('rgTabColNum')(tabularRow.totalCppService, locale);
        tabularRow.monoCppService = $filter('rgTabColNum')(tabularRow.monoCppService, locale);
        tabularRow.colorCppService = $filter('rgTabColNum')(tabularRow.colorCppService, locale);
        tabularRow.totalTcoService = $filter('rgTabColNum')(tabularRow.totalTcoService, locale);
        tabularRow.monoTcoService = $filter('rgTabColNum')(tabularRow.monoTcoService, locale);
        tabularRow.colorTcoService = $filter('rgTabColNum')(tabularRow.colorTcoService, locale);
      }
    }

    /**
     * Set power pricing
     * @param tabularRow
     * @param power
     */
    function setPowerPricing(tabularRow, power) {
      tabularRow.avgPowPricePower = power.avgPowerPrice;
      tabularRow.workHoursPerDayPower = power.workHoursPerDay;
      tabularRow.workDaysPerWeekPower = power.workDaysPerWeek;
      tabularRow.weeksPerYearPower = power.weeksPerYear;
      tabularRow.isOnNightAndWeekends = power.isOnNightsAndWeekends;
      tabularRow.powerRateActive = power.powerRateActive;
      tabularRow.powerRateIdle = power.powerRateIdle;
      tabularRow.powerRateSave = power.powerRateSave;
      tabularRow.totalCppPower = power.totalCPP;
      tabularRow.monoCppPower = power.monoCPP;
      tabularRow.colorCppPower = power.colorCPP;
      tabularRow.totalTcoPower = power.totalTCO;
      tabularRow.monoTcoPower = power.monoTCO;
      tabularRow.colorTcoPower = power.colorTCO;

      if ($rootScope.token.featureFlag.regionSettings) {
        locale = RGSharedLocaleService.getSelectedColumnLocale().region;
        tabularRow.avgPowPricePower = $filter('rgTabColNum')(tabularRow.avgPowPricePower, locale);
        tabularRow.workHoursPerDayPower = $filter('rgTabColNum')(tabularRow.workHoursPerDayPower, locale);
        tabularRow.workDaysPerWeekPower = $filter('rgTabColNum')(tabularRow.workDaysPerWeekPower, locale);
        tabularRow.weeksPerYearPower = $filter('rgTabColNum')(tabularRow.weeksPerYearPower, locale);
        tabularRow.isOnNightAndWeekends = $filter('rgTabColNum')(tabularRow.isOnNightAndWeekends, locale);
        tabularRow.powerRateActive = $filter('rgTabColNum')(tabularRow.powerRateActive, locale);
        tabularRow.powerRateIdle = $filter('rgTabColNum')(tabularRow.powerRateIdle, locale);
        tabularRow.powerRateSave = $filter('rgTabColNum')(tabularRow.powerRateSave, locale);
        tabularRow.totalCppPower = $filter('rgTabColNum')(tabularRow.totalCppPower, locale);
        tabularRow.monoCppPower = $filter('rgTabColNum')(tabularRow.monoCppPower, locale);
        tabularRow.colorCppPower = $filter('rgTabColNum')(tabularRow.colorCppPower, locale);
        tabularRow.totalTcoPower = $filter('rgTabColNum')(tabularRow.totalTcoPower, locale);
        tabularRow.monoTcoPower = $filter('rgTabColNum')(tabularRow.monoTcoPower, locale);
        tabularRow.colorTcoPower = $filter('rgTabColNum')(tabularRow.colorTcoPower, locale);
      }
    }

    /**
     * Set paper pricing
     * @param tabularRow
     * @param paper
     */
    function setPaperPricing(tabularRow, paper) {
      tabularRow.paperCostPerSheetPaper = paper.paperCostPerSheet;
      tabularRow.duplexPercentagePaper = paper.duplexPercentage;
      tabularRow.isDuplexCapablePaper = paper.isDuplexCapable;
      tabularRow.totalCppPaper = paper.totalCPP;
      tabularRow.monoCppPaper = paper.monoCPP;
      tabularRow.colorCppPaper = paper.colorCPP;
      tabularRow.totalTcoPaper = paper.totalTCO;
      tabularRow.monoTcoPaper = paper.monoTCO;
      tabularRow.colorTcoPaper = paper.colorTCO;

      if ($rootScope.token.featureFlag.regionSettings) {
        locale = RGSharedLocaleService.getSelectedColumnLocale().region;
        tabularRow.paperCostPerSheetPaper = $filter('rgTabColNum')(tabularRow.paperCostPerSheetPaper, locale);
        tabularRow.duplexPercentagePaper = $filter('rgTabColNum')(tabularRow.duplexPercentagePaper, locale);
        tabularRow.isDuplexCapablePaper = $filter('rgTabColNum')(tabularRow.isDuplexCapablePaper, locale);
        tabularRow.totalCppPaper = $filter('rgTabColNum')(tabularRow.totalCppPaper, locale);
        tabularRow.monoCppPaper = $filter('rgTabColNum')(tabularRow.monoCppPaper, locale);
        tabularRow.colorCppPaper = $filter('rgTabColNum')(tabularRow.colorCppPaper, locale);
        tabularRow.totalTcoPaper = $filter('rgTabColNum')(tabularRow.totalTcoPaper, locale);
        tabularRow.monoTcoPaper = $filter('rgTabColNum')(tabularRow.monoTcoPaper, locale);
        tabularRow.colorTcoPaper = $filter('rgTabColNum')(tabularRow.colorTcoPaper, locale);
      }
    }

    /**
     * Set hardware pricing
     * @param tabularRow
     * @param hardware
     */
    function setHardwarePricing(tabularRow, hardware) {
      tabularRow.deviceCostHardware = hardware.deviceCost;
      tabularRow.totalCppHardware = hardware.totalCPP;
      tabularRow.monoCppHardware = hardware.monoCPP;
      tabularRow.colorCppHardware = hardware.colorCPP;
      tabularRow.totalTcoHardware = hardware.totalTCO;
      tabularRow.monoTcoHardware = hardware.monoTCO;
      tabularRow.colorTcoHardware = hardware.colorTCO;

      if ($rootScope.token.featureFlag.regionSettings) {
        locale = RGSharedLocaleService.getSelectedColumnLocale().region;
        tabularRow.deviceCostHardware = $filter('rgTabColNum')(tabularRow.deviceCostHardware, locale);
        tabularRow.totalCppHardware = $filter('rgTabColNum')(tabularRow.totalCppHardware, locale);
        tabularRow.monoCppHardware = $filter('rgTabColNum')(tabularRow.monoCppHardware, locale);
        tabularRow.colorCppHardware = $filter('rgTabColNum')(tabularRow.colorCppHardware, locale);
        tabularRow.totalTcoHardware = $filter('rgTabColNum')(tabularRow.totalTcoHardware, locale);
        tabularRow.monoTcoHardware = $filter('rgTabColNum')(tabularRow.monoTcoHardware, locale);
        tabularRow.colorTcoHardware = $filter('rgTabColNum')(tabularRow.colorTcoHardware, locale);
      }
    }

    /**
     * Set carbon Foot Print
     * @param tabularRow
     * @param carbonFootPrint
     */
    function setCarbonFootPrint(tabularRow, carbonFootPrint) {
      tabularRow.totalEnergyInKWH = carbonFootPrint.totalEnergyInKWH;
      tabularRow.co2PaperKG = carbonFootPrint.co2PaperKG;
      tabularRow.energyCO2KG = carbonFootPrint.energyCO2kG;
      tabularRow.energyCO2KGbyGeography = carbonFootPrint.energyCO2KGbyGeography;
      tabularRow.noOfTreesCutDown = carbonFootPrint.noOfTreesCutDown;
      tabularRow.literOfOilRequired = carbonFootPrint.literOfOilRequired;
    }

    /**
     * Set IndirectCosts
     * @param tabularRow
     * @param IndirectCosts
     */
    function setIndirectCosts(tabularRow, indirectCosts) {
      tabularRow.totalAssetManagementCost = indirectCosts.totalAssetManagementCost;
      tabularRow.supplyProcessTotalManHrsCostsPerYr = indirectCosts.supplyProcessTotalManHrsCostsPerYr;
      tabularRow.totalDeviceSpaceCostperYr = indirectCosts.totalDeviceSpaceCostperYr;
      tabularRow.helpDeskCostperPrinterBasedonNumberofPrinters = indirectCosts.helpDeskCostperPrinterBasedonNumberofPrinters;
      tabularRow.helpDeskCostperPrinterBasedonNumberofUsers = indirectCosts.helpDeskCostperPrinterBasedonNumberofUsers;
      tabularRow.totalPurchasingCostofDeviceperYr = indirectCosts.totalPurchasingCostofDeviceperYr;
      tabularRow.totalITMgmtCostofDevicePerYr = indirectCosts.totalITMgmtCostofDevicePerYr;

      if ($rootScope.token.featureFlag.regionSettings) {
        locale = RGSharedLocaleService.getSelectedColumnLocale().region;
        tabularRow.totalAssetManagementCost = $filter('rgTabColNum')(tabularRow.totalAssetManagementCost, locale);
        tabularRow.supplyProcessTotalManHrsCostsPerYr = $filter('rgTabColNum')(tabularRow.supplyProcessTotalManHrsCostsPerYr, locale);
        tabularRow.totalDeviceSpaceCostperYr = $filter('rgTabColNum')(tabularRow.totalDeviceSpaceCostperYr, locale);
        tabularRow.helpDeskCostperPrinterBasedonNumberofPrinters = $filter('rgTabColNum')(tabularRow.helpDeskCostperPrinterBasedonNumberofPrinters, locale);
        tabularRow.helpDeskCostperPrinterBasedonNumberofUsers = $filter('rgTabColNum')(tabularRow.helpDeskCostperPrinterBasedonNumberofUsers, locale);
        tabularRow.totalPurchasingCostofDeviceperYr = $filter('rgTabColNum')(tabularRow.totalPurchasingCostofDeviceperYr, locale);
        tabularRow.totalITMgmtCostofDevicePerYr = $filter('rgTabColNum')(tabularRow.totalITMgmtCostofDevicePerYr, locale);
        tabularRow.totalITMgmtCostofDevicePerYr = $filter('rgTabColNum')(tabularRow.totalITMgmtCostofDevicePerYr, locale);
      }
    }

    /**
     * Set Tco total pricing
     * @param tabularRow
     * @param basicPricing
     */
    function setTcoTotalPricing(tabularRow, basicPricing) {
      tabularRow.totalMonoCPP = basicPricing.monoCPP;
      tabularRow.totalColorCPP = basicPricing.colorCPP;
      tabularRow.totalCPP = basicPricing.totalCPP;

      if ($rootScope.token.featureFlag.regionSettings) {
        locale = RGSharedLocaleService.getSelectedColumnLocale().region;
        tabularRow.totalMonoCPP = $filter('rgTabColNum')(tabularRow.totalMonoCPP, locale);
        tabularRow.totalColorCPP = $filter('rgTabColNum')(tabularRow.totalColorCPP, locale);
        tabularRow.totalCPP = $filter('rgTabColNum')(tabularRow.totalCPP, locale);
      }
    }

    /**
     * Sets tco pricing
     * @param tabularRow
     */
    function setTcoPricingData(tabularRow) {
      try {
        var pricingData = TcoPricingService.getAssetPricing(tabularRow.assetId);
        if (pricingData) {
          tabularRow.isEligibleForTcoRemap = pricingData.eligibleForRemap || false;
          tabularRow.isAssetProxy = pricingData.assetProxied || false;
          if (pricingData.extended && pricingData.extended.deviceComponents) {
            if (pricingData.extended.deviceComponents.toner) {
              setTonerPricing(tabularRow, pricingData.extended.deviceComponents.toner);
            }

            if (pricingData.extended.deviceComponents.replaceableParts) {
              setReplaceablePartsPricing(tabularRow, pricingData.extended.deviceComponents.replaceableParts);
            }

            if (pricingData.extended.deviceComponents.service) {
              setServicePricing(tabularRow, pricingData.extended.deviceComponents.service);
            }

            if (pricingData.extended.deviceComponents.power) {
              setPowerPricing(tabularRow, pricingData.extended.deviceComponents.power);
            }

            if (pricingData.extended.deviceComponents.paper) {
              setPaperPricing(tabularRow, pricingData.extended.deviceComponents.paper);
            }

            if (pricingData.extended.deviceComponents.hardware) {
              setHardwarePricing(tabularRow, pricingData.extended.deviceComponents.hardware);
            }
          }
          if (pricingData.extended && pricingData.extended.carbonFootPrint) {
            setCarbonFootPrint(tabularRow, pricingData.extended.carbonFootPrint);
          }
          if (pricingData.extended && pricingData.extended.indirectCosts) {
            setIndirectCosts(tabularRow, pricingData.extended.indirectCosts);
          }
          if (pricingData.basic) {
            setTcoTotalPricing(tabularRow, pricingData.basic);
          }
        }
      }
      catch (ex) {
        $log.error('Error while binding Tco pricing data', ex);
      }
    }

    /**
     * Reset dart pricing data for tabularRow
     * @param tabularRow
     */
    function resetDartPricingDataForTabularRow(tabularRow) {
      var dartColumns, dartColumnsLength;
      try {
        if (tabularRow) {
          dartColumns = PorticoSDConfiguration.DART_PRICING_COLUMNS;
          dartColumnsLength = dartColumns.length;
          for (var index = 0; index < dartColumnsLength; index++) {
            tabularRow[dartColumns[index]] = "";
          }
        }
      }
      catch (ex) {
        $log.error('Error while resetting dart pricing data for tabularRow', ex);
      }
    }

    return {
      setPricingData: setPricingData,
      getCountryPricingData: getCountryPricingData,
      convertToUsd: convertToUsd,
      setTcoPricingData: setTcoPricingData,
      resetDartPricingDataForTabularRow: resetDartPricingDataForTabularRow
    };
  }

  var app = angular.module('app'),
      requires = [
        '$log',
        '$filter',
        '$rootScope',
        'ngCartosUtils.services.UtilityService',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'PorticoSdUtilityService',
        'TcoPricingService',
        'RGSharedLocaleService',
        PricingService
      ];
  app.factory('PricingService', requires);
}());

/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  function SyncTcoPricingService($log, $routeParams, $rootScope, PorticoSDConfiguration, PorticoSDService, AssetService, ProjectSetting,
                                 PorticoSDSharedService, TcoPricingService, AssetLocatorService, AppSettingService) {


    /**
     * Check feature flag for autoSyncTco
     * @returns {boolean}
     */
    function isAutoSyncTcoEnabled() {
      if ($rootScope.token && $rootScope.token.featureMatrix) {
        return $rootScope.token.featureMatrix.autoSyncTco || PorticoSDConfiguration.TRUE;
      }
      return false;
    }

    /**
     * Check whether tco pricing fetch is allowed or not.
     * @returns {*}
     */
    function isEligibleForTcoPricingFetch() {
      var settings, isTcoPricingFetched, activeState;
      settings = ProjectSetting.getProjectSettings($routeParams.id);
      if (settings && settings.pricingSettings && settings.pricingSettings.pricingSource &&
          settings.pricingSettings.pricingSource === PorticoSDConfiguration.PRICING_SOURCE.TCO) {
        isTcoPricingFetched = PorticoSDSharedService.getTcoPricingFetched();
        activeState = TcoPricingService.getActiveStateForTcoPricing();
        return activeState && isTcoPricingFetched[activeState];
      }
      return false;
    }

    /**
     * Check whether is auto sync tco pricing need to fetched or not.
     * As it is fetched based on certain conditions.
     * @returns {boolean}
     */
    function isAutoSyncTcoPricingFetched() {
      try {
        if (!isAutoSyncTcoEnabled()) {
          return false;
        }
        return isEligibleForTcoPricingFetch();
      }
      catch (ex) {
        $log.error('Error while checking auto sync TCO pricing fetched', ex);
        return false;
      }
    }

    /**
     * Get the valid assets to get the tco pricing
     * @param assets
     * @returns {Array}
     */
    function getValidAssetsForTcoPricing(assets) {
      var assetIds = [], assetLength;
      if (assets && assets.length) {
        assetLength = assets.length;
        for (var index = 0; index < assetLength; index++) {
          if (assets[index].outcome !== "replaceWithUnknown" && assets[index].state !== "unknown") {
            assetIds.push(assets[index]._id);
          }
        }
      }
      return assetIds;
    }

    /**
     * Get assets request for tco pricing
     * @param assetIds
     * @returns {Array}
     */
    function getAssetsRequestForTcoPricing(assetIds) {
      var asset, assetLocation, countrySet, assetLength, assetReq,
          assetsReq = [],
          assetReqTco = {assetId: "", countryId: "", country: "", region: "", countryCode: ""};
      if (assetIds && assetIds.length) {
        assetLength = assetIds.length;
        for (var index = 0; index < assetLength; index++) {
          asset = AssetService.getAsset(assetIds[index]);
          if (asset) {
            assetLocation = AssetLocatorService.getAssetLocation(asset);
            if (assetLocation && assetLocation.country && assetLocation.country.name) {
              countrySet = AppSettingService.getRegionFromCountrySet(assetLocation.country.name);
              if (countrySet) {
                assetReq = angular.copy(assetReqTco);
                assetReq.assetId = asset._id;
                assetReq.countryId = assetLocation.country._id;
                assetReq.country = assetLocation.country.name;
                assetReq.region = countrySet.region;
                assetReq.countryCode = countrySet.countryCode;
                assetsReq.push(assetReq);
              }
            }
          }
        }
      }
      return assetsReq;
    }

    /**
     * Update tco pricing of assets
     * @param assetIds
     */
    function updateTcoPricingForAssets(assetIds) {
      var assets = [], asset, assetLength;
      if (assetIds && assetIds.length) {
        assetLength = assetIds.length;
        for (var index = 0; index < assetLength; index++) {
          asset = AssetService.getAsset(assetIds[index]);
          if (asset) {
            assets.push(asset);
          }
        }

        // updating assets
        if (assets.length) {
          PorticoSDService.updateAssetsInPoolManager(assets);
        }
      }
    }

    /**
     * Update tco pricing of levels
     * @param assetIds
     * @param adjustType
     */
    function updateTcoPricingForLevels(assetIds, adjustType) {
      if (assetIds && assetIds.length) {
        PorticoSDService.updateLevelsTcoPricing(assetIds, adjustType);
      }
    }

    /**
     * Sync tco pricing for replaced asset
     * @param responseAssets
     */
    function syncTcoPricingForReplacedAsset(responseAssets) {
      try {
        var assetIds, assetsRequest;
        if (isAutoSyncTcoPricingFetched()) {
          assetIds = getValidAssetsForTcoPricing(responseAssets);
          assetsRequest = getAssetsRequestForTcoPricing(assetIds);
          if (!assetsRequest.length) {
            return;
          }

          TcoPricingService.updateAssetsTcoPricing(assetsRequest).then(function (response) {
            // Success
            updateTcoPricingForAssets(assetIds);
            updateTcoPricingForLevels(assetIds, PorticoSDConfiguration.ADJUST_PRICING.ADD);
          }, function (err) {
            // Error
            $log.info('Error in getting tco pricing for the asset');
          });
        }
      }
      catch (ex) {
        $log.error('Error while syncing the tco pricing for replaced asset', ex);
      }
    }

    /**
     * Update tco pricing for assets
     * @param assetIds
     */
    function updateTcoPricing(assetIds) {
      updateTcoPricingForLevels(assetIds, PorticoSDConfiguration.ADJUST_PRICING.SUBTRACT);
      updateTcoPricingForAssets(assetIds);
      updateTcoPricingForLevels(assetIds, PorticoSDConfiguration.ADJUST_PRICING.ADD);
    }

    /**
     * Sync tco pricing for updated asset
     * @param responseAssets
     */
    function syncTcoPricingForUpdatedAsset(responseAssets) {
      try {
        var assetIds, assetsRequest;
        if (isAutoSyncTcoPricingFetched()) {
          assetIds = getValidAssetsForTcoPricing(responseAssets);
          assetsRequest = getAssetsRequestForTcoPricing(assetIds);
          if (!assetsRequest.length) {
            return;
          }

          TcoPricingService.updateAssetsTcoPricing(assetsRequest).then(function (response) {
            // Success
            updateTcoPricing(assetIds);
          }, function (err) {
            // Error
            $log.info('Error in getting tco pricing for the asset');
          });
        }
      }
      catch (ex) {
        $log.error('Error while syncing the tco pricing for replaced asset', ex);
      }
    }

    return {
      syncTcoPricingForReplacedAsset: syncTcoPricingForReplacedAsset,
      syncTcoPricingForUpdatedAsset: syncTcoPricingForUpdatedAsset,
      isEligibleForTcoPricingFetch: isEligibleForTcoPricingFetch,
      updateTcoPricing: updateTcoPricing
    };
  }

  var app = angular.module('app'),
      requires = [
        '$log',
        '$routeParams',
        '$rootScope',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'ngPorticoSD.services.PorticoSDService',
        'ngCartosCore.services.AssetService',
        'ngCartosCore.services.ProjectSettingService',
        'PorticoSDSharedService',
        'TcoPricingService',
        'ngCartosCore.services.AssetLocatorService',
        'AppSettingService',
        SyncTcoPricingService
      ];
  app.factory('SyncTcoPricingService', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  function TabularDataManagerService($log, PorticoSDConfiguration, SharedService, AssetService, TransitionHelperService,
                                     PorticoSDHelperService) {

    /**
     * This method will provide the data which is on tabular view after applying any filter.
     * @param gridRows This will be the rows from "$scope.gridApi.grid.rows"
     * @returns filterTabularData
     */
    function getTabularFilteredData(gridRows) {
      var filteredRows,
          filterTabularData = null;
      try {
        if (gridRows) {
          // This will give all the data which is appearing on tabular view.
          // If any filter is applied then it will give us filtered data.
          filteredRows = gridRows.filter(function (element) {
            return element.visible;
          });
          if (filteredRows.length) {
            filterTabularData = filteredRows.map(function (row) {
              return row.entity;
            });
          }
        }
        return filterTabularData;
      }
      catch (ex) {
        $log.error('Error in loading tabular filtered data ', ex);
        return null;
      }
    }

    /**
     * Set the tabular data as selected or unselected
     * @param tabularData
     * @param selection
     */
    function setTabularDataRowSelection(tabularData, selection) {
      var asset, tabularDataIndex = 0,
          isMvView = SharedService.pricingView() === PorticoSDConfiguration.TABULAR_VIEWS.MULTIVENDOR;
      try {
        if (tabularData && tabularData.length) {
          if (selection) {
            for (tabularDataIndex = 0; tabularDataIndex < tabularData.length; tabularDataIndex++) {
              if (tabularData[tabularDataIndex].type === PorticoSDConfiguration.LEVEL ||
                  tabularData[tabularDataIndex].type === PorticoSDConfiguration.PROJECT) {
                tabularData[tabularDataIndex].selection = selection;
              }
              else if (tabularData[tabularDataIndex].type === PorticoSDConfiguration.ASSET) {
                asset = AssetService.getAsset(tabularData[tabularDataIndex].assetId);
                if (asset && !TransitionHelperService.isReplace(tabularData[tabularDataIndex].outcome) &&
                    !TransitionHelperService.isRealRepositionAsset(asset) &&
                    !(SharedService.isEmptyAsset(asset) && !TransitionHelperService.isReplaceWithNew(asset.outcome)) &&
                    !SharedService.isTrivialAsset(asset.getType())) {
                  if (isMvView) {
                    //To select only MultiVendor assets when pricing view is MV View
                    if (PorticoSDHelperService.isMultiVendorAsset(asset)) {
                      tabularData[tabularDataIndex].selection = selection;
                    }
                  } else {
                    tabularData[tabularDataIndex].selection = selection;
                  }
                }
              }
            }
          }
          else {
            for (tabularDataIndex = 0; tabularDataIndex < tabularData.length; tabularDataIndex++) {
              tabularData[tabularDataIndex].selection = selection;
            }
          }
        }
      }
      catch (ex) {
        $log.error('Error in selecting/un-selecting the data ', ex);
      }
    }

    /**
     * To Check if all of the row is selected.
     * @param tabularData
     * @returns {boolean}
     */
    function isAllRowSelected(tabularData) {
      try {
        var isSelected = false;
        if (tabularData && tabularData.length) {
          isSelected = true;
          for (var rowIndex = 0; rowIndex < tabularData.length; rowIndex++) {
            if (tabularData[rowIndex].type === PorticoSDConfiguration.LEVEL ||
                tabularData[rowIndex].type === PorticoSDConfiguration.PROJECT) {
              if (!tabularData[rowIndex].selection) {
                isSelected = false;
                break;
              }
            }
            else if (tabularData[rowIndex].type === PorticoSDConfiguration.ASSET) {
              if (!tabularData[rowIndex].selection && !TransitionHelperService.isReplace(tabularData[rowIndex].outcome)) {
                isSelected = false;
                break;
              }
            }
          }
        }
        return isSelected;
      }
      catch (ex) {
        $log.error('Error in checking all row selected ', ex);
        return false;
      }
    }

    /**
     * Get the filter tabular data from level.
     * @param row
     * @param tabularViewData
     * @returns {*}
     */
    function getFilterDataByLevel(tabularViewData, row) {
      var filteredTabularData = null;
      try {
        switch (row.$$treeLevel) {
          case PorticoSDConfiguration.TREE_LEVEL_PROJECT :
            filteredTabularData = tabularViewData;
            break;
          case PorticoSDConfiguration.TREE_LEVEL_COUNTRY :
            filteredTabularData = tabularViewData.filter(function (element) {
              return element.countryId === row.countryId ;
            });
            break;
          case PorticoSDConfiguration.TREE_LEVEL_SITE :
            filteredTabularData = tabularViewData.filter(function (element) {
              return element.siteId === row.siteId;
            });
            break;
          case PorticoSDConfiguration.TREE_LEVEL_BUILDING :
            filteredTabularData = tabularViewData.filter(function (element) {
              return element.buildingId === row.buildingId;
            });
            break;
          case PorticoSDConfiguration.TREE_LEVEL_FLOOR  :
            filteredTabularData = tabularViewData.filter(function (element) {
              return element.floorId === row.floorId;
            });
            break;
        }
        return filteredTabularData;
      }
      catch (ex) {
        $log.error('Error in getting the data from level ', ex);
        return null;
      }
    }

    /**
     * Get the filter data for the header level un-selection.
     * This method will give the levels which is above the row levels.
     * For ex : If row is of building then it will give use project, country and site rows.
     * @param tabularViewData
     * @param row
     * @returns filteredTabularData
     */
    function getFilterDataForHeaderUnSelection(tabularViewData, row) {
      var filteredTabularData = null;
      try {
        switch (row.$$treeLevel) {
          case PorticoSDConfiguration.TREE_LEVEL_PROJECT :
            filteredTabularData = null;
            break;
          case PorticoSDConfiguration.TREE_LEVEL_COUNTRY :
            filteredTabularData = tabularViewData.filter(function (element) {
              return element.type === PorticoSDConfiguration.PROJECT;
            });
            break;
          case PorticoSDConfiguration.TREE_LEVEL_SITE :
            filteredTabularData = tabularViewData.filter(function (element) {
              return ((element.countryId === row.countryId && element.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_COUNTRY) ||
              (element.type === PorticoSDConfiguration.PROJECT));
            });
            break;
          case PorticoSDConfiguration.TREE_LEVEL_BUILDING :
            filteredTabularData = tabularViewData.filter(function (element) {
              return ((element.siteId === row.siteId && element.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_SITE) ||
              (element.countryId === row.countryId && element.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_COUNTRY) ||
              (element.type === PorticoSDConfiguration.PROJECT));
            });
            break;
          case PorticoSDConfiguration.TREE_LEVEL_FLOOR  :
            filteredTabularData = tabularViewData.filter(function (element) {
              return ((element.buildingId === row.buildingId && element.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_BUILDING) ||
              (element.siteId === row.siteId && element.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_SITE) ||
              (element.countryId === row.countryId && element.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_COUNTRY) ||
              (element.type === PorticoSDConfiguration.PROJECT));
            });
            break;
          case PorticoSDConfiguration.TREE_LEVEL_ASSET  :
            filteredTabularData = tabularViewData.filter(function (element) {
              return ((element.floorId === row.floorId && element.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_FLOOR) ||
              (element.buildingId === row.buildingId && element.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_BUILDING) ||
              (element.siteId === row.siteId && element.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_SITE) ||
              (element.countryId === row.countryId && element.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_COUNTRY) ||
              (element.type === PorticoSDConfiguration.PROJECT));
            });
            break;
        }
        return filteredTabularData;
      }
      catch (ex) {
        $log.error('Error in getting the data for header levels ', ex);
        return null;
      }
    }

    /**
     * Is all level selected.
     * This method is used for checking the level only.
     * @param levelData
     * @returns {boolean}
     */
    function isAllLevelSelected(levelData) {
      var isLevelsSelected = true;
      try {
        for (var rowIndex = 0; rowIndex < levelData.length; rowIndex++) {
          if (!levelData[rowIndex].selection) {
            isLevelsSelected = false;
            break;
          }
        }
        return isLevelsSelected;
      }
      catch (ex) {
        $log.error('Error in checking the level selected ', ex);
        return false;
      }
    }

    /**
     * This method will give the levels need to be selected based on sub-level selection.
     * @param tabularData
     * @param row
     * @returns {{project: boolean, country: boolean, site: boolean, building: boolean, floor: boolean}}
     */
    function getLevelsToBeSelected(tabularData, row) {
      var allFloorSelection, allBuildingSelection, allSiteSelection, allCountrySelection,
          levelsSelected = {project: false, country: false, site: false, building: false, floor: true};

      try {
        if (tabularData) {
          allFloorSelection = tabularData.filter(function (element) {
            return ((element.buildingId === row.buildingId && element.floorId !== row.floorId &&
            element.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_FLOOR));
          });

          if (isAllLevelSelected(allFloorSelection)) {
            levelsSelected.building = true;
            allBuildingSelection = tabularData.filter(function (element) {
              return ((element.siteId === row.siteId && element.buildingId !== row.buildingId &&
              element.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_BUILDING));
            });

            if (isAllLevelSelected(allBuildingSelection)) {
              levelsSelected.site = true;
              allSiteSelection = tabularData.filter(function (element) {
                return ((element.countryId === row.countryId && element.siteId !== row.siteId &&
                element.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_SITE));
              });
              if (isAllLevelSelected(allSiteSelection)) {
                levelsSelected.country = true;
                allCountrySelection = tabularData.filter(function (element) {
                  return ((element.countryId !== row.countryId && element.$$treeLevel === PorticoSDConfiguration.TREE_LEVEL_COUNTRY));
                });
                if (isAllLevelSelected(allCountrySelection)) {
                  levelsSelected.project = true;
                }
              }
            }
          }
        }
        return levelsSelected;
      }
      catch (ex) {
        $log.error('Error in setting level selection based on sub-level selection. ', ex);
        return {project: false, country: false, site: false, building: false, floor: true};
      }
    }

    /**
     * Get the assets for the level.
     * @param filterTabularData
     * @param row
     * @returns {null}
     */
    function getAssetsBasedOnLevels(filterTabularData, row) {
      try {
        if (filterTabularData) {
          return filterTabularData.filter(function (element) {
            return ((element.floorId === row.floorId ) &&
            (element.buildingId === row.buildingId ) &&
            (element.siteId === row.siteId ) &&
            (element.countryId === row.countryId ) &&
            (element.type === PorticoSDConfiguration.ASSET));
          });
        }
      }
      catch (ex) {
        $log.error('Error in getting the assets from the level ', ex);
        return null;
      }
    }

    /**
     * This method will set the levels selection based on value from "selectedLevels"
     * @param tabularData
     * @param selectedLevels "{project: false, country: false, site: false, building: false, floor: false}"
     */
    function setSelectedLevels(tabularData, selectedLevels) {
      var selectedLevelsData = [];
      try {
        if (tabularData && tabularData.length) {
          for (var tabularDataIndex = 0; tabularDataIndex < tabularData.length; tabularDataIndex++) {
            switch (tabularData[tabularDataIndex].$$treeLevel) {
              case PorticoSDConfiguration.TREE_LEVEL_PROJECT :
                tabularData[tabularDataIndex].selection = selectedLevels.project;
                if(selectedLevels.project){
                  selectedLevelsData.push(tabularData[tabularDataIndex])
                }
                break;
              case PorticoSDConfiguration.TREE_LEVEL_COUNTRY :
                tabularData[tabularDataIndex].selection = selectedLevels.country;
                if(selectedLevels.country){
                  selectedLevelsData.push(tabularData[tabularDataIndex])
                }
                break;
              case PorticoSDConfiguration.TREE_LEVEL_SITE :
                tabularData[tabularDataIndex].selection = selectedLevels.site;
                if(selectedLevels.site){
                  selectedLevelsData.push(tabularData[tabularDataIndex])
                }
                break;
              case PorticoSDConfiguration.TREE_LEVEL_BUILDING :
                tabularData[tabularDataIndex].selection = selectedLevels.building;
                if(selectedLevels.building){
                  selectedLevelsData.push(tabularData[tabularDataIndex])
                }
                break;
              case PorticoSDConfiguration.TREE_LEVEL_FLOOR  :
                tabularData[tabularDataIndex].selection = selectedLevels.floor;
                if(selectedLevels.floor){
                  selectedLevelsData.push(tabularData[tabularDataIndex])
                }
                break;
            }
          }
        }
      }
      catch (ex) {
        $log.error('Error in setting the levels selection ', ex);
      }
      return selectedLevelsData;
    }

    return {
      getTabularFilteredData: getTabularFilteredData,
      setTabularDataRowSelection: setTabularDataRowSelection,
      isAllRowSelected: isAllRowSelected,
      getFilterDataByLevel: getFilterDataByLevel,
      getFilterDataForHeaderUnSelection: getFilterDataForHeaderUnSelection,
      getLevelsToBeSelected: getLevelsToBeSelected,
      getAssetsBasedOnLevels: getAssetsBasedOnLevels,
      setSelectedLevels: setSelectedLevels
    };
  }

  var app = angular.module('app'),
      requires = [
        '$log',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'SharedService',
        'ngCartosCore.services.AssetService',
        'TransitionHelperService',
        'ngPorticoSD.services.PorticoSDHelperService',
        TabularDataManagerService
      ];
  app.factory('ngPorticoSD.services.TabularDataManagerService', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  function TabularViewDataService($q, $routeParams, ViewEvent, SharedService, UtilityService, DealHandlerService,
                                  PricingHelperService, TcoPricingService, PorticoSDSharedService,
                                  PorticoSDConfiguration, MultiVendorService) {


    /**
     * Check is dart pricing allowed for the user
     * @returns {*}
     */
    function isDartPricingAllowed() {
      return SharedService.getRolePermissionByKey(PorticoSDConfiguration.FEATURE.DART_PRICING);
    }

    /**
     * Is multi vendor allowed
     * @returns {*|boolean}
     */
    function isMultiVendorAllowed() {
      return PorticoSDSharedService.getFeatureFlagByKey(PorticoSDConfiguration.FEATURE.MULTI_VENDOR);
    }

    /**
     * Get the api list in promise based on view selection.
     * @param apiCall
     * @returns {Array}
     */
    function getApiPromise(apiCall) {
      let dartPricingData, isTcoPricingFetched, promises = [];
      if (!apiCall) {
        return promises;
      }

      if (apiCall.isDart && isDartPricingAllowed()) {
        dartPricingData = PricingHelperService.assetDartPricingData() | [];
        if (!dartPricingData.length) {
          promises.push(DealHandlerService.getDartPricing($routeParams.id, PorticoSDConfiguration.PRICING_SOURCE.OTHER,
              PorticoSDConfiguration.PRICING_SOURCE.DART));
        }
      }
      if (apiCall.isTco) {
        isTcoPricingFetched = PorticoSDSharedService.getTcoPricingFetched();
        if (!(isTcoPricingFetched[PorticoSDConfiguration.STATES.CURRENT] ||
            isTcoPricingFetched[PorticoSDConfiguration.STATES.FUTURE])) {
          promises.push(TcoPricingService.getTcoPricing());
        }
      }
      if (apiCall.isMV && isMultiVendorAllowed()) {
        if (!UtilityService.isValid(PorticoSDSharedService.mvTemplates())) {
          promises.push(MultiVendorService.getMvTemplates(false));
        }
        if (!MultiVendorService.isMvCostFetched()) {
          promises.push(MultiVendorService.fetchMvCost(false));
        }
      }
      return promises;
    }


    /**
     * Load the project data based on view selected
     * @returns {*}
     */
    function loadProjectData() {
      let deferred = $q.defer(), promises,
          pricingView = SharedService.pricingView() || PorticoSDConfiguration.TABULAR_VIEWS.MANUAL,
          apiCall = {isDart: false, isTco: false, isMV: false};

      switch (pricingView) {
        case PorticoSDConfiguration.TABULAR_VIEWS.MANUAL :
        case PorticoSDConfiguration.TABULAR_VIEWS.PRESCRIPTIVE :
          break;
        case PorticoSDConfiguration.TABULAR_VIEWS.DART :
          apiCall = {isDart: true, isTco: false, isMV: false};
          break;
        case PorticoSDConfiguration.TABULAR_VIEWS.TCO :
          apiCall = {isDart: false, isTco: true, isMV: false};
          break;
        case PorticoSDConfiguration.TABULAR_VIEWS.MULTIVENDOR :
          apiCall = {isDart: false, isTco: true, isMV: true};
          break;
        case PorticoSDConfiguration.TABULAR_VIEWS.ALL :
          apiCall = {isDart: true, isTco: true, isMV: true};
          break;
        default :
          apiCall = {isDart: true, isTco: true, isMV: true};
          break;
      }
      promises = getApiPromise(apiCall);
      if (promises.length) {
        $q.allSettled(promises).then(function (response) {
          deferred.resolve(response);
        });
      } else {
        deferred.resolve();
      }
      return deferred.promise;
    }

    return {
      loadProjectData: loadProjectData
    };
  }

  let app = angular.module('app'),
      requires = [
        '$q',
        '$routeParams',
        'ViewEvent',
        'SharedService',
        'ngCartosUtils.services.UtilityService',
        'ngCartosIntegration.services.DealHandlerService',
        'PricingHelperService',
        'TcoPricingService',
        'PorticoSDSharedService',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'ngPorticoSD.services.MultiVendorService',
        TabularViewDataService
      ];
  app.factory('ngPorticoSD.services.TabularViewDataService', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';
  function TabularViewMapService(PorticoSDService) {

    /**
     * This method will check whether fleet has multi vendor asset or not.
     * @returns {boolean}
     */
    function hasMultiVendorAsset() {
      return PorticoSDService.hasMultiVendorAsset();
    }

    return {
      hasMultiVendorAsset: hasMultiVendorAsset
    };
  }

  var app = angular.module('app'),
      requires = [
        'ngPorticoSD.services.PorticoSDService',
        TabularViewMapService
      ];
  app.factory('ngPorticoSD.services.TabularViewMapService', requires);
}());
/*global angular*/
(function () {
  'use strict';

  /**
   *  Tabular View Setting Service
   * @param $q
   * @param $log
   * @param PorticoSDApiService
   * @param PorticoSDSharedService
   * @param PorticoSDConfig
   * @param Notify
   */
  function TabularViewSettingService($q, $log, $routeParams, PorticoSDApiService, PorticoSDSharedService, PorticoSDConfig,
                                     Notify, UtilityService, ProjectSettingService, SharedService, PorticoSDHelperService) {

    /**
     * Function to save the updated template
     * @param viewSettings
     * @param projectId
     */
    function updateTemplate(viewSettings, projectId) {
      let deferred = $q.defer();

      function onSuccess() {
        PorticoSDSharedService.updateTemplate(viewSettings);
        deferred.resolve();
      }

      function onError(error) {
        $log.error("Error while updating the tabular template", error);
        deferred.reject();
      }

      PorticoSDApiService.saveTabularTemplate(viewSettings).then(onSuccess, onError);
      return deferred.promise;
    }

    /**
     * Function to map the value as key
     * @param selectedTemplate
     */
    function mapValueAsKey(selectedTemplate) {
      let mappedTemplate = {};
      PorticoSDSharedService.getTemplateData(selectedTemplate).forEach(function (template) {
        mappedTemplate[template.name] = template;
      });
      return mappedTemplate;
    }

    /**
     * Check whether we need to update the project settings or not based on current view and selected view.
     * @param setting
     * @param templateName
     * @returns {boolean}
     */
    function isProjectSettingUpdate(setting, templateName) {
      let isUpdate = false;

      if (!(setting && setting.pricingSettings)) {
        return isUpdate;
      }

      if (templateName === setting.pricingSettings.pricingSource) {
        isUpdate = true;
      }
      return isUpdate;
    }

    /**
     * Function to delete the tabular template
     * @param templateName
     */
    function deleteTabularTemplate(templateName) {
      let deferred = $q.defer(),
          projectId = $routeParams.id,
          setting, projectSetting;

      function updateProjectSetting(projectSettingsUpdate) {
        let request = {
          projectId: projectId,
          pricingSettings: projectSettingsUpdate.pricingSettings
        };

        //success callback on updating project settings
        function onSuccessProjectSetting() {
          SharedService.pricingView(PorticoSDConfig.TABULAR_VIEWS.MANUAL);
          $log.info('Pricing settings update successfully.');
          deferred.resolve();
        }

        //error callback on updating project settings
        function onErrorProjectSetting(error) {
          $log.info('Error updating pricing setting.');
          deferred.resolve();
        }

        //update project setting
        ProjectSettingService.updateProjectSetting(request).then(onSuccessProjectSetting, onErrorProjectSetting);
      }

      function onSuccess() {
        PorticoSDSharedService.removeTemplate(templateName);
        setting = ProjectSettingService.getProjectSettings(projectId) || {};
        if (isProjectSettingUpdate(setting, templateName)) {
          projectSetting = angular.copy(setting);
          projectSetting.pricingSettings.pricingSource = PorticoSDConfig.TABULAR_VIEWS.MANUAL;
          projectSetting.pricingSettings.currentPricingView = PorticoSDConfig.TABULAR_VIEWS.MANUAL;
          projectSetting.pricingSettings.proposedPricingView = PorticoSDConfig.TABULAR_VIEWS.MANUAL;
          updateProjectSetting(projectSetting);
        } else {
          deferred.resolve();
        }
      }

      function onError(error) {
        $log.error("Error in deleting the tabular template", error);
        deferred.reject();
      }

      PorticoSDApiService.deleteTabularTemplate(templateName).then(onSuccess, onError);
      return deferred.promise;
    }

    /**
     * Function to update the column visibility on drop
     * @param list
     * @param  items
     * @param  index
     * @param  isVisible
     */
    function updateColumnsOnDrop(list, items, index, isVisible) {
      let nonDraggableColumnsLength = Object.keys(PorticoSDConfig.TABULAR_VIEW_TEMPLATE_SETTINGS.NON_DRAGGABLE_COLUMNS).length;
      index = index < nonDraggableColumnsLength ? nonDraggableColumnsLength : index;
      for (let selectedItems = 0; selectedItems < items.length; selectedItems++) {
        for (let availableColumnsLength = 0; availableColumnsLength < list.length; availableColumnsLength++) {
          if (list[availableColumnsLength].name === items[selectedItems].name) {
            list.splice(availableColumnsLength, 1);
            if (index > availableColumnsLength) {
              index = index - 1;
            }
          }
        }
        items[selectedItems].visible = isVisible;
        delete items[selectedItems].selected;
      }
      return list.slice(0, index).concat(items).concat(list.slice(index));
    }

    /**
     * Function to update the existing columns properties from the selected template
     * @param copyTemplateFrom
     */
    function updateColumnsFromSelectedTemplate(copyTemplateFrom) {
      let templates = PorticoSDSharedService.getTemplates();
      if (copyTemplateFrom !== PorticoSDConfig.TABULAR_VIEW_TEMPLATE_SETTINGS.ALL_COLUMNS_TEMPLATE_NAME) {
        let mappedTemplate = mapValueAsKey(copyTemplateFrom),
            columns = templates[PorticoSDConfig.TABULAR_VIEW_TEMPLATE_SETTINGS.ALL_COLUMNS_TEMPLATE_NAME];
        for (let templateLength = 0; templateLength < columns.length; templateLength++) {
          columns.forEach(function (column, index) {
            if (mappedTemplate[column.name]) {
              columns.splice(index, 1);
            } else {
              column.visible = false;
            }
          });
        }
        return templates[copyTemplateFrom].concat(columns);
      } else {
        return templates[copyTemplateFrom];
      }
    }

    /**
     * Function to show the image while dragging the columns
     * @param list
     * @param event
     */
    function showImageOnDragStart(list, event) {
      list.dragging = true;
      if (event.dataTransfer.setDragImage) {
        let img = new Image();
        img.src = PorticoSDConfig.MULTI_DND.IMAGE_PATH;
        event.dataTransfer.setDragImage(img, 0, 0);
      }
    }

    /**
     * Check is dart pricing allowed for the user
     * @returns {*}
     */
    function isDartPricingAllowed() {
      return SharedService.getRolePermissionByKey(PorticoSDConfig.FEATURE.DART_PRICING);
    }

    /**
     * Filter tabular data based on user access
     * @param tabularTemplates
     */
    function filterTabularDataBasedOnUserAccess(tabularTemplates) {
      let isDart = isDartPricingAllowed(),
          isMultiVendor = PorticoSDSharedService.getFeatureFlagByKey(PorticoSDConfig.FEATURE.MULTI_VENDOR),
          filteredTemplateData = [], templateData = [];
      if (UtilityService.isValid(tabularTemplates)) {
        for (let key in tabularTemplates) {
          if (tabularTemplates.hasOwnProperty(key)) {
            if ((key === PorticoSDConfig.TABULAR_VIEWS.DART && !isDart) ||
                (key === PorticoSDConfig.TABULAR_VIEWS.MULTIVENDOR && !isMultiVendor)) {
              delete tabularTemplates[key];
              continue;
            }

            if (!PorticoSDConfig.TABULAR_VIEW_TEMPLATE_SETTINGS.DEFAULT_TEMPLATES[key] &&
                (!isDart || !isMultiVendor)) {
              filteredTemplateData = [];
              templateData = tabularTemplates[key];
              for (let index = 0; index < templateData.length; index++) {
                if (!isDart) {
                  if (PorticoSDHelperService.isDartColumn(templateData[index].name)) {
                    continue;
                  }
                }

                if (!isMultiVendor) {
                  if (PorticoSDHelperService.isMultiVendorColumn(templateData[index].name)) {
                    continue;
                  }
                  filteredTemplateData.push(templateData[index]);
                }
              }
              tabularTemplates[key] = filteredTemplateData;
            }
          }
        }
      }
    }


    /**
     * Function to get the tabular templates
     */
    function getTabularSetting() {
      let deferred = $q.defer(),
          tabularTemplate;

      function onError(error) {
        $log.error("Error on getting user settings", error);
        Notify.error("ERROR_ON_GETTING_USER_SETTINGS");
        deferred.reject();
      }

      function onSuccess(response) {
        if (response.data && response.data.data) {
          tabularTemplate = response.data.data;
          filterTabularDataBasedOnUserAccess(tabularTemplate);
          PorticoSDSharedService.setTabularTemplates(tabularTemplate);
          deferred.resolve();
        } else {
          onError(response);
        }
      }

      tabularTemplate = PorticoSDSharedService.getTemplates();
      if (!tabularTemplate) {
        PorticoSDApiService.getTabularSetting().then(onSuccess, onError);
      } else {
        deferred.resolve();
      }
      return deferred.promise;
    }

    /**
     * Get all the tabular view names
     */
    function getTabularTemplatesName() {
      let tabularTemplate = PorticoSDSharedService.getTemplates(), templateNames = {};
      if (UtilityService.isValid(tabularTemplate)) {
        for (let key in tabularTemplate) {
          if (tabularTemplate.hasOwnProperty(key)) {
            if (key === PorticoSDConfig.TABULAR_VIEW_TEMPLATE_SETTINGS.ALL_COLUMNS_TEMPLATE_NAME) {
              continue;
            }
            templateNames[key] = key;
          }
        }
      }
      return templateNames;
    }

    return {
      updateTemplate: updateTemplate,
      getTabularSetting: getTabularSetting,
      deleteTabularTemplate: deleteTabularTemplate,
      updateColumnsOnDrop: updateColumnsOnDrop,
      updateColumnsFromSelectedTemplate: updateColumnsFromSelectedTemplate,
      showImageOnDragStart: showImageOnDragStart,
      getTabularTemplatesName: getTabularTemplatesName
    };

  }

  let app = angular.module('app'),
      requires = [
        '$q',
        '$log',
        '$routeParams',
        'PorticoSDApiService',
        'PorticoSDSharedService',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'ngCartosCore.services.NotifierService',
        'ngCartosUtils.services.UtilityService',
        'ngCartosCore.services.ProjectSettingService',
        'SharedService',
        'ngPorticoSD.services.PorticoSDHelperService',
        TabularViewSettingService
      ];
  app.factory('TabularViewSettingService', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';
  function TabularViewStateBehaviourService($log, Configuration, TransitionHelperService, OutcomeType, PorticoSDService,
                                            PorticoSDConfiguration, TcoPricingService) {

    /**
     * Get the filter data for current state.
     * @param tabularData
     * @returns {*}
     */
    function getCurrentStateFilterData(tabularData) {
      if (tabularData) {
        tabularData = tabularData.filter(function (asset) {
          return (asset.isRealAsset !== false &&
              (!TransitionHelperService.isReplaceWithNew(asset.outcome) && asset.outcome !== OutcomeType.REPOSITION_FROM)
          );
        });
      }
      return tabularData;
    }

    /**
     * Get the filter data for future state.
     * @param tabularData
     * @returns {*}
     */
    function getFutureStateFilterData(tabularData) {
      if (tabularData) {
        tabularData = tabularData.filter(function (asset) {
          return (!TransitionHelperService.isReplace(asset.outcome) && asset.outcome !== OutcomeType.RETIRE &&
          asset.outcome !== PorticoSDConfiguration.EMPTY && asset.outcome !== OutcomeType.REPOSITION_TO &&
          !(asset.outcome === OutcomeType.REPOSITION && asset.isRealAsset));
        });
      }
      return tabularData;
    }

    /**
     * This function will give the default data for current state
     * @returns {*}
     */
    function getCurrentStateAsDefault() {
      var tabularData = PorticoSDService.loadTabularData();
      if (tabularData && tabularData.length) {
        tabularData = getCurrentStateFilterData(tabularData);
        if (TcoPricingService.isTcoPricingFetched()) {
          PorticoSDService.setTcoPricingOnLevels(tabularData);
          tabularData = getCurrentStateDataFromPool();
        }
      }
      return tabularData;
    }

    /**
     * This function will give the default data for transition state
     * @returns {*}
     */
    function getTransitionStateAsDefault() {
      var tabularData = PorticoSDService.loadTabularData();
      if (tabularData && tabularData.length) {
        if (TcoPricingService.isTcoPricingFetched()) {
          PorticoSDService.setTcoPricingOnLevels(tabularData);
          tabularData = getTransitionStateDataFromPool();
        }
      }
      return tabularData;
    }

    /**
     * This function will give the default data for future state
     * @returns {*}
     */
    function getFutureStateAsDefault() {
      var tabularData = PorticoSDService.loadTabularData();
      if (tabularData && tabularData.length) {
        tabularData = getFutureStateFilterData(tabularData);
        if (TcoPricingService.isTcoPricingFetched()) {
          PorticoSDService.setTcoPricingOnLevels(tabularData);
          tabularData = getFutureStateDataFromPool();
        }
      }
      return tabularData;
    }

    /**
     * This function will give the pool data for current state
     * @returns {*}
     */
    function getCurrentStateDataFromPool() {
      var tabularData = PorticoSDService.getTabularViewData();
      if (tabularData && tabularData.length) {
        tabularData = getCurrentStateFilterData(tabularData);
      }
      return tabularData;
    }

    /**
     * This function will give the pool data for transition state
     * @returns {*}
     */
    function getTransitionStateDataFromPool() {
      return PorticoSDService.getTabularViewData();
    }

    /**
     * This function will give the pool data for future state
     * @returns {*}
     */
    function getFutureStateDataFromPool() {
      var tabularData = PorticoSDService.getTabularViewData();
      if (tabularData && tabularData.length) {
        tabularData = getFutureStateFilterData(tabularData);
      }
      return tabularData;
    }

    /**
     * To get the default state data when switch from floor map view to tabular view
     * @param state
     * @returns {Array}
     */
    function getDefaultDataForState(state) {
      var tabularData = [];
      try {
        if (state) {
          switch (state) {
            case Configuration.STATES.CURRENT:
              tabularData = getCurrentStateAsDefault();
              break;
            case Configuration.STATES.TRANSITION:
              tabularData = getTransitionStateAsDefault();
              break;
            case Configuration.STATES.FUTURE:
              tabularData = getFutureStateAsDefault();
              break;
          }
        }
      }
      catch (ex) {
        $log.error('Error in loading default data for tabular view : ', ex);
      }
      return tabularData;
    }

    /**
     * To get the pool data for the state
     * @param state
     * @returns {Array}
     */
    function getPoolDataForState(state) {
      var tabularData = [];
      try {
        if (state) {
          switch (state) {
            case Configuration.STATES.CURRENT:
              tabularData = getCurrentStateDataFromPool();
              break;
            case Configuration.STATES.TRANSITION:
              tabularData = getTransitionStateDataFromPool();
              break;
            case Configuration.STATES.FUTURE:
              tabularData = getFutureStateDataFromPool();
              break;
          }
        }
      }
      catch (ex) {
        $log.error('Error in loading pool data for tabular view : ', ex);
      }
      return tabularData;
    }

    //Expose function
    return {
      getDefaultDataForState: getDefaultDataForState,
      getPoolDataForState: getPoolDataForState
    };
  }

  var app = angular.module('ngCartosCore'),
      //inject all the required dependencies
      requires = [
        '$log',
        'Configuration',
        'TransitionHelperService',
        'ngCartosCanvas.constants.OutcomeType',
        'ngPorticoSD.services.PorticoSDService',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'TcoPricingService',
        TabularViewStateBehaviourService
      ];
  app.factory('ngPorticoSD.services.TabularViewStateBehaviourService', requires);
}());
/*global angular*/
(function () {
  'use strict';


  function TcoAdjustmentService($log, $q, $rootScope, Configuration, PorticoSDApiService, PorticoSDSharedService,
                                UtilityService) {

    /**
     * Get valid tco adjustment response
     * @param response
     * @returns {*}
     */
    function getValidTcoAdjustmentResponse(response) {
      var tcoAdjustmentResponse = null;
      if (response && response.data && response.data.data) {
        tcoAdjustmentResponse = response.data.data;
      }
      return tcoAdjustmentResponse;
    }

    /**
     * Save Tco Adjustment values
     * @param changes
     */
    function saveTcoAdjustment(changes) {
      var deferred = $q.defer();

      function onSuccess(response) {
        var tcoAdjustmentValues = getValidTcoAdjustmentResponse(response);
        PorticoSDSharedService.tcoAdjustmentValues(tcoAdjustmentValues);
        deferred.resolve(tcoAdjustmentValues);
      }

      function onError(error) {
        deferred.reject(error);
      }

      PorticoSDApiService.saveTcoAdjustment(changes).then(onSuccess, onError);
      return deferred.promise;

    }

    /**
     * Load Tco Adjustment values by making an get Api call
     */
    function loadTcoAdjustment() {

      var deferred = $q.defer();

      function onSuccess(response) {
        var tcoAdjustmentValues = getValidTcoAdjustmentResponse(response);
        PorticoSDSharedService.tcoAdjustmentValues(tcoAdjustmentValues);
        deferred.resolve(tcoAdjustmentValues);
      }

      function onError(error) {
        $log.error(error);
        PorticoSDSharedService.tcoAdjustmentValues(null);
        deferred.reject(error);
      }

      PorticoSDApiService.getTcoAdjustment().then(onSuccess, onError);
      return deferred.promise;
    }

    return {
      loadTcoAdjustment: loadTcoAdjustment,
      saveTcoAdjustment: saveTcoAdjustment
    };

  }

  var app = angular.module('app'),
      requires = [
        '$log',
        '$q',
        '$rootScope',
        'Configuration',
        'PorticoSDApiService',
        'PorticoSDSharedService',
        'ngCartosUtils.services.UtilityService',
        TcoAdjustmentService
      ];
  app.factory('TcoAdjustmentService', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  function TcoIntegrationHelperService($log) {

    /**
     * Set group property
     * @param config
     */
    function setGroupProperty(config) {
      try {
        switch (config.field) {
          case 'detailsToner':
          case 'totalCppToner':
          case 'monoCppToner':
          case 'colorCppToner':
          case 'totalTcoToner':
          case 'monoTcoToner':
          case 'colorTcoToner':
            config.group = 'tonerColumns';
            break;
          case 'detailsRP':
          case 'laborRateRP':
          case 'laborTravelHoursRP':
          case 'laborRepairHoursRP':
          case 'totalCppRP':
          case 'monoCppRP':
          case 'colorCppRP':
          case 'totalTcoRP':
          case 'monoTcoRP':
          case 'colorTcoRP':
            config.group = 'rpColumns';
            break;
          case 'avgPwcService':
          case 'totalCppService':
          case 'monoCppService':
          case 'colorCppService':
          case 'totalTcoService':
          case 'monoTcoService':
          case 'colorTcoService':
            config.group = 'serviceColumns';
            break;
          case 'avgPowPricePower':
          case 'workHoursPerDayPower':
          case 'workDaysPerWeekPower':
          case 'weeksPerYearPower':
          case 'isOnNightAndWeekends':
          case 'powerRateActive':
          case 'powerRateIdle':
          case 'powerRateSave':
          case 'totalCppPower':
          case 'monoCppPower':
          case 'colorCppPower':
          case 'totalTcoPower':
          case 'monoTcoPower':
          case 'colorTcoPower':
            config.group = 'powerColumns';
            break;
          case 'paperCostPerSheetPaper':
          case 'duplexPercentagePaper':
          case 'isDuplexCapablePaper':
          case 'totalCppPaper':
          case 'monoCppPaper':
          case 'colorCppPaper':
          case 'totalTcoPaper':
          case 'monoTcoPaper':
          case 'colorTcoPaper':
            config.group = 'paperColumns';
            break;
          case 'deviceCostHardware':
          case 'totalCppHardware':
          case 'monoCppHardware':
          case 'colorCppHardware':
          case 'totalTcoHardware':
          case 'monoTcoHardware':
          case 'colorTcoHardware':
            config.group = 'hardwareColumns';
            break;
          case 'totalEnergyInKWH':
          case 'co2PaperKG':
          case 'energyCO2KG':
          case 'energyCO2KGbyGeography':
          case 'noOfTreesCutDown':
          case 'literOfOilRequired':
            config.group = 'carbonColumns';
            break;
          case 'totalMonoCPP':
          case 'totalColorCPP':
          case 'totalCPP':
            config.group = 'tcoTotalColumns';
            break;
          case 'totalAssetManagementCost':
          case 'supplyProcessTotalManHrsCostsPerYr':
          case 'totalDeviceSpaceCostperYr':
          case 'helpDeskCostperPrinterBasedonNumberofPrinters':
          case 'helpDeskCostperPrinterBasedonNumberofUsers':
          case 'totalPurchasingCostofDeviceperYr':
          case 'totalITMgmtCostofDevicePerYr':
            config.group = 'indirectCosts';
            break;
        }
      }
      catch (ex) {
        $log.error('Error in setting group property for Tco columns : ', ex);
        return config;
      }
    }

    /**
     * Get the category active status
     * @param tabularColumns
     * @returns {{tonerColumns: boolean, rpColumns: boolean, serviceColumns: boolean, powerColumns: boolean, paperColumns: boolean, hardwareColumns: boolean, carbonColumns: boolean}}
     */
    function getCategoriesActiveStatus(tabularColumns) {
      var categoriesStatus = {
        tonerColumns: false,
        rpColumns: false,
        serviceColumns: false,
        powerColumns: false,
        paperColumns: false,
        hardwareColumns: false,
        carbonColumns: false,
        indirectCosts: false
      };
      try {
        if (tabularColumns && tabularColumns.length) {
          tabularColumns.forEach(function (col, index) {
            if (tabularColumns[index].visible || tabularColumns[index].visible === undefined) {
              switch (tabularColumns[index].field) {
                case 'detailsToner':
                case 'totalCppToner':
                case 'monoCppToner':
                case 'colorCppToner':
                case 'totalTcoToner':
                case 'monoTcoToner':
                case 'colorTcoToner':
                  categoriesStatus.tonerColumns = true;
                  break;
                case 'detailsRP':
                case 'laborRateRP':
                case 'laborTravelHoursRP':
                case 'laborRepairHoursRP':
                case 'totalCppRP':
                case 'monoCppRP':
                case 'colorCppRP':
                case 'totalTcoRP':
                case 'monoTcoRP':
                case 'colorTcoRP':
                  categoriesStatus.rpColumns = true;
                  break;
                case 'avgPwcService':
                case 'totalCppService':
                case 'monoCppService':
                case 'colorCppService':
                case 'totalTcoService':
                case 'monoTcoService':
                case 'colorTcoService':
                  categoriesStatus.serviceColumns = true;
                  break;
                case 'avgPowPricePower':
                case 'workHoursPerDayPower':
                case 'workDaysPerWeekPower':
                case 'weeksPerYearPower':
                case 'isOnNightAndWeekends':
                case 'powerRateActive':
                case 'powerRateIdle':
                case 'powerRateSave':
                case 'totalCppPower':
                case 'monoCppPower':
                case 'colorCppPower':
                case 'totalTcoPower':
                case 'monoTcoPower':
                case 'colorTcoPower':
                  categoriesStatus.powerColumns = true;
                  break;
                case 'paperCostPerSheetPaper':
                case 'duplexPercentagePaper':
                case 'isDuplexCapablePaper':
                case 'totalCppPaper':
                case 'monoCppPaper':
                case 'colorCppPaper':
                case 'totalTcoPaper':
                case 'monoTcoPaper':
                case 'colorTcoPaper':
                  categoriesStatus.paperColumns = true;
                  break;
                case 'deviceCostHardware':
                case 'totalCppHardware':
                case 'monoCppHardware':
                case 'colorCppHardware':
                case 'totalTcoHardware':
                case 'monoTcoHardware':
                case 'colorTcoHardware':
                  categoriesStatus.hardwareColumns = true;
                  break;
                case 'totalEnergyInKWH':
                case 'co2PaperKG':
                case 'energyCO2KG':
                case 'energyCO2KGbyGeography':
                case 'noOfTreesCutDown':
                case 'literOfOilRequired':
                  categoriesStatus.carbonColumns = true;
                  break;
                case 'totalAssetManagementCost':
                case 'supplyProcessTotalManHrsCostsPerYr':
                case 'totalDeviceSpaceCostperYr':
                case 'helpDeskCostperPrinterBasedonNumberofPrinters':
                case 'helpDeskCostperPrinterBasedonNumberofUsers':
                case 'totalPurchasingCostofDeviceperYr':
                case 'totalITMgmtCostofDevicePerYr':
                  categoriesStatus.indirectCosts = true;
                  break;
              }
            }
          });
        }
        return categoriesStatus;
      }
      catch (ex) {
        $log.error('Error in getting categories active status : ', ex);
        return categoriesStatus;
      }
    }

    /**
     * Get categories status from drop down.
     * @param tcoCategories
     */
    function getStatusFromCategories(tcoCategories) {
      var tcoCategoriesLength,
          categoriesStatus = {
            tonerColumns: false,
            rpColumns: false,
            serviceColumns: false,
            powerColumns: false,
            paperColumns: false,
            hardwareColumns: false,
            carbonColumns: false,
            indirectCosts: false
          };
      try {
        if (tcoCategories && tcoCategories.length) {
          tcoCategoriesLength = tcoCategories.length;
          for (var index = 0; index < tcoCategoriesLength; index++) {
            if (tcoCategories[index].active) {
              switch (tcoCategories[index].id) {
                case "tonerColumns":
                  categoriesStatus.tonerColumns = true;
                  break;
                case "rpColumns":
                  categoriesStatus.rpColumns = true;
                  break;
                case "serviceColumns":
                  categoriesStatus.serviceColumns = true;
                  break;
                case "powerColumns":
                  categoriesStatus.powerColumns = true;
                  break;
                case "paperColumns":
                  categoriesStatus.paperColumns = true;
                  break;
                case "hardwareColumns":
                  categoriesStatus.hardwareColumns = true;
                  break;
                case "carbonColumns":
                  categoriesStatus.carbonColumns = true;
                  break;
                case "indirectCosts":
                  categoriesStatus.indirectCosts = true;
                  break;
              }
            }
          }
        }
        return categoriesStatus;
      }
      catch (ex) {
        $log.error('Error in getting status from categories : ', ex);
        return categoriesStatus;
      }
    }

    return {
      setGroupProperty: setGroupProperty,
      getCategoriesActiveStatus: getCategoriesActiveStatus,
      getStatusFromCategories: getStatusFromCategories
    };
  }

  var app = angular.module('app'),
      requires = [
        '$log',
        TcoIntegrationHelperService
      ];
  app.factory('TcoIntegrationHelperService', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  function TcoPricingHelperService($log, PorticoSDConfiguration, PorticoSDUtilityService) {

    /**
     * Update Tco pricing for level
     * @param tabularViewLevelRow
     * @param levelPricing
     */
    function updateTcoPricingForLevel(tabularViewLevelRow, levelPricing) {
      var tcoColumns, tcoColumnsLength;
      try {
        if (tabularViewLevelRow && levelPricing) {
          tcoColumns = PorticoSDConfiguration.TCO_LEVEL_PRICING;
          tcoColumnsLength = tcoColumns.length;
          for (var index = 0; index < tcoColumnsLength; index++) {
            tabularViewLevelRow[tcoColumns[index]] = PorticoSDUtilityService.toFloat(levelPricing[tcoColumns[index]],
                PorticoSDConfiguration.NUMBER_FIVE);
          }
        }
      }
      catch (ex) {
        $log.error('Error while updating Tco pricing for level', ex);
      }
    }

    /**
     * Sum Tco pricing of levels
     * @param tabularRow
     * @param levelPricing
     */
    function sumTcoPricingOfLevels(tabularRow, levelPricing) {
      var tcoColumns, tcoColumnsLength;
      try {
        if (tabularRow && levelPricing) {
          tcoColumns = PorticoSDConfiguration.TCO_LEVEL_PRICING;
          tcoColumnsLength = tcoColumns.length;
          for (var index = 0; index < tcoColumnsLength; index++) {
            levelPricing[tcoColumns[index]] = PorticoSDUtilityService.sumValues(levelPricing[tcoColumns[index]],
                tabularRow[tcoColumns[index]]);
          }
        }
      }
      catch (ex) {
        $log.error('Error while adding tco pricing on level', ex);
      }
    }

    /**
     * Subtract Tco pricing of levels.
     * @param tabularRow
     * @param assetRow
     */
    function subtractTcoPricingOfLevel(tabularRow, assetRow) {
      var tcoColumns, tcoColumnsLength;
      try {
        if (tabularRow && assetRow) {
          tcoColumns = PorticoSDConfiguration.TCO_LEVEL_PRICING;
          tcoColumnsLength = tcoColumns.length;
          for (var index = 0; index < tcoColumnsLength; index++) {
            tabularRow[tcoColumns[index]] = PorticoSDUtilityService.subtractValues(tabularRow[tcoColumns[index]],
                assetRow[tcoColumns[index]]);
          }
        }
      }
      catch (ex) {
        $log.error('Error while subtracting level pricing', ex);
      }
    }

    /**
     * Set default tco pricing data for tabularRow
     * @param tabularRow
     */
    function setDefaultTcoPricingForTabularRow(tabularRow) {
      var tcoColumns, tcoColumnsLength;
      try {
        if (tabularRow) {
          tcoColumns = PorticoSDConfiguration.TCO_LEVEL_PRICING;
          tcoColumnsLength = tcoColumns.length;
          for (var index = 0; index < tcoColumnsLength; index++) {
            tabularRow[tcoColumns[index]] = 0;
          }
        }
      }
      catch (ex) {
        $log.error('Error while setting tco pricing data for tabularRow', ex);
      }
    }

    /**
     * Reset Tco pricing data for tabularRow
     * @param tabularRow
     */
    function resetTcoPricingDataForTabularRow(tabularRow) {
      var tcoColumns, tcoColumnsLength;
      try {
        if (tabularRow) {
          tcoColumns = PorticoSDConfiguration.TCO_VIEW;
          tcoColumnsLength = tcoColumns.length;
          for (var index = 0; index < tcoColumnsLength; index++) {
            if (tcoColumns[index] === PorticoSDConfiguration.DETAIL_TONER ||
                tcoColumns[index] === PorticoSDConfiguration.DETAIL_RP) {
              continue;
            }
            tabularRow[tcoColumns[index]] = "";
          }
        }
      }
      catch (ex) {
        $log.error('Error while resetting tco pricing data for tabularRow', ex);
      }
    }

    /**
     * Add asset tco pricing to level
     * @param tabularRow
     * @param assetRow
     */
    function addAssetTcoPricingToLevel(tabularRow, assetRow) {
      var tcoColumns, tcoColumnsLength;
      try {
        if (tabularRow && assetRow) {
          tcoColumns = PorticoSDConfiguration.TCO_LEVEL_PRICING;
          tcoColumnsLength = tcoColumns.length;
          for (var index = 0; index < tcoColumnsLength; index++) {
            tabularRow[tcoColumns[index]] = PorticoSDUtilityService.sumValues(tabularRow[tcoColumns[index]],
                assetRow[tcoColumns[index]]);
          }
        }
      }
      catch (ex) {
        $log.error('Error while subtracting level pricing', ex);
      }
    }

    return {
      updateTcoPricingForLevel: updateTcoPricingForLevel,
      sumTcoPricingOfLevels: sumTcoPricingOfLevels,
      subtractTcoPricingOfLevel: subtractTcoPricingOfLevel,
      setDefaultTcoPricingForTabularRow: setDefaultTcoPricingForTabularRow,
      resetTcoPricingDataForTabularRow: resetTcoPricingDataForTabularRow,
      addAssetTcoPricingToLevel: addAssetTcoPricingToLevel
    };
  }

  var app = angular.module('app'),
      requires = [
        '$log',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'PorticoSdUtilityService',
        TcoPricingHelperService
      ];
  app.factory('TcoPricingHelperService', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  /**
   * Constructor of Outcome. It should be used to create the instance of an Outcome
   * @param {object} data properties of the Outcome.
   */
  function TcoPricingData(data) {
    if (data !== undefined) {
      this.setData(data);
    }
  }

  TcoPricingData.prototype = {
    /**
     * Set properties to Outcome. Uses angular.extend to assign the values.
     * @param {object} data properties to be set.
     */
    setData: function (data) {
      angular.extend(this, data);
    }
  };

  function TcoPricingService($q, $log, $routeParams, PorticoSDApiService, PorticoSDPoolManager, SharedService,
                             PorticoSDConfiguration, PorticoSDSharedService) {

    var tcoPricingPool = [],
        retrieveInstance = PorticoSDPoolManager.retrieveInstance.bind(null, TcoPricingData, tcoPricingPool),
        removeInstance = PorticoSDPoolManager.remove.bind(null, tcoPricingPool);

    /**
     * Get the tco pricing value for the asset.
     * @param assetId
     * @param value
     * @param state
     * @returns {{tcoCurrentPricing: null, tcoFuturePricing: null}}
     */
    function getTcoPricingValueForAsset(assetId, value, state) {
      var assetPoolRow,
          tcoPricingValue = {
            "tcoCurrentPricing": null,
            "tcoFuturePricing": null
          };

      if (tcoPricingPool && tcoPricingPool.length) {
        assetPoolRow = tcoPricingPool.filter(function (tcoPricing) {
          return tcoPricing.id === assetId;
        });

        if (assetPoolRow && assetPoolRow.length) {
          tcoPricingValue = assetPoolRow.first().value;
        }
      }

      switch (state) {
        case PorticoSDConfiguration.STATES.CURRENT :
          tcoPricingValue.tcoCurrentPricing = value;
          break;
        case PorticoSDConfiguration.STATES.FUTURE :
          tcoPricingValue.tcoFuturePricing = value;
          break;
      }
      return tcoPricingValue;
    }

    /**
     * Remove and add the tco pricing pool data
     * @param tcoPricing
     */
    function loadPoolData(tcoPricing) {
      var tcoProjectPricing, tcoStatePricing, tcoPricingLength, tcoValue,
          projectId = $routeParams.id;
      try {
        if (tcoPricing) {
          tcoPricingLength = tcoPricing.length;
          for (var index = 0; index < tcoPricingLength; index++) {
            tcoStatePricing = tcoPricing[index];
            if (tcoStatePricing && tcoStatePricing.state && tcoStatePricing[PorticoSDConfiguration.STRING_PROJECT_ID] === projectId &&
                tcoStatePricing[PorticoSDConfiguration.STRING_SOURCE] === PorticoSDConfiguration.STRING_TCO) {
              PorticoSDSharedService.setTcoPricingFetched(tcoStatePricing.state, true);
              tcoProjectPricing = tcoStatePricing.assetPrices;
              if (tcoProjectPricing) {
                angular.forEach(tcoProjectPricing, function (value, key) {
                  tcoValue = getTcoPricingValueForAsset(key, value, tcoStatePricing.state);
                  retrieveInstance(key, tcoValue);
                });
              }
            }
          }
        }
      }
      catch (ex) {
        $log.error('Error in loading tco pricing pool data : ', ex);
      }
    }

    /**
     * Update pool data
     * @param tcoPricing
     */
    function updatePoolData(tcoPricing) {
      var tcoProjectPricing, tcoStatePricing, tcoPricingLength, tcoValue,
          projectId = $routeParams.id;
      try {
        if (tcoPricing) {
          tcoPricingLength = tcoPricing.length;
          for (var index = 0; index < tcoPricingLength; index++) {
            tcoStatePricing = tcoPricing[index];
            if (tcoStatePricing && tcoStatePricing.state &&
                tcoStatePricing[PorticoSDConfiguration.STRING_SOURCE] === PorticoSDConfiguration.STRING_TCO) {
              tcoProjectPricing = tcoStatePricing.assetPrices;
              if (tcoProjectPricing) {
                angular.forEach(tcoProjectPricing, function (value, key) {
                  tcoValue = getTcoPricingValueForAsset(key, value, tcoStatePricing.state);
                  retrieveInstance(key, tcoValue);
                });
              }
            }
          }
        }
      }
      catch (ex) {
        $log.error('Error in updating tco pricing pool data : ', ex);
      }
    }

    /**
     * Make call to fetch tco pricing data
     * @returns {object} promise.
     */
    function fetchTcoPricing() {
      var deferred = $q.defer(),
          activeState = getActiveStateForTcoPricing() || PorticoSDConfiguration.STATES.CURRENT;

      function onSuccess(response) {
        loadPoolData(response.data.data);
        deferred.resolve({tcoPricing: response.data.data});
      }

      function onError(error) {
        deferred.reject(error);
      }

      PorticoSDApiService.fetchTcoPricing(activeState, PorticoSDConfiguration.STRING_TCO).then(onSuccess, onError);
      return deferred.promise;
    }

    /**
     * Make call to get tco pricing data
     * @returns {object} promise.
     */
    function getTcoPricing() {
      var deferred = $q.defer();

      function onSuccess(response) {
        removeInstance();
        loadPoolData(response.data.data);
        deferred.resolve({tcoPricing: response.data.data});
      }

      function onError(error) {
        deferred.reject(error);
      }

      PorticoSDApiService.getTcoPricing(PorticoSDConfiguration.STRING_TCO).then(onSuccess, onError);
      return deferred.promise;
    }

    /**
     * Get the active state for Tco pricing
     * For Tco, there are only two state current and future
     * @returns {*}
     */
    function getActiveStateForTcoPricing() {
      var tcoPricingState,
          activeState = SharedService.activeState();
      if (activeState) {
        switch (activeState) {
          case PorticoSDConfiguration.STATES.CURRENT:
            tcoPricingState = PorticoSDConfiguration.STATES.CURRENT;
            break;
          case PorticoSDConfiguration.STATES.TRANSITION:
          case PorticoSDConfiguration.STATES.FUTURE:
            tcoPricingState = PorticoSDConfiguration.STATES.FUTURE;
            break;
        }
      }
      return tcoPricingState;
    }

    /**
     * Get toner details from asset Id.
     * @param assetId
     * @returns {Array}
     */
    function getTonerDetails(assetId) {
      var assetTcoPricing, filterAssetPricing,
          tonerDetails = [],
          activeTcoPricingState = getActiveStateForTcoPricing();
      try {
        if (assetId) {
          if (tcoPricingPool && tcoPricingPool.length) {
            filterAssetPricing = tcoPricingPool.filter(function (tcoPricing) {
              return tcoPricing.id === assetId;
            });
            if (filterAssetPricing && filterAssetPricing.length) {
              assetTcoPricing = filterAssetPricing.first().value;
              if (activeTcoPricingState === PorticoSDConfiguration.STATES.CURRENT) {
                if (assetTcoPricing.tcoCurrentPricing && assetTcoPricing.tcoCurrentPricing.extended &&
                    assetTcoPricing.tcoCurrentPricing.extended.deviceComponents &&
                    assetTcoPricing.tcoCurrentPricing.extended.deviceComponents.toner) {
                  tonerDetails = assetTcoPricing.tcoCurrentPricing.extended.deviceComponents.toner.tonerDetails;
                }
              }
              else if (activeTcoPricingState === PorticoSDConfiguration.STATES.FUTURE) {
                if (assetTcoPricing.tcoFuturePricing && assetTcoPricing.tcoFuturePricing.extended &&
                    assetTcoPricing.tcoFuturePricing.extended.deviceComponents &&
                    assetTcoPricing.tcoFuturePricing.extended.deviceComponents.toner) {
                  tonerDetails = assetTcoPricing.tcoFuturePricing.extended.deviceComponents.toner.tonerDetails;
                }
              }
            }
          }
        }
        return tonerDetails;
      }
      catch (ex) {
        $log.error('Error in getting toner details : ', ex);
        return tonerDetails;
      }
    }

    /**
     * Get replaceable parts details from asset Id.
     * @param assetId
     * @returns {Array}
     */
    function getReplaceablePartsDetails(assetId) {
      var assetTcoPricing, filterAssetPricing,
          replaceablePartsDetails = [],
          activeTcoPricingState = getActiveStateForTcoPricing();
      try {
        if (assetId) {
          if (tcoPricingPool && tcoPricingPool.length) {
            filterAssetPricing = tcoPricingPool.filter(function (tcoPricing) {
              return tcoPricing.id === assetId;
            });
            if (filterAssetPricing && filterAssetPricing.length) {
              assetTcoPricing = filterAssetPricing.first().value;
              if (activeTcoPricingState === PorticoSDConfiguration.STATES.CURRENT) {
                if (assetTcoPricing.tcoCurrentPricing && assetTcoPricing.tcoCurrentPricing.extended &&
                    assetTcoPricing.tcoCurrentPricing.extended.deviceComponents &&
                    assetTcoPricing.tcoCurrentPricing.extended.deviceComponents.replaceableParts) {
                  replaceablePartsDetails = assetTcoPricing.tcoCurrentPricing.extended.deviceComponents.replaceableParts.replaceablePartsDetails;
                }
              }
              else if (activeTcoPricingState === PorticoSDConfiguration.STATES.FUTURE) {
                if (assetTcoPricing.tcoFuturePricing && assetTcoPricing.tcoFuturePricing.extended &&
                    assetTcoPricing.tcoFuturePricing.extended.deviceComponents &&
                    assetTcoPricing.tcoFuturePricing.extended.deviceComponents.replaceableParts) {
                  replaceablePartsDetails = assetTcoPricing.tcoFuturePricing.extended.deviceComponents.replaceableParts.replaceablePartsDetails;
                }
              }
            }
          }
        }
        return replaceablePartsDetails;
      }
      catch (ex) {
        $log.error('Error in getting replaceable parts details : ', ex);
        return replaceablePartsDetails;
      }
    }

    /**
     * Get asset pricing
     * @param assetId
     * @returns {Array}
     */
    function getAssetPricing(assetId) {
      var assetTcoPricing, filterAssetPricing,
          assetStatePricing = [],
          activeTcoPricingState = getActiveStateForTcoPricing();
      try {
        if (assetId) {
          if (tcoPricingPool && tcoPricingPool.length) {
            filterAssetPricing = tcoPricingPool.filter(function (tcoPricing) {
              return tcoPricing.id === assetId;
            });
            if (filterAssetPricing && filterAssetPricing.length) {
              assetTcoPricing = filterAssetPricing.first().value;
              if (activeTcoPricingState === PorticoSDConfiguration.STATES.CURRENT) {
                if (assetTcoPricing.tcoCurrentPricing) {
                  assetStatePricing = assetTcoPricing.tcoCurrentPricing;
                }
              }
              else if (activeTcoPricingState === PorticoSDConfiguration.STATES.FUTURE) {
                if (assetTcoPricing.tcoFuturePricing) {
                  assetStatePricing = assetTcoPricing.tcoFuturePricing;
                }
              }
            }
          }
        }
        return assetStatePricing;
      }
      catch (ex) {
        $log.error('Error in getting asset pricing : ', ex);
        return assetStatePricing;
      }
    }

    /**
     * Get asset pricing for all states.
     * @param assetId - Id of the asset.
     * @returns {{current: object, future: object}}
     */
    function getAssetPricingForAllStates(assetId) {
      var assetTcoPricing, filterAssetPricing,
          assetPricing = {current: {}, future: {}};
      try {
        if (assetId) {
          if (tcoPricingPool && tcoPricingPool.length) {
            filterAssetPricing = tcoPricingPool.find(function (tcoPricing) {
              return tcoPricing.id === assetId;
            });
            if (filterAssetPricing) {
              assetTcoPricing = filterAssetPricing.value;
              if (assetTcoPricing.tcoCurrentPricing) {
                assetPricing.current = assetTcoPricing.tcoCurrentPricing;
              }
              if (assetTcoPricing.tcoFuturePricing) {
                assetPricing.future = assetTcoPricing.tcoFuturePricing;
              }
            }
          }
        }
        return assetPricing;
      }
      catch (ex) {
        $log.error('Error in getting asset pricing : ', ex);
        return assetPricing;
      }
    }

    /**
     * Get asset basic pricing
     * @param assetId
     * @returns {{currentBasic: string, futureBasic: string}}
     */
    function getBasicAssetPricing(assetId) {
      var assetTcoPricing, filterAssetPricing,
          assetBasicPricing = {currentBasic: "", futureBasic: ""};
      try {
        if (assetId) {
          if (tcoPricingPool && tcoPricingPool.length) {
            filterAssetPricing = tcoPricingPool.filter(function (tcoPricing) {
              return tcoPricing.id === assetId;
            });
            if (filterAssetPricing && filterAssetPricing.length) {
              assetTcoPricing = filterAssetPricing.first().value;
              if (assetTcoPricing.tcoCurrentPricing) {
                assetBasicPricing.currentBasic = assetTcoPricing.tcoCurrentPricing.basic;
              }
              if (assetTcoPricing.tcoFuturePricing) {
                assetBasicPricing.futureBasic = assetTcoPricing.tcoFuturePricing.basic;
              }
            }
          }
        }
        return assetBasicPricing;
      }
      catch (ex) {
        $log.error('Error in getting asset pricing : ', ex);
        return assetBasicPricing;
      }
    }

    /**
     * Is Tco pricing fetched
     * @returns {boolean}
     */
    function isTcoPricingFetched() {
      return !!(tcoPricingPool && tcoPricingPool.length);
    }

    /**
     * Make call to update assets pricing
     * @returns {object} promise.
     */
    function updateAssetsTcoPricing(assets) {
      var deferred = $q.defer(),
          activeState = getActiveStateForTcoPricing() || PorticoSDConfiguration.STATES.CURRENT;

      function onSuccess(response) {
        updatePoolData([response.data.data]);
        deferred.resolve({tcoPricing: response.data.data});
      }

      function onError(error) {
        deferred.reject(error);
      }

      PorticoSDApiService.updateAssetsTcoPricing(activeState, assets).then(onSuccess, onError);
      return deferred.promise;
    }

    /**
     * Make call to update proxy devices pricing
     * @returns {object} promise.
     */
    function updateProxyDevicesPricing(assetsRequest) {
      var deferred = $q.defer();

      assetsRequest.projectId = $routeParams.id;
      function onSuccess(response) {
        updatePoolData([response.data]);
        deferred.resolve({tcoPricing: response.data});
      }

      function onError(error) {
        deferred.reject(error);
      }

      PorticoSDApiService.updateProxyDevicesPricing(assetsRequest).then(onSuccess, onError);
      return deferred.promise;
    }

    return {
      fetchTcoPricing: fetchTcoPricing,
      getTcoPricing: getTcoPricing,
      getTonerDetails: getTonerDetails,
      getReplaceablePartsDetails: getReplaceablePartsDetails,
      getAssetPricing: getAssetPricing,
      getBasicAssetPricing: getBasicAssetPricing,
      isTcoPricingFetched: isTcoPricingFetched,
      updateAssetsTcoPricing: updateAssetsTcoPricing,
      getActiveStateForTcoPricing: getActiveStateForTcoPricing,
      updateProxyDevicesPricing: updateProxyDevicesPricing,
      getAssetPricingForAllStates: getAssetPricingForAllStates
    };
  }

  var app = angular.module('app'),
      requires = [
        '$q',
        '$log',
        '$routeParams',
        'PorticoSDApiService',
        'ngPorticoSD.services.PorticoSDPoolManager',
        'SharedService',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        'PorticoSDSharedService',
        TcoPricingService
      ];
  app.factory('TcoPricingService', requires);
}());
/*global angular*/
/*jslint nomen: true*/
(function () {
  'use strict';

  function TcoProxyDeviceService($q, $log, SharedService, PorticoSDApiService, SyncTcoPricingService, PorticoSDService,
                                 TcoPricingService, PorticoSDConfiguration) {

    /**
     * Make call to get tco proxy devices
     * @returns {object} promise.
     */
    function getTcoProxyDevices(source, latest) {
      var deferred = $q.defer();

      function onSuccess(response) {
        deferred.resolve(response);
      }

      function onError(error) {
        deferred.reject(error);
      }

      PorticoSDApiService.getProxyDevices(source, latest).then(onSuccess, onError);
      return deferred.promise;
    }

    /**
     * Create assets request
     * @param selectedAssetProductNumber
     * @param proxyDeviceProductId
     * @returns {{assetIds: Array, productNumber: string, proxyDeviceId: string, state: string, source: string, projectId: string}}
     */
    function createAssetsRequest(selectedAssetProductNumber, proxyDeviceProductId) {
      var assetsRequest = {assetIds: [], productNumber: "", proxyDeviceId: "", state: "", source: "", projectId: ""},
          assetsMatchedSKU = PorticoSDService.getSkuMatchedAssets(selectedAssetProductNumber),
          assetsMatchedSKULength;

      assetsRequest.productNumber = selectedAssetProductNumber;
      assetsRequest.proxyDeviceId = proxyDeviceProductId;
      assetsRequest.state = TcoPricingService.getActiveStateForTcoPricing() || PorticoSDConfiguration.STATES.CURRENT;
      assetsRequest.source = PorticoSDConfiguration.PRICING_SOURCE.TCO.toLowerCase();

      if (!(assetsMatchedSKU && assetsMatchedSKU.length)) {
        $log.error("Error in getting selected device from tabular pool");
        return assetsRequest;
      }
      assetsMatchedSKULength = assetsMatchedSKU.length;
      for (var index = 0; index < assetsMatchedSKULength; index++) {
        assetsRequest.assetIds.push(assetsMatchedSKU[index].id);
      }

      return assetsRequest;
    }

    /**
     * Update proxy device pricing
     * @param selectedProxyDevice
     */
    function updateProxyDevicePricing(selectedProxyDevice) {
      var deferred = $q.defer(),
          selectedAsset, assetsRequest;

      if (!selectedProxyDevice) {
        $log.error("Error in getting selected proxy device");
        deferred.reject();
        return deferred.promise;
      }

      selectedAsset = SharedService.selectedAsset();
      if (!selectedAsset) {
        $log.error("Error in getting selected device");
        deferred.reject();
        return deferred.promise;
      }

      if (!selectedAsset.productNumber) {
        $log.error("Error in selected asset product number");
        deferred.reject();
        return deferred.promise;
      }

      assetsRequest = createAssetsRequest(selectedAsset.productNumber, selectedProxyDevice.ProductID);
      if (!assetsRequest.assetIds.length) {
        deferred.reject();
        return deferred.promise;
      }

      function onSuccess(response) {
        SyncTcoPricingService.updateTcoPricing(assetsRequest.assetIds);
        deferred.resolve(response);
      }

      function onError(error) {
        $log.error("Error updating tco proxy device pricing", error);
        deferred.reject(error);
      }

      TcoPricingService.updateProxyDevicesPricing(assetsRequest).then(onSuccess, onError);
      return deferred.promise;
    }

    return {
      getTcoProxyDevices: getTcoProxyDevices,
      updateProxyDevicePricing: updateProxyDevicePricing
    };
  }

  var app = angular.module('app'),
      requires = [
        '$q',
        '$log',
        'SharedService',
        'PorticoSDApiService',
        'SyncTcoPricingService',
        'ngPorticoSD.services.PorticoSDService',
        'TcoPricingService',
        'ngPorticoSD.constants.PorticoSDConfiguration',
        TcoProxyDeviceService
      ];
  app.factory('ngPorticoSD.services.TcoProxyDeviceService', requires);
}());
//# sourceMappingURL=../dist/hpPorticoSDBundle.js.map
